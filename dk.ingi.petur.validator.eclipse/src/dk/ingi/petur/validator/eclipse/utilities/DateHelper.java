package dk.ingi.petur.validator.eclipse.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateHelper {
	/**
	 * Get the current time as a timestamp.
	 * @return a timestamp on the form dd-MM-yyyy HH.mm.ss
	 */
	public static String timeStamp() {
		final Date date = new Date();
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		final String formattedDate = sdf.format(date);
		return formattedDate;
	}
}
