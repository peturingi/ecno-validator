package dk.ingi.petur.validator.eclipse.utilities;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public final class WorkbenchHelper {
	
	public static List<IFile> getSelectedFiles() {
		final List<IFile> selectedFiles = new ArrayList<>();
		for (final Object o : getSelection().toList()) {
			if (o instanceof IFile) {
				selectedFiles.add((IFile)o);
			}
		}
		return selectedFiles;
	}
	
	/**
	 * Get current selection from workbench.
	 * @return current structured selection, null if it is not an IStructuredSelection.
	 */
	private static IStructuredSelection getSelection(){
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		final ISelection selection = window.getSelectionService().getSelection();
		final IStructuredSelection structuredSelection = selection instanceof IStructuredSelection ? (IStructuredSelection)selection : null;
		return structuredSelection;
	}
}
