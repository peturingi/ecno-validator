package dk.ingi.petur.validator.eclipse.command;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.dialogs.MessageDialog;

import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.validator.Validator;
import dk.ingi.petur.validator.eclipse.utilities.DateHelper;
import dk.ingi.petur.validator.eclipse.utilities.WorkbenchHelper;
import dk.ingi.petur.validator.operationMapping.OperationMapping;

public class Validate extends AbstractHandler {
	
	final Map<String, String> message2event = new HashMap<>();
	final Map<String, Map<String, String>> methodParameter2EventParameter = new HashMap<>();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Trace trace = null;
		StateSpace stateSpace = null;
		OperationMapping operationMapping = null;

		final List<IFile> selectedFiles = WorkbenchHelper.getSelectedFiles();
		assert selectedFiles.size() == 3;
		for (final IFile file : selectedFiles) {
			switch (file.getFileExtension()) {
				case "trace": {
					trace = loadAndGetModel(file, Trace.class);
					break;
				}
				case "statespace": {
					stateSpace = loadAndGetModel(file, StateSpace.class);
					break;
				}
				case "operationmapping": {
					operationMapping = loadAndGetModel(file, OperationMapping.class);
					break;
				}
				default: {
					throw new IllegalStateException("This method does not support files with extension: " + file.getFileExtension());
				}
			}
		}
		
		assert trace != null;
		assert stateSpace != null;
		assert operationMapping != null;

		loadOperationMappings(operationMapping);
		
		final Validator validator = new Validator(
				trace,
				stateSpace,
				message2event,
				methodParameter2EventParameter
				);
		validator.validate();
		
		if (validator.getReachableStates().isEmpty() == false) {			
			final ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("mapping", new XMIResourceFactoryImpl());
			final URI fileURI = URI.createFileURI(new File("mappings/" + DateHelper.timeStamp() + ".mapping").getAbsolutePath());
			
			final Resource resource = resourceSet.createResource(fileURI);
			resource.getContents().add(validator.getMapping());
			
			boolean error = false;
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				error = true;
				e.printStackTrace();
			}
			if (!error) {
				System.out.println("Wrote mapping to: " + fileURI.toFileString());
			}
			if (resource.getErrors().size() > 0) {
				System.err.println(resource.getErrors());
			}
		}
		
		MessageDialog.openInformation(null, "Validation Result", "Valid: " + (validator.getReachableStates().isEmpty() == false));
		
		return null;
	}

	private void loadOperationMappings(OperationMapping operationMapping) {
		operationMapping.getOperation2event().stream().forEach(o2e -> message2event.put(o2e.getKey(), o2e.getValue()));
		operationMapping.getOperationParameter2eventParameter().stream().forEach(
				op2ep -> mapMethodParameter2EventParameter(op2ep.getKey().getKey(), op2ep.getValue().getKey(), op2ep.getValue().getValue())
		);
	}
	
	@SuppressWarnings("unchecked")
	private <T> T loadAndGetModel(final IFile model, final Class<T> type) {
		T result = null;
		final XMIResourceImpl resource = new XMIResourceImpl();
		try {
			resource.load(model.getContents(), new HashMap<Object, Object>());
		} catch (IOException | CoreException e) {
			e.printStackTrace();
			return null;
		}
		if (resource.getContents() != null) {
			final EObject object = resource.getContents().get(0);
			result = type.isAssignableFrom(object.getClass()) ? (T)object : null;
		}
		return result;
	}
	
	void mapMethodParameter2EventParameter(final String message, final String messageParameter, final String eventParameter) {
		assert message != null;
		assert message.length() > 0;
		assert messageParameter != null;
		assert messageParameter.length() > 0;
		assert eventParameter != null;
		assert eventParameter.length() > 0;
		
		if (!methodParameter2EventParameter.containsKey(message)) {
			methodParameter2EventParameter.put(message, new HashMap<>());
		}
		methodParameter2EventParameter.get(message).put(messageParameter, eventParameter);
	}

}
