/**
 */
package dk.ingi.petur.validator.mapping;

import dk.dtu.compute.se.ecno.statespace.Link;
import dk.dtu.compute.se.ecno.statespace.StateObject;
import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.dtu.compute.se.ecno.statespace.Transition;

import dk.ingi.petur.trace.Message;
import dk.ingi.petur.trace.Trace;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getSegments <em>Segments</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getObject2element <em>Object2element</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getSegment2transition <em>Segment2transition</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getMessage2link <em>Message2link</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getMessage2event <em>Message2event</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getStateSpace <em>State Space</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.Mapping#getTrace <em>Trace</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='everyMessageIsInASingleSegment everySegmentMapsToATransition everySegmentMapsOnceToATransitionInTheStateSpace segmentsMapsToOneUniqueTransition segmentsMessagesAreInTrace segment2transitionKeysAreSegmentsInThisMapping segmentsRecordedInOrderTheyOccur everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment allObjectsWhichMapToElementsAreObjectsInTheTrace allElementsMappedToAreElementsInTheStateSpace objectMapsToStateObjectsWhichRepresentTheSameElement allMessagesWhichMapToLinksAreMessagesInTheTrace allLinksMappedToAreLinksInTheStateSpace allMappedToEventsAreInTheStateSpace messageMapsToASingleLink messageMappedToLinkIsInASegmentWhichMapsToLinksTransition'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot everyMessageIsInASingleSegment='\n\t\t\tsegments?.messages-&gt;includesAll(trace.messages)\n\t\t\tand\n\t\t\tsegments?.messages-&gt;size() = segments?.messages-&gt;asSet()-&gt;size()' everySegmentMapsToATransition='\n\t\t\tsegment2transition?.key-&gt;includesAll(segments)' everySegmentMapsOnceToATransitionInTheStateSpace='\n\t\t\tsegment2transition?.value-&gt;size() &lt;&gt; 0 implies stateSpace.transitions-&gt;includesAll(segment2transition?.value)' segmentsMapsToOneUniqueTransition='\n\t\t\tsegment2transition?-&gt;isUnique(key)' segmentsMessagesAreInTrace='\n\t\t\tsegments?.messages-&gt;size() &lt;&gt; 0 implies trace.messages-&gt;includesAll(segments?.messages)' segment2transitionKeysAreSegmentsInThisMapping='\n\t\t\tsegment2transition-&gt;size() &lt;&gt; 0 implies segments-&gt;includesAll(segment2transition?.key)' segmentsRecordedInOrderTheyOccur='\n\t\t\tsegments-&gt;forAll(segment |\n\t\t\t\tsegments-&gt;indexOf(segment)+1 &lt;= segments-&gt;size()\n\t\t\t\timplies\n\t\t\t\tsegment2transition-&gt;exists(s2t |\n\t\t\t\t\ts2t?.key = segment\n\t\t\t\t\tand\n\t\t\t\t\tsegment2transition-&gt;exists(s2tnext |\n\t\t\t\t\t\ts2tnext?.key = segments-&gt;at(segments-&gt;indexOf(segment)+1)\n\t\t\t\t\t\tand\n\t\t\t\t\t\ts2t?.value?.target = s2tnext?.value?.source\n\t\t\t\t\t)\n\t\t\t\t)\n\t\t\t)' everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps='\n\t\t\tsegments-&gt;forAll(s |\n\t\t\t\tobject2element-&gt;exists(o2e |\n\t\t\t\t\to2e?.key = s?.messages-&gt;at(1)?.target?.lifeline?.object\n\t\t\t\t\tand\n\t\t\t\t\tsegment2transition-&gt;exists(s2t |\n\t\t\t\t\t\ts2t?.key = s\n\t\t\t\t\t\tand\n\t\t\t\t\t\ts2t?.value?.triggerElement?.uid = o2e?.value?-&gt;at(1)?.uid\n\t\t\t\t\t)\n\t\t\t\t)\n\t\t\t)' secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps='\n\t\t\tsegments-&gt;forAll(s |\n\t\t\t\tobject2element-&gt;exists(o2e |\n\t\t\t\t\to2e?.key = s?.messages-&gt;at(2)?.source?.lifeline?.object\n\t\t\t\t\tand\n\t\t\t\t\tsegment2transition-&gt;exists(s2t |\n\t\t\t\t\t\ts2t?.key = s\n\t\t\t\t\t\tand\n\t\t\t\t\t\to2e?.value-&gt;includes(s2t?.value?.triggerElement)\n\t\t\t\t\t)\n\t\t\t\t)\n\t\t\t)' everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment='\n\t\t\t\tsegment2transition-&gt;forAll(s2t |\n\t\t\t\t\ts2t?.value?.links?-&gt;collect(l |\n\t\t\t\t\t\tl.feature-&gt;collect( f |\n\t\t\t\t\t\t\tSet{f.object, f.values-&gt;at(1)?.stateValues?-&gt;at(l.no+1)}\n\t\t\t\t\t\t)\n\t\t\t\t\t)-&gt;asSet()-&gt;forAll(element |\n\t\t\t\t\t\ts2t?.key?.messages?-&gt;select(m |\n\t\t\t\t\t\t\tm.source &lt;&gt; null\n\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\tm.target &lt;&gt; null\n\t\t\t\t\t\t)-&gt;collect(m |\n\t\t\t\t\t\t\tSet{m?.source, m?.target}\n\t\t\t\t\t\t).lifeline.object-&gt;asSet()-&gt;exists(object |\n\t\t\t\t\t\t\tobject2element?-&gt;exists(o2e |\n\t\t\t\t\t\t\t\to2e.key = object\n\t\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\t\to2e.value-&gt;at(1)?.uid = element.uid\n\t\t\t\t\t\t\t)\n\t\t\t\t\t\t)\n\t\t\t\t\t)\n\t\t\t\t)' everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment='\n\t\t\t\tsegment2transition-&gt;forAll(s2t |\n\t\t\t\t\ts2t?.value?.links-&gt;forAll(l |\n\t\t\t\t\t\tmessage2link-&gt;exists(m2l |\n\t\t\t\t\t\t\ts2t?.key?.messages-&gt;includes(m2l?.key)\n\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\tm2l?.value-&gt;includes(l)\n\t\t\t\t\t\t)\n\t\t\t\t\t)\n\t\t\t\t)' allObjectsWhichMapToElementsAreObjectsInTheTrace='\n\t\t\ttrace.lifelines?.object-&gt;includesAll(object2element?.key)' allElementsMappedToAreElementsInTheStateSpace='\n\t\t\tstateSpace.states?.objects-&gt;includesAll(object2element?.value)' objectMapsToStateObjectsWhichRepresentTheSameElement='\n\t\t\tobject2element-&gt;forAll(o2e|o2e?.value?.uid-&gt;asSet()-&gt;size()=1)' allMessagesWhichMapToLinksAreMessagesInTheTrace='\n\t\t\ttrace.messages-&gt;includesAll(message2link?.key)' allLinksMappedToAreLinksInTheStateSpace='\n\t\t\tstateSpace.states?.out?.links-&gt;includesAll(message2link?.value)' allMappedToEventsAreInTheStateSpace='\n\t\t\tstateSpace.states?.out?.events?.name-&gt;includesAll(message2event?.value)' messageMapsToASingleLink='\n\t\t\tmessage2link?-&gt;isUnique(key)' messageMappedToLinkIsInASegmentWhichMapsToLinksTransition='\n\t\tmessage2link-&gt;forAll(m2l|\n\t\t\tsegments-&gt;exists(s|\n\t\t\t\ts?.messages-&gt;includes(m2l?.key)\n\t\t\t\tand\n\t\t\t\tsegment2transition-&gt;exists(s2t|\n\t\t\t\t\ts2t?.key=s\n\t\t\t\t\tand\n\t\t\t\t\ts2t?.value?.links-&gt;includes(m2l?.value)\n\t\t\t\t)\n\t\t\tand\n\t\t\tmessage2event-&gt;exists(m2e|\n\t\t\t\tm2l?.key?.payload = m2e?.key.replaceFirst(\'.*;\',\'\')\n\t\t\t\tand\n\t\t\t\tm2l?.value?.events?.name-&gt;includes(m2e?.value)\n\t\t\t)\n\t\t\t)\n\t\t)'"
 * @generated
 */
public interface Mapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Segments</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.validator.mapping.Segment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segments</em>' containment reference list.
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_Segments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Segment> getSegments();

	/**
	 * Returns the value of the '<em><b>Object2element</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link dk.dtu.compute.se.ecno.statespace.StateObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object2element</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object2element</em>' map.
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_Object2element()
	 * @model mapType="dk.ingi.petur.validator.mapping.ObjectToElementMap&lt;org.eclipse.emf.ecore.EObject, dk.dtu.compute.se.ecno.statespace.StateObject&gt;"
	 * @generated
	 */
	EMap<EObject, EList<StateObject>> getObject2element();

	/**
	 * Returns the value of the '<em><b>Segment2transition</b></em>' map.
	 * The key is of type {@link dk.ingi.petur.validator.mapping.Segment},
	 * and the value is of type {@link dk.dtu.compute.se.ecno.statespace.Transition},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment2transition</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment2transition</em>' map.
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_Segment2transition()
	 * @model mapType="dk.ingi.petur.validator.mapping.SegmentToTransitionMap&lt;dk.ingi.petur.validator.mapping.Segment, dk.dtu.compute.se.ecno.statespace.Transition&gt;"
	 * @generated
	 */
	EMap<Segment, Transition> getSegment2transition();

	/**
	 * Returns the value of the '<em><b>Message2link</b></em>' map.
	 * The key is of type {@link dk.ingi.petur.trace.Message},
	 * and the value is of type {@link dk.dtu.compute.se.ecno.statespace.Link},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message2link</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message2link</em>' map.
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_Message2link()
	 * @model mapType="dk.ingi.petur.validator.mapping.MessageToLinkMap&lt;dk.ingi.petur.trace.Message, dk.dtu.compute.se.ecno.statespace.Link&gt;"
	 * @generated
	 */
	EMap<Message, Link> getMessage2link();

	/**
	 * Returns the value of the '<em><b>Message2event</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message2event</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message2event</em>' map.
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_Message2event()
	 * @model mapType="dk.ingi.petur.validator.mapping.MessageToEventMap&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EMap<String, String> getMessage2event();

	/**
	 * Returns the value of the '<em><b>State Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Space</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Space</em>' containment reference.
	 * @see #setStateSpace(StateSpace)
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_StateSpace()
	 * @model containment="true" required="true"
	 * @generated
	 */
	StateSpace getStateSpace();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.validator.mapping.Mapping#getStateSpace <em>State Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Space</em>' containment reference.
	 * @see #getStateSpace()
	 * @generated
	 */
	void setStateSpace(StateSpace value);

	/**
	 * Returns the value of the '<em><b>Trace</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trace</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trace</em>' containment reference.
	 * @see #setTrace(Trace)
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getMapping_Trace()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Trace getTrace();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.validator.mapping.Mapping#getTrace <em>Trace</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trace</em>' containment reference.
	 * @see #getTrace()
	 * @generated
	 */
	void setTrace(Trace value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Message> getUnmappedMessages();

} // Mapping
