/**
 */
package dk.ingi.petur.validator.mapping.util;

import dk.ingi.petur.validator.mapping.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.validator.mapping.MappingPackage
 * @generated
 */
public class MappingValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final MappingValidator INSTANCE = new MappingValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "dk.ingi.petur.validator.mapping";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MappingValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return MappingPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case MappingPackage.MAPPING:
				return validateMapping((Mapping)value, diagnostics, context);
			case MappingPackage.SEGMENT:
				return validateSegment((Segment)value, diagnostics, context);
			case MappingPackage.OBJECT_TO_ELEMENT_MAP:
				return validateObjectToElementMap((Map.Entry<?, ?>)value, diagnostics, context);
			case MappingPackage.SEGMENT_TO_TRANSITION_MAP:
				return validateSegmentToTransitionMap((Map.Entry<?, ?>)value, diagnostics, context);
			case MappingPackage.MESSAGE_TO_LINK_MAP:
				return validateMessageToLinkMap((Map.Entry<?, ?>)value, diagnostics, context);
			case MappingPackage.MESSAGE_TO_EVENT_MAP:
				return validateMessageToEventMap((Map.Entry<?, ?>)value, diagnostics, context);
			case MappingPackage.STATE_OBJECT_TO_ELEMENT:
				return validateStateObjectToElement((Map.Entry<?, ?>)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(mapping, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_everyMessageIsInASingleSegment(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_everySegmentMapsToATransition(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_everySegmentMapsOnceToATransitionInTheStateSpace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_segmentsMapsToOneUniqueTransition(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_segmentsMessagesAreInTrace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_segment2transitionKeysAreSegmentsInThisMapping(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_segmentsRecordedInOrderTheyOccur(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_allObjectsWhichMapToElementsAreObjectsInTheTrace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_allElementsMappedToAreElementsInTheStateSpace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_objectMapsToStateObjectsWhichRepresentTheSameElement(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_allMessagesWhichMapToLinksAreMessagesInTheTrace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_allLinksMappedToAreLinksInTheStateSpace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_allMappedToEventsAreInTheStateSpace(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_messageMapsToASingleLink(mapping, diagnostics, context);
		if (result || diagnostics != null) result &= validateMapping_messageMappedToLinkIsInASegmentWhichMapsToLinksTransition(mapping, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the everyMessageIsInASingleSegment constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__EVERY_MESSAGE_IS_IN_ASINGLE_SEGMENT__EEXPRESSION = "\n" +
		"\t\t\tsegments?.messages->includesAll(trace.messages)\n" +
		"\t\t\tand\n" +
		"\t\t\tsegments?.messages->size() = segments?.messages->asSet()->size()";

	/**
	 * Validates the everyMessageIsInASingleSegment constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_everyMessageIsInASingleSegment(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "everyMessageIsInASingleSegment",
				 MAPPING__EVERY_MESSAGE_IS_IN_ASINGLE_SEGMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the everySegmentMapsToATransition constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__EVERY_SEGMENT_MAPS_TO_ATRANSITION__EEXPRESSION = "\n" +
		"\t\t\tsegment2transition?.key->includesAll(segments)";

	/**
	 * Validates the everySegmentMapsToATransition constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_everySegmentMapsToATransition(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "everySegmentMapsToATransition",
				 MAPPING__EVERY_SEGMENT_MAPS_TO_ATRANSITION__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the everySegmentMapsOnceToATransitionInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__EVERY_SEGMENT_MAPS_ONCE_TO_ATRANSITION_IN_THE_STATE_SPACE__EEXPRESSION = "\n" +
		"\t\t\tsegment2transition?.value->size() <> 0 implies stateSpace.transitions->includesAll(segment2transition?.value)";

	/**
	 * Validates the everySegmentMapsOnceToATransitionInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_everySegmentMapsOnceToATransitionInTheStateSpace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "everySegmentMapsOnceToATransitionInTheStateSpace",
				 MAPPING__EVERY_SEGMENT_MAPS_ONCE_TO_ATRANSITION_IN_THE_STATE_SPACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the segmentsMapsToOneUniqueTransition constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__SEGMENTS_MAPS_TO_ONE_UNIQUE_TRANSITION__EEXPRESSION = "\n" +
		"\t\t\tsegment2transition?->isUnique(key)";

	/**
	 * Validates the segmentsMapsToOneUniqueTransition constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_segmentsMapsToOneUniqueTransition(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "segmentsMapsToOneUniqueTransition",
				 MAPPING__SEGMENTS_MAPS_TO_ONE_UNIQUE_TRANSITION__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the segmentsMessagesAreInTrace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__SEGMENTS_MESSAGES_ARE_IN_TRACE__EEXPRESSION = "\n" +
		"\t\t\tsegments?.messages->size() <> 0 implies trace.messages->includesAll(segments?.messages)";

	/**
	 * Validates the segmentsMessagesAreInTrace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_segmentsMessagesAreInTrace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "segmentsMessagesAreInTrace",
				 MAPPING__SEGMENTS_MESSAGES_ARE_IN_TRACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the segment2transitionKeysAreSegmentsInThisMapping constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__SEGMENT2TRANSITION_KEYS_ARE_SEGMENTS_IN_THIS_MAPPING__EEXPRESSION = "\n" +
		"\t\t\tsegment2transition->size() <> 0 implies segments->includesAll(segment2transition?.key)";

	/**
	 * Validates the segment2transitionKeysAreSegmentsInThisMapping constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_segment2transitionKeysAreSegmentsInThisMapping(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "segment2transitionKeysAreSegmentsInThisMapping",
				 MAPPING__SEGMENT2TRANSITION_KEYS_ARE_SEGMENTS_IN_THIS_MAPPING__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the segmentsRecordedInOrderTheyOccur constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__SEGMENTS_RECORDED_IN_ORDER_THEY_OCCUR__EEXPRESSION = "\n" +
		"\t\t\tsegments->forAll(segment |\n" +
		"\t\t\t\tsegments->indexOf(segment)+1 <= segments->size()\n" +
		"\t\t\t\timplies\n" +
		"\t\t\t\tsegment2transition->exists(s2t |\n" +
		"\t\t\t\t\ts2t?.key = segment\n" +
		"\t\t\t\t\tand\n" +
		"\t\t\t\t\tsegment2transition->exists(s2tnext |\n" +
		"\t\t\t\t\t\ts2tnext?.key = segments->at(segments->indexOf(segment)+1)\n" +
		"\t\t\t\t\t\tand\n" +
		"\t\t\t\t\t\ts2t?.value?.target = s2tnext?.value?.source\n" +
		"\t\t\t\t\t)\n" +
		"\t\t\t\t)\n" +
		"\t\t\t)";

	/**
	 * Validates the segmentsRecordedInOrderTheyOccur constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_segmentsRecordedInOrderTheyOccur(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "segmentsRecordedInOrderTheyOccur",
				 MAPPING__SEGMENTS_RECORDED_IN_ORDER_THEY_OCCUR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__EVERY_SEGMENTS_FIRST_MESSAGES_RECEIVING_OBJECT_MAPS_TO_THE_TRIGGER_ELEMENT_OF_THE_TRANSITION_TO_WHICH_THE_SEGMENT_MAPS__EEXPRESSION = "\n" +
		"\t\t\tsegments->forAll(s |\n" +
		"\t\t\t\tobject2element->exists(o2e |\n" +
		"\t\t\t\t\to2e?.key = s?.messages->at(1)?.target?.lifeline?.object\n" +
		"\t\t\t\t\tand\n" +
		"\t\t\t\t\tsegment2transition->exists(s2t |\n" +
		"\t\t\t\t\t\ts2t?.key = s\n" +
		"\t\t\t\t\t\tand\n" +
		"\t\t\t\t\t\ts2t?.value?.triggerElement?.uid = o2e?.value?->at(1)?.uid\n" +
		"\t\t\t\t\t)\n" +
		"\t\t\t\t)\n" +
		"\t\t\t)";

	/**
	 * Validates the everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps",
				 MAPPING__EVERY_SEGMENTS_FIRST_MESSAGES_RECEIVING_OBJECT_MAPS_TO_THE_TRIGGER_ELEMENT_OF_THE_TRANSITION_TO_WHICH_THE_SEGMENT_MAPS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__SECOND_MESSAGE_IN_EVERY_SEGMENT_IS_SENT_BY_OBJECT_WHICH_MAPS_TO_TRIGGER_ELEMENT_OF_TRANSITION_TO_WHICH_SEGMENT_MAPS__EEXPRESSION = "\n" +
		"\t\t\tsegments->forAll(s |\n" +
		"\t\t\t\tobject2element->exists(o2e |\n" +
		"\t\t\t\t\to2e?.key = s?.messages->at(2)?.source?.lifeline?.object\n" +
		"\t\t\t\t\tand\n" +
		"\t\t\t\t\tsegment2transition->exists(s2t |\n" +
		"\t\t\t\t\t\ts2t?.key = s\n" +
		"\t\t\t\t\t\tand\n" +
		"\t\t\t\t\t\to2e?.value->includes(s2t?.value?.triggerElement)\n" +
		"\t\t\t\t\t)\n" +
		"\t\t\t\t)\n" +
		"\t\t\t)";

	/**
	 * Validates the secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps",
				 MAPPING__SECOND_MESSAGE_IN_EVERY_SEGMENT_IS_SENT_BY_OBJECT_WHICH_MAPS_TO_TRIGGER_ELEMENT_OF_TRANSITION_TO_WHICH_SEGMENT_MAPS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__EVERY_ELEMENT_IN_ATRANSITION_TO_WHICH_ASEGMENT_MAPS_IS_MAPPED_AGAINST_BY_AN_OBJECT_WHICH_SENDS_OR_RECEIVES_AMESSAGE_IN_THAT_SEGMENT__EEXPRESSION = "\n" +
		"\t\t\t\tsegment2transition->forAll(s2t |\n" +
		"\t\t\t\t\ts2t?.value?.links?->collect(l |\n" +
		"\t\t\t\t\t\tl.feature->collect( f |\n" +
		"\t\t\t\t\t\t\tSet{f.object, f.values->at(1)?.stateValues?->at(l.no+1)}\n" +
		"\t\t\t\t\t\t)\n" +
		"\t\t\t\t\t)->asSet()->forAll(element |\n" +
		"\t\t\t\t\t\ts2t?.key?.messages?->select(m |\n" +
		"\t\t\t\t\t\t\tm.source <> null\n" +
		"\t\t\t\t\t\t\tand\n" +
		"\t\t\t\t\t\t\tm.target <> null\n" +
		"\t\t\t\t\t\t)->collect(m |\n" +
		"\t\t\t\t\t\t\tSet{m?.source, m?.target}\n" +
		"\t\t\t\t\t\t).lifeline.object->asSet()->exists(object |\n" +
		"\t\t\t\t\t\t\tobject2element?->exists(o2e |\n" +
		"\t\t\t\t\t\t\t\to2e.key = object\n" +
		"\t\t\t\t\t\t\t\tand\n" +
		"\t\t\t\t\t\t\t\to2e.value->at(1)?.uid = element.uid\n" +
		"\t\t\t\t\t\t\t)\n" +
		"\t\t\t\t\t\t)\n" +
		"\t\t\t\t\t)\n" +
		"\t\t\t\t)";

	/**
	 * Validates the everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment",
				 MAPPING__EVERY_ELEMENT_IN_ATRANSITION_TO_WHICH_ASEGMENT_MAPS_IS_MAPPED_AGAINST_BY_AN_OBJECT_WHICH_SENDS_OR_RECEIVES_AMESSAGE_IN_THAT_SEGMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__EVERY_LINK_IN_ATRANSITION_TO_WHICH_ASEGMENT_MAPS_IS_MAPPED_AGAINST_BY_AMESSAGE_IN_THAT_SEGMENT__EEXPRESSION = "\n" +
		"\t\t\t\tsegment2transition->forAll(s2t |\n" +
		"\t\t\t\t\ts2t?.value?.links->forAll(l |\n" +
		"\t\t\t\t\t\tmessage2link->exists(m2l |\n" +
		"\t\t\t\t\t\t\ts2t?.key?.messages->includes(m2l?.key)\n" +
		"\t\t\t\t\t\t\tand\n" +
		"\t\t\t\t\t\t\tm2l?.value->includes(l)\n" +
		"\t\t\t\t\t\t)\n" +
		"\t\t\t\t\t)\n" +
		"\t\t\t\t)";

	/**
	 * Validates the everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment",
				 MAPPING__EVERY_LINK_IN_ATRANSITION_TO_WHICH_ASEGMENT_MAPS_IS_MAPPED_AGAINST_BY_AMESSAGE_IN_THAT_SEGMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the allObjectsWhichMapToElementsAreObjectsInTheTrace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__ALL_OBJECTS_WHICH_MAP_TO_ELEMENTS_ARE_OBJECTS_IN_THE_TRACE__EEXPRESSION = "\n" +
		"\t\t\ttrace.lifelines?.object->includesAll(object2element?.key)";

	/**
	 * Validates the allObjectsWhichMapToElementsAreObjectsInTheTrace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_allObjectsWhichMapToElementsAreObjectsInTheTrace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "allObjectsWhichMapToElementsAreObjectsInTheTrace",
				 MAPPING__ALL_OBJECTS_WHICH_MAP_TO_ELEMENTS_ARE_OBJECTS_IN_THE_TRACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the allElementsMappedToAreElementsInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__ALL_ELEMENTS_MAPPED_TO_ARE_ELEMENTS_IN_THE_STATE_SPACE__EEXPRESSION = "\n" +
		"\t\t\tstateSpace.states?.objects->includesAll(object2element?.value)";

	/**
	 * Validates the allElementsMappedToAreElementsInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_allElementsMappedToAreElementsInTheStateSpace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "allElementsMappedToAreElementsInTheStateSpace",
				 MAPPING__ALL_ELEMENTS_MAPPED_TO_ARE_ELEMENTS_IN_THE_STATE_SPACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the objectMapsToStateObjectsWhichRepresentTheSameElement constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__OBJECT_MAPS_TO_STATE_OBJECTS_WHICH_REPRESENT_THE_SAME_ELEMENT__EEXPRESSION = "\n" +
		"\t\t\tobject2element->forAll(o2e|o2e?.value?.uid->asSet()->size()=1)";

	/**
	 * Validates the objectMapsToStateObjectsWhichRepresentTheSameElement constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_objectMapsToStateObjectsWhichRepresentTheSameElement(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "objectMapsToStateObjectsWhichRepresentTheSameElement",
				 MAPPING__OBJECT_MAPS_TO_STATE_OBJECTS_WHICH_REPRESENT_THE_SAME_ELEMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the allMessagesWhichMapToLinksAreMessagesInTheTrace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__ALL_MESSAGES_WHICH_MAP_TO_LINKS_ARE_MESSAGES_IN_THE_TRACE__EEXPRESSION = "\n" +
		"\t\t\ttrace.messages->includesAll(message2link?.key)";

	/**
	 * Validates the allMessagesWhichMapToLinksAreMessagesInTheTrace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_allMessagesWhichMapToLinksAreMessagesInTheTrace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "allMessagesWhichMapToLinksAreMessagesInTheTrace",
				 MAPPING__ALL_MESSAGES_WHICH_MAP_TO_LINKS_ARE_MESSAGES_IN_THE_TRACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the allLinksMappedToAreLinksInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__ALL_LINKS_MAPPED_TO_ARE_LINKS_IN_THE_STATE_SPACE__EEXPRESSION = "\n" +
		"\t\t\tstateSpace.states?.out?.links->includesAll(message2link?.value)";

	/**
	 * Validates the allLinksMappedToAreLinksInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_allLinksMappedToAreLinksInTheStateSpace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "allLinksMappedToAreLinksInTheStateSpace",
				 MAPPING__ALL_LINKS_MAPPED_TO_ARE_LINKS_IN_THE_STATE_SPACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the allMappedToEventsAreInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__ALL_MAPPED_TO_EVENTS_ARE_IN_THE_STATE_SPACE__EEXPRESSION = "\n" +
		"\t\t\tstateSpace.states?.out?.events?.name->includesAll(message2event?.value)";

	/**
	 * Validates the allMappedToEventsAreInTheStateSpace constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_allMappedToEventsAreInTheStateSpace(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "allMappedToEventsAreInTheStateSpace",
				 MAPPING__ALL_MAPPED_TO_EVENTS_ARE_IN_THE_STATE_SPACE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the messageMapsToASingleLink constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__MESSAGE_MAPS_TO_ASINGLE_LINK__EEXPRESSION = "\n" +
		"\t\t\tmessage2link?->isUnique(key)";

	/**
	 * Validates the messageMapsToASingleLink constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_messageMapsToASingleLink(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "messageMapsToASingleLink",
				 MAPPING__MESSAGE_MAPS_TO_ASINGLE_LINK__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the messageMappedToLinkIsInASegmentWhichMapsToLinksTransition constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MAPPING__MESSAGE_MAPPED_TO_LINK_IS_IN_ASEGMENT_WHICH_MAPS_TO_LINKS_TRANSITION__EEXPRESSION = "\n" +
		"\t\tmessage2link->forAll(m2l|\n" +
		"\t\t\tsegments->exists(s|\n" +
		"\t\t\t\ts?.messages->includes(m2l?.key)\n" +
		"\t\t\t\tand\n" +
		"\t\t\t\tsegment2transition->exists(s2t|\n" +
		"\t\t\t\t\ts2t?.key=s\n" +
		"\t\t\t\t\tand\n" +
		"\t\t\t\t\ts2t?.value?.links->includes(m2l?.value)\n" +
		"\t\t\t\t)\n" +
		"\t\t\tand\n" +
		"\t\t\tmessage2event->exists(m2e|\n" +
		"\t\t\t\tm2l?.key?.payload = m2e?.key.replaceFirst('.*;','')\n" +
		"\t\t\t\tand\n" +
		"\t\t\t\tm2l?.value?.events?.name->includes(m2e?.value)\n" +
		"\t\t\t)\n" +
		"\t\t\t)\n" +
		"\t\t)";

	/**
	 * Validates the messageMappedToLinkIsInASegmentWhichMapsToLinksTransition constraint of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMapping_messageMappedToLinkIsInASegmentWhichMapsToLinksTransition(Mapping mapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.MAPPING,
				 mapping,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "messageMappedToLinkIsInASegmentWhichMapsToLinksTransition",
				 MAPPING__MESSAGE_MAPPED_TO_LINK_IS_IN_ASEGMENT_WHICH_MAPS_TO_LINKS_TRANSITION__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSegment(Segment segment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(segment, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(segment, diagnostics, context);
		if (result || diagnostics != null) result &= validateSegment_containsASingleFoundMessage(segment, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the containsASingleFoundMessage constraint of '<em>Segment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SEGMENT__CONTAINS_ASINGLE_FOUND_MESSAGE__EEXPRESSION = "\n" +
		"\t\t\tmessages->select(m |\n" +
		"\t\t\t\t\tm?.target = null\n" +
		"\t\t\t\t)->size() = 1";

	/**
	 * Validates the containsASingleFoundMessage constraint of '<em>Segment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSegment_containsASingleFoundMessage(Segment segment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MappingPackage.Literals.SEGMENT,
				 segment,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "containsASingleFoundMessage",
				 SEGMENT__CONTAINS_ASINGLE_FOUND_MESSAGE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObjectToElementMap(Map.Entry<?, ?> objectToElementMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)objectToElementMap, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSegmentToTransitionMap(Map.Entry<?, ?> segmentToTransitionMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)segmentToTransitionMap, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMessageToLinkMap(Map.Entry<?, ?> messageToLinkMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)messageToLinkMap, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMessageToEventMap(Map.Entry<?, ?> messageToEventMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)messageToEventMap, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateObjectToElement(Map.Entry<?, ?> stateObjectToElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)stateObjectToElement, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //MappingValidator
