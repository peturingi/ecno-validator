/**
 */
package dk.ingi.petur.validator.mapping.impl;

import dk.dtu.compute.se.ecno.statespace.StatespacePackage;

import dk.dtu.imm.se.ecno.eclipse.save.behaviourstates.BehaviourstatesPackage;

import dk.ingi.petur.trace.TracePackage;

import dk.ingi.petur.validator.mapping.Mapping;
import dk.ingi.petur.validator.mapping.MappingFactory;
import dk.ingi.petur.validator.mapping.MappingPackage;
import dk.ingi.petur.validator.mapping.Segment;

import dk.ingi.petur.validator.mapping.util.MappingValidator;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MappingPackageImpl extends EPackageImpl implements MappingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mappingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectToElementMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentToTransitionMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageToLinkMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageToEventMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateObjectToElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MappingPackageImpl() {
		super(eNS_URI, MappingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MappingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MappingPackage init() {
		if (isInited) return (MappingPackage)EPackage.Registry.INSTANCE.getEPackage(MappingPackage.eNS_URI);

		// Obtain or create and register package
		MappingPackageImpl theMappingPackage = (MappingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MappingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MappingPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		BehaviourstatesPackage.eINSTANCE.eClass();
		StatespacePackage.eINSTANCE.eClass();
		TracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theMappingPackage.createPackageContents();

		// Initialize created meta-data
		theMappingPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theMappingPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return MappingValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theMappingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MappingPackage.eNS_URI, theMappingPackage);
		return theMappingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMapping() {
		return mappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_Segments() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_Object2element() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_Segment2transition() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_Message2link() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_Message2event() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_StateSpace() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapping_Trace() {
		return (EReference)mappingEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMapping__GetUnmappedMessages() {
		return mappingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegment() {
		return segmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegment_Messages() {
		return (EReference)segmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectToElementMap() {
		return objectToElementMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectToElementMap_Key() {
		return (EReference)objectToElementMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectToElementMap_Value() {
		return (EReference)objectToElementMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentToTransitionMap() {
		return segmentToTransitionMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentToTransitionMap_Key() {
		return (EReference)segmentToTransitionMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentToTransitionMap_Value() {
		return (EReference)segmentToTransitionMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageToLinkMap() {
		return messageToLinkMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageToLinkMap_Key() {
		return (EReference)messageToLinkMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageToLinkMap_Value() {
		return (EReference)messageToLinkMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageToEventMap() {
		return messageToEventMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageToEventMap_Key() {
		return (EAttribute)messageToEventMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageToEventMap_Value() {
		return (EAttribute)messageToEventMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateObjectToElement() {
		return stateObjectToElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateObjectToElement_Key() {
		return (EReference)stateObjectToElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateObjectToElement_Value() {
		return (EReference)stateObjectToElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MappingFactory getMappingFactory() {
		return (MappingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		mappingEClass = createEClass(MAPPING);
		createEReference(mappingEClass, MAPPING__SEGMENTS);
		createEReference(mappingEClass, MAPPING__OBJECT2ELEMENT);
		createEReference(mappingEClass, MAPPING__SEGMENT2TRANSITION);
		createEReference(mappingEClass, MAPPING__MESSAGE2LINK);
		createEReference(mappingEClass, MAPPING__MESSAGE2EVENT);
		createEReference(mappingEClass, MAPPING__STATE_SPACE);
		createEReference(mappingEClass, MAPPING__TRACE);
		createEOperation(mappingEClass, MAPPING___GET_UNMAPPED_MESSAGES);

		segmentEClass = createEClass(SEGMENT);
		createEReference(segmentEClass, SEGMENT__MESSAGES);

		objectToElementMapEClass = createEClass(OBJECT_TO_ELEMENT_MAP);
		createEReference(objectToElementMapEClass, OBJECT_TO_ELEMENT_MAP__KEY);
		createEReference(objectToElementMapEClass, OBJECT_TO_ELEMENT_MAP__VALUE);

		segmentToTransitionMapEClass = createEClass(SEGMENT_TO_TRANSITION_MAP);
		createEReference(segmentToTransitionMapEClass, SEGMENT_TO_TRANSITION_MAP__KEY);
		createEReference(segmentToTransitionMapEClass, SEGMENT_TO_TRANSITION_MAP__VALUE);

		messageToLinkMapEClass = createEClass(MESSAGE_TO_LINK_MAP);
		createEReference(messageToLinkMapEClass, MESSAGE_TO_LINK_MAP__KEY);
		createEReference(messageToLinkMapEClass, MESSAGE_TO_LINK_MAP__VALUE);

		messageToEventMapEClass = createEClass(MESSAGE_TO_EVENT_MAP);
		createEAttribute(messageToEventMapEClass, MESSAGE_TO_EVENT_MAP__KEY);
		createEAttribute(messageToEventMapEClass, MESSAGE_TO_EVENT_MAP__VALUE);

		stateObjectToElementEClass = createEClass(STATE_OBJECT_TO_ELEMENT);
		createEReference(stateObjectToElementEClass, STATE_OBJECT_TO_ELEMENT__KEY);
		createEReference(stateObjectToElementEClass, STATE_OBJECT_TO_ELEMENT__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StatespacePackage theStatespacePackage = (StatespacePackage)EPackage.Registry.INSTANCE.getEPackage(StatespacePackage.eNS_URI);
		TracePackage theTracePackage = (TracePackage)EPackage.Registry.INSTANCE.getEPackage(TracePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(mappingEClass, Mapping.class, "Mapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMapping_Segments(), this.getSegment(), null, "segments", null, 0, -1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapping_Object2element(), this.getObjectToElementMap(), null, "object2element", null, 0, -1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapping_Segment2transition(), this.getSegmentToTransitionMap(), null, "segment2transition", null, 0, -1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapping_Message2link(), this.getMessageToLinkMap(), null, "message2link", null, 0, -1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapping_Message2event(), this.getMessageToEventMap(), null, "message2event", null, 0, -1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapping_StateSpace(), theStatespacePackage.getStateSpace(), null, "stateSpace", null, 1, 1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapping_Trace(), theTracePackage.getTrace(), null, "trace", null, 1, 1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMapping__GetUnmappedMessages(), theTracePackage.getMessage(), "getUnmappedMessages", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(segmentEClass, Segment.class, "Segment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegment_Messages(), theTracePackage.getMessage(), null, "messages", null, 1, -1, Segment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectToElementMapEClass, Map.Entry.class, "ObjectToElementMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObjectToElementMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectToElementMap_Value(), theStatespacePackage.getStateObject(), null, "value", null, 1, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(segmentToTransitionMapEClass, Map.Entry.class, "SegmentToTransitionMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegmentToTransitionMap_Key(), this.getSegment(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentToTransitionMap_Value(), theStatespacePackage.getTransition(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageToLinkMapEClass, Map.Entry.class, "MessageToLinkMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageToLinkMap_Key(), theTracePackage.getMessage(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageToLinkMap_Value(), theStatespacePackage.getLink(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageToEventMapEClass, Map.Entry.class, "MessageToEventMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMessageToEventMap_Key(), ecorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageToEventMap_Value(), ecorePackage.getEString(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateObjectToElementEClass, Map.Entry.class, "StateObjectToElement", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStateObjectToElement_Key(), theStatespacePackage.getStateObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateObjectToElement_Value(), theStatespacePackage.getElement(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/OCL/Import
		createImportAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createImportAnnotations() {
		String source = "http://www.eclipse.org/OCL/Import";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "ecore", "http://www.eclipse.org/emf/2002/Ecore",
			 "eu.petur", "../../dk.ingi.petur.trace/model/Trace.ecore#/",
			 "statespace", "../../dk.dtu.compute.se.ecno.statespace/model/statespace.ecore#/"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (mappingEClass, 
		   source, 
		   new String[] {
			 "constraints", "everyMessageIsInASingleSegment everySegmentMapsToATransition everySegmentMapsOnceToATransitionInTheStateSpace segmentsMapsToOneUniqueTransition segmentsMessagesAreInTrace segment2transitionKeysAreSegmentsInThisMapping segmentsRecordedInOrderTheyOccur everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment allObjectsWhichMapToElementsAreObjectsInTheTrace allElementsMappedToAreElementsInTheStateSpace objectMapsToStateObjectsWhichRepresentTheSameElement allMessagesWhichMapToLinksAreMessagesInTheTrace allLinksMappedToAreLinksInTheStateSpace allMappedToEventsAreInTheStateSpace messageMapsToASingleLink messageMappedToLinkIsInASegmentWhichMapsToLinksTransition"
		   });	
		addAnnotation
		  (segmentEClass, 
		   source, 
		   new String[] {
			 "constraints", "containsASingleFoundMessage"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (mappingEClass, 
		   source, 
		   new String[] {
			 "everyMessageIsInASingleSegment", "\n\t\t\tsegments?.messages->includesAll(trace.messages)\n\t\t\tand\n\t\t\tsegments?.messages->size() = segments?.messages->asSet()->size()",
			 "everySegmentMapsToATransition", "\n\t\t\tsegment2transition?.key->includesAll(segments)",
			 "everySegmentMapsOnceToATransitionInTheStateSpace", "\n\t\t\tsegment2transition?.value->size() <> 0 implies stateSpace.transitions->includesAll(segment2transition?.value)",
			 "segmentsMapsToOneUniqueTransition", "\n\t\t\tsegment2transition?->isUnique(key)",
			 "segmentsMessagesAreInTrace", "\n\t\t\tsegments?.messages->size() <> 0 implies trace.messages->includesAll(segments?.messages)",
			 "segment2transitionKeysAreSegmentsInThisMapping", "\n\t\t\tsegment2transition->size() <> 0 implies segments->includesAll(segment2transition?.key)",
			 "segmentsRecordedInOrderTheyOccur", "\n\t\t\tsegments->forAll(segment |\n\t\t\t\tsegments->indexOf(segment)+1 <= segments->size()\n\t\t\t\timplies\n\t\t\t\tsegment2transition->exists(s2t |\n\t\t\t\t\ts2t?.key = segment\n\t\t\t\t\tand\n\t\t\t\t\tsegment2transition->exists(s2tnext |\n\t\t\t\t\t\ts2tnext?.key = segments->at(segments->indexOf(segment)+1)\n\t\t\t\t\t\tand\n\t\t\t\t\t\ts2t?.value?.target = s2tnext?.value?.source\n\t\t\t\t\t)\n\t\t\t\t)\n\t\t\t)",
			 "everySegmentsFirstMessagesReceivingObjectMapsToTheTriggerElementOfTheTransitionToWhichTheSegmentMaps", "\n\t\t\tsegments->forAll(s |\n\t\t\t\tobject2element->exists(o2e |\n\t\t\t\t\to2e?.key = s?.messages->at(1)?.target?.lifeline?.object\n\t\t\t\t\tand\n\t\t\t\t\tsegment2transition->exists(s2t |\n\t\t\t\t\t\ts2t?.key = s\n\t\t\t\t\t\tand\n\t\t\t\t\t\ts2t?.value?.triggerElement?.uid = o2e?.value?->at(1)?.uid\n\t\t\t\t\t)\n\t\t\t\t)\n\t\t\t)",
			 "secondMessageInEverySegmentIsSentByObjectWhichMapsToTriggerElementOfTransitionToWhichSegmentMaps", "\n\t\t\tsegments->forAll(s |\n\t\t\t\tobject2element->exists(o2e |\n\t\t\t\t\to2e?.key = s?.messages->at(2)?.source?.lifeline?.object\n\t\t\t\t\tand\n\t\t\t\t\tsegment2transition->exists(s2t |\n\t\t\t\t\t\ts2t?.key = s\n\t\t\t\t\t\tand\n\t\t\t\t\t\to2e?.value->includes(s2t?.value?.triggerElement)\n\t\t\t\t\t)\n\t\t\t\t)\n\t\t\t)",
			 "everyElementInATransitionToWhichASegmentMapsIsMappedAgainstByAnObjectWhichSendsOrReceivesAMessageInThatSegment", "\n\t\t\t\tsegment2transition->forAll(s2t |\n\t\t\t\t\ts2t?.value?.links?->collect(l |\n\t\t\t\t\t\tl.feature->collect( f |\n\t\t\t\t\t\t\tSet{f.object, f.values->at(1)?.stateValues?->at(l.no+1)}\n\t\t\t\t\t\t)\n\t\t\t\t\t)->asSet()->forAll(element |\n\t\t\t\t\t\ts2t?.key?.messages?->select(m |\n\t\t\t\t\t\t\tm.source <> null\n\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\tm.target <> null\n\t\t\t\t\t\t)->collect(m |\n\t\t\t\t\t\t\tSet{m?.source, m?.target}\n\t\t\t\t\t\t).lifeline.object->asSet()->exists(object |\n\t\t\t\t\t\t\tobject2element?->exists(o2e |\n\t\t\t\t\t\t\t\to2e.key = object\n\t\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\t\to2e.value->at(1)?.uid = element.uid\n\t\t\t\t\t\t\t)\n\t\t\t\t\t\t)\n\t\t\t\t\t)\n\t\t\t\t)",
			 "everyLinkInATransitionToWhichASegmentMapsIsMappedAgainstByAMessageInThatSegment", "\n\t\t\t\tsegment2transition->forAll(s2t |\n\t\t\t\t\ts2t?.value?.links->forAll(l |\n\t\t\t\t\t\tmessage2link->exists(m2l |\n\t\t\t\t\t\t\ts2t?.key?.messages->includes(m2l?.key)\n\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\tm2l?.value->includes(l)\n\t\t\t\t\t\t)\n\t\t\t\t\t)\n\t\t\t\t)",
			 "allObjectsWhichMapToElementsAreObjectsInTheTrace", "\n\t\t\ttrace.lifelines?.object->includesAll(object2element?.key)",
			 "allElementsMappedToAreElementsInTheStateSpace", "\n\t\t\tstateSpace.states?.objects->includesAll(object2element?.value)",
			 "objectMapsToStateObjectsWhichRepresentTheSameElement", "\n\t\t\tobject2element->forAll(o2e|o2e?.value?.uid->asSet()->size()=1)",
			 "allMessagesWhichMapToLinksAreMessagesInTheTrace", "\n\t\t\ttrace.messages->includesAll(message2link?.key)",
			 "allLinksMappedToAreLinksInTheStateSpace", "\n\t\t\tstateSpace.states?.out?.links->includesAll(message2link?.value)",
			 "allMappedToEventsAreInTheStateSpace", "\n\t\t\tstateSpace.states?.out?.events?.name->includesAll(message2event?.value)",
			 "messageMapsToASingleLink", "\n\t\t\tmessage2link?->isUnique(key)",
			 "messageMappedToLinkIsInASegmentWhichMapsToLinksTransition", "\n\t\tmessage2link->forAll(m2l|\n\t\t\tsegments->exists(s|\n\t\t\t\ts?.messages->includes(m2l?.key)\n\t\t\t\tand\n\t\t\t\tsegment2transition->exists(s2t|\n\t\t\t\t\ts2t?.key=s\n\t\t\t\t\tand\n\t\t\t\t\ts2t?.value?.links->includes(m2l?.value)\n\t\t\t\t)\n\t\t\tand\n\t\t\tmessage2event->exists(m2e|\n\t\t\t\tm2l?.key?.payload = m2e?.key.replaceFirst(\'.*;\',\'\')\n\t\t\t\tand\n\t\t\t\tm2l?.value?.events?.name->includes(m2e?.value)\n\t\t\t)\n\t\t\t)\n\t\t)"
		   });	
		addAnnotation
		  (segmentEClass, 
		   source, 
		   new String[] {
			 "containsASingleFoundMessage", "\n\t\t\tmessages->select(m |\n\t\t\t\t\tm?.target = null\n\t\t\t\t)->size() = 1"
		   });
	}

} //MappingPackageImpl
