/**
 */
package dk.ingi.petur.validator.mapping.impl;

import dk.dtu.compute.se.ecno.statespace.Link;
import dk.dtu.compute.se.ecno.statespace.StateObject;
import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.dtu.compute.se.ecno.statespace.Transition;

import dk.ingi.petur.trace.Message;
import dk.ingi.petur.trace.Trace;

import dk.ingi.petur.validator.mapping.Mapping;
import dk.ingi.petur.validator.mapping.MappingPackage;
import dk.ingi.petur.validator.mapping.Segment;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getSegments <em>Segments</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getObject2element <em>Object2element</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getSegment2transition <em>Segment2transition</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getMessage2link <em>Message2link</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getMessage2event <em>Message2event</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getStateSpace <em>State Space</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.mapping.impl.MappingImpl#getTrace <em>Trace</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MappingImpl extends MinimalEObjectImpl.Container implements Mapping {
	/**
	 * The cached value of the '{@link #getSegments() <em>Segments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<Segment> segments;

	/**
	 * The cached value of the '{@link #getObject2element() <em>Object2element</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject2element()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<StateObject>> object2element;

	/**
	 * The cached value of the '{@link #getSegment2transition() <em>Segment2transition</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegment2transition()
	 * @generated
	 * @ordered
	 */
	protected EMap<Segment, Transition> segment2transition;

	/**
	 * The cached value of the '{@link #getMessage2link() <em>Message2link</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage2link()
	 * @generated
	 * @ordered
	 */
	protected EMap<Message, Link> message2link;

	/**
	 * The cached value of the '{@link #getMessage2event() <em>Message2event</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage2event()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> message2event;

	/**
	 * The cached value of the '{@link #getStateSpace() <em>State Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateSpace()
	 * @generated
	 * @ordered
	 */
	protected StateSpace stateSpace;

	/**
	 * The cached value of the '{@link #getTrace() <em>Trace</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrace()
	 * @generated
	 * @ordered
	 */
	protected Trace trace;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Segment> getSegments() {
		if (segments == null) {
			segments = new EObjectContainmentEList<Segment>(Segment.class, this, MappingPackage.MAPPING__SEGMENTS);
		}
		return segments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<StateObject>> getObject2element() {
		if (object2element == null) {
			object2element = new EcoreEMap<EObject,EList<StateObject>>(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP, ObjectToElementMapImpl.class, this, MappingPackage.MAPPING__OBJECT2ELEMENT);
		}
		return object2element;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Segment, Transition> getSegment2transition() {
		if (segment2transition == null) {
			segment2transition = new EcoreEMap<Segment,Transition>(MappingPackage.Literals.SEGMENT_TO_TRANSITION_MAP, SegmentToTransitionMapImpl.class, this, MappingPackage.MAPPING__SEGMENT2TRANSITION);
		}
		return segment2transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Message, Link> getMessage2link() {
		if (message2link == null) {
			message2link = new EcoreEMap<Message,Link>(MappingPackage.Literals.MESSAGE_TO_LINK_MAP, MessageToLinkMapImpl.class, this, MappingPackage.MAPPING__MESSAGE2LINK);
		}
		return message2link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getMessage2event() {
		if (message2event == null) {
			message2event = new EcoreEMap<String,String>(MappingPackage.Literals.MESSAGE_TO_EVENT_MAP, MessageToEventMapImpl.class, this, MappingPackage.MAPPING__MESSAGE2EVENT);
		}
		return message2event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateSpace getStateSpace() {
		return stateSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStateSpace(StateSpace newStateSpace, NotificationChain msgs) {
		StateSpace oldStateSpace = stateSpace;
		stateSpace = newStateSpace;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MappingPackage.MAPPING__STATE_SPACE, oldStateSpace, newStateSpace);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateSpace(StateSpace newStateSpace) {
		if (newStateSpace != stateSpace) {
			NotificationChain msgs = null;
			if (stateSpace != null)
				msgs = ((InternalEObject)stateSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MappingPackage.MAPPING__STATE_SPACE, null, msgs);
			if (newStateSpace != null)
				msgs = ((InternalEObject)newStateSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MappingPackage.MAPPING__STATE_SPACE, null, msgs);
			msgs = basicSetStateSpace(newStateSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.MAPPING__STATE_SPACE, newStateSpace, newStateSpace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trace getTrace() {
		return trace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTrace(Trace newTrace, NotificationChain msgs) {
		Trace oldTrace = trace;
		trace = newTrace;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MappingPackage.MAPPING__TRACE, oldTrace, newTrace);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrace(Trace newTrace) {
		if (newTrace != trace) {
			NotificationChain msgs = null;
			if (trace != null)
				msgs = ((InternalEObject)trace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MappingPackage.MAPPING__TRACE, null, msgs);
			if (newTrace != null)
				msgs = ((InternalEObject)newTrace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MappingPackage.MAPPING__TRACE, null, msgs);
			msgs = basicSetTrace(newTrace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.MAPPING__TRACE, newTrace, newTrace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getUnmappedMessages() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MappingPackage.MAPPING__SEGMENTS:
				return ((InternalEList<?>)getSegments()).basicRemove(otherEnd, msgs);
			case MappingPackage.MAPPING__OBJECT2ELEMENT:
				return ((InternalEList<?>)getObject2element()).basicRemove(otherEnd, msgs);
			case MappingPackage.MAPPING__SEGMENT2TRANSITION:
				return ((InternalEList<?>)getSegment2transition()).basicRemove(otherEnd, msgs);
			case MappingPackage.MAPPING__MESSAGE2LINK:
				return ((InternalEList<?>)getMessage2link()).basicRemove(otherEnd, msgs);
			case MappingPackage.MAPPING__MESSAGE2EVENT:
				return ((InternalEList<?>)getMessage2event()).basicRemove(otherEnd, msgs);
			case MappingPackage.MAPPING__STATE_SPACE:
				return basicSetStateSpace(null, msgs);
			case MappingPackage.MAPPING__TRACE:
				return basicSetTrace(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MappingPackage.MAPPING__SEGMENTS:
				return getSegments();
			case MappingPackage.MAPPING__OBJECT2ELEMENT:
				if (coreType) return getObject2element();
				else return getObject2element().map();
			case MappingPackage.MAPPING__SEGMENT2TRANSITION:
				if (coreType) return getSegment2transition();
				else return getSegment2transition().map();
			case MappingPackage.MAPPING__MESSAGE2LINK:
				if (coreType) return getMessage2link();
				else return getMessage2link().map();
			case MappingPackage.MAPPING__MESSAGE2EVENT:
				if (coreType) return getMessage2event();
				else return getMessage2event().map();
			case MappingPackage.MAPPING__STATE_SPACE:
				return getStateSpace();
			case MappingPackage.MAPPING__TRACE:
				return getTrace();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MappingPackage.MAPPING__SEGMENTS:
				getSegments().clear();
				getSegments().addAll((Collection<? extends Segment>)newValue);
				return;
			case MappingPackage.MAPPING__OBJECT2ELEMENT:
				((EStructuralFeature.Setting)getObject2element()).set(newValue);
				return;
			case MappingPackage.MAPPING__SEGMENT2TRANSITION:
				((EStructuralFeature.Setting)getSegment2transition()).set(newValue);
				return;
			case MappingPackage.MAPPING__MESSAGE2LINK:
				((EStructuralFeature.Setting)getMessage2link()).set(newValue);
				return;
			case MappingPackage.MAPPING__MESSAGE2EVENT:
				((EStructuralFeature.Setting)getMessage2event()).set(newValue);
				return;
			case MappingPackage.MAPPING__STATE_SPACE:
				setStateSpace((StateSpace)newValue);
				return;
			case MappingPackage.MAPPING__TRACE:
				setTrace((Trace)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MappingPackage.MAPPING__SEGMENTS:
				getSegments().clear();
				return;
			case MappingPackage.MAPPING__OBJECT2ELEMENT:
				getObject2element().clear();
				return;
			case MappingPackage.MAPPING__SEGMENT2TRANSITION:
				getSegment2transition().clear();
				return;
			case MappingPackage.MAPPING__MESSAGE2LINK:
				getMessage2link().clear();
				return;
			case MappingPackage.MAPPING__MESSAGE2EVENT:
				getMessage2event().clear();
				return;
			case MappingPackage.MAPPING__STATE_SPACE:
				setStateSpace((StateSpace)null);
				return;
			case MappingPackage.MAPPING__TRACE:
				setTrace((Trace)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MappingPackage.MAPPING__SEGMENTS:
				return segments != null && !segments.isEmpty();
			case MappingPackage.MAPPING__OBJECT2ELEMENT:
				return object2element != null && !object2element.isEmpty();
			case MappingPackage.MAPPING__SEGMENT2TRANSITION:
				return segment2transition != null && !segment2transition.isEmpty();
			case MappingPackage.MAPPING__MESSAGE2LINK:
				return message2link != null && !message2link.isEmpty();
			case MappingPackage.MAPPING__MESSAGE2EVENT:
				return message2event != null && !message2event.isEmpty();
			case MappingPackage.MAPPING__STATE_SPACE:
				return stateSpace != null;
			case MappingPackage.MAPPING__TRACE:
				return trace != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MappingPackage.MAPPING___GET_UNMAPPED_MESSAGES:
				return getUnmappedMessages();
		}
		return super.eInvoke(operationID, arguments);
	}

} //MappingImpl
