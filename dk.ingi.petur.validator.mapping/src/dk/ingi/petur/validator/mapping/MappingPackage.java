/**
 */
package dk.ingi.petur.validator.mapping;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.validator.mapping.MappingFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore' eu.petur='../../dk.ingi.petur.trace/model/Trace.ecore#/' statespace='../../dk.dtu.compute.se.ecno.statespace/model/statespace.ecore#/'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface MappingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mapping";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://petur.ingi.dk/ECNO/validator/mapping";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dk.ingi.petur.validator";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MappingPackage eINSTANCE = dk.ingi.petur.validator.mapping.impl.MappingPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.MappingImpl <em>Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.MappingImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getMapping()
	 * @generated
	 */
	int MAPPING = 0;

	/**
	 * The feature id for the '<em><b>Segments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__SEGMENTS = 0;

	/**
	 * The feature id for the '<em><b>Object2element</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__OBJECT2ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Segment2transition</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__SEGMENT2TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Message2link</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__MESSAGE2LINK = 3;

	/**
	 * The feature id for the '<em><b>Message2event</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__MESSAGE2EVENT = 4;

	/**
	 * The feature id for the '<em><b>State Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__STATE_SPACE = 5;

	/**
	 * The feature id for the '<em><b>Trace</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__TRACE = 6;

	/**
	 * The number of structural features of the '<em>Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_FEATURE_COUNT = 7;

	/**
	 * The operation id for the '<em>Get Unmapped Messages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING___GET_UNMAPPED_MESSAGES = 0;

	/**
	 * The number of operations of the '<em>Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.SegmentImpl <em>Segment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.SegmentImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getSegment()
	 * @generated
	 */
	int SEGMENT = 1;

	/**
	 * The feature id for the '<em><b>Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT__MESSAGES = 0;

	/**
	 * The number of structural features of the '<em>Segment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Segment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.ObjectToElementMapImpl <em>Object To Element Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.ObjectToElementMapImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getObjectToElementMap()
	 * @generated
	 */
	int OBJECT_TO_ELEMENT_MAP = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TO_ELEMENT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TO_ELEMENT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Object To Element Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TO_ELEMENT_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Object To Element Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TO_ELEMENT_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.SegmentToTransitionMapImpl <em>Segment To Transition Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.SegmentToTransitionMapImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getSegmentToTransitionMap()
	 * @generated
	 */
	int SEGMENT_TO_TRANSITION_MAP = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_TO_TRANSITION_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_TO_TRANSITION_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Segment To Transition Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_TO_TRANSITION_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Segment To Transition Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_TO_TRANSITION_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.MessageToLinkMapImpl <em>Message To Link Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.MessageToLinkMapImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getMessageToLinkMap()
	 * @generated
	 */
	int MESSAGE_TO_LINK_MAP = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_LINK_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_LINK_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Message To Link Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_LINK_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Message To Link Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_LINK_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.MessageToEventMapImpl <em>Message To Event Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.MessageToEventMapImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getMessageToEventMap()
	 * @generated
	 */
	int MESSAGE_TO_EVENT_MAP = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_EVENT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_EVENT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Message To Event Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_EVENT_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Message To Event Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TO_EVENT_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.mapping.impl.StateObjectToElementImpl <em>State Object To Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.mapping.impl.StateObjectToElementImpl
	 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getStateObjectToElement()
	 * @generated
	 */
	int STATE_OBJECT_TO_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OBJECT_TO_ELEMENT__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OBJECT_TO_ELEMENT__VALUE = 1;

	/**
	 * The number of structural features of the '<em>State Object To Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OBJECT_TO_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>State Object To Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OBJECT_TO_ELEMENT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.validator.mapping.Mapping <em>Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapping</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping
	 * @generated
	 */
	EClass getMapping();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.validator.mapping.Mapping#getSegments <em>Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Segments</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getSegments()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Segments();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.mapping.Mapping#getObject2element <em>Object2element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object2element</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getObject2element()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Object2element();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.mapping.Mapping#getSegment2transition <em>Segment2transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Segment2transition</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getSegment2transition()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Segment2transition();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.mapping.Mapping#getMessage2link <em>Message2link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Message2link</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getMessage2link()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Message2link();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.mapping.Mapping#getMessage2event <em>Message2event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Message2event</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getMessage2event()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Message2event();

	/**
	 * Returns the meta object for the containment reference '{@link dk.ingi.petur.validator.mapping.Mapping#getStateSpace <em>State Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>State Space</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getStateSpace()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_StateSpace();

	/**
	 * Returns the meta object for the containment reference '{@link dk.ingi.petur.validator.mapping.Mapping#getTrace <em>Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Trace</em>'.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getTrace()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Trace();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.validator.mapping.Mapping#getUnmappedMessages() <em>Get Unmapped Messages</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Unmapped Messages</em>' operation.
	 * @see dk.ingi.petur.validator.mapping.Mapping#getUnmappedMessages()
	 * @generated
	 */
	EOperation getMapping__GetUnmappedMessages();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.validator.mapping.Segment <em>Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment</em>'.
	 * @see dk.ingi.petur.validator.mapping.Segment
	 * @generated
	 */
	EClass getSegment();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.validator.mapping.Segment#getMessages <em>Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Messages</em>'.
	 * @see dk.ingi.petur.validator.mapping.Segment#getMessages()
	 * @see #getSegment()
	 * @generated
	 */
	EReference getSegment_Messages();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Object To Element Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object To Element Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
	 *        valueType="dk.dtu.compute.se.ecno.statespace.StateObject" valueRequired="true" valueMany="true"
	 * @generated
	 */
	EClass getObjectToElementMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectToElementMap()
	 * @generated
	 */
	EReference getObjectToElementMap_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectToElementMap()
	 * @generated
	 */
	EReference getObjectToElementMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Segment To Transition Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment To Transition Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.ingi.petur.validator.mapping.Segment" keyRequired="true"
	 *        valueType="dk.dtu.compute.se.ecno.statespace.Transition" valueRequired="true"
	 * @generated
	 */
	EClass getSegmentToTransitionMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSegmentToTransitionMap()
	 * @generated
	 */
	EReference getSegmentToTransitionMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSegmentToTransitionMap()
	 * @generated
	 */
	EReference getSegmentToTransitionMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Message To Link Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message To Link Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.ingi.petur.trace.Message" keyRequired="true"
	 *        valueType="dk.dtu.compute.se.ecno.statespace.Link" valueRequired="true"
	 * @generated
	 */
	EClass getMessageToLinkMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageToLinkMap()
	 * @generated
	 */
	EReference getMessageToLinkMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageToLinkMap()
	 * @generated
	 */
	EReference getMessageToLinkMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Message To Event Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message To Event Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EString" valueRequired="true"
	 * @generated
	 */
	EClass getMessageToEventMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageToEventMap()
	 * @generated
	 */
	EAttribute getMessageToEventMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageToEventMap()
	 * @generated
	 */
	EAttribute getMessageToEventMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>State Object To Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Object To Element</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.dtu.compute.se.ecno.statespace.StateObject" keyRequired="true"
	 *        valueType="dk.dtu.compute.se.ecno.statespace.Element" valueRequired="true"
	 * @generated
	 */
	EClass getStateObjectToElement();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateObjectToElement()
	 * @generated
	 */
	EReference getStateObjectToElement_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateObjectToElement()
	 * @generated
	 */
	EReference getStateObjectToElement_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MappingFactory getMappingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.MappingImpl <em>Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.MappingImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getMapping()
		 * @generated
		 */
		EClass MAPPING = eINSTANCE.getMapping();

		/**
		 * The meta object literal for the '<em><b>Segments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__SEGMENTS = eINSTANCE.getMapping_Segments();

		/**
		 * The meta object literal for the '<em><b>Object2element</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__OBJECT2ELEMENT = eINSTANCE.getMapping_Object2element();

		/**
		 * The meta object literal for the '<em><b>Segment2transition</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__SEGMENT2TRANSITION = eINSTANCE.getMapping_Segment2transition();

		/**
		 * The meta object literal for the '<em><b>Message2link</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__MESSAGE2LINK = eINSTANCE.getMapping_Message2link();

		/**
		 * The meta object literal for the '<em><b>Message2event</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__MESSAGE2EVENT = eINSTANCE.getMapping_Message2event();

		/**
		 * The meta object literal for the '<em><b>State Space</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__STATE_SPACE = eINSTANCE.getMapping_StateSpace();

		/**
		 * The meta object literal for the '<em><b>Trace</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__TRACE = eINSTANCE.getMapping_Trace();

		/**
		 * The meta object literal for the '<em><b>Get Unmapped Messages</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MAPPING___GET_UNMAPPED_MESSAGES = eINSTANCE.getMapping__GetUnmappedMessages();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.SegmentImpl <em>Segment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.SegmentImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getSegment()
		 * @generated
		 */
		EClass SEGMENT = eINSTANCE.getSegment();

		/**
		 * The meta object literal for the '<em><b>Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT__MESSAGES = eINSTANCE.getSegment_Messages();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.ObjectToElementMapImpl <em>Object To Element Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.ObjectToElementMapImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getObjectToElementMap()
		 * @generated
		 */
		EClass OBJECT_TO_ELEMENT_MAP = eINSTANCE.getObjectToElementMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_TO_ELEMENT_MAP__KEY = eINSTANCE.getObjectToElementMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_TO_ELEMENT_MAP__VALUE = eINSTANCE.getObjectToElementMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.SegmentToTransitionMapImpl <em>Segment To Transition Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.SegmentToTransitionMapImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getSegmentToTransitionMap()
		 * @generated
		 */
		EClass SEGMENT_TO_TRANSITION_MAP = eINSTANCE.getSegmentToTransitionMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_TO_TRANSITION_MAP__KEY = eINSTANCE.getSegmentToTransitionMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_TO_TRANSITION_MAP__VALUE = eINSTANCE.getSegmentToTransitionMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.MessageToLinkMapImpl <em>Message To Link Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.MessageToLinkMapImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getMessageToLinkMap()
		 * @generated
		 */
		EClass MESSAGE_TO_LINK_MAP = eINSTANCE.getMessageToLinkMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_TO_LINK_MAP__KEY = eINSTANCE.getMessageToLinkMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_TO_LINK_MAP__VALUE = eINSTANCE.getMessageToLinkMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.MessageToEventMapImpl <em>Message To Event Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.MessageToEventMapImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getMessageToEventMap()
		 * @generated
		 */
		EClass MESSAGE_TO_EVENT_MAP = eINSTANCE.getMessageToEventMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_TO_EVENT_MAP__KEY = eINSTANCE.getMessageToEventMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_TO_EVENT_MAP__VALUE = eINSTANCE.getMessageToEventMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.mapping.impl.StateObjectToElementImpl <em>State Object To Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.mapping.impl.StateObjectToElementImpl
		 * @see dk.ingi.petur.validator.mapping.impl.MappingPackageImpl#getStateObjectToElement()
		 * @generated
		 */
		EClass STATE_OBJECT_TO_ELEMENT = eINSTANCE.getStateObjectToElement();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_OBJECT_TO_ELEMENT__KEY = eINSTANCE.getStateObjectToElement_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_OBJECT_TO_ELEMENT__VALUE = eINSTANCE.getStateObjectToElement_Value();

	}

} //MappingPackage
