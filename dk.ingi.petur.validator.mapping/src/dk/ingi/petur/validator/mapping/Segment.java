/**
 */
package dk.ingi.petur.validator.mapping;

import dk.ingi.petur.trace.Message;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Segment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.validator.mapping.Segment#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.validator.mapping.MappingPackage#getSegment()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='containsASingleFoundMessage'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot containsASingleFoundMessage='\n\t\t\tmessages-&gt;select(m |\n\t\t\t\t\tm?.target = null\n\t\t\t\t)-&gt;size() = 1'"
 * @generated
 */
public interface Segment extends EObject {
	/**
	 * Returns the value of the '<em><b>Messages</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.trace.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Messages</em>' reference list.
	 * @see dk.ingi.petur.validator.mapping.MappingPackage#getSegment_Messages()
	 * @model required="true"
	 * @generated
	 */
	EList<Message> getMessages();

} // Segment
