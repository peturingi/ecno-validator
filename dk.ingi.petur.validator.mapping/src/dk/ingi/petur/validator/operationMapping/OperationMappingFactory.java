/**
 */
package dk.ingi.petur.validator.operationMapping;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.validator.operationMapping.OperationMappingPackage
 * @generated
 */
public interface OperationMappingFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperationMappingFactory eINSTANCE = dk.ingi.petur.validator.operationMapping.impl.OperationMappingFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Operation Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation Mapping</em>'.
	 * @generated
	 */
	OperationMapping createOperationMapping();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OperationMappingPackage getOperationMappingPackage();

} //OperationMappingFactory
