/**
 */
package dk.ingi.petur.validator.operationMapping.impl;

import dk.ingi.petur.validator.operationMapping.*;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperationMappingFactoryImpl extends EFactoryImpl implements OperationMappingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OperationMappingFactory init() {
		try {
			OperationMappingFactory theOperationMappingFactory = (OperationMappingFactory)EPackage.Registry.INSTANCE.getEFactory(OperationMappingPackage.eNS_URI);
			if (theOperationMappingFactory != null) {
				return theOperationMappingFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OperationMappingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationMappingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OperationMappingPackage.OPERATION_MAPPING: return createOperationMapping();
			case OperationMappingPackage.OPERATION_TO_EVENT_MAP: return (EObject)createOperationToEventMap();
			case OperationMappingPackage.OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP: return (EObject)createOperationParameterToEventParameterMap();
			case OperationMappingPackage.PARAMETER_TO_PARAMETER: return (EObject)createParameterToParameter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationMapping createOperationMapping() {
		OperationMappingImpl operationMapping = new OperationMappingImpl();
		return operationMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, String> createOperationToEventMap() {
		OperationToEventMapImpl operationToEventMap = new OperationToEventMapImpl();
		return operationToEventMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Map.Entry<String, String>, Map.Entry<String, String>> createOperationParameterToEventParameterMap() {
		OperationParameterToEventParameterMapImpl operationParameterToEventParameterMap = new OperationParameterToEventParameterMapImpl();
		return operationParameterToEventParameterMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, String> createParameterToParameter() {
		ParameterToParameterImpl parameterToParameter = new ParameterToParameterImpl();
		return parameterToParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationMappingPackage getOperationMappingPackage() {
		return (OperationMappingPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OperationMappingPackage getPackage() {
		return OperationMappingPackage.eINSTANCE;
	}

} //OperationMappingFactoryImpl
