/**
 */
package dk.ingi.petur.validator.operationMapping.impl;

import dk.ingi.petur.validator.operationMapping.OperationMapping;
import dk.ingi.petur.validator.operationMapping.OperationMappingPackage;

import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl#getOperation2event <em>Operation2event</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl#getOperationParameter2eventParameter <em>Operation Parameter2event Parameter</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl#getParameter2parameter <em>Parameter2parameter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationMappingImpl extends MinimalEObjectImpl.Container implements OperationMapping {
	/**
	 * The cached value of the '{@link #getOperation2event() <em>Operation2event</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation2event()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> operation2event;

	/**
	 * The cached value of the '{@link #getOperationParameter2eventParameter() <em>Operation Parameter2event Parameter</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationParameter2eventParameter()
	 * @generated
	 * @ordered
	 */
	protected EMap<Map.Entry<String, String>, Map.Entry<String, String>> operationParameter2eventParameter;

	/**
	 * The cached value of the '{@link #getParameter2parameter() <em>Parameter2parameter</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter2parameter()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> parameter2parameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationMappingPackage.Literals.OPERATION_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getOperation2event() {
		if (operation2event == null) {
			operation2event = new EcoreEMap<String,String>(OperationMappingPackage.Literals.OPERATION_TO_EVENT_MAP, OperationToEventMapImpl.class, this, OperationMappingPackage.OPERATION_MAPPING__OPERATION2EVENT);
		}
		return operation2event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Map.Entry<String, String>, Map.Entry<String, String>> getOperationParameter2eventParameter() {
		if (operationParameter2eventParameter == null) {
			operationParameter2eventParameter = new EcoreEMap<Map.Entry<String, String>,Map.Entry<String, String>>(OperationMappingPackage.Literals.OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP, OperationParameterToEventParameterMapImpl.class, this, OperationMappingPackage.OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER);
		}
		return operationParameter2eventParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getParameter2parameter() {
		if (parameter2parameter == null) {
			parameter2parameter = new EcoreEMap<String,String>(OperationMappingPackage.Literals.PARAMETER_TO_PARAMETER, ParameterToParameterImpl.class, this, OperationMappingPackage.OPERATION_MAPPING__PARAMETER2PARAMETER);
		}
		return parameter2parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION2EVENT:
				return ((InternalEList<?>)getOperation2event()).basicRemove(otherEnd, msgs);
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER:
				return ((InternalEList<?>)getOperationParameter2eventParameter()).basicRemove(otherEnd, msgs);
			case OperationMappingPackage.OPERATION_MAPPING__PARAMETER2PARAMETER:
				return ((InternalEList<?>)getParameter2parameter()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION2EVENT:
				if (coreType) return getOperation2event();
				else return getOperation2event().map();
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER:
				if (coreType) return getOperationParameter2eventParameter();
				else return getOperationParameter2eventParameter().map();
			case OperationMappingPackage.OPERATION_MAPPING__PARAMETER2PARAMETER:
				if (coreType) return getParameter2parameter();
				else return getParameter2parameter().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION2EVENT:
				((EStructuralFeature.Setting)getOperation2event()).set(newValue);
				return;
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER:
				((EStructuralFeature.Setting)getOperationParameter2eventParameter()).set(newValue);
				return;
			case OperationMappingPackage.OPERATION_MAPPING__PARAMETER2PARAMETER:
				((EStructuralFeature.Setting)getParameter2parameter()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION2EVENT:
				getOperation2event().clear();
				return;
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER:
				getOperationParameter2eventParameter().clear();
				return;
			case OperationMappingPackage.OPERATION_MAPPING__PARAMETER2PARAMETER:
				getParameter2parameter().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION2EVENT:
				return operation2event != null && !operation2event.isEmpty();
			case OperationMappingPackage.OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER:
				return operationParameter2eventParameter != null && !operationParameter2eventParameter.isEmpty();
			case OperationMappingPackage.OPERATION_MAPPING__PARAMETER2PARAMETER:
				return parameter2parameter != null && !parameter2parameter.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationMappingImpl
