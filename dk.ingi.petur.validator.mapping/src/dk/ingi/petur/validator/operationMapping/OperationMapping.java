/**
 */
package dk.ingi.petur.validator.operationMapping;

import java.util.Map;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.validator.operationMapping.OperationMapping#getOperation2event <em>Operation2event</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.operationMapping.OperationMapping#getOperationParameter2eventParameter <em>Operation Parameter2event Parameter</em>}</li>
 *   <li>{@link dk.ingi.petur.validator.operationMapping.OperationMapping#getParameter2parameter <em>Parameter2parameter</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.validator.operationMapping.OperationMappingPackage#getOperationMapping()
 * @model
 * @generated
 */
public interface OperationMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Operation2event</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation2event</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation2event</em>' map.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMappingPackage#getOperationMapping_Operation2event()
	 * @model mapType="dk.ingi.petur.validator.operationMapping.OperationToEventMap&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EMap<String, String> getOperation2event();

	/**
	 * Returns the value of the '<em><b>Operation Parameter2event Parameter</b></em>' map.
	 * The key is of type {@link java.util.Map$Entry<java.lang.String, java.lang.String>},
	 * and the value is of type {@link java.util.Map$Entry<java.lang.String, java.lang.String>},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Parameter2event Parameter</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Parameter2event Parameter</em>' map.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMappingPackage#getOperationMapping_OperationParameter2eventParameter()
	 * @model mapType="dk.ingi.petur.validator.operationMapping.OperationParameterToEventParameterMap&lt;dk.ingi.petur.validator.operationMapping.OperationToEventMap, dk.ingi.petur.validator.operationMapping.ParameterToParameter&gt;"
	 * @generated
	 */
	EMap<Map.Entry<String, String>, Map.Entry<String, String>> getOperationParameter2eventParameter();

	/**
	 * Returns the value of the '<em><b>Parameter2parameter</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter2parameter</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter2parameter</em>' map.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMappingPackage#getOperationMapping_Parameter2parameter()
	 * @model mapType="dk.ingi.petur.validator.operationMapping.ParameterToParameter&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EMap<String, String> getParameter2parameter();

} // OperationMapping
