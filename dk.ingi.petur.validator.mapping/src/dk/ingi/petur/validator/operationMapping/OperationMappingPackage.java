/**
 */
package dk.ingi.petur.validator.operationMapping;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.validator.operationMapping.OperationMappingFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface OperationMappingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operationMapping";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://petur.ingi.dk/validator/operationMapping";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dk.ingi.petur.validator.operatingMapping";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperationMappingPackage eINSTANCE = dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl <em>Operation Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getOperationMapping()
	 * @generated
	 */
	int OPERATION_MAPPING = 0;

	/**
	 * The feature id for the '<em><b>Operation2event</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_MAPPING__OPERATION2EVENT = 0;

	/**
	 * The feature id for the '<em><b>Operation Parameter2event Parameter</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Parameter2parameter</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_MAPPING__PARAMETER2PARAMETER = 2;

	/**
	 * The number of structural features of the '<em>Operation Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_MAPPING_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Operation Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_MAPPING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.operationMapping.impl.OperationToEventMapImpl <em>Operation To Event Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationToEventMapImpl
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getOperationToEventMap()
	 * @generated
	 */
	int OPERATION_TO_EVENT_MAP = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TO_EVENT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TO_EVENT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Operation To Event Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TO_EVENT_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Operation To Event Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TO_EVENT_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.operationMapping.impl.OperationParameterToEventParameterMapImpl <em>Operation Parameter To Event Parameter Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationParameterToEventParameterMapImpl
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getOperationParameterToEventParameterMap()
	 * @generated
	 */
	int OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Operation Parameter To Event Parameter Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Operation Parameter To Event Parameter Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.validator.operationMapping.impl.ParameterToParameterImpl <em>Parameter To Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.validator.operationMapping.impl.ParameterToParameterImpl
	 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getParameterToParameter()
	 * @generated
	 */
	int PARAMETER_TO_PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TO_PARAMETER__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TO_PARAMETER__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Parameter To Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TO_PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Parameter To Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TO_PARAMETER_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.validator.operationMapping.OperationMapping <em>Operation Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Mapping</em>'.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMapping
	 * @generated
	 */
	EClass getOperationMapping();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.operationMapping.OperationMapping#getOperation2event <em>Operation2event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Operation2event</em>'.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMapping#getOperation2event()
	 * @see #getOperationMapping()
	 * @generated
	 */
	EReference getOperationMapping_Operation2event();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.operationMapping.OperationMapping#getOperationParameter2eventParameter <em>Operation Parameter2event Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Operation Parameter2event Parameter</em>'.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMapping#getOperationParameter2eventParameter()
	 * @see #getOperationMapping()
	 * @generated
	 */
	EReference getOperationMapping_OperationParameter2eventParameter();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.validator.operationMapping.OperationMapping#getParameter2parameter <em>Parameter2parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Parameter2parameter</em>'.
	 * @see dk.ingi.petur.validator.operationMapping.OperationMapping#getParameter2parameter()
	 * @see #getOperationMapping()
	 * @generated
	 */
	EReference getOperationMapping_Parameter2parameter();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Operation To Event Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation To Event Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EString" valueRequired="true"
	 * @generated
	 */
	EClass getOperationToEventMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOperationToEventMap()
	 * @generated
	 */
	EAttribute getOperationToEventMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOperationToEventMap()
	 * @generated
	 */
	EAttribute getOperationToEventMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Operation Parameter To Event Parameter Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Parameter To Event Parameter Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyMapType="dk.ingi.petur.validator.operationMapping.OperationToEventMap&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 *        valueMapType="dk.ingi.petur.validator.operationMapping.ParameterToParameter&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EClass getOperationParameterToEventParameterMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOperationParameterToEventParameterMap()
	 * @generated
	 */
	EReference getOperationParameterToEventParameterMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOperationParameterToEventParameterMap()
	 * @generated
	 */
	EReference getOperationParameterToEventParameterMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Parameter To Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter To Parameter</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EString" valueRequired="true"
	 * @generated
	 */
	EClass getParameterToParameter();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getParameterToParameter()
	 * @generated
	 */
	EAttribute getParameterToParameter_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getParameterToParameter()
	 * @generated
	 */
	EAttribute getParameterToParameter_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperationMappingFactory getOperationMappingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl <em>Operation Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingImpl
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getOperationMapping()
		 * @generated
		 */
		EClass OPERATION_MAPPING = eINSTANCE.getOperationMapping();

		/**
		 * The meta object literal for the '<em><b>Operation2event</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_MAPPING__OPERATION2EVENT = eINSTANCE.getOperationMapping_Operation2event();

		/**
		 * The meta object literal for the '<em><b>Operation Parameter2event Parameter</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_MAPPING__OPERATION_PARAMETER2EVENT_PARAMETER = eINSTANCE.getOperationMapping_OperationParameter2eventParameter();

		/**
		 * The meta object literal for the '<em><b>Parameter2parameter</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_MAPPING__PARAMETER2PARAMETER = eINSTANCE.getOperationMapping_Parameter2parameter();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.operationMapping.impl.OperationToEventMapImpl <em>Operation To Event Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationToEventMapImpl
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getOperationToEventMap()
		 * @generated
		 */
		EClass OPERATION_TO_EVENT_MAP = eINSTANCE.getOperationToEventMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_TO_EVENT_MAP__KEY = eINSTANCE.getOperationToEventMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_TO_EVENT_MAP__VALUE = eINSTANCE.getOperationToEventMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.operationMapping.impl.OperationParameterToEventParameterMapImpl <em>Operation Parameter To Event Parameter Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationParameterToEventParameterMapImpl
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getOperationParameterToEventParameterMap()
		 * @generated
		 */
		EClass OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP = eINSTANCE.getOperationParameterToEventParameterMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP__KEY = eINSTANCE.getOperationParameterToEventParameterMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_PARAMETER_TO_EVENT_PARAMETER_MAP__VALUE = eINSTANCE.getOperationParameterToEventParameterMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.validator.operationMapping.impl.ParameterToParameterImpl <em>Parameter To Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.validator.operationMapping.impl.ParameterToParameterImpl
		 * @see dk.ingi.petur.validator.operationMapping.impl.OperationMappingPackageImpl#getParameterToParameter()
		 * @generated
		 */
		EClass PARAMETER_TO_PARAMETER = eINSTANCE.getParameterToParameter();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TO_PARAMETER__KEY = eINSTANCE.getParameterToParameter_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TO_PARAMETER__VALUE = eINSTANCE.getParameterToParameter_Value();

	}

} //OperationMappingPackage
