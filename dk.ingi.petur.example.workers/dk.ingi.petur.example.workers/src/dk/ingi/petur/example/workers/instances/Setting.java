package dk.ingi.petur.example.workers.instances;

import org.eclipse.emf.ecore.EObject;

import dk.dtu.imm.se.ecno.engine.ExecutionEngine;
import dk.dtu.imm.se.ecno.gui.ECNOGUI;

/**
 * A simple setting an a driver for the Workers Example's ECNO Application.
 * @author P�tur Ingi Egilsson
 * @generated NOT
 *
 */
public class Setting {

    final private emf.workersexample.WorkersFactory factory1;

	public Setting() {
		factory1 = emf.workersexample.WorkersFactory.eINSTANCE;
	}

	EObject createInstance(ExecutionEngine engine) {
        emf.workersexample.Setting o1 = factory1.createSetting();
        emf.workersexample.Worker o2 = factory1.createWorker();
        emf.workersexample.Job o3 = factory1.createJob();
        emf.workersexample.Car o4 = factory1.createCar();	

        o2.setUid(2);
        o2.setName("Dan");
        o3.setUid(3);
        o3.setName("d");
        o4.setUid(1);
        o4.setName("BMW");	
        o2.setCar(o4);	
        o1.getObjects().add(o2);
        o1.getObjects().add(o3);
        o1.getObjects().add(o4);
        o2.getAssigned().add(o3);	

        // Register the GUI objects with the controllers (objects 
		// not visible at a GUI will be added to the engine on the fly)
        engine.addElement(o2);
        engine.addElement(o3);
        engine.addElement(o4);
		return o1;
	}

    public static void main(String[] args) {
    	ExecutionEngine engine = ExecutionEngine.createNewInstance();
    	engine.addPackageAdapter(ecno.workersexample.coordination.WorkersModel.getModel(engine));
    	if (!engine.resolveNamespaceImports()) {
    		System.err.println("Package imports could not be resolved");
    		System.exit(-1);
    	}
    	new ECNOGUI(engine);
    	Setting instance = new Setting();
    	instance.createInstance(engine);
    }

}
