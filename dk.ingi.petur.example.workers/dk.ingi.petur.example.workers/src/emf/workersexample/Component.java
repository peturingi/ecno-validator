/**
 */
package emf.workersexample;

import dk.dtu.compute.se.ecno.statespace.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see emf.workersexample.WorkersPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends Element {
} // Component
