/**
 */
package emf.workersexample;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Worker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link emf.workersexample.Worker#getName <em>Name</em>}</li>
 *   <li>{@link emf.workersexample.Worker#getCar <em>Car</em>}</li>
 *   <li>{@link emf.workersexample.Worker#getAssigned <em>Assigned</em>}</li>
 * </ul>
 *
 * @see emf.workersexample.WorkersPackage#getWorker()
 * @model
 * @generated
 */
public interface Worker extends Component {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see emf.workersexample.WorkersPackage#getWorker_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link emf.workersexample.Worker#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Car</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link emf.workersexample.Car#getPassenger <em>Passenger</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Car</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Car</em>' reference.
	 * @see #setCar(Car)
	 * @see emf.workersexample.WorkersPackage#getWorker_Car()
	 * @see emf.workersexample.Car#getPassenger
	 * @model opposite="passenger" resolveProxies="false"
	 * @generated
	 */
	Car getCar();

	/**
	 * Sets the value of the '{@link emf.workersexample.Worker#getCar <em>Car</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Car</em>' reference.
	 * @see #getCar()
	 * @generated
	 */
	void setCar(Car value);

	/**
	 * Returns the value of the '<em><b>Assigned</b></em>' reference list.
	 * The list contents are of type {@link emf.workersexample.Job}.
	 * It is bidirectional and its opposite is '{@link emf.workersexample.Job#getNeeded <em>Needed</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assigned</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assigned</em>' reference list.
	 * @see emf.workersexample.WorkersPackage#getWorker_Assigned()
	 * @see emf.workersexample.Job#getNeeded
	 * @model opposite="needed" resolveProxies="false" transient="true"
	 * @generated
	 */
	EList<Job> getAssigned();

} // Worker
