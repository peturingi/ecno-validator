/**
 */
package emf.workersexample.impl;

import dk.dtu.compute.se.ecno.statespace.impl.ElementImpl;

import emf.workersexample.Component;
import emf.workersexample.WorkersPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ComponentImpl extends ElementImpl implements Component {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkersPackage.Literals.COMPONENT;
	}

} //ComponentImpl
