/**
 */
package emf.workersexample;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Car</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link emf.workersexample.Car#getName <em>Name</em>}</li>
 *   <li>{@link emf.workersexample.Car#getPassenger <em>Passenger</em>}</li>
 * </ul>
 *
 * @see emf.workersexample.WorkersPackage#getCar()
 * @model
 * @generated
 */
public interface Car extends Component {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see emf.workersexample.WorkersPackage#getCar_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link emf.workersexample.Car#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Passenger</b></em>' reference list.
	 * The list contents are of type {@link emf.workersexample.Worker}.
	 * It is bidirectional and its opposite is '{@link emf.workersexample.Worker#getCar <em>Car</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Passenger</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Passenger</em>' reference list.
	 * @see emf.workersexample.WorkersPackage#getCar_Passenger()
	 * @see emf.workersexample.Worker#getCar
	 * @model opposite="car" resolveProxies="false" transient="true"
	 * @generated
	 */
	EList<Worker> getPassenger();

} // Car
