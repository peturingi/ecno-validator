/**
 */
package dk.ingi.petur.workers;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.workers.Job#getNeeded <em>Needed</em>}</li>
 *   <li>{@link dk.ingi.petur.workers.Job#getState <em>State</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.workers.WorkersPackage#getJob()
 * @model
 * @generated
 */
public interface Job extends EObject {
	/**
	 * Returns the value of the '<em><b>Needed</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.workers.Worker}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.workers.Worker#getAssigned <em>Assigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Needed</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Needed</em>' reference list.
	 * @see dk.ingi.petur.workers.WorkersPackage#getJob_Needed()
	 * @see dk.ingi.petur.workers.Worker#getAssigned
	 * @model opposite="assigned"
	 * @generated
	 */
	EList<Worker> getNeeded();

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.ingi.petur.workers.JobStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see dk.ingi.petur.workers.JobStatus
	 * @see #setState(JobStatus)
	 * @see dk.ingi.petur.workers.WorkersPackage#getJob_State()
	 * @model
	 * @generated
	 */
	JobStatus getState();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.workers.Job#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see dk.ingi.petur.workers.JobStatus
	 * @see #getState()
	 * @generated
	 */
	void setState(JobStatus value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void doJob();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void cancelJob();

} // Job
