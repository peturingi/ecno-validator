/**
 */
package dk.ingi.petur.workers.impl;

import dk.ingi.petur.workers.Car;
import dk.ingi.petur.workers.Job;
import dk.ingi.petur.workers.Location;
import dk.ingi.petur.workers.Worker;
import dk.ingi.petur.workers.WorkersPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Worker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.workers.impl.WorkerImpl#getCar <em>Car</em>}</li>
 *   <li>{@link dk.ingi.petur.workers.impl.WorkerImpl#getAssigned <em>Assigned</em>}</li>
 *   <li>{@link dk.ingi.petur.workers.impl.WorkerImpl#getState <em>State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WorkerImpl extends MinimalEObjectImpl.Container implements Worker {
	/**
	 * The cached value of the '{@link #getCar() <em>Car</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCar()
	 * @generated
	 * @ordered
	 */
	protected Car car;

	/**
	 * The cached value of the '{@link #getAssigned() <em>Assigned</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssigned()
	 * @generated
	 * @ordered
	 */
	protected EList<Job> assigned;

	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final Location STATE_EDEFAULT = Location.HOME;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected Location state = STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkersPackage.Literals.WORKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Car getCar() {
		if (car != null && car.eIsProxy()) {
			InternalEObject oldCar = (InternalEObject)car;
			car = (Car)eResolveProxy(oldCar);
			if (car != oldCar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkersPackage.WORKER__CAR, oldCar, car));
			}
		}
		return car;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Car basicGetCar() {
		return car;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCar(Car newCar, NotificationChain msgs) {
		Car oldCar = car;
		car = newCar;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorkersPackage.WORKER__CAR, oldCar, newCar);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCar(Car newCar) {
		if (newCar != car) {
			NotificationChain msgs = null;
			if (car != null)
				msgs = ((InternalEObject)car).eInverseRemove(this, WorkersPackage.CAR__PASSENGER, Car.class, msgs);
			if (newCar != null)
				msgs = ((InternalEObject)newCar).eInverseAdd(this, WorkersPackage.CAR__PASSENGER, Car.class, msgs);
			msgs = basicSetCar(newCar, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkersPackage.WORKER__CAR, newCar, newCar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Job> getAssigned() {
		if (assigned == null) {
			assigned = new EObjectWithInverseResolvingEList.ManyInverse<Job>(Job.class, this, WorkersPackage.WORKER__ASSIGNED, WorkersPackage.JOB__NEEDED);
		}
		return assigned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(Location newState) {
		Location oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkersPackage.WORKER__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void arrive() {
		if (this.state != Location.WORK) {
			this.state = Location.WORK;
			this.car.arrive();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void depart() {
		if (this.state != Location.HOME) {
			this.state = Location.HOME;
			this.car.depart();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void doJob(Job job) {
		job.doJob();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkersPackage.WORKER__CAR:
				if (car != null)
					msgs = ((InternalEObject)car).eInverseRemove(this, WorkersPackage.CAR__PASSENGER, Car.class, msgs);
				return basicSetCar((Car)otherEnd, msgs);
			case WorkersPackage.WORKER__ASSIGNED:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAssigned()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkersPackage.WORKER__CAR:
				return basicSetCar(null, msgs);
			case WorkersPackage.WORKER__ASSIGNED:
				return ((InternalEList<?>)getAssigned()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkersPackage.WORKER__CAR:
				if (resolve) return getCar();
				return basicGetCar();
			case WorkersPackage.WORKER__ASSIGNED:
				return getAssigned();
			case WorkersPackage.WORKER__STATE:
				return getState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkersPackage.WORKER__CAR:
				setCar((Car)newValue);
				return;
			case WorkersPackage.WORKER__ASSIGNED:
				getAssigned().clear();
				getAssigned().addAll((Collection<? extends Job>)newValue);
				return;
			case WorkersPackage.WORKER__STATE:
				setState((Location)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkersPackage.WORKER__CAR:
				setCar((Car)null);
				return;
			case WorkersPackage.WORKER__ASSIGNED:
				getAssigned().clear();
				return;
			case WorkersPackage.WORKER__STATE:
				setState(STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkersPackage.WORKER__CAR:
				return car != null;
			case WorkersPackage.WORKER__ASSIGNED:
				return assigned != null && !assigned.isEmpty();
			case WorkersPackage.WORKER__STATE:
				return state != STATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkersPackage.WORKER___ARRIVE:
				arrive();
				return null;
			case WorkersPackage.WORKER___DEPART:
				depart();
				return null;
			case WorkersPackage.WORKER___DO_JOB__JOB:
				doJob((Job)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (state: ");
		result.append(state);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	public void unknown() {
		this.car.unknown();
	}

} //WorkerImpl
