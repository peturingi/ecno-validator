/**
 */
package dk.ingi.petur.workers;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.workers.WorkersFactory
 * @model kind="package"
 * @generated
 */
public interface WorkersPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workers";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://petur.ingi.dk/ECNO/workers/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workers";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkersPackage eINSTANCE = dk.ingi.petur.workers.impl.WorkersPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.ingi.petur.workers.impl.WorkerImpl <em>Worker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.workers.impl.WorkerImpl
	 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getWorker()
	 * @generated
	 */
	int WORKER = 0;

	/**
	 * The feature id for the '<em><b>Car</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER__CAR = 0;

	/**
	 * The feature id for the '<em><b>Assigned</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER__ASSIGNED = 1;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER__STATE = 2;

	/**
	 * The number of structural features of the '<em>Worker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Arrive</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER___ARRIVE = 0;

	/**
	 * The operation id for the '<em>Depart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER___DEPART = 1;

	/**
	 * The operation id for the '<em>Do Job</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER___DO_JOB__JOB = 2;

	/**
	 * The number of operations of the '<em>Worker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKER_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.workers.impl.JobImpl <em>Job</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.workers.impl.JobImpl
	 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getJob()
	 * @generated
	 */
	int JOB = 1;

	/**
	 * The feature id for the '<em><b>Needed</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__NEEDED = 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__STATE = 1;

	/**
	 * The number of structural features of the '<em>Job</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Do Job</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB___DO_JOB = 0;

	/**
	 * The operation id for the '<em>Cancel Job</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB___CANCEL_JOB = 1;

	/**
	 * The number of operations of the '<em>Job</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.workers.impl.CarImpl <em>Car</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.workers.impl.CarImpl
	 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getCar()
	 * @generated
	 */
	int CAR = 2;

	/**
	 * The feature id for the '<em><b>Passenger</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__PASSENGER = 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__STATE = 1;

	/**
	 * The number of structural features of the '<em>Car</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Arrive</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___ARRIVE = 0;

	/**
	 * The operation id for the '<em>Depart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___DEPART = 1;

	/**
	 * The number of operations of the '<em>Car</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.workers.impl.SettingImpl <em>Setting</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.workers.impl.SettingImpl
	 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getSetting()
	 * @generated
	 */
	int SETTING = 3;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTING__OBJECTS = 0;

	/**
	 * The number of structural features of the '<em>Setting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Setting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.workers.JobStatus <em>Job Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.workers.JobStatus
	 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getJobStatus()
	 * @generated
	 */
	int JOB_STATUS = 4;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.workers.Location <em>Location</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.workers.Location
	 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getLocation()
	 * @generated
	 */
	int LOCATION = 5;


	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.workers.Worker <em>Worker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Worker</em>'.
	 * @see dk.ingi.petur.workers.Worker
	 * @generated
	 */
	EClass getWorker();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.workers.Worker#getCar <em>Car</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Car</em>'.
	 * @see dk.ingi.petur.workers.Worker#getCar()
	 * @see #getWorker()
	 * @generated
	 */
	EReference getWorker_Car();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.workers.Worker#getAssigned <em>Assigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assigned</em>'.
	 * @see dk.ingi.petur.workers.Worker#getAssigned()
	 * @see #getWorker()
	 * @generated
	 */
	EReference getWorker_Assigned();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.workers.Worker#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see dk.ingi.petur.workers.Worker#getState()
	 * @see #getWorker()
	 * @generated
	 */
	EAttribute getWorker_State();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Worker#arrive() <em>Arrive</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Arrive</em>' operation.
	 * @see dk.ingi.petur.workers.Worker#arrive()
	 * @generated
	 */
	EOperation getWorker__Arrive();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Worker#depart() <em>Depart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Depart</em>' operation.
	 * @see dk.ingi.petur.workers.Worker#depart()
	 * @generated
	 */
	EOperation getWorker__Depart();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Worker#doJob(dk.ingi.petur.workers.Job) <em>Do Job</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Job</em>' operation.
	 * @see dk.ingi.petur.workers.Worker#doJob(dk.ingi.petur.workers.Job)
	 * @generated
	 */
	EOperation getWorker__DoJob__Job();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.workers.Job <em>Job</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Job</em>'.
	 * @see dk.ingi.petur.workers.Job
	 * @generated
	 */
	EClass getJob();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.workers.Job#getNeeded <em>Needed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Needed</em>'.
	 * @see dk.ingi.petur.workers.Job#getNeeded()
	 * @see #getJob()
	 * @generated
	 */
	EReference getJob_Needed();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.workers.Job#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see dk.ingi.petur.workers.Job#getState()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_State();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Job#doJob() <em>Do Job</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Job</em>' operation.
	 * @see dk.ingi.petur.workers.Job#doJob()
	 * @generated
	 */
	EOperation getJob__DoJob();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Job#cancelJob() <em>Cancel Job</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Job</em>' operation.
	 * @see dk.ingi.petur.workers.Job#cancelJob()
	 * @generated
	 */
	EOperation getJob__CancelJob();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.workers.Car <em>Car</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Car</em>'.
	 * @see dk.ingi.petur.workers.Car
	 * @generated
	 */
	EClass getCar();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.workers.Car#getPassenger <em>Passenger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Passenger</em>'.
	 * @see dk.ingi.petur.workers.Car#getPassenger()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_Passenger();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.workers.Car#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see dk.ingi.petur.workers.Car#getState()
	 * @see #getCar()
	 * @generated
	 */
	EAttribute getCar_State();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Car#arrive() <em>Arrive</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Arrive</em>' operation.
	 * @see dk.ingi.petur.workers.Car#arrive()
	 * @generated
	 */
	EOperation getCar__Arrive();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.workers.Car#depart() <em>Depart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Depart</em>' operation.
	 * @see dk.ingi.petur.workers.Car#depart()
	 * @generated
	 */
	EOperation getCar__Depart();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.workers.Setting <em>Setting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Setting</em>'.
	 * @see dk.ingi.petur.workers.Setting
	 * @generated
	 */
	EClass getSetting();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.workers.Setting#getObjects <em>Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Objects</em>'.
	 * @see dk.ingi.petur.workers.Setting#getObjects()
	 * @see #getSetting()
	 * @generated
	 */
	EReference getSetting_Objects();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.workers.JobStatus <em>Job Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Job Status</em>'.
	 * @see dk.ingi.petur.workers.JobStatus
	 * @generated
	 */
	EEnum getJobStatus();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.workers.Location <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Location</em>'.
	 * @see dk.ingi.petur.workers.Location
	 * @generated
	 */
	EEnum getLocation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkersFactory getWorkersFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.ingi.petur.workers.impl.WorkerImpl <em>Worker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.workers.impl.WorkerImpl
		 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getWorker()
		 * @generated
		 */
		EClass WORKER = eINSTANCE.getWorker();

		/**
		 * The meta object literal for the '<em><b>Car</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKER__CAR = eINSTANCE.getWorker_Car();

		/**
		 * The meta object literal for the '<em><b>Assigned</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKER__ASSIGNED = eINSTANCE.getWorker_Assigned();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORKER__STATE = eINSTANCE.getWorker_State();

		/**
		 * The meta object literal for the '<em><b>Arrive</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation WORKER___ARRIVE = eINSTANCE.getWorker__Arrive();

		/**
		 * The meta object literal for the '<em><b>Depart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation WORKER___DEPART = eINSTANCE.getWorker__Depart();

		/**
		 * The meta object literal for the '<em><b>Do Job</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation WORKER___DO_JOB__JOB = eINSTANCE.getWorker__DoJob__Job();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.workers.impl.JobImpl <em>Job</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.workers.impl.JobImpl
		 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getJob()
		 * @generated
		 */
		EClass JOB = eINSTANCE.getJob();

		/**
		 * The meta object literal for the '<em><b>Needed</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JOB__NEEDED = eINSTANCE.getJob_Needed();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__STATE = eINSTANCE.getJob_State();

		/**
		 * The meta object literal for the '<em><b>Do Job</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JOB___DO_JOB = eINSTANCE.getJob__DoJob();

		/**
		 * The meta object literal for the '<em><b>Cancel Job</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JOB___CANCEL_JOB = eINSTANCE.getJob__CancelJob();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.workers.impl.CarImpl <em>Car</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.workers.impl.CarImpl
		 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getCar()
		 * @generated
		 */
		EClass CAR = eINSTANCE.getCar();

		/**
		 * The meta object literal for the '<em><b>Passenger</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__PASSENGER = eINSTANCE.getCar_Passenger();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CAR__STATE = eINSTANCE.getCar_State();

		/**
		 * The meta object literal for the '<em><b>Arrive</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___ARRIVE = eINSTANCE.getCar__Arrive();

		/**
		 * The meta object literal for the '<em><b>Depart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___DEPART = eINSTANCE.getCar__Depart();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.workers.impl.SettingImpl <em>Setting</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.workers.impl.SettingImpl
		 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getSetting()
		 * @generated
		 */
		EClass SETTING = eINSTANCE.getSetting();

		/**
		 * The meta object literal for the '<em><b>Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SETTING__OBJECTS = eINSTANCE.getSetting_Objects();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.workers.JobStatus <em>Job Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.workers.JobStatus
		 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getJobStatus()
		 * @generated
		 */
		EEnum JOB_STATUS = eINSTANCE.getJobStatus();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.workers.Location <em>Location</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.workers.Location
		 * @see dk.ingi.petur.workers.impl.WorkersPackageImpl#getLocation()
		 * @generated
		 */
		EEnum LOCATION = eINSTANCE.getLocation();

	}

} //WorkersPackage
