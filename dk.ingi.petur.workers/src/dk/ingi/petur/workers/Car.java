/**
 */
package dk.ingi.petur.workers;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Car</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.workers.Car#getPassenger <em>Passenger</em>}</li>
 *   <li>{@link dk.ingi.petur.workers.Car#getState <em>State</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.workers.WorkersPackage#getCar()
 * @model
 * @generated
 */
public interface Car extends EObject {
	/**
	 * Returns the value of the '<em><b>Passenger</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.workers.Worker}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.workers.Worker#getCar <em>Car</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Passenger</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Passenger</em>' reference list.
	 * @see dk.ingi.petur.workers.WorkersPackage#getCar_Passenger()
	 * @see dk.ingi.petur.workers.Worker#getCar
	 * @model opposite="car"
	 * @generated
	 */
	EList<Worker> getPassenger();

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.ingi.petur.workers.Location}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see dk.ingi.petur.workers.Location
	 * @see #setState(Location)
	 * @see dk.ingi.petur.workers.WorkersPackage#getCar_State()
	 * @model
	 * @generated
	 */
	Location getState();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.workers.Car#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see dk.ingi.petur.workers.Location
	 * @see #getState()
	 * @generated
	 */
	void setState(Location value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void arrive();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void depart();
	
	/**
	 * @generated NOT
	 * Used to test the validator by a test which validates that a call to an unknown method should not be allowed.
	 */
	void unknown();

} // Car
