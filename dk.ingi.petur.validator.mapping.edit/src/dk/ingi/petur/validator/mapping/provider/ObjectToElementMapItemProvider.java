/**
 */
package dk.ingi.petur.validator.mapping.provider;


import dk.dtu.compute.se.ecno.statespace.StatespaceFactory;

import dk.dtu.imm.se.ecno.eclipse.save.behaviourstates.BehaviourstatesFactory;
import dk.ingi.petur.trace.TraceFactory;
import dk.ingi.petur.validator.mapping.MappingFactory;
import dk.ingi.petur.validator.mapping.MappingPackage;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link java.util.Map.Entry} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectToElementMapItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectToElementMapItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addKeyPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ObjectToElementMap_key_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ObjectToElementMap_key_feature", "_UI_ObjectToElementMap_type"),
				 MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ObjectToElementMap_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ObjectToElementMap_value_feature", "_UI_ObjectToElementMap_type"),
				 MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__VALUE,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY);
			childrenFeatures.add(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ObjectToElementMap.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ObjectToElementMap"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Map.Entry<?, ?> objectToElementMap = (Map.Entry<?, ?>)object;
		return "" + objectToElementMap.getKey() + " -> " + objectToElementMap.getValue();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Map.Entry.class)) {
			case MappingPackage.OBJECT_TO_ELEMENT_MAP__KEY:
			case MappingPackage.OBJECT_TO_ELEMENT_MAP__VALUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.createMapping()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.createSegment()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.create(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.create(MappingPackage.Literals.SEGMENT_TO_TRANSITION_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.create(MappingPackage.Literals.MESSAGE_TO_LINK_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.create(MappingPackage.Literals.MESSAGE_TO_EVENT_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 MappingFactory.eINSTANCE.create(MappingPackage.Literals.STATE_OBJECT_TO_ELEMENT)));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createBehaviourStates()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createDefaultState()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createPTNetState()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createInheritedBehaviourState()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createDefaultContainer()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createElementBehaviourState()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createControllerConfigurator()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 BehaviourstatesFactory.eINSTANCE.createObjectReference()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createStateSpace()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createState()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createTransition()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createStateObject()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createLink()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createEvent()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createObjectFeatureValues()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createElement()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 StatespaceFactory.eINSTANCE.createStateSpaceControllerState()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 TraceFactory.eINSTANCE.createTrace()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 TraceFactory.eINSTANCE.createLifeline()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 TraceFactory.eINSTANCE.createMessageOccurrence()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 TraceFactory.eINSTANCE.createMessage()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 TraceFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 TraceFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEClass()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEDataType()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEEnum()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEEnumLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEFactory()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEObject()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEOperation()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEPackage()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEParameter()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEReference()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.create(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createEGenericType()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY,
				 EcoreFactory.eINSTANCE.createETypeParameter()));

		newChildDescriptors.add
			(createChildParameter
				(MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__VALUE,
				 StatespaceFactory.eINSTANCE.createStateObject()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__KEY ||
			childFeature == MappingPackage.Literals.OBJECT_TO_ELEMENT_MAP__VALUE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MappingEditPlugin.INSTANCE;
	}

}
