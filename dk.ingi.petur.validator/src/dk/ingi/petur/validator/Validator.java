package dk.ingi.petur.validator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.WeakHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;

import dk.dtu.compute.se.ecno.statespace.Link;
import dk.dtu.compute.se.ecno.statespace.State;
import dk.dtu.compute.se.ecno.statespace.StateObject;
import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.dtu.compute.se.ecno.statespace.Transition;
import dk.ingi.petur.trace.Message;
import dk.ingi.petur.trace.Parameter;
import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.validator.comparators.ParameterValueEqualityComparator;
import dk.ingi.petur.validator.mapping.Mapping;
import dk.ingi.petur.validator.mapping.MappingFactory;
import dk.ingi.petur.validator.mapping.MappingPackage;
import dk.ingi.petur.validator.mapping.Segment;
import dk.ingi.petur.validator.utils.ECNOUtils;
import dk.ingi.petur.validator.utils.TraceUtils;

public class Validator {
	
	final Mapping mapping;
	final MappingFactory mappingFactory;
	final StateSpace stateSpace;
	final Trace trace;
	final BiMap<Integer, EObject> element2object = HashBiMap.create();
	State state;
	final MessageMapper messageMapper;
	
	final List<Message> eventTriggers = new LinkedList<>(); // Messages which are executed in order to trigger events.
	
	Map<List<Integer>, Map<Message, Stack<Link>>> transitionPathToGuesses = new HashMap<>();
	Map<List<Integer>, Stack<Message>> transitionPathToGuessOrder = new HashMap<>();
	
	public Mapping getMapping() {
		return this.mapping;
	}
	public StateSpace getStateSpace() {
		return this.stateSpace;
	}
	public Trace getTrace() {
		return this.trace;
	}
	
	public Validator(
			final dk.ingi.petur.trace.Trace trace,
			final StateSpace stateSpace,
			final Map<String, String> message2event,
			final Map<String, Map<String, String>> methodParameter2EventParameter
			) {
		this.trace = trace;
		this.stateSpace = stateSpace;
		this.mappingFactory = MappingPackage.eINSTANCE.getMappingFactory();
		this.mapping = mappingFactory.createMapping();
		this.mapping.setTrace(trace);
		this.mapping.setStateSpace(stateSpace);
		this.messageMapper = new MessageMapper(
				message2event,
				methodParameter2EventParameter);
		this.messageMapper.message2event.forEach((k,v) -> this.mapping.getMessage2event().put(k, v)); // Store the mapping in our mapper.
	}
	
	public void validate() {
		mapSegments();
		this.state = this.stateSpace.getStates().get(0); // Requires unit test to be set up in state 1.
		do {
			setForNextGuess();
			btMapObjects2Elements();
		} while (this.transitionPathToGuesses.isEmpty() == false);
	}
	
	public void setForNextGuess() {
		this.state = this.stateSpace.getStates().get(0); 
		this.eventTriggers.clear();
	}
	
	void mapSegments() {
		final SegmentMapper segmentMapper = new SegmentMapper(this.trace.getMessages());
		this.mapping.getSegments().addAll(segmentMapper.getSegments());
		this.targetDepth = segmentMapper.segmentCount();
	}
	
	boolean areConsistentlyMapped(final Integer key, final EObject value) {
		final boolean objectAlreadyMapped = this.element2object.containsKey(key);
		final boolean elementAlreadyMapped = this.element2object.inverse().containsKey(value);
		if ((objectAlreadyMapped || elementAlreadyMapped) && !consistentNonNullMappingExists(key, value)) {
			return false;
		}
		else {
			return true;
		}
	}
	
	boolean consistentNonNullMappingExists(final Integer key, final EObject value) {
		if ((this.element2object.get(key) != null && !this.element2object.get(key).equals(value)) || (this.element2object.inverse().get(value) != null && !this.element2object.inverse().get(value).equals(key))) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * Map each segment to a transition in the list of taken transitions.
	 */
	void mapSegmentsWithTakenTransitions() {
		if (this.takenTransitions.size() != this.mapping.getSegments().size()) {
			throw new IllegalStateException("Segments can not be mapped to transitions unless their count is the same.");
		}
		
		for (int i = 0; i < this.takenTransitions.size(); i++) {
			final Segment segment = this.mapping.getSegments().get(i);
			final Transition transition = this.takenTransitions.get(i);
			
			final Transition oldValue = this.mapping.getSegment2transition().get(segment);
			if (oldValue != null && !oldValue.equals(transition)) {
				throw new IllegalStateException(); // Segment already maps against different transition.
			}
			
			this.mapping.getSegment2transition().put(segment, transition);
		}
	}
	
	Stack<Transition> takenTransitions = new Stack<>();
	int depth() { return takenTransitions.size(); }
	String tabs() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < depth(); i++) {
			sb.append("\t");
		}
		return sb.toString();
	}
	int targetDepth;
	/**
	 * 
	 * @return true if accepted, false if not accepted.
	 */
	boolean btMapObjects2Elements() {
		if (depth() == this.targetDepth) {
			mapSegmentsWithTakenTransitions();
			mapObjectsToStateObjects();
			return true;
		}
		final Segment segment = this.mapping.getSegments().get(depth());
		final List<Transition> outTransitionsCorrectTriggerEvent = ECNOUtils.transitionsWithTriggerEventInState(this.mapping.getMessage2event().get(MessageMapper.messageKey(segment.getMessages().get(0))), this.state);
				
		boolean reachedFinalTargetState = false;
		
		// Try to move down each outgoing transition.
		transitionLoop:
		for (final Transition candidate : outTransitionsCorrectTriggerEvent) {
			this.takenTransitions.push(candidate);
			this.state = candidate.getTarget();
			final BiMap<Integer, EObject> element2ObjectJustMapped = HashBiMap.create();
			final EObject foundMessageTarget = segment.getMessages().get(0).getTarget().getLifeline().getObject(); assert(foundMessageTarget != null);
			final Integer triggerElement = candidate.getTriggerElement().getUid(); assert(triggerElement != null);
			if (!areConsistentlyMapped(triggerElement, foundMessageTarget)) {
				this.takenTransitions.pop();
				continue;
			}
			final List<Message> triggers = new LinkedList<>();
			
			final Deque<StateObject> visitedElements = new ArrayDeque<>();
			visitedElements.addFirst(candidate.getTriggerElement());
			final Deque<EObject> visitedObjects = new ArrayDeque<>();
			visitedObjects.addFirst(foundMessageTarget);
			
			map(foundMessageTarget, triggerElement);
			element2ObjectJustMapped.put(triggerElement, foundMessageTarget);
		
			/* Get all messages except found message. Found message must have been just mapped or previously mapped. */
			final Set<Integer> unmappedElements = ECNOUtils.involvedElements(candidate).stream().map(StateObject::getUid).collect(Collectors.toSet());
			
			final List<Message> messagesMatchingEvents = messagesWhichMapToEvents(segment);
			triggers.add(messagesMatchingEvents.get(0)); // The first message in any segment, is a message which triggers events.
			final List<Link> unvisitedLinks = new ArrayList<>(candidate.getLinks());
			final Map<Message, Link> message2linkmap = new WeakHashMap<>(); // Working map, might be invalid if transition can not be taken.
			for (final Message message : messagesMatchingEvents/*not including the found message*/.subList(1, messagesMatchingEvents.size())) {
				
				boolean didReturn = false;
				while (!visitedObjects.isEmpty() && !visitedObjects.peekFirst().equals(TraceUtils.messageSource(message))) {
					visitedElements.removeFirst();
					visitedObjects.removeFirst();
					didReturn = true;
				}
				if (didReturn && TraceUtils.isSelfCall(message)){
					/* We do not care about self messages which are executed just before the execution of messages for a new event type.
					 * Such messages are optional.
					 */
					triggers.add(message);
					continue; 
				}
				
				final String event = this.mapping.getMessage2event().get(MessageMapper.messageKey(message));

				List<Link> candidateLinks = getValidLinksForMessage(message, ECNOUtils.getLinkWithEventFromElementInTransition(event, visitedElements.peekFirst(), candidate));
				
				// If no link found, abort
				if (candidateLinks.isEmpty()) {
					undoTakingTransition(element2ObjectJustMapped);
					continue transitionLoop;
				} else {
					Link nextLink = null;
					
					final Stack<Integer> transitionPath = new Stack<>();
					takenTransitions.forEach(t -> transitionPath.add(t.getNumber()));
					
					if (this.transitionPathToGuesses.containsKey(transitionPath) && this.transitionPathToGuesses.get(transitionPath).containsKey(message)) { // TODO AND FOR CURRENT TRANSITION !!
						final Stack<Link> links = this.transitionPathToGuesses.get(transitionPath).get(message);
						nextLink = Iterables.getLast(links);
						if (this.transitionPathToGuessOrder.get(transitionPath).peek().equals(message) && links.size() > 1) {
							// This means we are on playback and have reached the deepest repetition.
							// Instead of changing this repetition, we choose a different link to see what happens from here.
							links.remove(Iterables.getLast(links)); // Remove last link, as it has been taken before without success.
							nextLink = Iterables.getLast(links);
						} else {
							if (this.transitionPathToGuessOrder.get(transitionPath).peek().equals(message) && links.size() == 1) {
								nextLink = links.get(0);
								links.remove(0);
								this.transitionPathToGuessOrder.remove(transitionPath);
								this.transitionPathToGuesses.remove(transitionPath);
							}
						}
					} else
					{
						if (candidateLinks.size() == 1) {
							nextLink = candidateLinks.get(0);
						} else {
	// BEGIN CHOOSE LINK
							
	//						 //FILTER
							final Object messageSender = TraceUtils.messageSource(message);
							final Object messageTarget = TraceUtils.messageTarget(message);
							final boolean messageSenderPreviouslyMapped = this.element2object.inverse().containsKey(messageSender);
							final boolean messageTargetPreviouslyMapped = this.element2object.inverse().containsKey(messageTarget);
							
							if (messageSenderPreviouslyMapped && messageTargetPreviouslyMapped) {
	//						 	candidateLinks remove all with unknown sender or receiver
								final List<Link> invalidLinksDueToUnknownSenderOrReceiver = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkSourceID = ECNOUtils.sourceOfLink(link).getUid();
									final Integer linkTargetID = ECNOUtils.targetOfLink(link).getUid();
									final boolean knownSource = this.element2object.containsKey(linkSourceID);
									final boolean knownTarget = this.element2object.containsKey(linkTargetID);
									if ((knownSource && knownTarget) == false) {
										invalidLinksDueToUnknownSenderOrReceiver.add(link);
									}
								}
	
	//						 	candidateLinks remove all with wrong sender or receiver
								final List<Link> invalidLinksDueToWrongSenderOrReceiver = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkSourceID = ECNOUtils.sourceOfLink(link).getUid();
									final Integer linkTargetID = ECNOUtils.targetOfLink(link).getUid();
									final Integer messageSourceID = this.element2object.inverse().get(messageSender);
									final Integer messageTargetID = this.element2object.inverse().get(messageTarget);
									if (linkSourceID != messageSourceID || linkTargetID != messageTargetID) {
										invalidLinksDueToWrongSenderOrReceiver.add(link);
									}
								}
								
								candidateLinks.removeAll(invalidLinksDueToUnknownSenderOrReceiver);
								candidateLinks.removeAll(invalidLinksDueToWrongSenderOrReceiver);
	//						 }
							}
							if (messageSenderPreviouslyMapped && messageTargetPreviouslyMapped == false) {
	//						 	candidateLinks remove all with unknown sender
	//						 	
								
								final List<Link> invalidLinksDueToUnknownSender = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkSourceID = ECNOUtils.sourceOfLink(link).getUid();
									final boolean knownSource = this.element2object.containsKey(linkSourceID);
									if (knownSource == false) {
										invalidLinksDueToUnknownSender.add(link);
									}
								}
	
	//							candidateLinks remove all with wrong sender
								final List<Link> invalidLinksDueToWrongSender = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkSourceID = ECNOUtils.sourceOfLink(link).getUid();
									final Integer messageSourceID = this.element2object.inverse().get(messageSender);
									if (linkSourceID != messageSourceID) {
										invalidLinksDueToWrongSender.add(link);
									}
								}
								
	//							candidateLinks remove all with known receiver
								final List<Link> invalidLinksDueToKnownReceiver = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkTargetID = ECNOUtils.targetOfLink(link).getUid();
									if (this.element2object.containsKey(linkTargetID)) {
										invalidLinksDueToKnownReceiver.add(link);
									}
								}
								
								candidateLinks.removeAll(invalidLinksDueToUnknownSender);
								candidateLinks.removeAll(invalidLinksDueToWrongSender);
								candidateLinks.removeAll(invalidLinksDueToKnownReceiver);
							}
							if (messageSenderPreviouslyMapped == false && messageTargetPreviouslyMapped) {
	//						 	candidateLinks remove all with unknown receiver
	//						 	candidateLinks remove all with wrong receiver
								final List<Link> invalidLinksDueToUnknownReceiver = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkTargetID = ECNOUtils.targetOfLink(link).getUid();
									final boolean knownTarget = this.element2object.containsKey(linkTargetID);
									if (knownTarget == false) {
										invalidLinksDueToUnknownReceiver.add(link);
									}
								}
	
	//						 	candidateLinks remove all with wrong sender or receiver
								final List<Link> invalidLinksDueToWrongReceiver = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkTargetID = ECNOUtils.targetOfLink(link).getUid();
									final Integer messageTargetID = this.element2object.inverse().get(messageTarget);
									if (linkTargetID != messageTargetID) {
										invalidLinksDueToWrongReceiver.add(link);
									}
								}
								
	//							candidateLinks remove all with known sender
								final List<Link> invalidLinksDueToKnownSender = new ArrayList<>();
								for (final Link link : candidateLinks) {
									final Integer linkSourceID = ECNOUtils.sourceOfLink(link).getUid();
									if (this.element2object.containsKey(linkSourceID)) {
										invalidLinksDueToKnownSender.add(link);
									}
								}
								
								
								candidateLinks.removeAll(invalidLinksDueToUnknownReceiver);
								candidateLinks.removeAll(invalidLinksDueToWrongReceiver);
								candidateLinks.removeAll(invalidLinksDueToKnownSender);
								
							}
							if (messageSenderPreviouslyMapped == false && messageTargetPreviouslyMapped == false) {
								final List<Link> invalidLinksDueToKnownSenderOrReceiver = new ArrayList<>();
	//						 	candidateLinks remove all with known sender or receiver
								for (final Link link : candidateLinks) {
									final Integer linkSourceID = ECNOUtils.sourceOfLink(link).getUid();
									final Integer linkTargetID = ECNOUtils.targetOfLink(link).getUid();
									final boolean knownSource = this.element2object.containsKey(linkSourceID);
									final boolean knownTarget = this.element2object.containsKey(linkTargetID);
									if (knownSource || knownTarget) {
										invalidLinksDueToKnownSenderOrReceiver.add(link);
									}
								}
								
								candidateLinks.removeAll(invalidLinksDueToKnownSenderOrReceiver);
							}
							if (candidateLinks.size() == 0) {
								undoTakingTransition(element2ObjectJustMapped);
								continue transitionLoop;
							}
							
							
							if (candidateLinks.size() > 1) {
								Map<Message, Stack<Link>> guesses = new HashMap<>();
								guesses.put(message, new Stack<>());
								
								Stack<Message> guessOrder = new Stack<>();
								guessOrder.push(message);
								
								for (int i = 0; i < candidateLinks.size(); i++) {
									guesses.get(message).push(candidateLinks.get(i));
								}
								
								nextLink = guesses.get(message).peek();
								
								if (!this.transitionPathToGuesses.containsKey(transitionPath)) {
									this.transitionPathToGuesses.put(transitionPath, guesses);
								}
								if (!this.transitionPathToGuessOrder.containsKey(transitionPath)) {
									this.transitionPathToGuessOrder.put(transitionPath, guessOrder);
								}
							}
							else
							{
								nextLink = candidateLinks.get(0);
							}
						}
					}
					
					final StateObject targetElement = ECNOUtils.targetOfLink(nextLink);
					visitedElements.addFirst(targetElement);
					final EObject targetObject = TraceUtils.messageTarget(message);
					visitedObjects.addFirst(targetObject);
					if (!areConsistentlyMapped(targetElement.getUid(), targetObject)) {
						final Stack<Message> guessOrder = this.transitionPathToGuessOrder.get(transitionPath);
						final Map<Message, Stack<Link>> guesses = this.transitionPathToGuesses.get(transitionPath);
						if (guessOrder != null && guessOrder.isEmpty() == false)
						{
							// If last guess was only guess for this message, then delete that guess so previous guess can be iterated.
							final Message lastGuessWasForThisMessage = guessOrder.peek();
							if (guesses.get(lastGuessWasForThisMessage).size() == 1) {
								guessOrder.pop();
								guesses.remove(lastGuessWasForThisMessage);
								if (guesses.isEmpty()) {
									this.transitionPathToGuesses.remove(transitionPath);
									this.transitionPathToGuessOrder.remove(transitionPath);
								}
							}
						}
						undoTakingTransition(element2ObjectJustMapped);
						continue transitionLoop;
					}
					else {
						mapIfNotPreviouslyMapped(targetElement, targetObject, element2ObjectJustMapped);
						message2linkmap.put(message, nextLink);
						unvisitedLinks.remove(nextLink);
					}
				}
// END CHOOSE LINK
				
			}// for message
	
			unmappedElements.removeAll(this.element2object.keySet());
			if (unmappedElements.isEmpty() && unvisitedLinks.isEmpty()) {
				reachedFinalTargetState = btMapObjects2Elements();
				if (reachedFinalTargetState) {
					message2linkmap.forEach((k,v) -> mapMessage2Link(k, v));
				}
				
				// Remember triggers.
				this.eventTriggers.addAll(triggers);
				
				// If a state was accepted, then do not try to find more states. (ANY semantics).
				if (reachedFinalTargetState) {
					this.transitionPathToGuesses.clear();
					this.transitionPathToGuessOrder.clear();
					return reachedFinalTargetState;
				}
			}
			else {
				undoTakingTransition(element2ObjectJustMapped);
				continue;
			}
			undoTakingTransition(element2ObjectJustMapped);
		}// for transition candidate
		return reachedFinalTargetState;
	}//btMapObjects2Elements()

	private void undoTakingTransition(final BiMap<Integer, EObject> element2ObjectJustMapped) {
		unmapAll(element2ObjectJustMapped);
		this.takenTransitions.pop();
	}
	
	/**
	 * Given a list of links and a message, return valid links from within the list.
	 */
	List<Link> getValidLinksForMessage(final Message message, final List<Link> candidateLinks) {
		final Predicate<Parameter> knownParameter = parameter -> this.messageMapper.mapsParameterFrom(message);
		final Map<String, Object> messageParameter2Value = message.getParameters().stream().filter(knownParameter).collect(Collectors.toMap(Parameter::getName, Parameter::getValueAsObject));

		final List<Link> validLinks = new ArrayList<>();
		for (final Link link : candidateLinks) {
			boolean equalParameterValues = true;
			for (final Parameter parameter : message.getParameters()) {
				if (!this.messageMapper.mapsParameterFrom(message)) continue;
				if (this.messageMapper.mapsFromMessageToEventParameter(message, parameter)) {
					final String eventParameterName = messageMapper.getParameterNameForMapping(message, parameter);
					final Object messageParameterValue = messageParameter2Value.get(parameter.getName());
					if (messageParameterValue == null)
						continue; // ignore empty parameters
					
					final dk.dtu.compute.se.ecno.statespace.Parameter eventParameter = ECNOUtils.eventParametersForLink(link).stream().filter(p -> p.getName().equals(eventParameterName.split("\\.")[0])).findAny().get();
					final Object eventParameterValue = ECNOUtils.getParameterValueAsObject(eventParameter, link);
					
					equalParameterValues = ParameterValueEqualityComparator.equalInValue(parameter.getName(), eventParameterName, messageParameterValue, eventParameterValue);
					if (!equalParameterValues)
						break;
				}
			}
			if (equalParameterValues) {
				validLinks.add(link);
			}
		}
		
		return validLinks;
	}
	
	private List<Message> messagesWhichMapToEvents(final Segment segment) {
		return segment.getMessages().stream()
				.filter(message ->
				!TraceUtils.isReturnMessage(message)
				&& this.mapping.getMessage2event().get(MessageMapper.messageKey(message)) != null
				).collect(Collectors.toList());
	}

	private void map(final EObject object, final Integer element) {
		this.element2object.put(element, object);
		this.element2object.inverse().put(object, element);
	}

	private void unmapAll(final BiMap<Integer, EObject> map) {
		map.keySet().forEach(element -> this.element2object.remove(element));
		map.values().forEach(value -> this.element2object.inverse().remove(value));
	}

	private void mapIfNotPreviouslyMapped(final StateObject element, final EObject object,
			final BiMap<Integer, EObject> map) {
		if (!this.element2object.containsKey(element.getUid())) {
			this.element2object.put(element.getUid(), object);
			this.element2object.inverse().put(object,  element.getUid());
			map.put(element.getUid(), object);
		}
	}

	private void mapObjectsToStateObjects() {
		for (final EObject object : this.element2object.inverse().keySet()) {
			final Integer elementID = this.element2object.inverse().get(object);
			final StateObject stateObject = ECNOUtils.getStateObjectWithIdInState(elementID, this.state);
			mapObjectToStateObject(object, stateObject);
		}
		
		// XXX Add state objects from all states to the list of stateobjects to which each object maps.
		// This would better be done in the algorithm itself, added here as a quick hack since I forgot to do this initially.
		for (final EObject object : this.element2object.inverse().keySet()) {
			final Integer elementID = this.element2object.inverse().get(object);
			final List<StateObject> elements = new ArrayList<>();
			final Iterator<Transition> tIter = this.takenTransitions.iterator();
			while (tIter.hasNext()) {
				final Transition t = tIter.next();
				for (StateObject so : t.getSource().getObjects()) {
					if (so.getUid() == elementID) {
						elements.add(so);
						break;
					}
				}
			}
			for (StateObject so : elements) {
				mapObjectToStateObject(object, so);
			}
		}
	}
		
	public List<Integer> getReachableStates() {
		final Segment last = Iterables.getLast(this.mapping.getSegments(), null);
		final List<Integer> result = new ArrayList<>();
		if (this.mapping.getSegment2transition().isEmpty() == false) {
			result.add(this.mapping.getSegment2transition().get(last).getTarget().getNumber());
		}
		return result;
	}
	
	void mapMessage2Link(final Message message, final Link link) {
		final Link oldValue = this.mapping.getMessage2link().get(message);
		if (oldValue != null && !oldValue.equals(link)) {
			throw new IllegalStateException(); // Contains a mapping from message to a different link.
		}
		
		this.mapping.getMessage2link().put(message, link);
	}
	
	void mapObjectToStateObject(final EObject object, final StateObject stateObject) {
		if (!this.mapping.getObject2element().containsKey(object)) {
			this.mapping.getObject2element().put(object, new BasicEList<StateObject>());
		}
		this.mapping.getObject2element().get(object).add(stateObject);
	}
	
}
