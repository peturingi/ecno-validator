package dk.ingi.petur.validator.tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLParserPool;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLParserPoolImpl;
import org.junit.Rule;
import org.junit.rules.TestName;

import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.validator.Validator;

public abstract class Test {
	StateSpace stateSpace;
	
	static ResourceSet resSet;
	
	static XMLParserPool pool = new XMLParserPoolImpl();
	static Map<URI, Resource> cache = new HashMap<>();
	
	final Map<String, String> message2event = new HashMap<>();
	final Map<String, Map<String, String>> methodParameter2EventParameter = new HashMap<>();
	
	void mapMethodParameter2EventParameter(final String message, final String messageParameter, final String eventParameter) {
		if (!methodParameter2EventParameter.containsKey(message)) {
			methodParameter2EventParameter.put(message, new HashMap<>());
		}
		methodParameter2EventParameter.get(message).put(messageParameter, eventParameter);
	}
	
	@Rule
	public TestName name = new TestName();
	
	public abstract void configureValidator();
	
	synchronized
	Validator validatorForFile(final String path) {
		final Resource traceRes = resSet.getResource(URI.createFileURI(new File(path).getAbsolutePath()), true);
		final dk.ingi.petur.trace.Trace trace = (Trace)traceRes.getContents().get(0);
		return new Validator(
				trace,
				stateSpace,
				message2event,
				methodParameter2EventParameter
				);
	}
	
	void saveTrace(final Validator validator) {
		
		// FIXME magic strings.
		final ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("mapping", new XMIResourceFactoryImpl());
		final URI fileURI = URI.createFileURI(new File("mappings/" + timeStamp() + " " + this.name.getMethodName() + ".mapping").getAbsolutePath());
		final Resource resource = resourceSet.createResource(fileURI);
		validator.getMapping().setStateSpace(validator.getStateSpace());
		validator.getMapping().setTrace(validator.getTrace());
		resource.getContents().add(validator.getMapping());

		boolean error = false;
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			error = true;
			e.printStackTrace();
		}
		if (!error) {
			System.out.println("Wrote mapping to: " + fileURI.toFileString());
		}
		if (resource.getErrors().size() > 0) {
			System.err.println(resource.getErrors());
		}
	}

	private String timeStamp() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		String formattedDate = sdf.format(date);
		return formattedDate;
	}
}
