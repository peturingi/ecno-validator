package dk.ingi.petur.validator.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.dtu.compute.se.ecno.statespace.StatespacePackage;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.trace.TracePackage;
import dk.ingi.petur.validator.Validator;

public class ValidateBanking extends dk.ingi.petur.validator.tests.Test {
	
	@Before
	public void configureValidator() {
		final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		reg.getExtensionToFactoryMap().put("trace", new XMIResourceFactoryImpl());
		reg.getExtensionToFactoryMap().put("statespace", new XMIResourceFactoryImpl());
		resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(BankingsystemPackage.eNS_URI, BankingsystemPackage.eINSTANCE); // Resolve
		resSet.getPackageRegistry().put(TracePackage.eNS_URI, TracePackage.eINSTANCE);
		resSet.getPackageRegistry().put(StatespacePackage.eNS_URI, StatespacePackage.eINSTANCE);
		
		final Resource stateSpaceRes = resSet.getResource(URI.createFileURI(new File("testTraces/config1.statespace").getAbsolutePath()), true);
//		EcoreUtil.resolveAll(resSet);
		stateSpace = (StateSpace) stateSpaceRes.getContents().get(0);
		
		/* Message mappings for Banking System */
		final String insertCard = "insertCard";
		message2event.put("PhysicalBankCardImpl;void insertCard()", insertCard);
		message2event.put("NaturalPersonImpl;InsertedCardStatus insertCard(PhysicalBankCard card)", insertCard);
		message2event.put("HWImpl;InsertedCardStatus setInserted(PhysicalBankCard newInserted)", insertCard);
		message2event.put("ATMControllerImpl;void insertCard(int cardNumber, AccountMngr issuer, int pin)", insertCard);
		//missing parameter, this.methodParameter2EventParameter.put("NaturalPersonImpl;InsertedCardStatus insertCard(PhysicalBankCard card);card", insertCard+";card");
		mapMethodParameter2EventParameter("NaturalPersonImpl;InsertedCardStatus insertCard(PhysicalBankCard card)", "card", "card");
		mapMethodParameter2EventParameter("HWImpl;InsertedCardStatus setInserted(PhysicalBankCard newInserted)", "newInserted", "card");
		//too many parameters, this.mapping.put("ATMControllerImpl;void insertCard(int cardNumber, AccountMngr issuer, int pin)", insertCard);
		mapMethodParameter2EventParameter("ATMControllerImpl;void insertCard(int cardNumber, AccountMngr issuer, int pin)", "cardNumber", "card.number");
		mapMethodParameter2EventParameter("ATMControllerImpl;void insertCard(int cardNumber, AccountMngr issuer, int pin)", "pin", "card.pin");
		
		final String enterPIN = "enterPIN";
		message2event.put("NaturalPersonImpl;InsertedCardStatus enterPin(int pin)", enterPIN);
		message2event.put("HWImpl;InsertedCardStatus enterPin(int pin)", enterPIN);
		message2event.put("ATMControllerImpl;InsertedCardStatus setPin(int enteredPin)", enterPIN);
		mapMethodParameter2EventParameter("NaturalPersonImpl;InsertedCardStatus enterPin(int pin)", "pin", "pin");
		mapMethodParameter2EventParameter("HWImpl;InsertedCardStatus enterPin(int pin)", "pin", "pin");
		mapMethodParameter2EventParameter("ATMControllerImpl;InsertedCardStatus setPin(int enteredPin)", "enteredPin", "pin");
		
		final String createSession = "createSession";
		message2event.put("BankGatewayImpl;SessionOpening openSession(int card, AccountMngr issuer, ATMController controller)", createSession);
		message2event.put("AccountMngrImpl;SessionOpening openSession(int cardNumber, ATMController controller)", createSession);
		message2event.put("BankCardImpl;void createSession()", createSession);
		message2event.put("AccountImpl;void createSession(int cardNumber)", createSession);
		mapMethodParameter2EventParameter("BankGatewayImpl;SessionOpening openSession(int card, AccountMngr issuer, ATMController controller)", "card", "card.number");
		mapMethodParameter2EventParameter("BankGatewayImpl;SessionOpening openSession(int card, AccountMngr issuer, ATMController controller)", "issuer", "issuer");
		
		final String withdraw = "withdraw";
		message2event.put("NaturalPersonImpl;WithdrawMoneyStatus withdraw(int amount)", withdraw);
		message2event.put("HWImpl;WithdrawMoneyStatus withdraw(int i)", withdraw);
		message2event.put("ATMControllerImpl;WithdrawMoneyStatus withdrawMoney(int amount)", withdraw);
		message2event.put("BankGatewayImpl;WithdrawMoneyStatus withdraw(int amount, Session session)", withdraw);
		message2event.put("AccountMngrImpl;WithdrawMoneyStatus withdraw(int amount, Session session)", withdraw);
		message2event.put("SessionImpl;void withdraw(int amount)", withdraw);
		message2event.put("AccountImpl;void withdraw(int amount)", withdraw);
		mapMethodParameter2EventParameter("NaturalPersonImpl;WithdrawMoneyStatus withdraw(int amount)", "amount", "amount");
		mapMethodParameter2EventParameter("HWImpl;WithdrawMoneyStatus withdraw(int i)", "i", "amount");
		mapMethodParameter2EventParameter("ATMControllerImpl;WithdrawMoneyStatus withdrawMoney(int amount)", "amount", "amount");
		mapMethodParameter2EventParameter("BankGatewayImpl;WithdrawMoneyStatus withdraw(int amount, Session session)", "amount", "amount");
		mapMethodParameter2EventParameter("AccountMngrImpl;WithdrawMoneyStatus withdraw(int amount, Session session)", "amount", "amount");
		mapMethodParameter2EventParameter("SessionImpl;void withdraw(int amount)", "amount", "amount");
		mapMethodParameter2EventParameter("AccountImpl;void withdraw(int amount)", "amount", "amount");
		
		final String presentMoney = "presentMoney";
		message2event.put("ATMControllerImpl;void presentMoney(Integer amount)", presentMoney);
		message2event.put("HWImpl;void presentMoney(Integer amount)", presentMoney);
		mapMethodParameter2EventParameter("ATMControllerImpl;void presentMoney(Integer amount)", "amount", "amount");
	    mapMethodParameter2EventParameter("HWImpl;void presentMoney(Integer amount)", "amount", "amount");
		
		final String takeMoney = "takeMoney";
		message2event.put("NaturalPersonImpl;void takeMoney()", takeMoney);
		message2event.put("HWImpl;EList takeMoney()", takeMoney);
		message2event.put("BankNoteImpl;void takeMoney()", takeMoney);
		message2event.put("ATMControllerImpl;void takeMoney(EList money)", takeMoney);
		mapMethodParameter2EventParameter("ATMControllerImpl;void takeMoney(EList money)", "money", "money");
		
		final String exit = "exit";
		message2event.put("NaturalPersonImpl;void exit()", exit);
		message2event.put("HWImpl;void exit()", exit);
		message2event.put("ATMControllerImpl;void exit()", exit);
		final String exitSession = "exitSession";
		message2event.put("ATMControllerImpl;void exitSession()", exitSession);
		message2event.put("BankGatewayImpl;void exitSession()", exitSession);
		message2event.put("AccountMngrImpl;void exitSession()", exitSession);
		message2event.put("SessionImpl;void exitSession()", exitSession);
		message2event.put("AccountMngrImpl;void exitSession2()", exitSession);
		final String presentCard = "presentCard";
		message2event.put("HWImpl;void presentCard()", presentCard);
		message2event.put("ATMControllerImpl;void presentCard()", presentCard);
		
		final String takeCard = "takeCard";
		message2event.put("NaturalPersonImpl;void takeCard()", takeCard);
		message2event.put("HWImpl;void takeCard()", takeCard);
		message2event.put("ATMControllerImpl;void takeCard()", takeCard);
	}
	
//	@Test
//	public void testReachableStates_noSelfCallToTriggerPresentMoney() {
//		final Validator validator = validatorForFile("testTraces/invalid/28-03-2016 17.56.48 testInsertCard_enterPin_correctPin__missingSelfCallToTriggerPresentMoney.trace");
//		validator.validate();
//		
//		
//		assertTrue(validator.getReachableStates().isEmpty());
//	}
//	
//	@Test
//	public void testReachableStates_noSelfCall_lastEventMessageMissing() {
//		final Validator validator = validatorForFile("testTraces/09-03-2016 19.27.32 testInsertCard_enterPin_withdraw1_pinIsCorrect_modified_wrong_lastEventMessageMissing.sequence");
//		validator.validate();
//		
//		final List<Integer> expected = Arrays.asList(16, 17, 26, 27, 29, 30);
//		
//		assertTrue(expected.containsAll(validator.getReachableStates()));
//		assertFalse(validator.getReachableStates().containsAll(expected));
//	}
//	
//	@Test
//	public void testReachableStates_noSelfCall_missingEventMessage() {
//		final Validator validator = validatorForFile("testTraces/09-03-2016 19.27.32 testInsertCard_enterPin_withdraw1_pinIsCorrect_modified_wrong_missingMessage.sequence");
//		validator.validate();
//		
//		final List<Integer> expected = Arrays.asList(16, 17, 26, 27, 29, 30);
//		
//		assertTrue(expected.containsAll(validator.getReachableStates()));
//		assertFalse(validator.getReachableStates().containsAll(expected));
//	}
//	
	@Test
	public void testLomCard1_enterPin0_withdraw1() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.36.08 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		
		final List<Integer> expected = Arrays.asList(29);
		
		assertTrue(expected.containsAll(validator.getReachableStates()));
		assertTrue(validator.getReachableStates().containsAll(expected));
		saveTrace(validator);
	}
	
	@Test
	public void testLomCard1_enterPin0_withdraw2() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.38.26 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		
		final List<Integer> expected = Arrays.asList(30);
		
		assertTrue(expected.containsAll(validator.getReachableStates()));
		assertTrue(validator.getReachableStates().containsAll(expected));
		saveTrace(validator);
	}
	
	@Test
	public void testLomCard1_enterPin0_withdraw2_takeMoney() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.42.08 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		
		final List<Integer> expected = Arrays.asList(58);
		System.out.println(stateSpace.hashCode());
		assertTrue(expected.containsAll(validator.getReachableStates()));
		assertTrue(validator.getReachableStates().containsAll(expected));
		saveTrace(validator);
	}
	
	
	
	@Test
	public void testLomCard1() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.27.15 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		
		final List<Integer> expected = Arrays.asList(4);
		
		assertTrue(expected.containsAll(validator.getReachableStates()));
		assertTrue(validator.getReachableStates().containsAll(expected));
		saveTrace(validator);
	}
	
	@Test
	public void testLomCard1_enterPin0() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.26.02 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		
		final List<Integer> expected = Arrays.asList(12);
		
		assertTrue(expected.containsAll(validator.getReachableStates()));
		assertTrue(validator.getReachableStates().containsAll(expected));
		saveTrace(validator);
	}
	
	@Test
	public void testLomCard2_enterPin0_withdraw2_takeMoney_ekkartCard1_enterPin0_withdraw1_takeMoney() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.30.04 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		
		final List<Integer> expected = Arrays.asList(423);
		
		assertTrue(expected.containsAll(validator.getReachableStates()));
		assertTrue(validator.getReachableStates().containsAll(expected));
		saveTrace(validator);
	}
	//
	
	@Test
	public void testLomCard1_enterPin_exit() {
		final Validator validator = validatorForFile("testTraces/09-05-2016 10.59.43 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		assertTrue(!validator.getReachableStates().isEmpty());
		saveTrace(validator);
	}
	
	@Test
	public void testLomInsertCard_enterPin_exit_takeCard_insertCardAgain() {
		
		final Validator validator = validatorForFile("testTraces/09-05-2016 11.12.00 testInsertCard_enterPin_correctPin.trace");
		validator.validate();
		

		assertTrue(validator.getReachableStates().isEmpty() == false);
		saveTrace(validator);
		
	}
	
}
