package dk.ingi.petur.validator.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.compute.se.ecno.statespace.StateSpace;
import dk.dtu.compute.se.ecno.statespace.StatespacePackage;
import dk.ingi.petur.trace.TracePackage;
import dk.ingi.petur.validator.Validator;
import dk.ingi.petur.workers.WorkersPackage;

public class ValidateWorkers extends dk.ingi.petur.validator.tests.Test {
	
	@Before
	public void configureValidator() {
		final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		reg.getExtensionToFactoryMap().put("trace", new XMIResourceFactoryImpl());
		reg.getExtensionToFactoryMap().put("statespace", new XMIResourceFactoryImpl());
		resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(WorkersPackage.eNS_URI, WorkersPackage.eINSTANCE);
		resSet.getPackageRegistry().put(TracePackage.eNS_URI, TracePackage.eINSTANCE);
		resSet.getPackageRegistry().put(StatespacePackage.eNS_URI, StatespacePackage.eINSTANCE);
		
		final Resource stateSpaceRes = resSet.getResource(URI.createFileURI(new File("workers/config1.statespace").getAbsolutePath()), true);
		stateSpace = (StateSpace) stateSpaceRes.getContents().get(0);
		
		/* Message mappings for Workers */
		message2event.put("WorkerImpl;void doJob(Job job)", "doJob");
		message2event.put("WorkerImpl;void arrive()", "arrive");
		message2event.put("WorkerImpl;void depart()", "depart");
		message2event.put("CarImpl;void arrive()", "arrive");
		message2event.put("CarImpl;void depart()", "depart");
		message2event.put("JobImpl;void doJob()", "doJob");
		message2event.put("JobImpl;void cancelJob()", "cancelJob");
		mapMethodParameter2EventParameter("WorkerImpl;void doJob(Job job)", "job", "job");
	}
	
	
	@Test
	public void testBmwDepartFromHome() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 bmwDepartFromHome.trace");
		validator.validate();
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testWorkers() {
		
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 testWorkers.trace");
		validator.validate();

		assertTrue(validator.getReachableStates().size() == 1 && validator.getReachableStates().get(0) == 245);
		saveTrace(validator);
	}
	
	@Test
	public void testBmw2xArriveDepart() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 testBmw2xArriveDepart.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 1 && validator.getReachableStates().get(0) == 1);
		saveTrace(validator);
	}
	
	@Test
	public void testVw2xArriveDepart() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.15 testVw2xArriveDepart.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 1 && validator.getReachableStates().get(0) == 1);
		saveTrace(validator);
	}
	
	@Test
	public void testDanArriveDepartArriveDoJobD() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 testDanArriveDepartArriveDoJobD.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 1 && validator.getReachableStates().get(0) == 9);
		saveTrace(validator);
	}
	
	public void testDoJobAbcdBmwIsHome() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.15 doJobAbcdBmwIsHome.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testDanDoesAcdAtWorkWhileAliBertAtHome() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 danDoesAcdAtWorkWhileAliBertAtHome.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testDanDoDAtHome() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 danDoDAtHome.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testDanAndBMWUnknownInteraction() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 danAndBMWUnknownInteraction.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testCancelJobDThenDanDoesIt() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 cancelJobDThenDanDoesIt.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testCancelCancelledJobD() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 cancelCancelledJobD.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testCancelJobD() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.15 cancelJobD.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 1);
		saveTrace(validator);
	}
	
	@Test
	public void testDoJobAbcdEveryoneAtWork() {
		final Validator validator = validatorForFile("workers/30-05-2016 08.37.27 doJobAbcdEveryoneAtWork.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 1 && validator.getReachableStates().contains(26));
		saveTrace(validator);
	}
	
	@Test
	public void testCleoDoesJobDAtWork() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 cleoDoesJobDAtWork.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testDoJobDTwoTimes() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 doJobDTwoTimes.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testBmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertDepart() {
		final Validator validator = validatorForFile("workers/28-05-2016 15.15.50 bmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertDepart.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 1 && validator.getReachableStates().get(0) == 229);
		saveTrace(validator);
	}
	
	@Test
	public void testBmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertArrive() {
		final Validator validator = validatorForFile("workers/27-05-2016 14.18.28 bmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertArrive.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
	
	@Test
	public void testEmptyTrace() {
		final Validator validator = validatorForFile("workers/26-05-2016 20.57.14 emptyTrace.trace");
		validator.validate();
		
		assertTrue(validator.getReachableStates().size() == 0);
	}
}
