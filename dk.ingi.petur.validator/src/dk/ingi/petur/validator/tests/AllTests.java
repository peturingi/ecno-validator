package dk.ingi.petur.validator.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ValidateBanking.class, ValidateWorkers.class, ValidateWorkers_danCleoSwitchedUIDs.class})
public class AllTests {

}
