package dk.ingi.petur.validator.comparators;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.compute.se.ecno.statespace.ObjectFeatureValues;
import dk.dtu.compute.se.ecno.statespace.StateObject;
import dk.dtu.compute.se.ecno.statespace.Value;

public class ParameterValueEqualityComparator {

	private static boolean equalInValue(Object messageParameterValue, Object eventParameterValue) {
		
		if (objectsOfSameType(messageParameterValue, eventParameterValue)) { // Must be either int or string.
			if (messageParameterValue instanceof Integer) {
				if (((Integer) messageParameterValue).intValue() == ((Integer) eventParameterValue).intValue()) {
					return true;
				} else {
					return false;
				}
			}
			
			if (messageParameterValue instanceof String) {
				if ((((String) messageParameterValue).equals((String) eventParameterValue))) {
					return true;
				} else {
					return false;
				}
			}
		}
		
		if (messageParameterValue instanceof EObject && eventParameterValue instanceof StateObject) {
			// TODO implement
			return true;
		} else {
			throw new ClassCastException("Cannot compare " + messageParameterValue.getClass().getName() + " against " + eventParameterValue.getClass().getName());
		}
		
	}
	
	static private void validateParameters(Object messageParameterValue, Object eventParameterValue) {
		if (!ofValidMessageParameterValueType(messageParameterValue) || !ofValidEventParameterValueType(eventParameterValue)) {
			throw new ClassCastException();
		}
	}
	
	static public boolean equalInValue(final String messageParameterName, final String eventParameterName, final Object messageParameterValue, final Object eventParameterValue) {
		validateParameters(messageParameterValue, eventParameterValue);
		final String[] eventParameterNameComponents = eventParameterName.split("\\.");

		switch (eventParameterNameComponents.length) {
		case 1:
			return equalInValue(messageParameterValue, eventParameterValue);
			
		case 2:
			boolean result = false;
			/* First component refers to the object, second component to a field within the object.
			 * An example is card.pin where card is the object and pin is a field of card.
			 * We compare the value of the field, with the value of the message parameter.
			 */
			List<ObjectFeatureValues> featureValues = ((StateObject)eventParameterValue).getFeatures();
			for (final ObjectFeatureValues ofv : featureValues) {
				final String[] uriParts = EcoreUtil.getURI(((ObjectFeatureValues)ofv).getFeature()).fragment().split("\\/");
				final String lastPart = uriParts[uriParts.length-1];
				if (lastPart.equals(eventParameterNameComponents[1])) {
					final Value value = ofv.getValues().get(0);
					if (!value.getIntValues().isEmpty() && value.getIntValues().get(0).equals(messageParameterValue)) {
						result = true;
						break;
					}
					if (!value.getStringValues().isEmpty() && value.getStringValues().get(0).equals(messageParameterValue)) {
						result = true;
						break;
					}
					if (!value.getStateValues().isEmpty() && value.getStateValues().get(0).equals(messageParameterValue)) { // Not sure if 0 or link.getNo()
						result = true;
						throw new IllegalStateException("Not implemented."); // XXX not sure this works
						//break; //Uncomment break once implemented.
					}
				}
			}
			return result;
			
			default:
				throw new IllegalStateException("Expected 1 or 2 values.");
		}
	}
	
	static private boolean objectsOfSameType(final Object a, final Object b) {
		return a.getClass().equals(b.getClass());
	}
	
	static private boolean ofValidMessageParameterValueType(final Object object) {
		return ofValidSimpleType(object) || object instanceof EObject;
	}
	
	static private boolean ofValidEventParameterValueType(final Object object) {
		return ofValidSimpleType(object) || object instanceof StateObject;
	}
	
	static private boolean ofValidSimpleType(final Object object) {
		return object instanceof Integer || object instanceof String;
	}

}
