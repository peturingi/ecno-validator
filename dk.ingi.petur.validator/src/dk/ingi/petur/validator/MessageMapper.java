package dk.ingi.petur.validator;

import java.util.HashMap;
import java.util.Map;

import dk.ingi.petur.trace.Message;
import dk.ingi.petur.trace.Parameter;
import dk.ingi.petur.validator.utils.TraceUtils;

public class MessageMapper {
	final Map<String, String> message2event;
	final Map<String, Map<String, String>> methodParameter2EventParameter;

	MessageMapper(
			final Map<String, String> message2event,
			final Map<String, Map<String, String>> methodParameter2EventParameter
			) {
		this.message2event = message2event;
		this.methodParameter2EventParameter = methodParameter2EventParameter;
	}
	
	void mapMethodParameter2EventParameter(final String message, final String messageParameter, final String eventParameter) {
		if (!this.methodParameter2EventParameter.containsKey(message)) {
			this.methodParameter2EventParameter.put(message, new HashMap<>());
		}
		this.methodParameter2EventParameter.get(message).put(messageParameter, eventParameter);
	}
	
	public boolean mapsParameterFrom(final Message message) {
		return this.methodParameter2EventParameter.containsKey(messageKey(message));
	}
	
	/**
	 * Does there exist a mapping from the given message, to the given parameter?
	 * @param message
	 * @param parameter
	 * @return
	 */
	public boolean mapsFromMessageToEventParameter(final Message message, final Parameter parameter) {
		return this.methodParameter2EventParameter.get(messageKey(message)).containsKey(parameter.getName());
	}
	
	public String getParameterNameForMapping(final Message message, final Parameter parameter) {
		return this.methodParameter2EventParameter.get(messageKey(message)).get(parameter.getName());
	}
	
	/**
	 * Returns a key for this message
	 * note: to be used when looking up an event.
	 * @param message
	 * @return
	 */
	static public String messageKey(final Message message) {
		if (message.getTarget() == null) return null;
		if (TraceUtils.messageTarget(message) == null) return null;
		final String key;
		{
			final String message_triggerObjectType = message.getTarget().getLifeline().getObject().getClass().getSimpleName();
			final String message_triggerMessage = message.getPayload();
			key = message_triggerObjectType + ';' + message_triggerMessage;
		}
		return key;
	}
}
