package dk.ingi.petur.validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import dk.ingi.petur.trace.Message;
import dk.ingi.petur.validator.mapping.MappingFactory;
import dk.ingi.petur.validator.mapping.Segment;
import dk.ingi.petur.validator.utils.TraceUtils;

public class SegmentMapper {
	final List<Message> messages;
	final List<Segment> segments;
	
	public SegmentMapper(final List<Message> messages) {
		this.messages = messages;
		segments = computeSegments();
	}
	
	List<Segment> computeSegments() {
		List<Segment> result = new ArrayList<>();
		for (int i : indicesOfFoundMessages(this.messages)) {
			final Segment segment = MappingFactory.eINSTANCE.createSegment();
			segment.getMessages().addAll(TraceUtils.nextInteraction(i, this.messages));
			result.add(segment);
		}
		return result;
	}
	
	List<Integer> indicesOfFoundMessages(final List<Message> messages) {
		final List<Integer> indices = new LinkedList<>();
		for (int i = 0; i < messages.size(); i++) {
			if (messages.get(i).getSource() == null) {
				indices.add(i);
			}
		}
		return indices;
	}
	
	public int segmentCount() {
		return this.segments.size();
	}
	
	public List<Segment> getSegments() {
		return Collections.unmodifiableList(this.segments);
	}

}
