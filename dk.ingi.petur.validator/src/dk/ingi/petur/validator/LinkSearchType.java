package dk.ingi.petur.validator;

public enum LinkSearchType {
	NORMAL, BACKTRACK, MULTIPLE
}
