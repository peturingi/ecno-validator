package dk.ingi.petur.validator.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import dk.ingi.petur.trace.Message;
import dk.ingi.petur.trace.MessageOccurrenceType;

public final class TraceUtils {
	
	/**
	 * Get all objects involved the given list of messages.
	 * @param interaction
	 * @return
	 */
	static public Set<EObject> involvedObjects(final List<Message> messages) {
		final Set<EObject> involved = new HashSet<>();
		involved.addAll(involvedSources(messages));
		involved.addAll(involvedTargets(messages));
		return involved;
	}
	
	static public Set<EObject> involvedSources(final List<Message> messages) {
		final Set<EObject> involved = new HashSet<>();
		for (final Message message : messages) {
			if (message.getSource() != null) {
				final EObject source = message.getSource().getLifeline().getObject();
				if (source != null) {
					involved.add(source);
				}
			}
		}
		return involved;
	}
	
	static public Set<EObject> involvedTargets(final List<Message> messages) {
		final Set<EObject> involved = new HashSet<>();
		for (final Message message : messages) {
			if (message.getTarget() != null) {
				final EObject target = message.getTarget().getLifeline().getObject();
				if (target != null) {
					involved.add(target);
				}
			}
		}
		return involved;
	}
	
	/**
	 * Delete all getters from the list of messages.
	 */
	static public void deleteGetters(final List<Message> messages) {
		final Iterator<Message> messageIterator = messages.iterator();
		while (messageIterator.hasNext()) {
			final Message message = messageIterator.next();
			if (isGetter(message)) {
				messageIterator.remove();
			}
		}
	}
	
	/**
	 * @param message
	 * @return true if method name begins with "get".
	 */
	static public boolean isGetter(final Message message) {
		final String[] methodSignature = message.getPayload().split(" ");
		if (methodSignature.length < 2) { // Getters always have return types, so length is 2 or more.
			return false;
		}
		else {
			final String methodName = methodSignature[1];
			return methodName.length() > 3 && methodName.substring(0,3).equals("get");
		}
	}
	
	static public EObject messageSource(final Message message) {
		return message.getSource() == null ? null : message.getSource().getLifeline().getObject();
	}
	
	static public EObject messageTarget(final Message message) {
		return message.getTarget() == null ? null : message.getTarget().getLifeline().getObject();
	}
	
	static public boolean isSelfCall(final Message message) {
		final EObject source = messageSource(message);
		final EObject target = messageTarget(message);
		return source == null ? false : source.equals(target);
	}
	
	static public boolean isEMFNotification(Message message) {
		return message.getPayload().startsWith("NotificationChain");
	}
	
	static public boolean isFoundMessage(final Message message) {
		return message.getSource() == null && message.getTarget() != null && message.getTarget().getType() == MessageOccurrenceType.RECEIVE;
	}
	
	static public boolean isReturnMessage(final Message message) {
		if (message.getSource() == null && message.getTarget() == null) {
			return false;
		}
		final boolean sentAsReturn = message.getSource() == null || message.getSource().getType() == MessageOccurrenceType.SEND_RETURN;
		final boolean receivedAsReturn = message.getTarget() == null || message.getTarget().getType() == MessageOccurrenceType.RECEIVE_RETURN;
		return sentAsReturn && receivedAsReturn;
	}
	
	/**
	 * Delete all getters from the list of messages.
	 */
	static public void deleteEMFNotifications(final List<Message> messages) {
		final Iterator<Message> messageIterator = messages.iterator();
		while (messageIterator.hasNext()) {
			final Message message = messageIterator.next();
			if (TraceUtils.isEMFNotification(message)) {
				messageIterator.remove();
			}
		}
	}
	
	static public List<Message> nextInteraction(final int fromIndex, final List<Message> messages) {
		final List<Message> interaction = new ArrayList<>();
		if (messages.size() < fromIndex) {
			return interaction;
		}
		boolean found = false;
		for (int i = fromIndex; i < messages.size(); i++) {
			final Message message = messages.get(i);
			if (TraceUtils.isFoundMessage(message)) {
				if (!found) {
					interaction.add(message);
					found = true;
				}
				else {
					break;
				}
			}
			else {
				interaction.add(message);
			}
		}
		return interaction;
	}

}
