package dk.ingi.petur.validator.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

//import org.jgrapht.DirectedGraph;
//import org.jgrapht.graph.DefaultDirectedGraph;

import com.google.common.collect.Collections2;

import dk.dtu.compute.se.ecno.statespace.Event;
import dk.dtu.compute.se.ecno.statespace.Link;
import dk.dtu.compute.se.ecno.statespace.Parameter;
import dk.dtu.compute.se.ecno.statespace.State;
import dk.dtu.compute.se.ecno.statespace.StateObject;
import dk.dtu.compute.se.ecno.statespace.Transition;

public final class ECNOUtils {
	
	static public StateObject getStateObjectWithIdInState(final Integer id, final State state) {
		Optional<StateObject> stateObject = state.getObjects().stream().filter(element -> element.getUid() == id.intValue()).findAny();
		return stateObject.orElse(null);
	}
	
	static public Set<Integer> uidOfElements(final Set<StateObject> elements) {
		final Set<Integer> uids = new HashSet<>();
		for (final StateObject object : elements) {
			uids.add(object.getUid());
		}
		return uids;
	}
	
	static public Object getParameterValueAsObject(final Parameter parameter, final Link link) {
		if (parameter == null)
			return null;
		if (!parameter.getValue().getIntValues().isEmpty()) {
			return parameter.getValue().getIntValues().get(0); // XXX not sure if 0 or link.getNo()
		}
		if (!parameter.getValue().getStringValues().isEmpty()) {
			return parameter.getValue().getStringValues().get(0); // XXX not sure if 0 or link.getNo()
		}
		if (!parameter.getValue().getStateValues().isEmpty()) {
			if (parameter.getValue().getStateValues().size() > 1) {
				throw new IllegalStateException(); // For debugging. This should never happen.
			}
			return parameter.getValue().getStateValues().get(0);
		}
		return null;
	}
	
	/**
	 * Given a list of transitions and a UID, return a list of the transitions who's trigger element has the UID.
	 * @param transitions
	 * @param UID
	 * @return
	 */
	static public List<Transition> transitionsWithTriggerElement(final List<Transition> transitions, final int UID) {
		return transitions.stream().filter(transition -> transition.getTriggerElement().getUid() == UID).collect(Collectors.toList());
	}
	
	static public List<Transition> transitionsWithTriggerEventInState(final String triggerEvent, final State state) {
		final List<Transition> result = new ArrayList<>();
		for (final Transition transition : state.getOut()) {
			if (transition.getTrigger().getName().equals(triggerEvent)) {
				result.add(transition);
			}
		}
		return result;
	}
	
	static public List<Parameter> eventParametersForLink(final Link link) {
		final List<Parameter> parameters = new ArrayList<>();
		for (final Event e : link.getEvents()) {
			parameters.addAll(e.getParameters());
		}
		return parameters;
	}
	
	/** Returns a link, with the given event, which has the given element as its source */
	static public List<Link> getLinkWithEventFromElementInTransition(final String event, final StateObject element, final Transition transition) {
		final List<Link> result = new ArrayList<>();
		if (element == null)
			return result;
		for (final Link link : transition.getLinks()) {
			boolean matchesEvent = false;
			for (final Event e : link.getEvents()) {
				if (e.getName().equals(event)) {
					matchesEvent = true;
					break; // Prevent adding many times a link with multiple events.
				}
			}
			if (matchesEvent) {
				final StateObject source = link.getFeature().getObject();
				if (source.getUid() == element.getUid()) {
					result.add(link);
				}
			}
		}
		return result;
	}
	
	static public List<Link> linksLeadingFrom(final String element, final Transition transition) {
		
		final List<Link> links = transition.getLinks().stream().filter(link -> ECNOUtils.sourceOfLink(link).equals(element)).collect(Collectors.toList());
		return links;
	}
	
//	/**
//	 * Gets the first element (the trigger element) of an event.
//	 * @param event
//	 * @return UID of first element, null if none was found.
//	 */
//	static public Integer firstElementIssuingEventInTransition(final String event, final Transition transition) {
//		DirectedGraph<Integer, Link> dg = new DefaultDirectedGraph<>(Link.class);
//		
//		// Links with matching event
//		final Set<Link> links = new HashSet<>();
//		for (final Link link : transition.getLinks()) {
//			for (final Event e : link.getEvents()) {
//				if (e.getName().equals(event)) {
//					links.add(link);
//				}
//			}
//		}
//		
//		// This is not a transition with the given event.
//		if (links.size() == 0) { //
//			return null;
//		}
//		
//		// Add source and target elements for each link, to the graph, and the link between the two.
//		for (final Link link : links) {
//			final Integer sourceUid = link.getFeature().getObject().getUid();
//			final Integer targetUid = link.getFeature().getValues().get(0/*always 0*/).getStateValues().get(link.getNo()).getUid();
//			if (!dg.containsVertex(sourceUid)) {
//				dg.addVertex(sourceUid);
//			}
//			if (!dg.containsVertex(targetUid)) {
//				dg.addVertex(targetUid);
//			}
//			dg.addEdge(sourceUid, targetUid, link);
//		}
//		
//		// Link with in-degree 0, must be the first link travelled during execution.
//
//		final ArrayList<Integer> verticesWithIndegree0 = new ArrayList<>(1);
//		for (final Integer vertex : dg.vertexSet()) {
//			if (dg.inDegreeOf(vertex) == 0) {
//				verticesWithIndegree0.add(vertex);
//			}
//		}
//		
//		// Confirm an assumption that the there is only one first element, and it does not have two or more outgoing links of the same type.
//		assert(verticesWithIndegree0.size() == 1);
//		assert(dg.outDegreeOf(verticesWithIndegree0.get(0)) == 1);
//		
//		return verticesWithIndegree0.get(0);
//	}
	
	static public String formatTransitionPath(final List<Transition> transition) {
		if (transition.isEmpty()) {
			return "";
		}
		
		final StringBuilder sb = new StringBuilder();
		sb.append("["+transition.get(0).getSource().getNumber() + "]");
		transition.forEach(t -> sb.append(" --(" + t.getNumber() + ")--> [" + t.getTarget().getNumber() +"]"));
		return sb.toString();
	}
	static public String formatTransition(final Transition transition) {
		return transition.getSource().getNumber() + " --(" + transition.getNumber() + ")-->" + " " + transition.getTarget().getNumber(); 
	}
	

	
	static public StateObject sourceOfLink(final Link link) {
		return link.getFeature().getObject();
	}
	static public StateObject targetOfLink(final Link link) {
		return link.getFeature().getValues().get(0).getStateValues().get(link.getNo());
	}
	
	 /**
	 * Get all elements involved in transition.
	 * @note These are elements which must send and receive messages in a real system.
	 * @param transition
	 * @return
	 */
	static public Set<StateObject> involvedElements(final Transition transition) {
		final Set<StateObject> involved = new HashSet<>();
		for (final Link link : transition.getLinks()) {
			final StateObject source = link.getFeature().getObject();
			involved.add(source);
			final StateObject target = link.getFeature().getValues().get(0).getStateValues().get(link.getNo());
			involved.add(target);
		}
		return involved;
	}
	
	/**
	 * Get all elements passed as arguments in transitions
	 * @return
	 */
	static public Set<StateObject> involvedArgumentElements(final Transition transition) {
		final Set<StateObject> involved = new HashSet<>();
		for (final Event event : transition.getEvents()) {
			for (final Parameter parameter : event.getParameters()) {
				if (parameter.getValue() != null) {
					final List<StateObject> argumentElements = parameter.getValue().getStateValues();
					for (StateObject argument : argumentElements) {
						involved.add(argument);
					}
				}
			}
		}
		return involved;
	}
	
	/**
	 * Get links siblings.
	 * A sibling is a link which originates from the same source, is for the same event but has a different target.
	 * @param link
	 * @param transition
	 * @return
	 */
	static public List<Link> getSiblings(final Link link, final String eventName, final Transition transition) {
		final List<Link> siblings = new ArrayList<>();
		
		for (Link l : transition.getLinks()) {
			for (Event e : l.getEvents()) {
				if (e.getName().equals(eventName) && sourceOfLink(l).equals(sourceOfLink(link))) {
					siblings.add(l);
					break;
				}
			}
		}
		siblings.remove(link);

		return siblings;
	}
	
	static public List<List<Link>> linkPermutations(final Transition transition) {
		List<List<Link>> result = new ArrayList<>();
		for (List<Link> permutation : Collections2.permutations(transition.getLinks())) {
			result.add(permutation);
		}
		return result;
	}

}
