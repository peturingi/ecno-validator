package dk.ingi.petur.tracer;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used to mark code which is to be excluded by the Tracer.
 * @author P�tur Ingi Egilsson
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AJExclude {
	// Intentionally blank.
}
