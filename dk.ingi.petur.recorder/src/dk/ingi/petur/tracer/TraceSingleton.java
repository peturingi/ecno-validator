package dk.ingi.petur.tracer;

import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.trace.TraceFactory;

public class TraceSingleton {
	static Trace trace = TraceFactory.eINSTANCE.createTrace();
	
	public static Trace getTrace() {
		return trace;
	}
	
	public static void reset() {
		trace = TraceFactory.eINSTANCE.createTrace();
	}
}
