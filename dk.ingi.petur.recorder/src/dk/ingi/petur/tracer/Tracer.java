package dk.ingi.petur.tracer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.eclipse.emf.ecore.EObject;

import dk.ingi.petur.trace.Lifeline;
import dk.ingi.petur.trace.Message;
import dk.ingi.petur.trace.MessageOccurrence;
import dk.ingi.petur.trace.MessageOccurrenceType;
import dk.ingi.petur.trace.Parameter;
import dk.ingi.petur.trace.TraceFactory;
import dk.ingi.petur.trace.Value;

public @Aspect abstract class Tracer {

	/**
	 * Execution of methods.
	 */
	public @Pointcut abstract void traceMethods();

	/**
	 * Object creation.
	 */
	public @Pointcut abstract void constructors();
	
	static int timestamp = 0;
	public static void setTimestampToZero() {
		timestamp = 0;
	}
	/**
	 * After object creation.
	 * @param thisJoinPoint
	 * @param o Object created.
	 */
	@AfterReturning ( pointcut = "constructors()", returning="o", argNames="thisJoinPoint,o")
	public void constructorsPC(JoinPoint thisJoinPoint, Object o /*object created by a constructor call*/){
		assert(thisJoinPoint.getThis() != null);
		assert(o != null);
		
		/* XXX
		 * Do not log calls to an enum because lifelines represent EObjects as specified by the Trace metamodel; an enum is not an EObject.
		 * If the Tracer is to be made to support tracing calls to enum constructors, then the metamodel must be changed such that lifelines can represent enums.
		 */
		if (thisJoinPoint.getThis() instanceof Enum) {
			System.err.println("Tracer is not designed to trace calls to enum constructors. Skipping: " + getMethodSignature(thisJoinPoint));
			return;
		}

		createLifeline((EObject)o);
	}
	
	/**
	 * Before a call to a method of EObject instance.
	 * @param thisJoinPoint
	 */
	@Before ( value="traceMethods()", argNames="thisJoinPoint" )
	public void traceMethodsPC(JoinPoint thisJoinPoint) {
		assert(thisJoinPoint.getThis() != null);
		
		/* XXX
		 * Do not trace calls to non-EObject instance methods, rather place a sentinel on the call stack.
		 * This is because non-EObjects are not representable by lifelines.
		 */
		if (!(thisJoinPoint.getThis() instanceof EObject)) {
			lastUsedLifeline.push(Optional.empty());
			return;
		}

		/** Find process, create if not found. */
		final EObject thisObject = (EObject) thisJoinPoint.getThis();
		Optional<Lifeline> lifelineJustEntered =
				isSelfCall((EObject)thisJoinPoint.getThis())
				? lastUsedLifeline.peek()
				: TraceSingleton.trace.getLifelines().stream().filter(p -> p.getObject() == thisObject).findAny();
		
		if (lifelineJustEntered.isPresent() == false) {
			lifelineJustEntered = Optional.of(createLifeline(thisObject));
		}
		
		/** Create a SEND event for the current call. */
		MessageOccurrence source = null;
		if (!lastUsedLifeline.isEmpty() && lastUsedLifeline.peek().isPresent()) {
			source = createSendMessageOccurrence(lastUsedLifeline.peek().get());
		}
		
		/** Create a RECEIVE event for the current call. */
		MessageOccurrence target = null;
		target = createReceiveMessageOccurrence(lifelineJustEntered.get());
		
		/** Create a message between the SEND event and the RECEIVE event. */
		if (!(source == null && target == null)) {
			createMessage(source, target, thisJoinPoint);
		}
		
		/** Remember process as last one to be called. */
		lastUsedLifeline.push(lifelineJustEntered);
	}
	
	@After ( value="traceMethods()", argNames="thisJoinPoint")
	public void traceMethodsPT(JoinPoint thisJoinPoint) {
		/** Create a SEND RETURN event. */
		MessageOccurrence source = null;
		if (!lastUsedLifeline.isEmpty() && lastUsedLifeline.peek().isPresent()) {
			source = createSendReturnEvent(lastUsedLifeline.peek().get());
		}
		
		/** Return. */
		lastUsedLifeline.pop();
		
		/** Create a RECEIVE RETURN event. */
		MessageOccurrence target = null;
		if (!lastUsedLifeline.isEmpty() && lastUsedLifeline.peek().isPresent()) {
			target = createReceiveReturnEvent(lastUsedLifeline.peek().get());
		}
		
		/** Create a message between SEND RETURN event and RECEIVE RETURN event. */
		if (source != null) { // Ignore return messages from unknown sources.
			createMessage(source, target, thisJoinPoint);
		}
		
	}

	/**
	 * Used to remember to which life line the previous call was made.
	 */
	static Stack<Optional<Lifeline>> lastUsedLifeline = initStack();
	static Stack<Optional<Lifeline>> initStack() {
		Stack<Optional<Lifeline>> stack = new Stack<>();
		stack.push(Optional.empty());
		return stack;
	}
	
	MessageOccurrence createSendReturnEvent(final Lifeline lifeline) {
		final MessageOccurrence messageOccurrence = createMessageOccurrence(MessageOccurrenceType.SEND_RETURN, lifeline);
		return messageOccurrence;
	}
	
	MessageOccurrence createReceiveReturnEvent(final Lifeline lifeline) {
		final MessageOccurrence messageOccurrence = createMessageOccurrence(MessageOccurrenceType.RECEIVE_RETURN, lifeline);
		return messageOccurrence;
	}
	
	MessageOccurrence createReceiveMessageOccurrence(final Lifeline lifeline) {
		final MessageOccurrence messageOccurrence = createMessageOccurrence(MessageOccurrenceType.RECEIVE, lifeline);
		return messageOccurrence;
	}
	
	MessageOccurrence createSendMessageOccurrence(final Lifeline lifeline) {
		final MessageOccurrence messageOccurrence = createMessageOccurrence(MessageOccurrenceType.SEND, lifeline);
		return messageOccurrence;
	}
	
	Message createMessage(final MessageOccurrence sender, final MessageOccurrence receiver, final JoinPoint joinPoint) {
		final Message message = TraceFactory.eINSTANCE.createMessage();
		TraceSingleton.trace.getMessages().add(message);
		message.setPayload(getMethodSignature(joinPoint));
		message.setSource(sender);
		message.setTarget(receiver);
		
		String[] paramNames = ((CodeSignature)joinPoint.getSignature()).getParameterNames();
		Object[] paramValues = joinPoint.getArgs();
		assert(paramNames.length == paramValues.length);
		for (int i = 0; i < paramNames.length; i++) {
			final Value parameterValue = TraceFactory.eINSTANCE.createValue();
			final Object value = paramValues[i];
			// XXX FIXME this code will not work for collections which are passed as parameters.
			if (value instanceof EObject) {
				parameterValue.getObjectValues().add((EObject)value);
			} else
				if (value instanceof Integer) {
					parameterValue.getIntValues().add((Integer)value);
				} else
					if (value instanceof String) {
						parameterValue.getStringValues().add((String)value);
					} else {
						// Ignore if not EObject, Integer or String (types which ECNO supports).
						printParameterError(paramNames[i], ((MethodSignature) joinPoint.getSignature()).getParameterTypes()[i].getName());
						continue;
					}
	
			final Parameter parameter = TraceFactory.eINSTANCE.createParameter();
			parameter.setValue(parameterValue);
			parameter.setName(paramNames[i]);
			message.getParameters().add(parameter);
		}
		
		
		return message;
	}
	
	void printParameterError(final String name, final String type) {
		final String formattedName = "[" + name + "]";
		final String formattedType = "[" + type + "]";
		System.err.println("Error: Ignoring parameter with name: " + formattedName + " of type: " + formattedType + '.');
	}
	
	MessageOccurrence createMessageOccurrence(final MessageOccurrenceType type, final Lifeline lifeline) {
		final MessageOccurrence messageOccurrence = TraceFactory.eINSTANCE.createMessageOccurrence();
		messageOccurrence.setTimestamp(timestamp++);
		messageOccurrence.setType(type);
		lifeline.getMessageOccurrences().add(messageOccurrence);
		return messageOccurrence;
	}
	
	Lifeline createLifeline(final EObject object) {
		final Lifeline lifeline = TraceFactory.eINSTANCE.createLifeline();
		lifeline.setObject(object);
		lifeline.setObjectClass(object.getClass().getSimpleName());
		TraceSingleton.trace.getLifelines().add(lifeline);
		return lifeline;
	}
	
	boolean isSelfCall(final EObject currentObject) {
		final StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		boolean isSelfCall = false;
		if (stackTraceElements.length > 4) {
			/* 0 is the thread, 1 is this method. */
			StackTraceElement current = stackTraceElements[3];
			StackTraceElement previous = stackTraceElements[4];

			EObject obj = lastUsedLifeline.peek().isPresent() ? lastUsedLifeline.peek().get().getObject() : null;
			final String currentStackName = current.getClassName();
			final String previousLifelineName = obj != null ? obj.getClass().getCanonicalName() : "<unknown>";
			if (obj != null && !currentStackName.equals(previousLifelineName)) {
				// lastUsedLifeline.push(Optional.empty());
			} else {
				isSelfCall = current.getClassName().equals(previous.getClassName());
				isSelfCall &= lastUsedLifeline.peek().isPresent();
				
				final EObject lastUsedObject = lastUsedLifeline.size() > 0 && lastUsedLifeline.peek().isPresent() ? lastUsedLifeline.peek().get().getObject() : null;
				isSelfCall &= (lastUsedObject != null && lastUsedObject == currentObject);
			}
		}
		return isSelfCall;
	}

	private String getMethodSignature(final JoinPoint joinPoint) {
		String messageLabel = "init()"; // Assuming we are in a constructor.
		/* Are we actually in a method? Then get the method name. */
		if ((joinPoint.getSignature() instanceof MethodSignature)) {
			final MethodSignature signature = (MethodSignature) joinPoint.getSignature();

			final String returnType = signature.getReturnType().getSimpleName();
			final String methodName = signature.getName();
			final String[] parameterNames = signature.getParameterNames();
			final List<String> parameterTypes = new ArrayList<>(signature.getParameterTypes().length);
			for (Class c : signature.getParameterTypes()) {
				parameterTypes.add(c.getSimpleName());
			}
			String[] parameterTypess = new String[parameterTypes.size()];
			parameterTypes.toArray(parameterTypess);

			messageLabel = buildMethodSignature(returnType, methodName, parameterTypess, parameterNames);
		}
		return messageLabel;
	}
	
	private String buildMethodSignature(final String returnType, final String methodName, final String[] parameterTypes,
			final String[] parameterNames) {
		final StringBuilder sb = new StringBuilder();
		sb.append(returnType);
		sb.append(" ");
		sb.append(methodName);
		sb.append("(");
		for (int i = 0; i < parameterNames.length; i++) {
			sb.append(parameterTypes[i]);
			sb.append(" ");
			sb.append(parameterNames[i]);
			if (i < parameterNames.length - 1) {
				sb.append(", ");
			}
		}
		sb.append(")");
		return sb.toString();
	}

}
