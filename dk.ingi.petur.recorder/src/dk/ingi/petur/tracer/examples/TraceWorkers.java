package dk.ingi.petur.tracer.examples;

import dk.ingi.petur.trace.State;
import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.tracer.AJExclude;
import dk.ingi.petur.tracer.TraceSingleton;
import dk.ingi.petur.workers.Car;
import dk.ingi.petur.workers.Job;
import dk.ingi.petur.workers.JobStatus;
import dk.ingi.petur.workers.Location;
import dk.ingi.petur.workers.Setting;
import dk.ingi.petur.workers.Worker;
import dk.ingi.petur.workers.WorkersFactory;
import dk.ingi.petur.workers.WorkersPackage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class TraceWorkers {
	
	public static void main(String[] args) {
		TraceWorkers t = new TraceWorkers();
		t.setup();
		t.testDanArriveDepartArriveDoJobD();
		t = new TraceWorkers();
		t.setup();
		t.cancelJobDThenDanDoesIt();
	}

	WorkersFactory factory;
	Setting sourceSetting;
	Setting targetSetting;
	Car bmw;
	Car vw;
	Job ab;
	Job abcd;
	Job acd;
	Job d;
	Worker ali;
	Worker bert;
	Worker cleo;
	Worker dan;
	
	State source;
	
	final List<EObject> targetObjects = new ArrayList<>();

	/**
	 * Setup code, in own function as to exclude it during AspectJ tracing.
	 */
	@AJExclude
	public void setup() {
		TraceSingleton.reset();
		//Tracer.resetTime();
		
		/* two-ATMs-no-timeouts, State 1 in generated state space */
		this.factory = WorkersPackage.eINSTANCE.getWorkersFactory();
		
		this.vw = factory.createCar();
		this.bmw = factory.createCar();
		
		this.ab = factory.createJob();
		this.abcd = factory.createJob();
		this.acd = factory.createJob();
		this.d = factory.createJob();
		
		this.ali = factory.createWorker();
		this.bert = factory.createWorker();
		this.cleo = factory.createWorker();
		this.dan = factory.createWorker();
		
		bmw.setState(Location.HOME);
		vw.setState(Location.HOME);
		
		ab.setState(JobStatus.CREATED);
		abcd.setState(JobStatus.CREATED);
		acd.setState(JobStatus.CREATED);
		d.setState(JobStatus.CREATED);

		ali.setState(Location.HOME);
		bert.setState(Location.HOME);
		cleo.setState(Location.HOME);
		dan.setState(Location.HOME);
		
		ali.setCar(vw);
		bert.setCar(vw);
		
		cleo.setCar(bmw);
		dan.setCar(bmw);
		
		ali.getAssigned().add(ab);
		ali.getAssigned().add(abcd);
		ali.getAssigned().add(acd);
		bert.getAssigned().add(ab);
		bert.getAssigned().add(abcd);
		cleo.getAssigned().add(abcd);
		cleo.getAssigned().add(acd);
		dan.getAssigned().add(d);
		dan.getAssigned().add(abcd);
		dan.getAssigned().add(acd);
		
		targetSetting = WorkersPackage.eINSTANCE.getWorkersFactory().createSetting();
		targetSetting.getObjects().add(bmw);
		targetSetting.getObjects().add(vw);
		targetSetting.getObjects().add(abcd);
		targetSetting.getObjects().add(ab);
		targetSetting.getObjects().add(acd);
		targetSetting.getObjects().add(d);
		targetSetting.getObjects().add(ali);
		targetSetting.getObjects().add(bert);
		targetSetting.getObjects().add(cleo);
		targetSetting.getObjects().add(dan);
		targetSetting.getObjects().add(WorkersFactory.eINSTANCE);
	
		TraceSingleton.getTrace().setSetting(targetSetting);
}	
	public void testWorkers() {
		dan.arrive();
		vw.arrive();
		dan.doJob(d);
		cleo.depart();
		//ali.arrive();
		bert.doJob(ab);
		bert.depart();
		saveTrace("testWorkers");
	}
	
	public void testBmw2xArriveDepart() {
		bmw.arrive();
		bmw.depart();
		bmw.arrive();
		bmw.depart();
		saveTrace("testBmw2xArriveDepart");
	}
	
	public void testVw2xArriveDepart() {
		vw.arrive();
		vw.depart();
		vw.arrive();
		vw.depart();
		saveTrace("testVw2xArriveDepart");
	}
	
	public void testDanArriveDepartArriveDoJobD() {
		dan.arrive();
		dan.depart();
		dan.arrive();
		dan.doJob(d);
		saveTrace("testDanArriveDepartArriveDoJobD");
	}
	

	
	public void danDoDAtHome() {
		dan.doJob(d);
		saveTrace("danDoDAtHome");
	}
	
	public void bmwDepartFromHome() {
		bmw.depart();
		saveTrace("bmwDepartFromHome");
	}
	
	public void danDoesAcdAtWorkWhileAliBertAtHome() {
		dan.arrive();
		dan.doJob(acd);
		saveTrace("danDoesAcdAtWorkWhileAliBertAtHome");
	}
	
	public void cancelJobD() {
		d.cancelJob();
		saveTrace("cancelJobD");
	}
	
	public void cancelCancelledJobD() {
		d.cancelJob();
		d.cancelJob();
		saveTrace("cancelCancelledJobD");
	}
	
	public void doJobDTwoTimes() {
		dan.arrive();
		d.doJob();
		d.doJob();
		saveTrace("doJobDTwoTimes");
	}
	
	public void doJobAbcdEveryoneAtWork() {
		vw.arrive();
		bmw.arrive();
		abcd.doJob();
		saveTrace("doJobAbcdEveryoneAtWork");
	}
	
	public void doJobAbcdBmwIsHome() {
		vw.arrive();
		abcd.doJob();
		saveTrace("doJobAbcdBmwIsHome");
	}
	
	public void cancelJobDThenDanDoesIt() {
		bmw.arrive();
		d.cancelJob();
		dan.doJob(abcd);
		saveTrace("cancelJobDThenDanDoesIt");
	}
	
	public void cleoDoesJobDAtWork() {
		cleo.arrive();
		cleo.doJob(d);
		saveTrace("cleoDoesJobDAtWork");
	}
	
	public void danAndBMWUnknownInteraction() {
		dan.unknown();
		saveTrace("danAndBMWUnknownInteraction");
	}
	
	
	
	public void emptyTrace() {
		saveTrace("emptyTrace");
	}
	
	public void bmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertDepart() {
		bmw.arrive();
		dan.depart();
		dan.arrive();
		dan.doJob(d);
		bmw.depart();
		ali.arrive();
		bmw.arrive();
		cleo.doJob(abcd);
		cleo.depart();
		bert.depart();
		saveTrace("bmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertDepart");
	}
	
	public void bmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertArrive() {
		bmw.arrive();
		dan.depart();
		dan.arrive();
		dan.doJob(d);
		bmw.depart();
		ali.arrive();
		bmw.arrive();
		cleo.doJob(abcd);
		cleo.depart();
		bert.arrive();
		saveTrace("bmwArriveDanDepartDanArriveDanDoJobDBmwDepartAliArriveBmwArriveCleoDoJobAbcdCleoDepartBertArrive");
	}
	

	@AJExclude
	private void saveTrace(final String name) {
	
		final ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		final URI fileURI = URI.createFileURI(new File("traces/" + timeStamp() + " " + name + ".trace").getAbsolutePath());
		final Resource resource = resourceSet.createResource(fileURI);
		final Trace diagram = TraceSingleton.getTrace();
		resource.getContents().add(diagram);
		

		

		boolean error = false;
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			error = true;
			e.printStackTrace();
		}
		if (!error) {
			System.out.println("Wrote trace to: " + fileURI.toFileString());
		}
		if (resource.getErrors().size() > 0) {
			System.err.println(resource.getErrors());
		}
	}

	@AJExclude
	private String timeStamp() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		String formattedDate = sdf.format(date);
		return formattedDate;
	}
	
}
