package dk.ingi.petur.tracer.examples;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.Account;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankCard;
import dk.ingi.petur.bankingsystem.BankGateway;
import dk.ingi.petur.bankingsystem.BankNote;
import dk.ingi.petur.bankingsystem.BankingsystemFactory;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Customer;
import dk.ingi.petur.bankingsystem.HW;
import dk.ingi.petur.bankingsystem.NaturalPerson;
import dk.ingi.petur.bankingsystem.PhysicalBankCard;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.Setting;
import dk.ingi.petur.trace.State;
import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.tracer.AJExclude;
import dk.ingi.petur.tracer.TraceSingleton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class TraceBankingSystem {
	
	public static void main(String[] args) {
		TraceBankingSystem t = new TraceBankingSystem();
		t.setup();
		t.trace();
	}

	BankingsystemFactory factory;
	NaturalPerson ekkart;
	NaturalPerson lom;
	PhysicalBankCard ekkartCard1;
	PhysicalBankCard lomCard1;
	PhysicalBankCard lomCard2;
	HW hw1;
	HW hw2;
	BankNote bankNote1;
	BankNote bankNote2;
	BankNote bankNote3;
	BankNote bankNote4;
	ATMController controller1;
	ATMController controller2;
	BankGateway gateway;
	AccountMngr manager;
	Customer ekkartCustomer;
	Customer lomCustomer;
	Account ekkartAccount;
	Account lomAccount;
	BankCard ekkartBankCard1;
	BankCard lomBankCard1;
	BankCard lomBankCard2;
	Session session1;
	Session session2;
	
	Setting sourceSetting;
	Setting targetSetting;
	
	State source;
	
	final List<EObject> targetObjects = new ArrayList<>();

	/**
	 * Setup code, in own function as to exclude it during AspectJ tracing.
	 */
	@AJExclude
	public void setup() {
		
		TraceSingleton.reset();
		//Tracer.resetTime();
		
		/* two-ATMs-no-timeouts, State 1 in generated state space */
		factory = BankingsystemPackage.eINSTANCE.getBankingsystemFactory();
		ekkart = factory.createNaturalPerson();
		lom = factory.createNaturalPerson();
		ekkartCard1 = factory.createPhysicalBankCard();
		lomCard1 = factory.createPhysicalBankCard();
		lomCard2 = factory.createPhysicalBankCard();
		hw1 = factory.createHW();
		hw2 = factory.createHW();
		bankNote1 = factory.createBankNote();
		bankNote2 = factory.createBankNote();
		bankNote3 = factory.createBankNote();
		bankNote4 = factory.createBankNote();
		controller1 = factory.createATMController();
		controller2 = factory.createATMController();
		gateway = factory.createBankGateway();
		manager = factory.createAccountMngr();
		ekkartCustomer = factory.createCustomer();
		lomCustomer = factory.createCustomer();
		ekkartAccount = factory.createAccount();
		lomAccount = factory.createAccount();
		ekkartBankCard1 = factory.createBankCard();
		lomBankCard1 = factory.createBankCard();
		lomBankCard2 = factory.createBankCard();
		session1 = factory.createSession();
		session2 = factory.createSession();
		
		ekkart.getAvailableCards().add(ekkartCard1);
		ekkart.getUses().add(hw1);
		ekkart.setName("Ekkart");
		
		lom.getAvailableCards().add(lomCard1);
		lom.getAvailableCards().add(lomCard2);
		lom.getUses().add(hw2);
		lom.setName("Lom");
		
		ekkartCard1.setNumber(1);
		ekkartCard1.setPin(0);
		ekkartCard1.setOwner(ekkart);
	
		lomCard1.setNumber(2);
		lomCard1.setPin(0);
		lomCard1.setOwner(lom);
		
		lomCard2.setNumber(3);
		lomCard2.setPin(0);
		lomCard2.setOwner(lom);
		
		hw1.getNotes().add(bankNote1);
		hw1.getNotes().add(bankNote2);
		hw1.setController(controller1);
		
		hw2.getNotes().add(bankNote3);
		hw2.getNotes().add(bankNote4);
		hw2.setController(controller2);
		
		controller1.setHw(hw1);
		controller1.setGateway(gateway);
		
		controller2.setHw(hw2);
		controller2.setGateway(gateway);
		
		gateway.getATMs().add(controller1);
		gateway.getATMs().add(controller2);
		gateway.getAccountMngrs().add(manager);
		
		manager.getCustomers().add(ekkartCustomer);
		manager.getCustomers().add(lomCustomer);
		manager.getAccounts().add(ekkartAccount);
		manager.getAccounts().add(lomAccount);
		manager.setGateway(gateway);
		manager.getIdleSessions().add(session1);
		manager.getIdleSessions().add(session2);
		
		ekkartCustomer.getAccounts().add(ekkartAccount);
		ekkartCustomer.setNatural(ekkart);
		ekkartCustomer.setId(2);
		ekkartCustomer.setName("Ekkart Kindler");
		
		lomCustomer.getAccounts().add(lomAccount);
		lomCustomer.setNatural(lom);
		lomCustomer.setId(0);
		lomCustomer.setName("Lom Hillah");
		
		ekkartAccount.getCards().add(ekkartBankCard1);
		ekkartAccount.getHolder().add(ekkartCustomer);
		ekkartAccount.setBalance(2);
		
		lomAccount.getCards().add(lomBankCard1);
		lomAccount.getCards().add(lomBankCard2);
		lomAccount.getHolder().add(lomCustomer);
		lomAccount.setBalance(2);
		
		ekkartBankCard1.setAccount(ekkartAccount);
		ekkartBankCard1.setOwner(ekkartCustomer);
		ekkartBankCard1.setNumber(1);
		ekkartBankCard1.setPhysical(ekkartCard1);
		
		lomBankCard1.setAccount(lomAccount);
		lomBankCard1.setOwner(lomCustomer);
		lomBankCard1.setNumber(2);
		lomBankCard1.setPhysical(lomCard1);
		
		lomBankCard2.setAccount(lomAccount);
		lomBankCard2.setOwner(lomCustomer);
		lomBankCard2.setNumber(3);
		lomBankCard2.setPhysical(lomCard2);
		
		
		targetSetting = BankingsystemPackage.eINSTANCE.getBankingsystemFactory().createSetting();
		// Natural Persons
		targetSetting.getObjects().add(ekkart);
		targetSetting.getObjects().add(lom);
		// HW
		targetSetting.getObjects().add(hw1);
		targetSetting.getObjects().add(hw2);
		// ATM Controllers
		targetSetting.getObjects().add(controller1);
		targetSetting.getObjects().add(controller2);
		// Gateways
		targetSetting.getObjects().add(gateway);
		// Swift Networks
		// Account Managers
		targetSetting.getObjects().add(manager);
		// Factory itself, which can be called during recording.
		targetSetting.getObjects().add(BankingsystemFactory.eINSTANCE);
		
		/* Conceptual associations (gray in draft paper) */
		ekkartCard1.setIssuer(manager);
		lomCard1.setIssuer(manager);
		lomCard2.setIssuer(manager);
		
		hw1.configureShowCardOn1InvalidPin();
		hw2.configureShowCardOn1InvalidPin();
		
	
		//EcoreUtil.Copier copier = new EcoreUtil.Copier();
		//this.source.getObject().addAll(copier.copyAll(targetSetting.getObjects()));
		TraceSingleton.getTrace().setSetting(targetSetting);
}

	
	public void trace(){
		try {
			lomCard1.insertCard();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		saveTrace("lomCard1Inserted");
	}

	@AJExclude
	private void saveTrace(final String name) {
	
		final ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		final URI fileURI = URI.createFileURI(new File("traces/" + timeStamp() + " " + name + ".trace").getAbsolutePath());
		final Resource resource = resourceSet.createResource(fileURI);
		final Trace diagram = TraceSingleton.getTrace();
		resource.getContents().add(diagram);
		

		

		boolean error = false;
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			error = true;
			e.printStackTrace();
		}
		if (!error) {
			System.out.println("Wrote trace to: " + fileURI.toFileString());
		}
		if (resource.getErrors().size() > 0) {
			System.err.println(resource.getErrors());
		}
	}

	@AJExclude
	private String timeStamp() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		String formattedDate = sdf.format(date);
		return formattedDate;
	}
	
}
