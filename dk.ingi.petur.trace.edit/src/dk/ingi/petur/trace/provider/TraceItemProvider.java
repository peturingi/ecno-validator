/**
 */
package dk.ingi.petur.trace.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import dk.ingi.petur.trace.Trace;
import dk.ingi.petur.trace.TraceFactory;
import dk.ingi.petur.trace.TracePackage;

/**
 * This is the item provider adapter for a {@link dk.ingi.petur.trace.Trace} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TraceItemProvider 
	extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TraceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TracePackage.Literals.TRACE__LIFELINES);
			childrenFeatures.add(TracePackage.Literals.TRACE__SETTING);
			childrenFeatures.add(TracePackage.Literals.TRACE__MESSAGES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Trace.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Trace"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Trace)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Trace_type") :
			getString("_UI_Trace_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Trace.class)) {
			case TracePackage.TRACE__LIFELINES:
			case TracePackage.TRACE__SETTING:
			case TracePackage.TRACE__MESSAGES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__LIFELINES,
				 TraceFactory.eINSTANCE.createLifeline()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createNamedElement()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createTrace()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createLifeline()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createMessageOccurrence()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createMessage()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__SETTING,
				 TraceFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(TracePackage.Literals.TRACE__MESSAGES,
				 TraceFactory.eINSTANCE.createMessage()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == TracePackage.Literals.TRACE__LIFELINES ||
			childFeature == TracePackage.Literals.TRACE__SETTING ||
			childFeature == TracePackage.Literals.TRACE__MESSAGES;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
