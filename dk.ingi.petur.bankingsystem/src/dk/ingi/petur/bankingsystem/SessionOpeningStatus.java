/**
 */
package dk.ingi.petur.bankingsystem;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Session Opening Status</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionOpeningStatus()
 * @model
 * @generated
 */
public enum SessionOpeningStatus implements Enumerator {
	/**
	 * The '<em><b>SESSION OPENED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SESSION_OPENED_VALUE
	 * @generated
	 * @ordered
	 */
	SESSION_OPENED(0, "SESSION_OPENED", "SESSION_OPENED"),

	/**
	 * The '<em><b>UNKNOWN CUSTOMER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNKNOWN_CUSTOMER_VALUE
	 * @generated
	 * @ordered
	 */
	UNKNOWN_CUSTOMER(1, "UNKNOWN_CUSTOMER", "UNKNOWN_CUSTOMER"),

	/**
	 * The '<em><b>SESSION ERROR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SESSION_ERROR_VALUE
	 * @generated
	 * @ordered
	 */
	SESSION_ERROR(2, "SESSION_ERROR", "SESSION_ERROR"),

	/**
	 * The '<em><b>UNAUTHORIZED ATM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNAUTHORIZED_ATM_VALUE
	 * @generated
	 * @ordered
	 */
	UNAUTHORIZED_ATM(3, "UNAUTHORIZED_ATM", "UNAUTHORIZED_ATM");

	/**
	 * The '<em><b>SESSION OPENED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SESSION OPENED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SESSION_OPENED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SESSION_OPENED_VALUE = 0;

	/**
	 * The '<em><b>UNKNOWN CUSTOMER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNKNOWN CUSTOMER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNKNOWN_CUSTOMER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNKNOWN_CUSTOMER_VALUE = 1;

	/**
	 * The '<em><b>SESSION ERROR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SESSION ERROR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SESSION_ERROR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SESSION_ERROR_VALUE = 2;

	/**
	 * The '<em><b>UNAUTHORIZED ATM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNAUTHORIZED ATM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNAUTHORIZED_ATM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNAUTHORIZED_ATM_VALUE = 3;

	/**
	 * An array of all the '<em><b>Session Opening Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SessionOpeningStatus[] VALUES_ARRAY =
		new SessionOpeningStatus[] {
			SESSION_OPENED,
			UNKNOWN_CUSTOMER,
			SESSION_ERROR,
			UNAUTHORIZED_ATM,
		};

	/**
	 * A public read-only list of all the '<em><b>Session Opening Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SessionOpeningStatus> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Session Opening Status</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SessionOpeningStatus get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SessionOpeningStatus result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Session Opening Status</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SessionOpeningStatus getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SessionOpeningStatus result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Session Opening Status</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SessionOpeningStatus get(int value) {
		switch (value) {
			case SESSION_OPENED_VALUE: return SESSION_OPENED;
			case UNKNOWN_CUSTOMER_VALUE: return UNKNOWN_CUSTOMER;
			case SESSION_ERROR_VALUE: return SESSION_ERROR;
			case UNAUTHORIZED_ATM_VALUE: return UNAUTHORIZED_ATM;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SessionOpeningStatus(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SessionOpeningStatus
