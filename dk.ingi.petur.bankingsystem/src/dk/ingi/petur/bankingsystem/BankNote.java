/**
 */
package dk.ingi.petur.bankingsystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bank Note</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankNote()
 * @model
 * @generated
 */
public interface BankNote extends Component {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void takeMoney();
} // BankNote
