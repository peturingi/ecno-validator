/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Account Mngr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getCustomers <em>Customers</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getAccounts <em>Accounts</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getGateway <em>Gateway</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getSessions <em>Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getIdleSessions <em>Idle Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getName <em>Name</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getOpenedSessions <em>Opened Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.AccountMngr#getWithdrawls <em>Withdrawls</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr()
 * @model
 * @generated
 */
public interface AccountMngr extends Component {
	/**
	 * Returns the value of the '<em><b>Customers</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.Customer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Customers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customers</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_Customers()
	 * @model containment="true"
	 * @generated
	 */
	EList<Customer> getCustomers();

	/**
	 * Returns the value of the '<em><b>Accounts</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.Account}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accounts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accounts</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_Accounts()
	 * @model containment="true"
	 * @generated
	 */
	EList<Account> getAccounts();

	/**
	 * Returns the value of the '<em><b>Gateway</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.BankGateway#getAccountMngrs <em>Account Mngrs</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gateway</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gateway</em>' reference.
	 * @see #setGateway(BankGateway)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_Gateway()
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getAccountMngrs
	 * @model opposite="accountMngrs" required="true"
	 * @generated
	 */
	BankGateway getGateway();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.AccountMngr#getGateway <em>Gateway</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gateway</em>' reference.
	 * @see #getGateway()
	 * @generated
	 */
	void setGateway(BankGateway value);

	/**
	 * Returns the value of the '<em><b>Sessions</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.Session}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.Session#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sessions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sessions</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_Sessions()
	 * @see dk.ingi.petur.bankingsystem.Session#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Session> getSessions();

	/**
	 * Returns the value of the '<em><b>Idle Sessions</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.Session}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Idle Sessions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Idle Sessions</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_IdleSessions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Session> getIdleSessions();
	
	/**
	 * @generated NOT
	 * @param name
	 */
	void setName(String name);
	
	/**
	 * Returns the value of the '<em><b>Opened Sessions</b></em>' map.
	 * The key is of type {@link dk.ingi.petur.bankingsystem.Session},
	 * and the value is of type {@link dk.ingi.petur.bankingsystem.ATMController},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opened Sessions</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opened Sessions</em>' map.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_OpenedSessions()
	 * @model mapType="dk.ingi.petur.bankingsystem.SessionToATMControllerMap<dk.ingi.petur.bankingsystem.Session, dk.ingi.petur.bankingsystem.ATMController>"
	 * @generated
	 */
	EMap<Session, ATMController> getOpenedSessions();

	/**
	 * Returns the value of the '<em><b>Withdrawls</b></em>' map.
	 * The key is of type {@link dk.ingi.petur.bankingsystem.Session},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Withdrawls</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Withdrawls</em>' map.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccountMngr_Withdrawls()
	 * @model mapType="dk.ingi.petur.bankingsystem.SessionToIntegerMap<dk.ingi.petur.bankingsystem.Session, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<Session, Integer> getWithdrawls();

	/**
	 * @generated NOT
	 * @return
	 */
	String getName();
	
	/**
	 * @generated NOT
	 * @param cardNumber
	 * @param controller 
	 * @return
	 */
	SessionOpening openSession(int cardNumber, ATMController controller);
	
	/**
	 * @generated NOT
	 * @param amount
	 * @param session
	 * @return
	 */
	WithdrawMoneyStatus withdraw(int amount, Session session);
	/**
	 * @generated NOT
	 * @param s
	 * @return
	 */
	SessionTransactionState cancelSession(Session s);
	/**
	 * @generated NOT
	 * @param s
	 * @return
	 */
	SessionTransactionState commitSession(Session s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exitSession();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exitSession2();


} // AccountMngr
