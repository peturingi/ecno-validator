/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.Customer#getAccounts <em>Accounts</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.Customer#getNatural <em>Natural</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.Customer#getId <em>Id</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.Customer#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getCustomer()
 * @model
 * @generated
 */
public interface Customer extends Component {
	/**
	 * Returns the value of the '<em><b>Accounts</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.Account}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.Account#getHolder <em>Holder</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accounts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accounts</em>' reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getCustomer_Accounts()
	 * @see dk.ingi.petur.bankingsystem.Account#getHolder
	 * @model opposite="holder"
	 * @generated
	 */
	EList<Account> getAccounts();

	/**
	 * Returns the value of the '<em><b>Natural</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Natural</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Natural</em>' reference.
	 * @see #setNatural(NaturalPerson)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getCustomer_Natural()
	 * @model
	 * @generated
	 */
	NaturalPerson getNatural();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.Customer#getNatural <em>Natural</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Natural</em>' reference.
	 * @see #getNatural()
	 * @generated
	 */
	void setNatural(NaturalPerson value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getCustomer_Id()
	 * @model
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.Customer#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getCustomer_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.Customer#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Customer
