/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends EObject {
} // Component
