/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;

import dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException;
import dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Natural Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.NaturalPerson#getAvailableCards <em>Available Cards</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.NaturalPerson#getUses <em>Uses</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.NaturalPerson#getOwns <em>Owns</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.NaturalPerson#getName <em>Name</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.NaturalPerson#isTakeMoney <em>Take Money</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getNaturalPerson()
 * @model
 * @generated
 */
public interface NaturalPerson extends Component {
	/**
	 * Returns the value of the '<em><b>Available Cards</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.PhysicalBankCard}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available Cards</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Cards</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getNaturalPerson_AvailableCards()
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<PhysicalBankCard> getAvailableCards();

	/**
	 * Returns the value of the '<em><b>Uses</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.HW}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uses</em>' reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getNaturalPerson_Uses()
	 * @model
	 * @generated
	 */
	EList<HW> getUses();

	/**
	 * Returns the value of the '<em><b>Owns</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.BankNote}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owns</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owns</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getNaturalPerson_Owns()
	 * @model containment="true"
	 * @generated
	 */
	EList<BankNote> getOwns();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getNaturalPerson_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Take Money</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Take Money</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Take Money</em>' attribute.
	 * @see #setTakeMoney(boolean)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getNaturalPerson_TakeMoney()
	 * @model
	 * @generated
	 */
	boolean isTakeMoney();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#isTakeMoney <em>Take Money</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Take Money</em>' attribute.
	 * @see #isTakeMoney()
	 * @generated
	 */
	void setTakeMoney(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="dk.ingi.petur.bankingsystem.InsertedCardStatus" exceptions="dk.ingi.petur.bankingsystem.NotInFrontOfATMHardwareException dk.ingi.petur.bankingsystem.NotMyPhysicalCardException"
	 * @generated
	 */
	InsertedCardStatus insertCard(PhysicalBankCard card) throws NotInFrontOfATMHardwareException, NotMyPhysicalCardException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="dk.ingi.petur.bankingsystem.InsertedCardStatus"
	 * @generated
	 */
	InsertedCardStatus enterPin(int pin);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	WithdrawMoneyStatus withdraw(int amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void configureTakeMoney(boolean b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void takeCard();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void takeMoney();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exit();

} // NaturalPerson
