/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Account</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.Account#getCards <em>Cards</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.Account#getBalance <em>Balance</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.Account#getHolder <em>Holder</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccount()
 * @model
 * @generated
 */
public interface Account extends Component {
	/**
	 * Returns the value of the '<em><b>Cards</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.BankCard}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.BankCard#getAccount <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cards</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cards</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccount_Cards()
	 * @see dk.ingi.petur.bankingsystem.BankCard#getAccount
	 * @model opposite="account" containment="true"
	 * @generated
	 */
	EList<BankCard> getCards();

	/**
	 * Returns the value of the '<em><b>Balance</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Balance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Balance</em>' attribute.
	 * @see #setBalance(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccount_Balance()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getBalance();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.Account#getBalance <em>Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Balance</em>' attribute.
	 * @see #getBalance()
	 * @generated
	 */
	void setBalance(int value);

	/**
	 * Returns the value of the '<em><b>Holder</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.Customer}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.Customer#getAccounts <em>Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Holder</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Holder</em>' reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getAccount_Holder()
	 * @see dk.ingi.petur.bankingsystem.Customer#getAccounts
	 * @model opposite="accounts"
	 * @generated
	 */
	EList<Customer> getHolder();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isAccountFor(int physicalCardNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	BankCard getBankCard(int physicalCardNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void withdraw(int amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void createSession(int cardNumber);

} // Account
