/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.bankingsystem.BankingsystemFactory
 * @model kind="package"
 * @generated
 */
public interface BankingsystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bankingsystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://petur.ingi.dk/ECNO/bankingsystem/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dk.ingi.petur.bankingsystem";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BankingsystemPackage eINSTANCE = dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.ComponentImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 1;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl <em>Natural Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getNaturalPerson()
	 * @generated
	 */
	int NATURAL_PERSON = 0;

	/**
	 * The feature id for the '<em><b>Available Cards</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON__AVAILABLE_CARDS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON__USES = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON__OWNS = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON__NAME = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Take Money</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON__TAKE_MONEY = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Natural Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Insert Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___INSERT_CARD__PHYSICALBANKCARD = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Enter Pin</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___ENTER_PIN__INT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___WITHDRAW__INT = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Configure Take Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___CONFIGURE_TAKE_MONEY__BOOLEAN = COMPONENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Take Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___TAKE_CARD = COMPONENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Take Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___TAKE_MONEY = COMPONENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Exit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON___EXIT = COMPONENT_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>Natural Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURAL_PERSON_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.PhysicalBankCardImpl <em>Physical Bank Card</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.PhysicalBankCardImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getPhysicalBankCard()
	 * @generated
	 */
	int PHYSICAL_BANK_CARD = 2;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD__NUMBER = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD__PIN = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Issuer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD__ISSUER = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD__OWNER = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Physical Bank Card</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Insert Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD___INSERT_CARD = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Physical Bank Card</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BANK_CARD_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl <em>Account Mngr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.AccountMngrImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getAccountMngr()
	 * @generated
	 */
	int ACCOUNT_MNGR = 3;

	/**
	 * The feature id for the '<em><b>Customers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__CUSTOMERS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Accounts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__ACCOUNTS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Gateway</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__GATEWAY = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sessions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__SESSIONS = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Idle Sessions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__IDLE_SESSIONS = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__NAME = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Opened Sessions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__OPENED_SESSIONS = COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Withdrawls</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR__WITHDRAWLS = COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Account Mngr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Open Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR___OPEN_SESSION__INT_ATMCONTROLLER = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR___WITHDRAW__INT_SESSION = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Cancel Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR___CANCEL_SESSION__SESSION = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Commit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR___COMMIT_SESSION__SESSION = COMPONENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Exit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR___EXIT_SESSION = COMPONENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Exit Session2</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR___EXIT_SESSION2 = COMPONENT_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Account Mngr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_MNGR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.CustomerImpl <em>Customer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.CustomerImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getCustomer()
	 * @generated
	 */
	int CUSTOMER = 4;

	/**
	 * The feature id for the '<em><b>Accounts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__ACCOUNTS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Natural</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__NATURAL = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__ID = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__NAME = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.AccountImpl <em>Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.AccountImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getAccount()
	 * @generated
	 */
	int ACCOUNT = 5;

	/**
	 * The feature id for the '<em><b>Cards</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__CARDS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__BALANCE = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Holder</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__HOLDER = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Account For</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT___IS_ACCOUNT_FOR__INT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Bank Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT___GET_BANK_CARD__INT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT___WITHDRAW__INT = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT___CREATE_SESSION__INT = COMPONENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.BankCardImpl <em>Bank Card</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.BankCardImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getBankCard()
	 * @generated
	 */
	int BANK_CARD = 6;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD__OWNER = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD__NUMBER = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Physical</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD__PHYSICAL = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Account</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD__ACCOUNT = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Bank Card</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD___CREATE_SESSION = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bank Card</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_CARD_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl <em>Bank Gateway</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.BankGatewayImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getBankGateway()
	 * @generated
	 */
	int BANK_GATEWAY = 7;

	/**
	 * The feature id for the '<em><b>AT Ms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY__AT_MS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Swift</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY__SWIFT = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Account Mngrs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY__ACCOUNT_MNGRS = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sessions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY__SESSIONS = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Sessions State</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY__SESSIONS_STATE = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Bank Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY___WITHDRAW__INT_SESSION = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Open Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY___OPEN_SESSION__INT_ACCOUNTMNGR_ATMCONTROLLER = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Commit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY___COMMIT_SESSION__SESSION = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Cancel Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY___CANCEL_SESSION__SESSION = COMPONENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Exit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY___EXIT_SESSION = COMPONENT_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Bank Gateway</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_GATEWAY_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl <em>ATM Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.ATMControllerImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getATMController()
	 * @generated
	 */
	int ATM_CONTROLLER = 8;

	/**
	 * The feature id for the '<em><b>Hw</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__HW = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__SESSION = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Gateway</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__GATEWAY = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Current Card Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__CURRENT_CARD_NUM = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Current Issuer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__CURRENT_ISSUER = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Nb Tentatives</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__NB_TENTATIVES = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Current Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__CURRENT_PIN = COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Sessions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__SESSIONS = COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Last Session Trans State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER__LAST_SESSION_TRANS_STATE = COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>ATM Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>Insert Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___INSERT_CARD__INT_ACCOUNTMNGR_INT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Withdraw Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___WITHDRAW_MONEY__INT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Pin</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___SET_PIN__INT = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Cancel Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___CANCEL_SESSION__SESSION = COMPONENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Commit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___COMMIT_SESSION__INT = COMPONENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Rollback Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___ROLLBACK_WITHDRAW__INT = COMPONENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Take Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___TAKE_MONEY__ELIST = COMPONENT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Present Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___PRESENT_MONEY__INTEGER = COMPONENT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Take Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___TAKE_CARD = COMPONENT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Exit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___EXIT = COMPONENT_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Exit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___EXIT_SESSION = COMPONENT_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Present Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER___PRESENT_CARD = COMPONENT_OPERATION_COUNT + 11;

	/**
	 * The number of operations of the '<em>ATM Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATM_CONTROLLER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 12;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.HWImpl <em>HW</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.HWImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getHW()
	 * @generated
	 */
	int HW = 9;

	/**
	 * The feature id for the '<em><b>Inserted</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__INSERTED = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Held</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__HELD = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__NOTES = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ready</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__READY = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Show</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__SHOW = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__CONTROLLER = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Is Pin Requested</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__IS_PIN_REQUESTED = COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Show Card On1 Invalid Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__SHOW_CARD_ON1_INVALID_PIN = COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Retract Card On3 Invalid Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__RETRACT_CARD_ON3_INVALID_PIN = COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Retract Card On Time Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__RETRACT_CARD_ON_TIME_OUT = COMPONENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Retract Money On Time Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__RETRACT_MONEY_ON_TIME_OUT = COMPONENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Money Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__MONEY_TAKEN = COMPONENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Card Taken Back</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW__CARD_TAKEN_BACK = COMPONENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>HW</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Insert Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___INSERT_CARD = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Ask Pin</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___ASK_PIN = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Display</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___DISPLAY__STRING = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Enter Pin</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___ENTER_PIN__INT = COMPONENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Cancel Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___CANCEL_SESSION__INT = COMPONENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Present Card</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___PRESENT_CARD = COMPONENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Configure Show Card On1 Invalid Pin</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___CONFIGURE_SHOW_CARD_ON1_INVALID_PIN = COMPONENT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Configure Retract Card On3 Invalid Pin</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___CONFIGURE_RETRACT_CARD_ON3_INVALID_PIN = COMPONENT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Configure Retract Card On Time Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___CONFIGURE_RETRACT_CARD_ON_TIME_OUT__LONG = COMPONENT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Operation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___OPERATION__OPERATIONS = COMPONENT_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___WITHDRAW__INT = COMPONENT_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Configure Retract Money On Time Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___CONFIGURE_RETRACT_MONEY_ON_TIME_OUT__LONG = COMPONENT_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>Take Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___TAKE_MONEY = COMPONENT_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>Present Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___PRESENT_MONEY__INTEGER = COMPONENT_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>Exit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW___EXIT = COMPONENT_OPERATION_COUNT + 14;

	/**
	 * The number of operations of the '<em>HW</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 15;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.BankNoteImpl <em>Bank Note</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.BankNoteImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getBankNote()
	 * @generated
	 */
	int BANK_NOTE = 10;

	/**
	 * The number of structural features of the '<em>Bank Note</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_NOTE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Take Money</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_NOTE___TAKE_MONEY = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bank Note</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANK_NOTE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SessionImpl <em>Session</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SessionImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSession()
	 * @generated
	 */
	int SESSION = 11;

	/**
	 * The feature id for the '<em><b>Card</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__CARD = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ATM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ATM = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__OWNER = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Account</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ACCOUNT = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Opening Status</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__OPENING_STATUS = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Withdraw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION___WITHDRAW__INT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Exit Session</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION___EXIT_SESSION = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SwiftNetworkImpl <em>Swift Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SwiftNetworkImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSwiftNetwork()
	 * @generated
	 */
	int SWIFT_NETWORK = 12;

	/**
	 * The feature id for the '<em><b>Gateways</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWIFT_NETWORK__GATEWAYS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Swift Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWIFT_NETWORK_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Swift Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWIFT_NETWORK_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SettingImpl <em>Setting</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SettingImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSetting()
	 * @generated
	 */
	int SETTING = 13;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTING__OBJECTS = 0;

	/**
	 * The number of structural features of the '<em>Setting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Setting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToATMControllerMapImpl <em>Session To ATM Controller Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SessionToATMControllerMapImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToATMControllerMap()
	 * @generated
	 */
	int SESSION_TO_ATM_CONTROLLER_MAP = 14;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ATM_CONTROLLER_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ATM_CONTROLLER_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Session To ATM Controller Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ATM_CONTROLLER_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Session To ATM Controller Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ATM_CONTROLLER_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToIntegerMapImpl <em>Session To Integer Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SessionToIntegerMapImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToIntegerMap()
	 * @generated
	 */
	int SESSION_TO_INTEGER_MAP = 15;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_INTEGER_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_INTEGER_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Session To Integer Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_INTEGER_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Session To Integer Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_INTEGER_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SessionOpeningImpl <em>Session Opening</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SessionOpeningImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionOpening()
	 * @generated
	 */
	int SESSION_OPENING = 16;

	/**
	 * The feature id for the '<em><b>Opening Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_OPENING__OPENING_STATUS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_OPENING__SESSION = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Session Opening</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_OPENING_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Session Opening</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_OPENING_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.IntegerToSessionMapImpl <em>Integer To Session Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.IntegerToSessionMapImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getIntegerToSessionMap()
	 * @generated
	 */
	int INTEGER_TO_SESSION_MAP = 17;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TO_SESSION_MAP__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TO_SESSION_MAP__KEY = 1;

	/**
	 * The number of structural features of the '<em>Integer To Session Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TO_SESSION_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Integer To Session Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TO_SESSION_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToAccountMngrMapImpl <em>Session To Account Mngr Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SessionToAccountMngrMapImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToAccountMngrMap()
	 * @generated
	 */
	int SESSION_TO_ACCOUNT_MNGR_MAP = 18;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ACCOUNT_MNGR_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ACCOUNT_MNGR_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Session To Account Mngr Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ACCOUNT_MNGR_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Session To Account Mngr Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_ACCOUNT_MNGR_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToSessionStateMapImpl <em>Session To Session State Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.SessionToSessionStateMapImpl
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToSessionStateMap()
	 * @generated
	 */
	int SESSION_TO_SESSION_STATE_MAP = 19;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_SESSION_STATE_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_SESSION_STATE_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Session To Session State Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_SESSION_STATE_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Session To Session State Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_TO_SESSION_STATE_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.WithdrawMoneyStatus <em>Withdraw Money Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.WithdrawMoneyStatus
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getWithdrawMoneyStatus()
	 * @generated
	 */
	int WITHDRAW_MONEY_STATUS = 20;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.SessionTransactionState <em>Session Transaction State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.SessionTransactionState
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionTransactionState()
	 * @generated
	 */
	int SESSION_TRANSACTION_STATE = 21;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.InsertedCardStatus <em>Inserted Card Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.InsertedCardStatus
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getInsertedCardStatus()
	 * @generated
	 */
	int INSERTED_CARD_STATUS = 22;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.OperationChoiceStatus <em>Operation Choice Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.OperationChoiceStatus
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getOperationChoiceStatus()
	 * @generated
	 */
	int OPERATION_CHOICE_STATUS = 23;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.Operations <em>Operations</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.Operations
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getOperations()
	 * @generated
	 */
	int OPERATIONS = 24;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.SessionState <em>Session State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.SessionState
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionState()
	 * @generated
	 */
	int SESSION_STATE = 25;

	/**
	 * The meta object id for the '{@link dk.ingi.petur.bankingsystem.SessionOpeningStatus <em>Session Opening Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.SessionOpeningStatus
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionOpeningStatus()
	 * @generated
	 */
	int SESSION_OPENING_STATUS = 26;

	/**
	 * The meta object id for the '<em>Not My Physical Card Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getNotMyPhysicalCardException()
	 * @generated
	 */
	int NOT_MY_PHYSICAL_CARD_EXCEPTION = 27;

	/**
	 * The meta object id for the '<em>Not In Front Of ATM Hardware Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException
	 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getNotInFrontOfATMHardwareException()
	 * @generated
	 */
	int NOT_IN_FRONT_OF_ATM_HARDWARE_EXCEPTION = 28;


	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.NaturalPerson <em>Natural Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Natural Person</em>'.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson
	 * @generated
	 */
	EClass getNaturalPerson();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.NaturalPerson#getAvailableCards <em>Available Cards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Available Cards</em>'.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#getAvailableCards()
	 * @see #getNaturalPerson()
	 * @generated
	 */
	EReference getNaturalPerson_AvailableCards();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.bankingsystem.NaturalPerson#getUses <em>Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uses</em>'.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#getUses()
	 * @see #getNaturalPerson()
	 * @generated
	 */
	EReference getNaturalPerson_Uses();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.NaturalPerson#getOwns <em>Owns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owns</em>'.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#getOwns()
	 * @see #getNaturalPerson()
	 * @generated
	 */
	EReference getNaturalPerson_Owns();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.NaturalPerson#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#getName()
	 * @see #getNaturalPerson()
	 * @generated
	 */
	EAttribute getNaturalPerson_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.NaturalPerson#isTakeMoney <em>Take Money</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Take Money</em>'.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#isTakeMoney()
	 * @see #getNaturalPerson()
	 * @generated
	 */
	EAttribute getNaturalPerson_TakeMoney();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#insertCard(dk.ingi.petur.bankingsystem.PhysicalBankCard) <em>Insert Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Insert Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#insertCard(dk.ingi.petur.bankingsystem.PhysicalBankCard)
	 * @generated
	 */
	EOperation getNaturalPerson__InsertCard__PhysicalBankCard();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#enterPin(int) <em>Enter Pin</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Enter Pin</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#enterPin(int)
	 * @generated
	 */
	EOperation getNaturalPerson__EnterPin__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#withdraw(int) <em>Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#withdraw(int)
	 * @generated
	 */
	EOperation getNaturalPerson__Withdraw__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#configureTakeMoney(boolean) <em>Configure Take Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Configure Take Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#configureTakeMoney(boolean)
	 * @generated
	 */
	EOperation getNaturalPerson__ConfigureTakeMoney__boolean();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#takeCard() <em>Take Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Take Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#takeCard()
	 * @generated
	 */
	EOperation getNaturalPerson__TakeCard();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#takeMoney() <em>Take Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Take Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#takeMoney()
	 * @generated
	 */
	EOperation getNaturalPerson__TakeMoney();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.NaturalPerson#exit() <em>Exit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#exit()
	 * @generated
	 */
	EOperation getNaturalPerson__Exit();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see dk.ingi.petur.bankingsystem.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard <em>Physical Bank Card</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Bank Card</em>'.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard
	 * @generated
	 */
	EClass getPhysicalBankCard();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard#getNumber()
	 * @see #getPhysicalBankCard()
	 * @generated
	 */
	EAttribute getPhysicalBankCard_Number();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getPin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pin</em>'.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard#getPin()
	 * @see #getPhysicalBankCard()
	 * @generated
	 */
	EAttribute getPhysicalBankCard_Pin();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getIssuer <em>Issuer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Issuer</em>'.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard#getIssuer()
	 * @see #getPhysicalBankCard()
	 * @generated
	 */
	EReference getPhysicalBankCard_Issuer();

	/**
	 * Returns the meta object for the container reference '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard#getOwner()
	 * @see #getPhysicalBankCard()
	 * @generated
	 */
	EReference getPhysicalBankCard_Owner();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#insertCard() <em>Insert Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Insert Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard#insertCard()
	 * @generated
	 */
	EOperation getPhysicalBankCard__InsertCard();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.AccountMngr <em>Account Mngr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Account Mngr</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr
	 * @generated
	 */
	EClass getAccountMngr();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.AccountMngr#getCustomers <em>Customers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Customers</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getCustomers()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_Customers();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.AccountMngr#getAccounts <em>Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Accounts</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getAccounts()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_Accounts();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.AccountMngr#getGateway <em>Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Gateway</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getGateway()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_Gateway();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.AccountMngr#getSessions <em>Sessions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sessions</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getSessions()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_Sessions();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.AccountMngr#getIdleSessions <em>Idle Sessions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Idle Sessions</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getIdleSessions()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_IdleSessions();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.AccountMngr#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getName()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EAttribute getAccountMngr_Name();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.bankingsystem.AccountMngr#getOpenedSessions <em>Opened Sessions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Opened Sessions</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getOpenedSessions()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_OpenedSessions();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.bankingsystem.AccountMngr#getWithdrawls <em>Withdrawls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Withdrawls</em>'.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getWithdrawls()
	 * @see #getAccountMngr()
	 * @generated
	 */
	EReference getAccountMngr_Withdrawls();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.AccountMngr#openSession(int, dk.ingi.petur.bankingsystem.ATMController) <em>Open Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Open Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#openSession(int, dk.ingi.petur.bankingsystem.ATMController)
	 * @generated
	 */
	EOperation getAccountMngr__OpenSession__int_ATMController();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.AccountMngr#withdraw(int, dk.ingi.petur.bankingsystem.Session) <em>Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#withdraw(int, dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getAccountMngr__Withdraw__int_Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.AccountMngr#cancelSession(dk.ingi.petur.bankingsystem.Session) <em>Cancel Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#cancelSession(dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getAccountMngr__CancelSession__Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.AccountMngr#commitSession(dk.ingi.petur.bankingsystem.Session) <em>Commit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Commit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#commitSession(dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getAccountMngr__CommitSession__Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.AccountMngr#exitSession() <em>Exit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#exitSession()
	 * @generated
	 */
	EOperation getAccountMngr__ExitSession();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.AccountMngr#exitSession2() <em>Exit Session2</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit Session2</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#exitSession2()
	 * @generated
	 */
	EOperation getAccountMngr__ExitSession2();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.Customer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customer</em>'.
	 * @see dk.ingi.petur.bankingsystem.Customer
	 * @generated
	 */
	EClass getCustomer();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.bankingsystem.Customer#getAccounts <em>Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Accounts</em>'.
	 * @see dk.ingi.petur.bankingsystem.Customer#getAccounts()
	 * @see #getCustomer()
	 * @generated
	 */
	EReference getCustomer_Accounts();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.Customer#getNatural <em>Natural</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Natural</em>'.
	 * @see dk.ingi.petur.bankingsystem.Customer#getNatural()
	 * @see #getCustomer()
	 * @generated
	 */
	EReference getCustomer_Natural();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.Customer#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.ingi.petur.bankingsystem.Customer#getId()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_Id();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.Customer#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.ingi.petur.bankingsystem.Customer#getName()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_Name();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.Account <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Account</em>'.
	 * @see dk.ingi.petur.bankingsystem.Account
	 * @generated
	 */
	EClass getAccount();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.Account#getCards <em>Cards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cards</em>'.
	 * @see dk.ingi.petur.bankingsystem.Account#getCards()
	 * @see #getAccount()
	 * @generated
	 */
	EReference getAccount_Cards();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.Account#getBalance <em>Balance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Balance</em>'.
	 * @see dk.ingi.petur.bankingsystem.Account#getBalance()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_Balance();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.bankingsystem.Account#getHolder <em>Holder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Holder</em>'.
	 * @see dk.ingi.petur.bankingsystem.Account#getHolder()
	 * @see #getAccount()
	 * @generated
	 */
	EReference getAccount_Holder();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.Account#isAccountFor(int) <em>Is Account For</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Account For</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.Account#isAccountFor(int)
	 * @generated
	 */
	EOperation getAccount__IsAccountFor__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.Account#getBankCard(int) <em>Get Bank Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Bank Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.Account#getBankCard(int)
	 * @generated
	 */
	EOperation getAccount__GetBankCard__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.Account#withdraw(int) <em>Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.Account#withdraw(int)
	 * @generated
	 */
	EOperation getAccount__Withdraw__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.Account#createSession(int) <em>Create Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.Account#createSession(int)
	 * @generated
	 */
	EOperation getAccount__CreateSession__int();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.BankCard <em>Bank Card</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bank Card</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankCard
	 * @generated
	 */
	EClass getBankCard();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.BankCard#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankCard#getOwner()
	 * @see #getBankCard()
	 * @generated
	 */
	EReference getBankCard_Owner();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.BankCard#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankCard#getNumber()
	 * @see #getBankCard()
	 * @generated
	 */
	EAttribute getBankCard_Number();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.BankCard#getPhysical <em>Physical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankCard#getPhysical()
	 * @see #getBankCard()
	 * @generated
	 */
	EReference getBankCard_Physical();

	/**
	 * Returns the meta object for the container reference '{@link dk.ingi.petur.bankingsystem.BankCard#getAccount <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Account</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankCard#getAccount()
	 * @see #getBankCard()
	 * @generated
	 */
	EReference getBankCard_Account();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankCard#createSession() <em>Create Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankCard#createSession()
	 * @generated
	 */
	EOperation getBankCard__CreateSession();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.BankGateway <em>Bank Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bank Gateway</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankGateway
	 * @generated
	 */
	EClass getBankGateway();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.bankingsystem.BankGateway#getATMs <em>AT Ms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AT Ms</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getATMs()
	 * @see #getBankGateway()
	 * @generated
	 */
	EReference getBankGateway_ATMs();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.BankGateway#getSwift <em>Swift</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Swift</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getSwift()
	 * @see #getBankGateway()
	 * @generated
	 */
	EReference getBankGateway_Swift();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.bankingsystem.BankGateway#getAccountMngrs <em>Account Mngrs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Account Mngrs</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getAccountMngrs()
	 * @see #getBankGateway()
	 * @generated
	 */
	EReference getBankGateway_AccountMngrs();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.bankingsystem.BankGateway#getSessions <em>Sessions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Sessions</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getSessions()
	 * @see #getBankGateway()
	 * @generated
	 */
	EReference getBankGateway_Sessions();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.bankingsystem.BankGateway#getSessionsState <em>Sessions State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Sessions State</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getSessionsState()
	 * @see #getBankGateway()
	 * @generated
	 */
	EReference getBankGateway_SessionsState();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankGateway#withdraw(int, dk.ingi.petur.bankingsystem.Session) <em>Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#withdraw(int, dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getBankGateway__Withdraw__int_Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankGateway#openSession(int, dk.ingi.petur.bankingsystem.AccountMngr, dk.ingi.petur.bankingsystem.ATMController) <em>Open Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Open Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#openSession(int, dk.ingi.petur.bankingsystem.AccountMngr, dk.ingi.petur.bankingsystem.ATMController)
	 * @generated
	 */
	EOperation getBankGateway__OpenSession__int_AccountMngr_ATMController();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankGateway#commitSession(dk.ingi.petur.bankingsystem.Session) <em>Commit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Commit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#commitSession(dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getBankGateway__CommitSession__Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankGateway#cancelSession(dk.ingi.petur.bankingsystem.Session) <em>Cancel Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#cancelSession(dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getBankGateway__CancelSession__Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankGateway#exitSession() <em>Exit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankGateway#exitSession()
	 * @generated
	 */
	EOperation getBankGateway__ExitSession();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.ATMController <em>ATM Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ATM Controller</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController
	 * @generated
	 */
	EClass getATMController();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.ATMController#getHw <em>Hw</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hw</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getHw()
	 * @see #getATMController()
	 * @generated
	 */
	EReference getATMController_Hw();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.ATMController#getSession <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Session</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getSession()
	 * @see #getATMController()
	 * @generated
	 */
	EReference getATMController_Session();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.ATMController#getGateway <em>Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Gateway</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getGateway()
	 * @see #getATMController()
	 * @generated
	 */
	EReference getATMController_Gateway();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentCardNum <em>Current Card Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Card Num</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getCurrentCardNum()
	 * @see #getATMController()
	 * @generated
	 */
	EAttribute getATMController_CurrentCardNum();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentIssuer <em>Current Issuer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Issuer</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getCurrentIssuer()
	 * @see #getATMController()
	 * @generated
	 */
	EReference getATMController_CurrentIssuer();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.ATMController#getNbTentatives <em>Nb Tentatives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Tentatives</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getNbTentatives()
	 * @see #getATMController()
	 * @generated
	 */
	EAttribute getATMController_NbTentatives();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentPin <em>Current Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Pin</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getCurrentPin()
	 * @see #getATMController()
	 * @generated
	 */
	EAttribute getATMController_CurrentPin();

	/**
	 * Returns the meta object for the map '{@link dk.ingi.petur.bankingsystem.ATMController#getSessions <em>Sessions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Sessions</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getSessions()
	 * @see #getATMController()
	 * @generated
	 */
	EReference getATMController_Sessions();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.ATMController#getLastSessionTransState <em>Last Session Trans State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Session Trans State</em>'.
	 * @see dk.ingi.petur.bankingsystem.ATMController#getLastSessionTransState()
	 * @see #getATMController()
	 * @generated
	 */
	EAttribute getATMController_LastSessionTransState();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#insertCard(int, dk.ingi.petur.bankingsystem.AccountMngr, int) <em>Insert Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Insert Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#insertCard(int, dk.ingi.petur.bankingsystem.AccountMngr, int)
	 * @generated
	 */
	EOperation getATMController__InsertCard__int_AccountMngr_int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#withdrawMoney(int) <em>Withdraw Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#withdrawMoney(int)
	 * @generated
	 */
	EOperation getATMController__WithdrawMoney__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#setPin(int) <em>Set Pin</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Pin</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#setPin(int)
	 * @generated
	 */
	EOperation getATMController__SetPin__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#cancelSession(dk.ingi.petur.bankingsystem.Session) <em>Cancel Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#cancelSession(dk.ingi.petur.bankingsystem.Session)
	 * @generated
	 */
	EOperation getATMController__CancelSession__Session();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#commitSession(int) <em>Commit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Commit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#commitSession(int)
	 * @generated
	 */
	EOperation getATMController__CommitSession__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#rollbackWithdraw(int) <em>Rollback Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Rollback Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#rollbackWithdraw(int)
	 * @generated
	 */
	EOperation getATMController__RollbackWithdraw__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#takeMoney(org.eclipse.emf.common.util.EList) <em>Take Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Take Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#takeMoney(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getATMController__TakeMoney__EList();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#presentMoney(java.lang.Integer) <em>Present Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Present Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#presentMoney(java.lang.Integer)
	 * @generated
	 */
	EOperation getATMController__PresentMoney__Integer();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#takeCard() <em>Take Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Take Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#takeCard()
	 * @generated
	 */
	EOperation getATMController__TakeCard();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#exit() <em>Exit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#exit()
	 * @generated
	 */
	EOperation getATMController__Exit();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#exitSession() <em>Exit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#exitSession()
	 * @generated
	 */
	EOperation getATMController__ExitSession();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.ATMController#presentCard() <em>Present Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Present Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.ATMController#presentCard()
	 * @generated
	 */
	EOperation getATMController__PresentCard();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.HW <em>HW</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW
	 * @generated
	 */
	EClass getHW();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.HW#getInserted <em>Inserted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inserted</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getInserted()
	 * @see #getHW()
	 * @generated
	 */
	EReference getHW_Inserted();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.HW#getHeld <em>Held</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Held</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getHeld()
	 * @see #getHW()
	 * @generated
	 */
	EReference getHW_Held();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.HW#getNotes <em>Notes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Notes</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getNotes()
	 * @see #getHW()
	 * @generated
	 */
	EReference getHW_Notes();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.HW#getReady <em>Ready</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ready</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getReady()
	 * @see #getHW()
	 * @generated
	 */
	EReference getHW_Ready();

	/**
	 * Returns the meta object for the containment reference '{@link dk.ingi.petur.bankingsystem.HW#getShow <em>Show</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Show</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getShow()
	 * @see #getHW()
	 * @generated
	 */
	EReference getHW_Show();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.HW#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controller</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getController()
	 * @see #getHW()
	 * @generated
	 */
	EReference getHW_Controller();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#isIsPinRequested <em>Is Pin Requested</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Pin Requested</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#isIsPinRequested()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_IsPinRequested();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#isShowCardOn1InvalidPin <em>Show Card On1 Invalid Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Card On1 Invalid Pin</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#isShowCardOn1InvalidPin()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_ShowCardOn1InvalidPin();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#isRetractCardOn3InvalidPin <em>Retract Card On3 Invalid Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Retract Card On3 Invalid Pin</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#isRetractCardOn3InvalidPin()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_RetractCardOn3InvalidPin();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#getRetractCardOnTimeOut <em>Retract Card On Time Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Retract Card On Time Out</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getRetractCardOnTimeOut()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_RetractCardOnTimeOut();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#getRetractMoneyOnTimeOut <em>Retract Money On Time Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Retract Money On Time Out</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#getRetractMoneyOnTimeOut()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_RetractMoneyOnTimeOut();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#isMoneyTaken <em>Money Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Money Taken</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#isMoneyTaken()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_MoneyTaken();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.HW#isCardTakenBack <em>Card Taken Back</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Card Taken Back</em>'.
	 * @see dk.ingi.petur.bankingsystem.HW#isCardTakenBack()
	 * @see #getHW()
	 * @generated
	 */
	EAttribute getHW_CardTakenBack();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#insertCard() <em>Insert Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Insert Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#insertCard()
	 * @generated
	 */
	EOperation getHW__InsertCard();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#askPin() <em>Ask Pin</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ask Pin</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#askPin()
	 * @generated
	 */
	EOperation getHW__AskPin();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#display(java.lang.String) <em>Display</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Display</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#display(java.lang.String)
	 * @generated
	 */
	EOperation getHW__Display__String();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#enterPin(int) <em>Enter Pin</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Enter Pin</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#enterPin(int)
	 * @generated
	 */
	EOperation getHW__EnterPin__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#cancelSession(int) <em>Cancel Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#cancelSession(int)
	 * @generated
	 */
	EOperation getHW__CancelSession__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#presentCard() <em>Present Card</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Present Card</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#presentCard()
	 * @generated
	 */
	EOperation getHW__PresentCard();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#configureShowCardOn1InvalidPin() <em>Configure Show Card On1 Invalid Pin</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Configure Show Card On1 Invalid Pin</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#configureShowCardOn1InvalidPin()
	 * @generated
	 */
	EOperation getHW__ConfigureShowCardOn1InvalidPin();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#configureRetractCardOn3InvalidPin() <em>Configure Retract Card On3 Invalid Pin</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Configure Retract Card On3 Invalid Pin</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#configureRetractCardOn3InvalidPin()
	 * @generated
	 */
	EOperation getHW__ConfigureRetractCardOn3InvalidPin();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#configureRetractCardOnTimeOut(long) <em>Configure Retract Card On Time Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Configure Retract Card On Time Out</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#configureRetractCardOnTimeOut(long)
	 * @generated
	 */
	EOperation getHW__ConfigureRetractCardOnTimeOut__long();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#operation(dk.ingi.petur.bankingsystem.Operations) <em>Operation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Operation</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#operation(dk.ingi.petur.bankingsystem.Operations)
	 * @generated
	 */
	EOperation getHW__Operation__Operations();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#withdraw(int) <em>Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#withdraw(int)
	 * @generated
	 */
	EOperation getHW__Withdraw__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#configureRetractMoneyOnTimeOut(long) <em>Configure Retract Money On Time Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Configure Retract Money On Time Out</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#configureRetractMoneyOnTimeOut(long)
	 * @generated
	 */
	EOperation getHW__ConfigureRetractMoneyOnTimeOut__long();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#takeMoney() <em>Take Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Take Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#takeMoney()
	 * @generated
	 */
	EOperation getHW__TakeMoney();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#presentMoney(java.lang.Integer) <em>Present Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Present Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#presentMoney(java.lang.Integer)
	 * @generated
	 */
	EOperation getHW__PresentMoney__Integer();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.HW#exit() <em>Exit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.HW#exit()
	 * @generated
	 */
	EOperation getHW__Exit();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.BankNote <em>Bank Note</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bank Note</em>'.
	 * @see dk.ingi.petur.bankingsystem.BankNote
	 * @generated
	 */
	EClass getBankNote();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.BankNote#takeMoney() <em>Take Money</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Take Money</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.BankNote#takeMoney()
	 * @generated
	 */
	EOperation getBankNote__TakeMoney();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.Session <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session</em>'.
	 * @see dk.ingi.petur.bankingsystem.Session
	 * @generated
	 */
	EClass getSession();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.Session#getCard <em>Card</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Card</em>'.
	 * @see dk.ingi.petur.bankingsystem.Session#getCard()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_Card();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.Session#getATM <em>ATM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ATM</em>'.
	 * @see dk.ingi.petur.bankingsystem.Session#getATM()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_ATM();

	/**
	 * Returns the meta object for the container reference '{@link dk.ingi.petur.bankingsystem.Session#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see dk.ingi.petur.bankingsystem.Session#getOwner()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_Owner();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.Session#getAccount <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Account</em>'.
	 * @see dk.ingi.petur.bankingsystem.Session#getAccount()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_Account();

	/**
	 * Returns the meta object for the containment reference '{@link dk.ingi.petur.bankingsystem.Session#getOpeningStatus <em>Opening Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Opening Status</em>'.
	 * @see dk.ingi.petur.bankingsystem.Session#getOpeningStatus()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_OpeningStatus();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.Session#withdraw(int) <em>Withdraw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Withdraw</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.Session#withdraw(int)
	 * @generated
	 */
	EOperation getSession__Withdraw__int();

	/**
	 * Returns the meta object for the '{@link dk.ingi.petur.bankingsystem.Session#exitSession() <em>Exit Session</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit Session</em>' operation.
	 * @see dk.ingi.petur.bankingsystem.Session#exitSession()
	 * @generated
	 */
	EOperation getSession__ExitSession();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.SwiftNetwork <em>Swift Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Swift Network</em>'.
	 * @see dk.ingi.petur.bankingsystem.SwiftNetwork
	 * @generated
	 */
	EClass getSwiftNetwork();

	/**
	 * Returns the meta object for the reference list '{@link dk.ingi.petur.bankingsystem.SwiftNetwork#getGateways <em>Gateways</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Gateways</em>'.
	 * @see dk.ingi.petur.bankingsystem.SwiftNetwork#getGateways()
	 * @see #getSwiftNetwork()
	 * @generated
	 */
	EReference getSwiftNetwork_Gateways();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.Setting <em>Setting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Setting</em>'.
	 * @see dk.ingi.petur.bankingsystem.Setting
	 * @generated
	 */
	EClass getSetting();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.ingi.petur.bankingsystem.Setting#getObjects <em>Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Objects</em>'.
	 * @see dk.ingi.petur.bankingsystem.Setting#getObjects()
	 * @see #getSetting()
	 * @generated
	 */
	EReference getSetting_Objects();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Session To ATM Controller Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session To ATM Controller Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.ingi.petur.bankingsystem.Session"
	 *        valueType="dk.ingi.petur.bankingsystem.ATMController"
	 * @generated
	 */
	EClass getSessionToATMControllerMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToATMControllerMap()
	 * @generated
	 */
	EReference getSessionToATMControllerMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToATMControllerMap()
	 * @generated
	 */
	EReference getSessionToATMControllerMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Session To Integer Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session To Integer Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.ingi.petur.bankingsystem.Session"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject"
	 * @generated
	 */
	EClass getSessionToIntegerMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToIntegerMap()
	 * @generated
	 */
	EReference getSessionToIntegerMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToIntegerMap()
	 * @generated
	 */
	EAttribute getSessionToIntegerMap_Value();

	/**
	 * Returns the meta object for class '{@link dk.ingi.petur.bankingsystem.SessionOpening <em>Session Opening</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Opening</em>'.
	 * @see dk.ingi.petur.bankingsystem.SessionOpening
	 * @generated
	 */
	EClass getSessionOpening();

	/**
	 * Returns the meta object for the attribute '{@link dk.ingi.petur.bankingsystem.SessionOpening#getOpeningStatus <em>Opening Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opening Status</em>'.
	 * @see dk.ingi.petur.bankingsystem.SessionOpening#getOpeningStatus()
	 * @see #getSessionOpening()
	 * @generated
	 */
	EAttribute getSessionOpening_OpeningStatus();

	/**
	 * Returns the meta object for the reference '{@link dk.ingi.petur.bankingsystem.SessionOpening#getSession <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Session</em>'.
	 * @see dk.ingi.petur.bankingsystem.SessionOpening#getSession()
	 * @see #getSessionOpening()
	 * @generated
	 */
	EReference getSessionOpening_Session();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Integer To Session Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer To Session Map</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="dk.ingi.petur.bankingsystem.Session"
	 *        keyDataType="org.eclipse.emf.ecore.EIntegerObject"
	 * @generated
	 */
	EClass getIntegerToSessionMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIntegerToSessionMap()
	 * @generated
	 */
	EReference getIntegerToSessionMap_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIntegerToSessionMap()
	 * @generated
	 */
	EAttribute getIntegerToSessionMap_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Session To Account Mngr Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session To Account Mngr Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.ingi.petur.bankingsystem.Session"
	 *        valueType="dk.ingi.petur.bankingsystem.AccountMngr"
	 * @generated
	 */
	EClass getSessionToAccountMngrMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToAccountMngrMap()
	 * @generated
	 */
	EReference getSessionToAccountMngrMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToAccountMngrMap()
	 * @generated
	 */
	EReference getSessionToAccountMngrMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Session To Session State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session To Session State Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.ingi.petur.bankingsystem.Session"
	 *        valueDataType="dk.ingi.petur.bankingsystem.SessionState"
	 * @generated
	 */
	EClass getSessionToSessionStateMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToSessionStateMap()
	 * @generated
	 */
	EReference getSessionToSessionStateMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSessionToSessionStateMap()
	 * @generated
	 */
	EAttribute getSessionToSessionStateMap_Value();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.WithdrawMoneyStatus <em>Withdraw Money Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Withdraw Money Status</em>'.
	 * @see dk.ingi.petur.bankingsystem.WithdrawMoneyStatus
	 * @generated
	 */
	EEnum getWithdrawMoneyStatus();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.SessionTransactionState <em>Session Transaction State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Session Transaction State</em>'.
	 * @see dk.ingi.petur.bankingsystem.SessionTransactionState
	 * @generated
	 */
	EEnum getSessionTransactionState();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.InsertedCardStatus <em>Inserted Card Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Inserted Card Status</em>'.
	 * @see dk.ingi.petur.bankingsystem.InsertedCardStatus
	 * @generated
	 */
	EEnum getInsertedCardStatus();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.OperationChoiceStatus <em>Operation Choice Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operation Choice Status</em>'.
	 * @see dk.ingi.petur.bankingsystem.OperationChoiceStatus
	 * @generated
	 */
	EEnum getOperationChoiceStatus();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.Operations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operations</em>'.
	 * @see dk.ingi.petur.bankingsystem.Operations
	 * @generated
	 */
	EEnum getOperations();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.SessionState <em>Session State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Session State</em>'.
	 * @see dk.ingi.petur.bankingsystem.SessionState
	 * @generated
	 */
	EEnum getSessionState();

	/**
	 * Returns the meta object for enum '{@link dk.ingi.petur.bankingsystem.SessionOpeningStatus <em>Session Opening Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Session Opening Status</em>'.
	 * @see dk.ingi.petur.bankingsystem.SessionOpeningStatus
	 * @generated
	 */
	EEnum getSessionOpeningStatus();

	/**
	 * Returns the meta object for data type '{@link dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException <em>Not My Physical Card Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Not My Physical Card Exception</em>'.
	 * @see dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException
	 * @model instanceClass="dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException"
	 * @generated
	 */
	EDataType getNotMyPhysicalCardException();

	/**
	 * Returns the meta object for data type '{@link dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException <em>Not In Front Of ATM Hardware Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Not In Front Of ATM Hardware Exception</em>'.
	 * @see dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException
	 * @model instanceClass="dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException"
	 * @generated
	 */
	EDataType getNotInFrontOfATMHardwareException();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BankingsystemFactory getBankingsystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl <em>Natural Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getNaturalPerson()
		 * @generated
		 */
		EClass NATURAL_PERSON = eINSTANCE.getNaturalPerson();

		/**
		 * The meta object literal for the '<em><b>Available Cards</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NATURAL_PERSON__AVAILABLE_CARDS = eINSTANCE.getNaturalPerson_AvailableCards();

		/**
		 * The meta object literal for the '<em><b>Uses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NATURAL_PERSON__USES = eINSTANCE.getNaturalPerson_Uses();

		/**
		 * The meta object literal for the '<em><b>Owns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NATURAL_PERSON__OWNS = eINSTANCE.getNaturalPerson_Owns();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NATURAL_PERSON__NAME = eINSTANCE.getNaturalPerson_Name();

		/**
		 * The meta object literal for the '<em><b>Take Money</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NATURAL_PERSON__TAKE_MONEY = eINSTANCE.getNaturalPerson_TakeMoney();

		/**
		 * The meta object literal for the '<em><b>Insert Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___INSERT_CARD__PHYSICALBANKCARD = eINSTANCE.getNaturalPerson__InsertCard__PhysicalBankCard();

		/**
		 * The meta object literal for the '<em><b>Enter Pin</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___ENTER_PIN__INT = eINSTANCE.getNaturalPerson__EnterPin__int();

		/**
		 * The meta object literal for the '<em><b>Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___WITHDRAW__INT = eINSTANCE.getNaturalPerson__Withdraw__int();

		/**
		 * The meta object literal for the '<em><b>Configure Take Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___CONFIGURE_TAKE_MONEY__BOOLEAN = eINSTANCE.getNaturalPerson__ConfigureTakeMoney__boolean();

		/**
		 * The meta object literal for the '<em><b>Take Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___TAKE_CARD = eINSTANCE.getNaturalPerson__TakeCard();

		/**
		 * The meta object literal for the '<em><b>Take Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___TAKE_MONEY = eINSTANCE.getNaturalPerson__TakeMoney();

		/**
		 * The meta object literal for the '<em><b>Exit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NATURAL_PERSON___EXIT = eINSTANCE.getNaturalPerson__Exit();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.ComponentImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.PhysicalBankCardImpl <em>Physical Bank Card</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.PhysicalBankCardImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getPhysicalBankCard()
		 * @generated
		 */
		EClass PHYSICAL_BANK_CARD = eINSTANCE.getPhysicalBankCard();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_BANK_CARD__NUMBER = eINSTANCE.getPhysicalBankCard_Number();

		/**
		 * The meta object literal for the '<em><b>Pin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_BANK_CARD__PIN = eINSTANCE.getPhysicalBankCard_Pin();

		/**
		 * The meta object literal for the '<em><b>Issuer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_BANK_CARD__ISSUER = eINSTANCE.getPhysicalBankCard_Issuer();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_BANK_CARD__OWNER = eINSTANCE.getPhysicalBankCard_Owner();

		/**
		 * The meta object literal for the '<em><b>Insert Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PHYSICAL_BANK_CARD___INSERT_CARD = eINSTANCE.getPhysicalBankCard__InsertCard();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl <em>Account Mngr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.AccountMngrImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getAccountMngr()
		 * @generated
		 */
		EClass ACCOUNT_MNGR = eINSTANCE.getAccountMngr();

		/**
		 * The meta object literal for the '<em><b>Customers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__CUSTOMERS = eINSTANCE.getAccountMngr_Customers();

		/**
		 * The meta object literal for the '<em><b>Accounts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__ACCOUNTS = eINSTANCE.getAccountMngr_Accounts();

		/**
		 * The meta object literal for the '<em><b>Gateway</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__GATEWAY = eINSTANCE.getAccountMngr_Gateway();

		/**
		 * The meta object literal for the '<em><b>Sessions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__SESSIONS = eINSTANCE.getAccountMngr_Sessions();

		/**
		 * The meta object literal for the '<em><b>Idle Sessions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__IDLE_SESSIONS = eINSTANCE.getAccountMngr_IdleSessions();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT_MNGR__NAME = eINSTANCE.getAccountMngr_Name();

		/**
		 * The meta object literal for the '<em><b>Opened Sessions</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__OPENED_SESSIONS = eINSTANCE.getAccountMngr_OpenedSessions();

		/**
		 * The meta object literal for the '<em><b>Withdrawls</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_MNGR__WITHDRAWLS = eINSTANCE.getAccountMngr_Withdrawls();

		/**
		 * The meta object literal for the '<em><b>Open Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_MNGR___OPEN_SESSION__INT_ATMCONTROLLER = eINSTANCE.getAccountMngr__OpenSession__int_ATMController();

		/**
		 * The meta object literal for the '<em><b>Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_MNGR___WITHDRAW__INT_SESSION = eINSTANCE.getAccountMngr__Withdraw__int_Session();

		/**
		 * The meta object literal for the '<em><b>Cancel Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_MNGR___CANCEL_SESSION__SESSION = eINSTANCE.getAccountMngr__CancelSession__Session();

		/**
		 * The meta object literal for the '<em><b>Commit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_MNGR___COMMIT_SESSION__SESSION = eINSTANCE.getAccountMngr__CommitSession__Session();

		/**
		 * The meta object literal for the '<em><b>Exit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_MNGR___EXIT_SESSION = eINSTANCE.getAccountMngr__ExitSession();

		/**
		 * The meta object literal for the '<em><b>Exit Session2</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_MNGR___EXIT_SESSION2 = eINSTANCE.getAccountMngr__ExitSession2();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.CustomerImpl <em>Customer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.CustomerImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getCustomer()
		 * @generated
		 */
		EClass CUSTOMER = eINSTANCE.getCustomer();

		/**
		 * The meta object literal for the '<em><b>Accounts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOMER__ACCOUNTS = eINSTANCE.getCustomer_Accounts();

		/**
		 * The meta object literal for the '<em><b>Natural</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOMER__NATURAL = eINSTANCE.getCustomer_Natural();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__ID = eINSTANCE.getCustomer_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__NAME = eINSTANCE.getCustomer_Name();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.AccountImpl <em>Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.AccountImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getAccount()
		 * @generated
		 */
		EClass ACCOUNT = eINSTANCE.getAccount();

		/**
		 * The meta object literal for the '<em><b>Cards</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT__CARDS = eINSTANCE.getAccount_Cards();

		/**
		 * The meta object literal for the '<em><b>Balance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__BALANCE = eINSTANCE.getAccount_Balance();

		/**
		 * The meta object literal for the '<em><b>Holder</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT__HOLDER = eINSTANCE.getAccount_Holder();

		/**
		 * The meta object literal for the '<em><b>Is Account For</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT___IS_ACCOUNT_FOR__INT = eINSTANCE.getAccount__IsAccountFor__int();

		/**
		 * The meta object literal for the '<em><b>Get Bank Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT___GET_BANK_CARD__INT = eINSTANCE.getAccount__GetBankCard__int();

		/**
		 * The meta object literal for the '<em><b>Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT___WITHDRAW__INT = eINSTANCE.getAccount__Withdraw__int();

		/**
		 * The meta object literal for the '<em><b>Create Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT___CREATE_SESSION__INT = eINSTANCE.getAccount__CreateSession__int();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.BankCardImpl <em>Bank Card</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.BankCardImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getBankCard()
		 * @generated
		 */
		EClass BANK_CARD = eINSTANCE.getBankCard();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_CARD__OWNER = eINSTANCE.getBankCard_Owner();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BANK_CARD__NUMBER = eINSTANCE.getBankCard_Number();

		/**
		 * The meta object literal for the '<em><b>Physical</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_CARD__PHYSICAL = eINSTANCE.getBankCard_Physical();

		/**
		 * The meta object literal for the '<em><b>Account</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_CARD__ACCOUNT = eINSTANCE.getBankCard_Account();

		/**
		 * The meta object literal for the '<em><b>Create Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_CARD___CREATE_SESSION = eINSTANCE.getBankCard__CreateSession();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl <em>Bank Gateway</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.BankGatewayImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getBankGateway()
		 * @generated
		 */
		EClass BANK_GATEWAY = eINSTANCE.getBankGateway();

		/**
		 * The meta object literal for the '<em><b>AT Ms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_GATEWAY__AT_MS = eINSTANCE.getBankGateway_ATMs();

		/**
		 * The meta object literal for the '<em><b>Swift</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_GATEWAY__SWIFT = eINSTANCE.getBankGateway_Swift();

		/**
		 * The meta object literal for the '<em><b>Account Mngrs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_GATEWAY__ACCOUNT_MNGRS = eINSTANCE.getBankGateway_AccountMngrs();

		/**
		 * The meta object literal for the '<em><b>Sessions</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_GATEWAY__SESSIONS = eINSTANCE.getBankGateway_Sessions();

		/**
		 * The meta object literal for the '<em><b>Sessions State</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANK_GATEWAY__SESSIONS_STATE = eINSTANCE.getBankGateway_SessionsState();

		/**
		 * The meta object literal for the '<em><b>Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_GATEWAY___WITHDRAW__INT_SESSION = eINSTANCE.getBankGateway__Withdraw__int_Session();

		/**
		 * The meta object literal for the '<em><b>Open Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_GATEWAY___OPEN_SESSION__INT_ACCOUNTMNGR_ATMCONTROLLER = eINSTANCE.getBankGateway__OpenSession__int_AccountMngr_ATMController();

		/**
		 * The meta object literal for the '<em><b>Commit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_GATEWAY___COMMIT_SESSION__SESSION = eINSTANCE.getBankGateway__CommitSession__Session();

		/**
		 * The meta object literal for the '<em><b>Cancel Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_GATEWAY___CANCEL_SESSION__SESSION = eINSTANCE.getBankGateway__CancelSession__Session();

		/**
		 * The meta object literal for the '<em><b>Exit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_GATEWAY___EXIT_SESSION = eINSTANCE.getBankGateway__ExitSession();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl <em>ATM Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.ATMControllerImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getATMController()
		 * @generated
		 */
		EClass ATM_CONTROLLER = eINSTANCE.getATMController();

		/**
		 * The meta object literal for the '<em><b>Hw</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATM_CONTROLLER__HW = eINSTANCE.getATMController_Hw();

		/**
		 * The meta object literal for the '<em><b>Session</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATM_CONTROLLER__SESSION = eINSTANCE.getATMController_Session();

		/**
		 * The meta object literal for the '<em><b>Gateway</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATM_CONTROLLER__GATEWAY = eINSTANCE.getATMController_Gateway();

		/**
		 * The meta object literal for the '<em><b>Current Card Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATM_CONTROLLER__CURRENT_CARD_NUM = eINSTANCE.getATMController_CurrentCardNum();

		/**
		 * The meta object literal for the '<em><b>Current Issuer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATM_CONTROLLER__CURRENT_ISSUER = eINSTANCE.getATMController_CurrentIssuer();

		/**
		 * The meta object literal for the '<em><b>Nb Tentatives</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATM_CONTROLLER__NB_TENTATIVES = eINSTANCE.getATMController_NbTentatives();

		/**
		 * The meta object literal for the '<em><b>Current Pin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATM_CONTROLLER__CURRENT_PIN = eINSTANCE.getATMController_CurrentPin();

		/**
		 * The meta object literal for the '<em><b>Sessions</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATM_CONTROLLER__SESSIONS = eINSTANCE.getATMController_Sessions();

		/**
		 * The meta object literal for the '<em><b>Last Session Trans State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATM_CONTROLLER__LAST_SESSION_TRANS_STATE = eINSTANCE.getATMController_LastSessionTransState();

		/**
		 * The meta object literal for the '<em><b>Insert Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___INSERT_CARD__INT_ACCOUNTMNGR_INT = eINSTANCE.getATMController__InsertCard__int_AccountMngr_int();

		/**
		 * The meta object literal for the '<em><b>Withdraw Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___WITHDRAW_MONEY__INT = eINSTANCE.getATMController__WithdrawMoney__int();

		/**
		 * The meta object literal for the '<em><b>Set Pin</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___SET_PIN__INT = eINSTANCE.getATMController__SetPin__int();

		/**
		 * The meta object literal for the '<em><b>Cancel Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___CANCEL_SESSION__SESSION = eINSTANCE.getATMController__CancelSession__Session();

		/**
		 * The meta object literal for the '<em><b>Commit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___COMMIT_SESSION__INT = eINSTANCE.getATMController__CommitSession__int();

		/**
		 * The meta object literal for the '<em><b>Rollback Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___ROLLBACK_WITHDRAW__INT = eINSTANCE.getATMController__RollbackWithdraw__int();

		/**
		 * The meta object literal for the '<em><b>Take Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___TAKE_MONEY__ELIST = eINSTANCE.getATMController__TakeMoney__EList();

		/**
		 * The meta object literal for the '<em><b>Present Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___PRESENT_MONEY__INTEGER = eINSTANCE.getATMController__PresentMoney__Integer();

		/**
		 * The meta object literal for the '<em><b>Take Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___TAKE_CARD = eINSTANCE.getATMController__TakeCard();

		/**
		 * The meta object literal for the '<em><b>Exit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___EXIT = eINSTANCE.getATMController__Exit();

		/**
		 * The meta object literal for the '<em><b>Exit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___EXIT_SESSION = eINSTANCE.getATMController__ExitSession();

		/**
		 * The meta object literal for the '<em><b>Present Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATM_CONTROLLER___PRESENT_CARD = eINSTANCE.getATMController__PresentCard();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.HWImpl <em>HW</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.HWImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getHW()
		 * @generated
		 */
		EClass HW = eINSTANCE.getHW();

		/**
		 * The meta object literal for the '<em><b>Inserted</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW__INSERTED = eINSTANCE.getHW_Inserted();

		/**
		 * The meta object literal for the '<em><b>Held</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW__HELD = eINSTANCE.getHW_Held();

		/**
		 * The meta object literal for the '<em><b>Notes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW__NOTES = eINSTANCE.getHW_Notes();

		/**
		 * The meta object literal for the '<em><b>Ready</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW__READY = eINSTANCE.getHW_Ready();

		/**
		 * The meta object literal for the '<em><b>Show</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW__SHOW = eINSTANCE.getHW_Show();

		/**
		 * The meta object literal for the '<em><b>Controller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW__CONTROLLER = eINSTANCE.getHW_Controller();

		/**
		 * The meta object literal for the '<em><b>Is Pin Requested</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__IS_PIN_REQUESTED = eINSTANCE.getHW_IsPinRequested();

		/**
		 * The meta object literal for the '<em><b>Show Card On1 Invalid Pin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__SHOW_CARD_ON1_INVALID_PIN = eINSTANCE.getHW_ShowCardOn1InvalidPin();

		/**
		 * The meta object literal for the '<em><b>Retract Card On3 Invalid Pin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__RETRACT_CARD_ON3_INVALID_PIN = eINSTANCE.getHW_RetractCardOn3InvalidPin();

		/**
		 * The meta object literal for the '<em><b>Retract Card On Time Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__RETRACT_CARD_ON_TIME_OUT = eINSTANCE.getHW_RetractCardOnTimeOut();

		/**
		 * The meta object literal for the '<em><b>Retract Money On Time Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__RETRACT_MONEY_ON_TIME_OUT = eINSTANCE.getHW_RetractMoneyOnTimeOut();

		/**
		 * The meta object literal for the '<em><b>Money Taken</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__MONEY_TAKEN = eINSTANCE.getHW_MoneyTaken();

		/**
		 * The meta object literal for the '<em><b>Card Taken Back</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW__CARD_TAKEN_BACK = eINSTANCE.getHW_CardTakenBack();

		/**
		 * The meta object literal for the '<em><b>Insert Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___INSERT_CARD = eINSTANCE.getHW__InsertCard();

		/**
		 * The meta object literal for the '<em><b>Ask Pin</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___ASK_PIN = eINSTANCE.getHW__AskPin();

		/**
		 * The meta object literal for the '<em><b>Display</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___DISPLAY__STRING = eINSTANCE.getHW__Display__String();

		/**
		 * The meta object literal for the '<em><b>Enter Pin</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___ENTER_PIN__INT = eINSTANCE.getHW__EnterPin__int();

		/**
		 * The meta object literal for the '<em><b>Cancel Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___CANCEL_SESSION__INT = eINSTANCE.getHW__CancelSession__int();

		/**
		 * The meta object literal for the '<em><b>Present Card</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___PRESENT_CARD = eINSTANCE.getHW__PresentCard();

		/**
		 * The meta object literal for the '<em><b>Configure Show Card On1 Invalid Pin</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___CONFIGURE_SHOW_CARD_ON1_INVALID_PIN = eINSTANCE.getHW__ConfigureShowCardOn1InvalidPin();

		/**
		 * The meta object literal for the '<em><b>Configure Retract Card On3 Invalid Pin</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___CONFIGURE_RETRACT_CARD_ON3_INVALID_PIN = eINSTANCE.getHW__ConfigureRetractCardOn3InvalidPin();

		/**
		 * The meta object literal for the '<em><b>Configure Retract Card On Time Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___CONFIGURE_RETRACT_CARD_ON_TIME_OUT__LONG = eINSTANCE.getHW__ConfigureRetractCardOnTimeOut__long();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___OPERATION__OPERATIONS = eINSTANCE.getHW__Operation__Operations();

		/**
		 * The meta object literal for the '<em><b>Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___WITHDRAW__INT = eINSTANCE.getHW__Withdraw__int();

		/**
		 * The meta object literal for the '<em><b>Configure Retract Money On Time Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___CONFIGURE_RETRACT_MONEY_ON_TIME_OUT__LONG = eINSTANCE.getHW__ConfigureRetractMoneyOnTimeOut__long();

		/**
		 * The meta object literal for the '<em><b>Take Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___TAKE_MONEY = eINSTANCE.getHW__TakeMoney();

		/**
		 * The meta object literal for the '<em><b>Present Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___PRESENT_MONEY__INTEGER = eINSTANCE.getHW__PresentMoney__Integer();

		/**
		 * The meta object literal for the '<em><b>Exit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HW___EXIT = eINSTANCE.getHW__Exit();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.BankNoteImpl <em>Bank Note</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.BankNoteImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getBankNote()
		 * @generated
		 */
		EClass BANK_NOTE = eINSTANCE.getBankNote();

		/**
		 * The meta object literal for the '<em><b>Take Money</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANK_NOTE___TAKE_MONEY = eINSTANCE.getBankNote__TakeMoney();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SessionImpl <em>Session</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SessionImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSession()
		 * @generated
		 */
		EClass SESSION = eINSTANCE.getSession();

		/**
		 * The meta object literal for the '<em><b>Card</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__CARD = eINSTANCE.getSession_Card();

		/**
		 * The meta object literal for the '<em><b>ATM</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__ATM = eINSTANCE.getSession_ATM();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__OWNER = eINSTANCE.getSession_Owner();

		/**
		 * The meta object literal for the '<em><b>Account</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__ACCOUNT = eINSTANCE.getSession_Account();

		/**
		 * The meta object literal for the '<em><b>Opening Status</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__OPENING_STATUS = eINSTANCE.getSession_OpeningStatus();

		/**
		 * The meta object literal for the '<em><b>Withdraw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SESSION___WITHDRAW__INT = eINSTANCE.getSession__Withdraw__int();

		/**
		 * The meta object literal for the '<em><b>Exit Session</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SESSION___EXIT_SESSION = eINSTANCE.getSession__ExitSession();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SwiftNetworkImpl <em>Swift Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SwiftNetworkImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSwiftNetwork()
		 * @generated
		 */
		EClass SWIFT_NETWORK = eINSTANCE.getSwiftNetwork();

		/**
		 * The meta object literal for the '<em><b>Gateways</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SWIFT_NETWORK__GATEWAYS = eINSTANCE.getSwiftNetwork_Gateways();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SettingImpl <em>Setting</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SettingImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSetting()
		 * @generated
		 */
		EClass SETTING = eINSTANCE.getSetting();

		/**
		 * The meta object literal for the '<em><b>Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SETTING__OBJECTS = eINSTANCE.getSetting_Objects();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToATMControllerMapImpl <em>Session To ATM Controller Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SessionToATMControllerMapImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToATMControllerMap()
		 * @generated
		 */
		EClass SESSION_TO_ATM_CONTROLLER_MAP = eINSTANCE.getSessionToATMControllerMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_TO_ATM_CONTROLLER_MAP__KEY = eINSTANCE.getSessionToATMControllerMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_TO_ATM_CONTROLLER_MAP__VALUE = eINSTANCE.getSessionToATMControllerMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToIntegerMapImpl <em>Session To Integer Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SessionToIntegerMapImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToIntegerMap()
		 * @generated
		 */
		EClass SESSION_TO_INTEGER_MAP = eINSTANCE.getSessionToIntegerMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_TO_INTEGER_MAP__KEY = eINSTANCE.getSessionToIntegerMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_TO_INTEGER_MAP__VALUE = eINSTANCE.getSessionToIntegerMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SessionOpeningImpl <em>Session Opening</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SessionOpeningImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionOpening()
		 * @generated
		 */
		EClass SESSION_OPENING = eINSTANCE.getSessionOpening();

		/**
		 * The meta object literal for the '<em><b>Opening Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_OPENING__OPENING_STATUS = eINSTANCE.getSessionOpening_OpeningStatus();

		/**
		 * The meta object literal for the '<em><b>Session</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_OPENING__SESSION = eINSTANCE.getSessionOpening_Session();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.IntegerToSessionMapImpl <em>Integer To Session Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.IntegerToSessionMapImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getIntegerToSessionMap()
		 * @generated
		 */
		EClass INTEGER_TO_SESSION_MAP = eINSTANCE.getIntegerToSessionMap();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTEGER_TO_SESSION_MAP__VALUE = eINSTANCE.getIntegerToSessionMap_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_TO_SESSION_MAP__KEY = eINSTANCE.getIntegerToSessionMap_Key();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToAccountMngrMapImpl <em>Session To Account Mngr Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SessionToAccountMngrMapImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToAccountMngrMap()
		 * @generated
		 */
		EClass SESSION_TO_ACCOUNT_MNGR_MAP = eINSTANCE.getSessionToAccountMngrMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_TO_ACCOUNT_MNGR_MAP__KEY = eINSTANCE.getSessionToAccountMngrMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_TO_ACCOUNT_MNGR_MAP__VALUE = eINSTANCE.getSessionToAccountMngrMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.impl.SessionToSessionStateMapImpl <em>Session To Session State Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.SessionToSessionStateMapImpl
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionToSessionStateMap()
		 * @generated
		 */
		EClass SESSION_TO_SESSION_STATE_MAP = eINSTANCE.getSessionToSessionStateMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_TO_SESSION_STATE_MAP__KEY = eINSTANCE.getSessionToSessionStateMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_TO_SESSION_STATE_MAP__VALUE = eINSTANCE.getSessionToSessionStateMap_Value();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.WithdrawMoneyStatus <em>Withdraw Money Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.WithdrawMoneyStatus
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getWithdrawMoneyStatus()
		 * @generated
		 */
		EEnum WITHDRAW_MONEY_STATUS = eINSTANCE.getWithdrawMoneyStatus();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.SessionTransactionState <em>Session Transaction State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.SessionTransactionState
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionTransactionState()
		 * @generated
		 */
		EEnum SESSION_TRANSACTION_STATE = eINSTANCE.getSessionTransactionState();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.InsertedCardStatus <em>Inserted Card Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.InsertedCardStatus
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getInsertedCardStatus()
		 * @generated
		 */
		EEnum INSERTED_CARD_STATUS = eINSTANCE.getInsertedCardStatus();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.OperationChoiceStatus <em>Operation Choice Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.OperationChoiceStatus
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getOperationChoiceStatus()
		 * @generated
		 */
		EEnum OPERATION_CHOICE_STATUS = eINSTANCE.getOperationChoiceStatus();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.Operations <em>Operations</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.Operations
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getOperations()
		 * @generated
		 */
		EEnum OPERATIONS = eINSTANCE.getOperations();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.SessionState <em>Session State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.SessionState
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionState()
		 * @generated
		 */
		EEnum SESSION_STATE = eINSTANCE.getSessionState();

		/**
		 * The meta object literal for the '{@link dk.ingi.petur.bankingsystem.SessionOpeningStatus <em>Session Opening Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.SessionOpeningStatus
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getSessionOpeningStatus()
		 * @generated
		 */
		EEnum SESSION_OPENING_STATUS = eINSTANCE.getSessionOpeningStatus();

		/**
		 * The meta object literal for the '<em>Not My Physical Card Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getNotMyPhysicalCardException()
		 * @generated
		 */
		EDataType NOT_MY_PHYSICAL_CARD_EXCEPTION = eINSTANCE.getNotMyPhysicalCardException();

		/**
		 * The meta object literal for the '<em>Not In Front Of ATM Hardware Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException
		 * @see dk.ingi.petur.bankingsystem.impl.BankingsystemPackageImpl#getNotInFrontOfATMHardwareException()
		 * @generated
		 */
		EDataType NOT_IN_FRONT_OF_ATM_HARDWARE_EXCEPTION = eINSTANCE.getNotInFrontOfATMHardwareException();

	}

} //BankingsystemPackage
