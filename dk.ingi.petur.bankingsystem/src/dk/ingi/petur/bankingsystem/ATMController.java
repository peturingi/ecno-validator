/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ATM Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getHw <em>Hw</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getSession <em>Session</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getGateway <em>Gateway</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentCardNum <em>Current Card Num</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentIssuer <em>Current Issuer</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getNbTentatives <em>Nb Tentatives</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentPin <em>Current Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getSessions <em>Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.ATMController#getLastSessionTransState <em>Last Session Trans State</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController()
 * @model
 * @generated
 */
public interface ATMController extends Component {
	/**
	 * Returns the value of the '<em><b>Hw</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.HW#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hw</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hw</em>' reference.
	 * @see #setHw(HW)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_Hw()
	 * @see dk.ingi.petur.bankingsystem.HW#getController
	 * @model opposite="controller" required="true"
	 * @generated
	 */
	HW getHw();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getHw <em>Hw</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hw</em>' reference.
	 * @see #getHw()
	 * @generated
	 */
	void setHw(HW value);

	/**
	 * Returns the value of the '<em><b>Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Session</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session</em>' reference.
	 * @see #setSession(Session)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_Session()
	 * @model
	 * @generated
	 */
	Session getSession();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getSession <em>Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Session</em>' reference.
	 * @see #getSession()
	 * @generated
	 */
	void setSession(Session value);

	/**
	 * Returns the value of the '<em><b>Gateway</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.BankGateway#getATMs <em>AT Ms</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gateway</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gateway</em>' reference.
	 * @see #setGateway(BankGateway)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_Gateway()
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getATMs
	 * @model opposite="ATMs" required="true"
	 * @generated
	 */
	BankGateway getGateway();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getGateway <em>Gateway</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gateway</em>' reference.
	 * @see #getGateway()
	 * @generated
	 */
	void setGateway(BankGateway value);

	/**
	 * Returns the value of the '<em><b>Current Card Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Card Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Card Num</em>' attribute.
	 * @see #setCurrentCardNum(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_CurrentCardNum()
	 * @model
	 * @generated
	 */
	int getCurrentCardNum();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentCardNum <em>Current Card Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Card Num</em>' attribute.
	 * @see #getCurrentCardNum()
	 * @generated
	 */
	void setCurrentCardNum(int value);

	/**
	 * Returns the value of the '<em><b>Current Issuer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Issuer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Issuer</em>' reference.
	 * @see #setCurrentIssuer(AccountMngr)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_CurrentIssuer()
	 * @model
	 * @generated
	 */
	AccountMngr getCurrentIssuer();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentIssuer <em>Current Issuer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Issuer</em>' reference.
	 * @see #getCurrentIssuer()
	 * @generated
	 */
	void setCurrentIssuer(AccountMngr value);

	/**
	 * Returns the value of the '<em><b>Nb Tentatives</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nb Tentatives</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Tentatives</em>' attribute.
	 * @see #setNbTentatives(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_NbTentatives()
	 * @model
	 * @generated
	 */
	int getNbTentatives();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getNbTentatives <em>Nb Tentatives</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Tentatives</em>' attribute.
	 * @see #getNbTentatives()
	 * @generated
	 */
	void setNbTentatives(int value);

	/**
	 * Returns the value of the '<em><b>Current Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Pin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Pin</em>' attribute.
	 * @see #setCurrentPin(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_CurrentPin()
	 * @model
	 * @generated
	 */
	int getCurrentPin();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getCurrentPin <em>Current Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Pin</em>' attribute.
	 * @see #getCurrentPin()
	 * @generated
	 */
	void setCurrentPin(int value);

	/**
	 * Returns the value of the '<em><b>Sessions</b></em>' map.
	 * The key is of type {@link java.lang.Integer},
	 * and the value is of type {@link dk.ingi.petur.bankingsystem.Session},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sessions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sessions</em>' map.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_Sessions()
	 * @model mapType="dk.ingi.petur.bankingsystem.IntegerToSessionMap<org.eclipse.emf.ecore.EIntegerObject, dk.ingi.petur.bankingsystem.Session>"
	 * @generated
	 */
	EMap<Integer, Session> getSessions();

	/**
	 * Returns the value of the '<em><b>Last Session Trans State</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.ingi.petur.bankingsystem.SessionTransactionState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Session Trans State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Session Trans State</em>' attribute.
	 * @see dk.ingi.petur.bankingsystem.SessionTransactionState
	 * @see #setLastSessionTransState(SessionTransactionState)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getATMController_LastSessionTransState()
	 * @model
	 * @generated
	 */
	SessionTransactionState getLastSessionTransState();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.ATMController#getLastSessionTransState <em>Last Session Trans State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Session Trans State</em>' attribute.
	 * @see dk.ingi.petur.bankingsystem.SessionTransactionState
	 * @see #getLastSessionTransState()
	 * @generated
	 */
	void setLastSessionTransState(SessionTransactionState value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void insertCard(int cardNumber, AccountMngr issuer, int pin);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	WithdrawMoneyStatus withdrawMoney(int amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="dk.ingi.petur.bankingsystem.InsertedCardStatus"
	 * @generated
	 */
	InsertedCardStatus setPin(int enteredPin);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void cancelSession(Session s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SessionTransactionState commitSession(int cardNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SessionTransactionState rollbackWithdraw(int cardNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model moneyMany="true"
	 * @generated
	 */
	void takeMoney(EList<BankNote> money);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void presentMoney(Integer amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void takeCard();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exit();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exitSession();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void presentCard();

} // ATMController
