/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session To Session State Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.SessionToSessionStateMap#getKey <em>Key</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.SessionToSessionStateMap#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionToSessionStateMap()
 * @model
 * @generated
 */
public interface SessionToSessionStateMap extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(Session)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionToSessionStateMap_Key()
	 * @model
	 * @generated
	 */
	Session getKey();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.SessionToSessionStateMap#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Session value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.ingi.petur.bankingsystem.SessionState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see dk.ingi.petur.bankingsystem.SessionState
	 * @see #setValue(SessionState)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionToSessionStateMap_Value()
	 * @model
	 * @generated
	 */
	SessionState getValue();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.SessionToSessionStateMap#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see dk.ingi.petur.bankingsystem.SessionState
	 * @see #getValue()
	 * @generated
	 */
	void setValue(SessionState value);

} // SessionToSessionStateMap
