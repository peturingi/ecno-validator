/**
 */
package dk.ingi.petur.bankingsystem.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import dk.ingi.petur.bankingsystem.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage
 * @generated
 */
public class BankingsystemAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BankingsystemPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankingsystemAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BankingsystemPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BankingsystemSwitch<Adapter> modelSwitch =
		new BankingsystemSwitch<Adapter>() {
			@Override
			public Adapter caseNaturalPerson(NaturalPerson object) {
				return createNaturalPersonAdapter();
			}
			@Override
			public Adapter caseComponent(Component object) {
				return createComponentAdapter();
			}
			@Override
			public Adapter casePhysicalBankCard(PhysicalBankCard object) {
				return createPhysicalBankCardAdapter();
			}
			@Override
			public Adapter caseAccountMngr(AccountMngr object) {
				return createAccountMngrAdapter();
			}
			@Override
			public Adapter caseCustomer(Customer object) {
				return createCustomerAdapter();
			}
			@Override
			public Adapter caseAccount(Account object) {
				return createAccountAdapter();
			}
			@Override
			public Adapter caseBankCard(BankCard object) {
				return createBankCardAdapter();
			}
			@Override
			public Adapter caseBankGateway(BankGateway object) {
				return createBankGatewayAdapter();
			}
			@Override
			public Adapter caseATMController(ATMController object) {
				return createATMControllerAdapter();
			}
			@Override
			public Adapter caseHW(HW object) {
				return createHWAdapter();
			}
			@Override
			public Adapter caseBankNote(BankNote object) {
				return createBankNoteAdapter();
			}
			@Override
			public Adapter caseSession(Session object) {
				return createSessionAdapter();
			}
			@Override
			public Adapter caseSwiftNetwork(SwiftNetwork object) {
				return createSwiftNetworkAdapter();
			}
			@Override
			public Adapter caseSetting(Setting object) {
				return createSettingAdapter();
			}
			@Override
			public Adapter caseSessionToATMControllerMap(Map.Entry<Session, ATMController> object) {
				return createSessionToATMControllerMapAdapter();
			}
			@Override
			public Adapter caseSessionToIntegerMap(Map.Entry<Session, Integer> object) {
				return createSessionToIntegerMapAdapter();
			}
			@Override
			public Adapter caseSessionOpening(SessionOpening object) {
				return createSessionOpeningAdapter();
			}
			@Override
			public Adapter caseIntegerToSessionMap(Map.Entry<Integer, Session> object) {
				return createIntegerToSessionMapAdapter();
			}
			@Override
			public Adapter caseSessionToAccountMngrMap(Map.Entry<Session, AccountMngr> object) {
				return createSessionToAccountMngrMapAdapter();
			}
			@Override
			public Adapter caseSessionToSessionStateMap(Map.Entry<Session, SessionState> object) {
				return createSessionToSessionStateMapAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.NaturalPerson <em>Natural Person</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson
	 * @generated
	 */
	public Adapter createNaturalPersonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.Component
	 * @generated
	 */
	public Adapter createComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard <em>Physical Bank Card</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.PhysicalBankCard
	 * @generated
	 */
	public Adapter createPhysicalBankCardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.AccountMngr <em>Account Mngr</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.AccountMngr
	 * @generated
	 */
	public Adapter createAccountMngrAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.Customer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.Customer
	 * @generated
	 */
	public Adapter createCustomerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.Account <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.Account
	 * @generated
	 */
	public Adapter createAccountAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.BankCard <em>Bank Card</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.BankCard
	 * @generated
	 */
	public Adapter createBankCardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.BankGateway <em>Bank Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.BankGateway
	 * @generated
	 */
	public Adapter createBankGatewayAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.ATMController <em>ATM Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.ATMController
	 * @generated
	 */
	public Adapter createATMControllerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.HW <em>HW</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.HW
	 * @generated
	 */
	public Adapter createHWAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.BankNote <em>Bank Note</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.BankNote
	 * @generated
	 */
	public Adapter createBankNoteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.Session <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.Session
	 * @generated
	 */
	public Adapter createSessionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.SwiftNetwork <em>Swift Network</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.SwiftNetwork
	 * @generated
	 */
	public Adapter createSwiftNetworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.Setting <em>Setting</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.Setting
	 * @generated
	 */
	public Adapter createSettingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Session To ATM Controller Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createSessionToATMControllerMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Session To Integer Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createSessionToIntegerMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.ingi.petur.bankingsystem.SessionOpening <em>Session Opening</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.ingi.petur.bankingsystem.SessionOpening
	 * @generated
	 */
	public Adapter createSessionOpeningAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Integer To Session Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createIntegerToSessionMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Session To Account Mngr Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createSessionToAccountMngrMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Session To Session State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createSessionToSessionStateMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BankingsystemAdapterFactory
