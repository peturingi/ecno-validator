/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bank Gateway</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankGateway#getATMs <em>AT Ms</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankGateway#getSwift <em>Swift</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankGateway#getAccountMngrs <em>Account Mngrs</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankGateway#getSessions <em>Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankGateway#getSessionsState <em>Sessions State</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankGateway()
 * @model
 * @generated
 */
public interface BankGateway extends Component {
	/**
	 * Returns the value of the '<em><b>AT Ms</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.ATMController}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.ATMController#getGateway <em>Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Ms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Ms</em>' reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankGateway_ATMs()
	 * @see dk.ingi.petur.bankingsystem.ATMController#getGateway
	 * @model opposite="gateway"
	 * @generated
	 */
	EList<ATMController> getATMs();

	/**
	 * Returns the value of the '<em><b>Swift</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.SwiftNetwork#getGateways <em>Gateways</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Swift</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Swift</em>' reference.
	 * @see #setSwift(SwiftNetwork)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankGateway_Swift()
	 * @see dk.ingi.petur.bankingsystem.SwiftNetwork#getGateways
	 * @model opposite="gateways"
	 * @generated
	 */
	SwiftNetwork getSwift();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.BankGateway#getSwift <em>Swift</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Swift</em>' reference.
	 * @see #getSwift()
	 * @generated
	 */
	void setSwift(SwiftNetwork value);

	/**
	 * Returns the value of the '<em><b>Account Mngrs</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.AccountMngr}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.AccountMngr#getGateway <em>Gateway</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Account Mngrs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Account Mngrs</em>' reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankGateway_AccountMngrs()
	 * @see dk.ingi.petur.bankingsystem.AccountMngr#getGateway
	 * @model opposite="gateway"
	 * @generated
	 */
	EList<AccountMngr> getAccountMngrs();

	/**
	 * Returns the value of the '<em><b>Sessions</b></em>' map.
	 * The key is of type {@link dk.ingi.petur.bankingsystem.Session},
	 * and the value is of type {@link dk.ingi.petur.bankingsystem.AccountMngr},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sessions</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sessions</em>' map.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankGateway_Sessions()
	 * @model mapType="dk.ingi.petur.bankingsystem.SessionToAccountMngrMap<dk.ingi.petur.bankingsystem.Session, dk.ingi.petur.bankingsystem.AccountMngr>"
	 * @generated
	 */
	EMap<Session, AccountMngr> getSessions();

	/**
	 * Returns the value of the '<em><b>Sessions State</b></em>' map.
	 * The key is of type {@link dk.ingi.petur.bankingsystem.Session},
	 * and the value is of type {@link dk.ingi.petur.bankingsystem.SessionState},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sessions State</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sessions State</em>' map.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankGateway_SessionsState()
	 * @model mapType="dk.ingi.petur.bankingsystem.SessionToSessionStateMap<dk.ingi.petur.bankingsystem.Session, dk.ingi.petur.bankingsystem.SessionState>"
	 * @generated
	 */
	EMap<Session, SessionState> getSessionsState();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	WithdrawMoneyStatus withdraw(int amount, Session session);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SessionOpening openSession(int card, AccountMngr issuer, ATMController controller);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SessionTransactionState commitSession(Session session);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SessionTransactionState cancelSession(Session session);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exitSession();

} // BankGateway
