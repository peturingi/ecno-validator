/**
 */
package dk.ingi.petur.bankingsystem;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Inserted Card Status</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getInsertedCardStatus()
 * @model instanceClass="dk.ingi.petur.bankingsystem.InsertedCardStatus"
 * @generated
 */
public enum InsertedCardStatus implements Enumerator {
	/**
	 * The '<em><b>OK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OK_VALUE
	 * @generated
	 * @ordered
	 */
	OK(0, "OK", "OK"),

	/**
	 * The '<em><b>UNRECOGNIZED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNRECOGNIZED_VALUE
	 * @generated
	 * @ordered
	 */
	UNRECOGNIZED(1, "UNRECOGNIZED", "UNRECOGNIZED"),

	/**
	 * The '<em><b>UNREADABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNREADABLE_VALUE
	 * @generated
	 * @ordered
	 */
	UNREADABLE(2, "UNREADABLE", "UNREADABLE"),

	/**
	 * The '<em><b>EXPIRED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXPIRED_VALUE
	 * @generated
	 * @ordered
	 */
	EXPIRED(3, "EXPIRED", "EXPIRED"),

	/**
	 * The '<em><b>INVALID PIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INVALID_PIN_VALUE
	 * @generated
	 * @ordered
	 */
	INVALID_PIN(4, "INVALID_PIN", "INVALID_PIN"),

	/**
	 * The '<em><b>RETAIN CARD 3PIN ERROR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RETAIN_CARD_3PIN_ERROR_VALUE
	 * @generated
	 * @ordered
	 */
	RETAIN_CARD_3PIN_ERROR(5, "RETAIN_CARD_3_PIN_ERROR", "RETAIN_CARD_3_PIN_ERROR"),

	/**
	 * The '<em><b>NO CARD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_CARD_VALUE
	 * @generated
	 * @ordered
	 */
	NO_CARD(6, "NO_CARD", "NO_CARD");

	/**
	 * The '<em><b>OK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OK_VALUE = 0;

	/**
	 * The '<em><b>UNRECOGNIZED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNRECOGNIZED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNRECOGNIZED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNRECOGNIZED_VALUE = 1;

	/**
	 * The '<em><b>UNREADABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNREADABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNREADABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNREADABLE_VALUE = 2;

	/**
	 * The '<em><b>EXPIRED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXPIRED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXPIRED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXPIRED_VALUE = 3;

	/**
	 * The '<em><b>INVALID PIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INVALID PIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INVALID_PIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INVALID_PIN_VALUE = 4;

	/**
	 * The '<em><b>RETAIN CARD 3PIN ERROR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RETAIN CARD 3PIN ERROR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RETAIN_CARD_3PIN_ERROR
	 * @model name="RETAIN_CARD_3_PIN_ERROR"
	 * @generated
	 * @ordered
	 */
	public static final int RETAIN_CARD_3PIN_ERROR_VALUE = 5;

	/**
	 * The '<em><b>NO CARD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NO CARD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_CARD
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NO_CARD_VALUE = 6;

	/**
	 * An array of all the '<em><b>Inserted Card Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final InsertedCardStatus[] VALUES_ARRAY =
		new InsertedCardStatus[] {
			OK,
			UNRECOGNIZED,
			UNREADABLE,
			EXPIRED,
			INVALID_PIN,
			RETAIN_CARD_3PIN_ERROR,
			NO_CARD,
		};

	/**
	 * A public read-only list of all the '<em><b>Inserted Card Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<InsertedCardStatus> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Inserted Card Status</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static InsertedCardStatus get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			InsertedCardStatus result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Inserted Card Status</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static InsertedCardStatus getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			InsertedCardStatus result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Inserted Card Status</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static InsertedCardStatus get(int value) {
		switch (value) {
			case OK_VALUE: return OK;
			case UNRECOGNIZED_VALUE: return UNRECOGNIZED;
			case UNREADABLE_VALUE: return UNREADABLE;
			case EXPIRED_VALUE: return EXPIRED;
			case INVALID_PIN_VALUE: return INVALID_PIN;
			case RETAIN_CARD_3PIN_ERROR_VALUE: return RETAIN_CARD_3PIN_ERROR;
			case NO_CARD_VALUE: return NO_CARD;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private InsertedCardStatus(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //InsertedCardStatus
