/**
 */
package dk.ingi.petur.bankingsystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session Opening</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.SessionOpening#getOpeningStatus <em>Opening Status</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.SessionOpening#getSession <em>Session</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionOpening()
 * @model
 * @generated
 */
public interface SessionOpening extends Component {
	/**
	 * Returns the value of the '<em><b>Opening Status</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.ingi.petur.bankingsystem.SessionOpeningStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opening Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opening Status</em>' attribute.
	 * @see dk.ingi.petur.bankingsystem.SessionOpeningStatus
	 * @see #setOpeningStatus(SessionOpeningStatus)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionOpening_OpeningStatus()
	 * @model
	 * @generated
	 */
	SessionOpeningStatus getOpeningStatus();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.SessionOpening#getOpeningStatus <em>Opening Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opening Status</em>' attribute.
	 * @see dk.ingi.petur.bankingsystem.SessionOpeningStatus
	 * @see #getOpeningStatus()
	 * @generated
	 */
	void setOpeningStatus(SessionOpeningStatus value);

	/**
	 * Returns the value of the '<em><b>Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Session</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session</em>' reference.
	 * @see #setSession(Session)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSessionOpening_Session()
	 * @model
	 * @generated
	 */
	Session getSession();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.SessionOpening#getSession <em>Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Session</em>' reference.
	 * @see #getSession()
	 * @generated
	 */
	void setSession(Session value);

} // SessionOpening
