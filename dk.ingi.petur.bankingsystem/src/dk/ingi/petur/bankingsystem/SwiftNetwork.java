/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Swift Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.SwiftNetwork#getGateways <em>Gateways</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSwiftNetwork()
 * @model
 * @generated
 */
public interface SwiftNetwork extends Component {
	/**
	 * Returns the value of the '<em><b>Gateways</b></em>' reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.BankGateway}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.BankGateway#getSwift <em>Swift</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gateways</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gateways</em>' reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getSwiftNetwork_Gateways()
	 * @see dk.ingi.petur.bankingsystem.BankGateway#getSwift
	 * @model opposite="swift"
	 * @generated
	 */
	EList<BankGateway> getGateways();

} // SwiftNetwork
