/**
 */
package dk.ingi.petur.bankingsystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bank Card</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankCard#getOwner <em>Owner</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankCard#getNumber <em>Number</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankCard#getPhysical <em>Physical</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.BankCard#getAccount <em>Account</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankCard()
 * @model
 * @generated
 */
public interface BankCard extends Component {
	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(Customer)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankCard_Owner()
	 * @model
	 * @generated
	 */
	Customer getOwner();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.BankCard#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Customer value);

	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankCard_Number()
	 * @model
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.BankCard#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * Returns the value of the '<em><b>Physical</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical</em>' reference.
	 * @see #setPhysical(PhysicalBankCard)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankCard_Physical()
	 * @model
	 * @generated
	 */
	PhysicalBankCard getPhysical();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.BankCard#getPhysical <em>Physical</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical</em>' reference.
	 * @see #getPhysical()
	 * @generated
	 */
	void setPhysical(PhysicalBankCard value);

	/**
	 * Returns the value of the '<em><b>Account</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.Account#getCards <em>Cards</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Account</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Account</em>' container reference.
	 * @see #setAccount(Account)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getBankCard_Account()
	 * @see dk.ingi.petur.bankingsystem.Account#getCards
	 * @model opposite="cards" transient="false"
	 * @generated
	 */
	Account getAccount();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.BankCard#getAccount <em>Account</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Account</em>' container reference.
	 * @see #getAccount()
	 * @generated
	 */
	void setAccount(Account value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void createSession();

} // BankCard
