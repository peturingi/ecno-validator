/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import dk.ingi.petur.bankingsystem.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BankingsystemFactoryImpl extends EFactoryImpl implements BankingsystemFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BankingsystemFactory init() {
		try {
			BankingsystemFactory theBankingsystemFactory = (BankingsystemFactory)EPackage.Registry.INSTANCE.getEFactory(BankingsystemPackage.eNS_URI);
			if (theBankingsystemFactory != null) {
				return theBankingsystemFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BankingsystemFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankingsystemFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BankingsystemPackage.NATURAL_PERSON: return createNaturalPerson();
			case BankingsystemPackage.COMPONENT: return createComponent();
			case BankingsystemPackage.PHYSICAL_BANK_CARD: return createPhysicalBankCard();
			case BankingsystemPackage.ACCOUNT_MNGR: return createAccountMngr();
			case BankingsystemPackage.CUSTOMER: return createCustomer();
			case BankingsystemPackage.ACCOUNT: return createAccount();
			case BankingsystemPackage.BANK_CARD: return createBankCard();
			case BankingsystemPackage.BANK_GATEWAY: return createBankGateway();
			case BankingsystemPackage.ATM_CONTROLLER: return createATMController();
			case BankingsystemPackage.HW: return createHW();
			case BankingsystemPackage.BANK_NOTE: return createBankNote();
			case BankingsystemPackage.SESSION: return createSession();
			case BankingsystemPackage.SWIFT_NETWORK: return createSwiftNetwork();
			case BankingsystemPackage.SETTING: return createSetting();
			case BankingsystemPackage.SESSION_TO_ATM_CONTROLLER_MAP: return (EObject)createSessionToATMControllerMap();
			case BankingsystemPackage.SESSION_TO_INTEGER_MAP: return (EObject)createSessionToIntegerMap();
			case BankingsystemPackage.SESSION_OPENING: return createSessionOpening();
			case BankingsystemPackage.INTEGER_TO_SESSION_MAP: return (EObject)createIntegerToSessionMap();
			case BankingsystemPackage.SESSION_TO_ACCOUNT_MNGR_MAP: return (EObject)createSessionToAccountMngrMap();
			case BankingsystemPackage.SESSION_TO_SESSION_STATE_MAP: return (EObject)createSessionToSessionStateMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case BankingsystemPackage.WITHDRAW_MONEY_STATUS:
				return createWithdrawMoneyStatusFromString(eDataType, initialValue);
			case BankingsystemPackage.SESSION_TRANSACTION_STATE:
				return createSessionTransactionStateFromString(eDataType, initialValue);
			case BankingsystemPackage.INSERTED_CARD_STATUS:
				return createInsertedCardStatusFromString(eDataType, initialValue);
			case BankingsystemPackage.OPERATION_CHOICE_STATUS:
				return createOperationChoiceStatusFromString(eDataType, initialValue);
			case BankingsystemPackage.OPERATIONS:
				return createOperationsFromString(eDataType, initialValue);
			case BankingsystemPackage.SESSION_STATE:
				return createSessionStateFromString(eDataType, initialValue);
			case BankingsystemPackage.SESSION_OPENING_STATUS:
				return createSessionOpeningStatusFromString(eDataType, initialValue);
			case BankingsystemPackage.NOT_MY_PHYSICAL_CARD_EXCEPTION:
				return createNotMyPhysicalCardExceptionFromString(eDataType, initialValue);
			case BankingsystemPackage.NOT_IN_FRONT_OF_ATM_HARDWARE_EXCEPTION:
				return createNotInFrontOfATMHardwareExceptionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case BankingsystemPackage.WITHDRAW_MONEY_STATUS:
				return convertWithdrawMoneyStatusToString(eDataType, instanceValue);
			case BankingsystemPackage.SESSION_TRANSACTION_STATE:
				return convertSessionTransactionStateToString(eDataType, instanceValue);
			case BankingsystemPackage.INSERTED_CARD_STATUS:
				return convertInsertedCardStatusToString(eDataType, instanceValue);
			case BankingsystemPackage.OPERATION_CHOICE_STATUS:
				return convertOperationChoiceStatusToString(eDataType, instanceValue);
			case BankingsystemPackage.OPERATIONS:
				return convertOperationsToString(eDataType, instanceValue);
			case BankingsystemPackage.SESSION_STATE:
				return convertSessionStateToString(eDataType, instanceValue);
			case BankingsystemPackage.SESSION_OPENING_STATUS:
				return convertSessionOpeningStatusToString(eDataType, instanceValue);
			case BankingsystemPackage.NOT_MY_PHYSICAL_CARD_EXCEPTION:
				return convertNotMyPhysicalCardExceptionToString(eDataType, instanceValue);
			case BankingsystemPackage.NOT_IN_FRONT_OF_ATM_HARDWARE_EXCEPTION:
				return convertNotInFrontOfATMHardwareExceptionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NaturalPerson createNaturalPerson() {
		NaturalPersonImpl naturalPerson = new NaturalPersonImpl();
		return naturalPerson;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component createComponent() {
		ComponentImpl component = new ComponentImpl();
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalBankCard createPhysicalBankCard() {
		PhysicalBankCardImpl physicalBankCard = new PhysicalBankCardImpl();
		return physicalBankCard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccountMngr createAccountMngr() {
		AccountMngrImpl accountMngr = new AccountMngrImpl();
		return accountMngr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer createCustomer() {
		CustomerImpl customer = new CustomerImpl();
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Account createAccount() {
		AccountImpl account = new AccountImpl();
		return account;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankCard createBankCard() {
		BankCardImpl bankCard = new BankCardImpl();
		return bankCard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankGateway createBankGateway() {
		BankGatewayImpl bankGateway = new BankGatewayImpl();
		return bankGateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATMController createATMController() {
		ATMControllerImpl atmController = new ATMControllerImpl();
		return atmController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HW createHW() {
		HWImpl hw = new HWImpl();
		return hw;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankNote createBankNote() {
		BankNoteImpl bankNote = new BankNoteImpl();
		return bankNote;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Session createSession() {
		SessionImpl session = new SessionImpl();
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwiftNetwork createSwiftNetwork() {
		SwiftNetworkImpl swiftNetwork = new SwiftNetworkImpl();
		return swiftNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Setting createSetting() {
		SettingImpl setting = new SettingImpl();
		return setting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Session, ATMController> createSessionToATMControllerMap() {
		SessionToATMControllerMapImpl sessionToATMControllerMap = new SessionToATMControllerMapImpl();
		return sessionToATMControllerMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Session, Integer> createSessionToIntegerMap() {
		SessionToIntegerMapImpl sessionToIntegerMap = new SessionToIntegerMapImpl();
		return sessionToIntegerMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionOpening createSessionOpening() {
		SessionOpeningImpl sessionOpening = new SessionOpeningImpl();
		return sessionOpening;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Integer, Session> createIntegerToSessionMap() {
		IntegerToSessionMapImpl integerToSessionMap = new IntegerToSessionMapImpl();
		return integerToSessionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Session, AccountMngr> createSessionToAccountMngrMap() {
		SessionToAccountMngrMapImpl sessionToAccountMngrMap = new SessionToAccountMngrMapImpl();
		return sessionToAccountMngrMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Session, SessionState> createSessionToSessionStateMap() {
		SessionToSessionStateMapImpl sessionToSessionStateMap = new SessionToSessionStateMapImpl();
		return sessionToSessionStateMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WithdrawMoneyStatus createWithdrawMoneyStatusFromString(EDataType eDataType, String initialValue) {
		WithdrawMoneyStatus result = WithdrawMoneyStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWithdrawMoneyStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionTransactionState createSessionTransactionStateFromString(EDataType eDataType, String initialValue) {
		SessionTransactionState result = SessionTransactionState.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSessionTransactionStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InsertedCardStatus createInsertedCardStatusFromString(EDataType eDataType, String initialValue) {
		InsertedCardStatus result = InsertedCardStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInsertedCardStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationChoiceStatus createOperationChoiceStatusFromString(EDataType eDataType, String initialValue) {
		OperationChoiceStatus result = OperationChoiceStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationChoiceStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operations createOperationsFromString(EDataType eDataType, String initialValue) {
		Operations result = Operations.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionState createSessionStateFromString(EDataType eDataType, String initialValue) {
		SessionState result = SessionState.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSessionStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionOpeningStatus createSessionOpeningStatusFromString(EDataType eDataType, String initialValue) {
		SessionOpeningStatus result = SessionOpeningStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSessionOpeningStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotMyPhysicalCardException createNotMyPhysicalCardExceptionFromString(EDataType eDataType, String initialValue) {
		return (NotMyPhysicalCardException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNotMyPhysicalCardExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotInFrontOfATMHardwareException createNotInFrontOfATMHardwareExceptionFromString(EDataType eDataType, String initialValue) {
		return (NotInFrontOfATMHardwareException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNotInFrontOfATMHardwareExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankingsystemPackage getBankingsystemPackage() {
		return (BankingsystemPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BankingsystemPackage getPackage() {
		return BankingsystemPackage.eINSTANCE;
	}

} //BankingsystemFactoryImpl
