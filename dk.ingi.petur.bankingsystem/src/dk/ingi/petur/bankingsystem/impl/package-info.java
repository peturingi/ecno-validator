/**
 * This is an extension of the ECNO-SUT-TestScenarios version by Lom Hillah.
 * 
 * This is a partial implementation of a banking system; this system is not correct in that
 * all of the systems public methods can be used. This system only correctly implements
 * those methods which were used during the development of the ECNO Validator.
 * @author P�tur Ingi Egilsson
 *
 */
package dk.ingi.petur.bankingsystem.impl;