/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.Account;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankCard;
import dk.ingi.petur.bankingsystem.BankGateway;
import dk.ingi.petur.bankingsystem.BankNote;
import dk.ingi.petur.bankingsystem.BankingsystemFactory;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Component;
import dk.ingi.petur.bankingsystem.Customer;
import dk.ingi.petur.bankingsystem.InsertedCardStatus;
import dk.ingi.petur.bankingsystem.NaturalPerson;
import dk.ingi.petur.bankingsystem.OperationChoiceStatus;
import dk.ingi.petur.bankingsystem.Operations;
import dk.ingi.petur.bankingsystem.PhysicalBankCard;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.SessionOpening;
import dk.ingi.petur.bankingsystem.SessionOpeningStatus;
import dk.ingi.petur.bankingsystem.SessionState;
import dk.ingi.petur.bankingsystem.SessionTransactionState;
import dk.ingi.petur.bankingsystem.Setting;
import dk.ingi.petur.bankingsystem.SwiftNetwork;
import dk.ingi.petur.bankingsystem.WithdrawMoneyStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BankingsystemPackageImpl extends EPackageImpl implements BankingsystemPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass naturalPersonEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalBankCardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accountMngrEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accountEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bankCardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bankGatewayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atmControllerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hwEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bankNoteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swiftNetworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass settingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionToATMControllerMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionToIntegerMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionOpeningEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerToSessionMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionToAccountMngrMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionToSessionStateMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum withdrawMoneyStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum sessionTransactionStateEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum insertedCardStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operationChoiceStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operationsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum sessionStateEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum sessionOpeningStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType notMyPhysicalCardExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType notInFrontOfATMHardwareExceptionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BankingsystemPackageImpl() {
		super(eNS_URI, BankingsystemFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BankingsystemPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BankingsystemPackage init() {
		if (isInited) return (BankingsystemPackage)EPackage.Registry.INSTANCE.getEPackage(BankingsystemPackage.eNS_URI);

		// Obtain or create and register package
		BankingsystemPackageImpl theBankingsystemPackage = (BankingsystemPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BankingsystemPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BankingsystemPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theBankingsystemPackage.createPackageContents();

		// Initialize created meta-data
		theBankingsystemPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBankingsystemPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BankingsystemPackage.eNS_URI, theBankingsystemPackage);
		return theBankingsystemPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNaturalPerson() {
		return naturalPersonEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNaturalPerson_AvailableCards() {
		return (EReference)naturalPersonEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNaturalPerson_Uses() {
		return (EReference)naturalPersonEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNaturalPerson_Owns() {
		return (EReference)naturalPersonEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNaturalPerson_Name() {
		return (EAttribute)naturalPersonEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNaturalPerson_TakeMoney() {
		return (EAttribute)naturalPersonEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__InsertCard__PhysicalBankCard() {
		return naturalPersonEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__EnterPin__int() {
		return naturalPersonEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__Withdraw__int() {
		return naturalPersonEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__ConfigureTakeMoney__boolean() {
		return naturalPersonEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__TakeCard() {
		return naturalPersonEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__TakeMoney() {
		return naturalPersonEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNaturalPerson__Exit() {
		return naturalPersonEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponent() {
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalBankCard() {
		return physicalBankCardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalBankCard_Number() {
		return (EAttribute)physicalBankCardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalBankCard_Pin() {
		return (EAttribute)physicalBankCardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalBankCard_Issuer() {
		return (EReference)physicalBankCardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalBankCard_Owner() {
		return (EReference)physicalBankCardEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPhysicalBankCard__InsertCard() {
		return physicalBankCardEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccountMngr() {
		return accountMngrEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_Customers() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_Accounts() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_Gateway() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_Sessions() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_IdleSessions() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccountMngr_Name() {
		return (EAttribute)accountMngrEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_OpenedSessions() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountMngr_Withdrawls() {
		return (EReference)accountMngrEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountMngr__OpenSession__int_ATMController() {
		return accountMngrEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountMngr__Withdraw__int_Session() {
		return accountMngrEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountMngr__CancelSession__Session() {
		return accountMngrEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountMngr__CommitSession__Session() {
		return accountMngrEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountMngr__ExitSession() {
		return accountMngrEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountMngr__ExitSession2() {
		return accountMngrEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomer() {
		return customerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomer_Accounts() {
		return (EReference)customerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomer_Natural() {
		return (EReference)customerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCustomer_Id() {
		return (EAttribute)customerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCustomer_Name() {
		return (EAttribute)customerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccount() {
		return accountEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccount_Cards() {
		return (EReference)accountEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccount_Balance() {
		return (EAttribute)accountEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccount_Holder() {
		return (EReference)accountEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccount__IsAccountFor__int() {
		return accountEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccount__GetBankCard__int() {
		return accountEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccount__Withdraw__int() {
		return accountEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccount__CreateSession__int() {
		return accountEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBankCard() {
		return bankCardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankCard_Owner() {
		return (EReference)bankCardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBankCard_Number() {
		return (EAttribute)bankCardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankCard_Physical() {
		return (EReference)bankCardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankCard_Account() {
		return (EReference)bankCardEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankCard__CreateSession() {
		return bankCardEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBankGateway() {
		return bankGatewayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankGateway_ATMs() {
		return (EReference)bankGatewayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankGateway_Swift() {
		return (EReference)bankGatewayEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankGateway_AccountMngrs() {
		return (EReference)bankGatewayEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankGateway_Sessions() {
		return (EReference)bankGatewayEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBankGateway_SessionsState() {
		return (EReference)bankGatewayEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankGateway__Withdraw__int_Session() {
		return bankGatewayEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankGateway__OpenSession__int_AccountMngr_ATMController() {
		return bankGatewayEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankGateway__CommitSession__Session() {
		return bankGatewayEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankGateway__CancelSession__Session() {
		return bankGatewayEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankGateway__ExitSession() {
		return bankGatewayEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getATMController() {
		return atmControllerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATMController_Hw() {
		return (EReference)atmControllerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATMController_Session() {
		return (EReference)atmControllerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATMController_Gateway() {
		return (EReference)atmControllerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATMController_CurrentCardNum() {
		return (EAttribute)atmControllerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATMController_CurrentIssuer() {
		return (EReference)atmControllerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATMController_NbTentatives() {
		return (EAttribute)atmControllerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATMController_CurrentPin() {
		return (EAttribute)atmControllerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATMController_Sessions() {
		return (EReference)atmControllerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATMController_LastSessionTransState() {
		return (EAttribute)atmControllerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__InsertCard__int_AccountMngr_int() {
		return atmControllerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__WithdrawMoney__int() {
		return atmControllerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__SetPin__int() {
		return atmControllerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__CancelSession__Session() {
		return atmControllerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__CommitSession__int() {
		return atmControllerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__RollbackWithdraw__int() {
		return atmControllerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__TakeMoney__EList() {
		return atmControllerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__PresentMoney__Integer() {
		return atmControllerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__TakeCard() {
		return atmControllerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__Exit() {
		return atmControllerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__ExitSession() {
		return atmControllerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATMController__PresentCard() {
		return atmControllerEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHW() {
		return hwEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHW_Inserted() {
		return (EReference)hwEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHW_Held() {
		return (EReference)hwEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHW_Notes() {
		return (EReference)hwEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHW_Ready() {
		return (EReference)hwEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHW_Show() {
		return (EReference)hwEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHW_Controller() {
		return (EReference)hwEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_IsPinRequested() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_ShowCardOn1InvalidPin() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_RetractCardOn3InvalidPin() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_RetractCardOnTimeOut() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_RetractMoneyOnTimeOut() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_MoneyTaken() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHW_CardTakenBack() {
		return (EAttribute)hwEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__InsertCard() {
		return hwEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__AskPin() {
		return hwEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__Display__String() {
		return hwEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__EnterPin__int() {
		return hwEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__CancelSession__int() {
		return hwEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__PresentCard() {
		return hwEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__ConfigureShowCardOn1InvalidPin() {
		return hwEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__ConfigureRetractCardOn3InvalidPin() {
		return hwEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__ConfigureRetractCardOnTimeOut__long() {
		return hwEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__Operation__Operations() {
		return hwEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__Withdraw__int() {
		return hwEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__ConfigureRetractMoneyOnTimeOut__long() {
		return hwEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__TakeMoney() {
		return hwEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__PresentMoney__Integer() {
		return hwEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getHW__Exit() {
		return hwEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBankNote() {
		return bankNoteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBankNote__TakeMoney() {
		return bankNoteEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSession() {
		return sessionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_Card() {
		return (EReference)sessionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_ATM() {
		return (EReference)sessionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_Owner() {
		return (EReference)sessionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_Account() {
		return (EReference)sessionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_OpeningStatus() {
		return (EReference)sessionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSession__Withdraw__int() {
		return sessionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSession__ExitSession() {
		return sessionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwiftNetwork() {
		return swiftNetworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSwiftNetwork_Gateways() {
		return (EReference)swiftNetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetting() {
		return settingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetting_Objects() {
		return (EReference)settingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionToATMControllerMap() {
		return sessionToATMControllerMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionToATMControllerMap_Key() {
		return (EReference)sessionToATMControllerMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionToATMControllerMap_Value() {
		return (EReference)sessionToATMControllerMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionToIntegerMap() {
		return sessionToIntegerMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionToIntegerMap_Key() {
		return (EReference)sessionToIntegerMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionToIntegerMap_Value() {
		return (EAttribute)sessionToIntegerMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionOpening() {
		return sessionOpeningEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionOpening_OpeningStatus() {
		return (EAttribute)sessionOpeningEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionOpening_Session() {
		return (EReference)sessionOpeningEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerToSessionMap() {
		return integerToSessionMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntegerToSessionMap_Value() {
		return (EReference)integerToSessionMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerToSessionMap_Key() {
		return (EAttribute)integerToSessionMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionToAccountMngrMap() {
		return sessionToAccountMngrMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionToAccountMngrMap_Key() {
		return (EReference)sessionToAccountMngrMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionToAccountMngrMap_Value() {
		return (EReference)sessionToAccountMngrMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionToSessionStateMap() {
		return sessionToSessionStateMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionToSessionStateMap_Key() {
		return (EReference)sessionToSessionStateMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionToSessionStateMap_Value() {
		return (EAttribute)sessionToSessionStateMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getWithdrawMoneyStatus() {
		return withdrawMoneyStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSessionTransactionState() {
		return sessionTransactionStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getInsertedCardStatus() {
		return insertedCardStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperationChoiceStatus() {
		return operationChoiceStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperations() {
		return operationsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSessionState() {
		return sessionStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSessionOpeningStatus() {
		return sessionOpeningStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNotMyPhysicalCardException() {
		return notMyPhysicalCardExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNotInFrontOfATMHardwareException() {
		return notInFrontOfATMHardwareExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankingsystemFactory getBankingsystemFactory() {
		return (BankingsystemFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		naturalPersonEClass = createEClass(NATURAL_PERSON);
		createEReference(naturalPersonEClass, NATURAL_PERSON__AVAILABLE_CARDS);
		createEReference(naturalPersonEClass, NATURAL_PERSON__USES);
		createEReference(naturalPersonEClass, NATURAL_PERSON__OWNS);
		createEAttribute(naturalPersonEClass, NATURAL_PERSON__NAME);
		createEAttribute(naturalPersonEClass, NATURAL_PERSON__TAKE_MONEY);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___INSERT_CARD__PHYSICALBANKCARD);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___ENTER_PIN__INT);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___WITHDRAW__INT);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___CONFIGURE_TAKE_MONEY__BOOLEAN);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___TAKE_CARD);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___TAKE_MONEY);
		createEOperation(naturalPersonEClass, NATURAL_PERSON___EXIT);

		componentEClass = createEClass(COMPONENT);

		physicalBankCardEClass = createEClass(PHYSICAL_BANK_CARD);
		createEAttribute(physicalBankCardEClass, PHYSICAL_BANK_CARD__NUMBER);
		createEAttribute(physicalBankCardEClass, PHYSICAL_BANK_CARD__PIN);
		createEReference(physicalBankCardEClass, PHYSICAL_BANK_CARD__ISSUER);
		createEReference(physicalBankCardEClass, PHYSICAL_BANK_CARD__OWNER);
		createEOperation(physicalBankCardEClass, PHYSICAL_BANK_CARD___INSERT_CARD);

		accountMngrEClass = createEClass(ACCOUNT_MNGR);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__CUSTOMERS);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__ACCOUNTS);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__GATEWAY);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__SESSIONS);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__IDLE_SESSIONS);
		createEAttribute(accountMngrEClass, ACCOUNT_MNGR__NAME);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__OPENED_SESSIONS);
		createEReference(accountMngrEClass, ACCOUNT_MNGR__WITHDRAWLS);
		createEOperation(accountMngrEClass, ACCOUNT_MNGR___OPEN_SESSION__INT_ATMCONTROLLER);
		createEOperation(accountMngrEClass, ACCOUNT_MNGR___WITHDRAW__INT_SESSION);
		createEOperation(accountMngrEClass, ACCOUNT_MNGR___CANCEL_SESSION__SESSION);
		createEOperation(accountMngrEClass, ACCOUNT_MNGR___COMMIT_SESSION__SESSION);
		createEOperation(accountMngrEClass, ACCOUNT_MNGR___EXIT_SESSION);
		createEOperation(accountMngrEClass, ACCOUNT_MNGR___EXIT_SESSION2);

		customerEClass = createEClass(CUSTOMER);
		createEReference(customerEClass, CUSTOMER__ACCOUNTS);
		createEReference(customerEClass, CUSTOMER__NATURAL);
		createEAttribute(customerEClass, CUSTOMER__ID);
		createEAttribute(customerEClass, CUSTOMER__NAME);

		accountEClass = createEClass(ACCOUNT);
		createEReference(accountEClass, ACCOUNT__CARDS);
		createEAttribute(accountEClass, ACCOUNT__BALANCE);
		createEReference(accountEClass, ACCOUNT__HOLDER);
		createEOperation(accountEClass, ACCOUNT___IS_ACCOUNT_FOR__INT);
		createEOperation(accountEClass, ACCOUNT___GET_BANK_CARD__INT);
		createEOperation(accountEClass, ACCOUNT___WITHDRAW__INT);
		createEOperation(accountEClass, ACCOUNT___CREATE_SESSION__INT);

		bankCardEClass = createEClass(BANK_CARD);
		createEReference(bankCardEClass, BANK_CARD__OWNER);
		createEAttribute(bankCardEClass, BANK_CARD__NUMBER);
		createEReference(bankCardEClass, BANK_CARD__PHYSICAL);
		createEReference(bankCardEClass, BANK_CARD__ACCOUNT);
		createEOperation(bankCardEClass, BANK_CARD___CREATE_SESSION);

		bankGatewayEClass = createEClass(BANK_GATEWAY);
		createEReference(bankGatewayEClass, BANK_GATEWAY__AT_MS);
		createEReference(bankGatewayEClass, BANK_GATEWAY__SWIFT);
		createEReference(bankGatewayEClass, BANK_GATEWAY__ACCOUNT_MNGRS);
		createEReference(bankGatewayEClass, BANK_GATEWAY__SESSIONS);
		createEReference(bankGatewayEClass, BANK_GATEWAY__SESSIONS_STATE);
		createEOperation(bankGatewayEClass, BANK_GATEWAY___WITHDRAW__INT_SESSION);
		createEOperation(bankGatewayEClass, BANK_GATEWAY___OPEN_SESSION__INT_ACCOUNTMNGR_ATMCONTROLLER);
		createEOperation(bankGatewayEClass, BANK_GATEWAY___COMMIT_SESSION__SESSION);
		createEOperation(bankGatewayEClass, BANK_GATEWAY___CANCEL_SESSION__SESSION);
		createEOperation(bankGatewayEClass, BANK_GATEWAY___EXIT_SESSION);

		atmControllerEClass = createEClass(ATM_CONTROLLER);
		createEReference(atmControllerEClass, ATM_CONTROLLER__HW);
		createEReference(atmControllerEClass, ATM_CONTROLLER__SESSION);
		createEReference(atmControllerEClass, ATM_CONTROLLER__GATEWAY);
		createEAttribute(atmControllerEClass, ATM_CONTROLLER__CURRENT_CARD_NUM);
		createEReference(atmControllerEClass, ATM_CONTROLLER__CURRENT_ISSUER);
		createEAttribute(atmControllerEClass, ATM_CONTROLLER__NB_TENTATIVES);
		createEAttribute(atmControllerEClass, ATM_CONTROLLER__CURRENT_PIN);
		createEReference(atmControllerEClass, ATM_CONTROLLER__SESSIONS);
		createEAttribute(atmControllerEClass, ATM_CONTROLLER__LAST_SESSION_TRANS_STATE);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___INSERT_CARD__INT_ACCOUNTMNGR_INT);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___WITHDRAW_MONEY__INT);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___SET_PIN__INT);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___CANCEL_SESSION__SESSION);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___COMMIT_SESSION__INT);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___ROLLBACK_WITHDRAW__INT);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___TAKE_MONEY__ELIST);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___PRESENT_MONEY__INTEGER);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___TAKE_CARD);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___EXIT);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___EXIT_SESSION);
		createEOperation(atmControllerEClass, ATM_CONTROLLER___PRESENT_CARD);

		hwEClass = createEClass(HW);
		createEReference(hwEClass, HW__INSERTED);
		createEReference(hwEClass, HW__HELD);
		createEReference(hwEClass, HW__NOTES);
		createEReference(hwEClass, HW__READY);
		createEReference(hwEClass, HW__SHOW);
		createEReference(hwEClass, HW__CONTROLLER);
		createEAttribute(hwEClass, HW__IS_PIN_REQUESTED);
		createEAttribute(hwEClass, HW__SHOW_CARD_ON1_INVALID_PIN);
		createEAttribute(hwEClass, HW__RETRACT_CARD_ON3_INVALID_PIN);
		createEAttribute(hwEClass, HW__RETRACT_CARD_ON_TIME_OUT);
		createEAttribute(hwEClass, HW__RETRACT_MONEY_ON_TIME_OUT);
		createEAttribute(hwEClass, HW__MONEY_TAKEN);
		createEAttribute(hwEClass, HW__CARD_TAKEN_BACK);
		createEOperation(hwEClass, HW___INSERT_CARD);
		createEOperation(hwEClass, HW___ASK_PIN);
		createEOperation(hwEClass, HW___DISPLAY__STRING);
		createEOperation(hwEClass, HW___ENTER_PIN__INT);
		createEOperation(hwEClass, HW___CANCEL_SESSION__INT);
		createEOperation(hwEClass, HW___PRESENT_CARD);
		createEOperation(hwEClass, HW___CONFIGURE_SHOW_CARD_ON1_INVALID_PIN);
		createEOperation(hwEClass, HW___CONFIGURE_RETRACT_CARD_ON3_INVALID_PIN);
		createEOperation(hwEClass, HW___CONFIGURE_RETRACT_CARD_ON_TIME_OUT__LONG);
		createEOperation(hwEClass, HW___OPERATION__OPERATIONS);
		createEOperation(hwEClass, HW___WITHDRAW__INT);
		createEOperation(hwEClass, HW___CONFIGURE_RETRACT_MONEY_ON_TIME_OUT__LONG);
		createEOperation(hwEClass, HW___TAKE_MONEY);
		createEOperation(hwEClass, HW___PRESENT_MONEY__INTEGER);
		createEOperation(hwEClass, HW___EXIT);

		bankNoteEClass = createEClass(BANK_NOTE);
		createEOperation(bankNoteEClass, BANK_NOTE___TAKE_MONEY);

		sessionEClass = createEClass(SESSION);
		createEReference(sessionEClass, SESSION__CARD);
		createEReference(sessionEClass, SESSION__ATM);
		createEReference(sessionEClass, SESSION__OWNER);
		createEReference(sessionEClass, SESSION__ACCOUNT);
		createEReference(sessionEClass, SESSION__OPENING_STATUS);
		createEOperation(sessionEClass, SESSION___WITHDRAW__INT);
		createEOperation(sessionEClass, SESSION___EXIT_SESSION);

		swiftNetworkEClass = createEClass(SWIFT_NETWORK);
		createEReference(swiftNetworkEClass, SWIFT_NETWORK__GATEWAYS);

		settingEClass = createEClass(SETTING);
		createEReference(settingEClass, SETTING__OBJECTS);

		sessionToATMControllerMapEClass = createEClass(SESSION_TO_ATM_CONTROLLER_MAP);
		createEReference(sessionToATMControllerMapEClass, SESSION_TO_ATM_CONTROLLER_MAP__KEY);
		createEReference(sessionToATMControllerMapEClass, SESSION_TO_ATM_CONTROLLER_MAP__VALUE);

		sessionToIntegerMapEClass = createEClass(SESSION_TO_INTEGER_MAP);
		createEReference(sessionToIntegerMapEClass, SESSION_TO_INTEGER_MAP__KEY);
		createEAttribute(sessionToIntegerMapEClass, SESSION_TO_INTEGER_MAP__VALUE);

		sessionOpeningEClass = createEClass(SESSION_OPENING);
		createEAttribute(sessionOpeningEClass, SESSION_OPENING__OPENING_STATUS);
		createEReference(sessionOpeningEClass, SESSION_OPENING__SESSION);

		integerToSessionMapEClass = createEClass(INTEGER_TO_SESSION_MAP);
		createEReference(integerToSessionMapEClass, INTEGER_TO_SESSION_MAP__VALUE);
		createEAttribute(integerToSessionMapEClass, INTEGER_TO_SESSION_MAP__KEY);

		sessionToAccountMngrMapEClass = createEClass(SESSION_TO_ACCOUNT_MNGR_MAP);
		createEReference(sessionToAccountMngrMapEClass, SESSION_TO_ACCOUNT_MNGR_MAP__KEY);
		createEReference(sessionToAccountMngrMapEClass, SESSION_TO_ACCOUNT_MNGR_MAP__VALUE);

		sessionToSessionStateMapEClass = createEClass(SESSION_TO_SESSION_STATE_MAP);
		createEReference(sessionToSessionStateMapEClass, SESSION_TO_SESSION_STATE_MAP__KEY);
		createEAttribute(sessionToSessionStateMapEClass, SESSION_TO_SESSION_STATE_MAP__VALUE);

		// Create enums
		withdrawMoneyStatusEEnum = createEEnum(WITHDRAW_MONEY_STATUS);
		sessionTransactionStateEEnum = createEEnum(SESSION_TRANSACTION_STATE);
		insertedCardStatusEEnum = createEEnum(INSERTED_CARD_STATUS);
		operationChoiceStatusEEnum = createEEnum(OPERATION_CHOICE_STATUS);
		operationsEEnum = createEEnum(OPERATIONS);
		sessionStateEEnum = createEEnum(SESSION_STATE);
		sessionOpeningStatusEEnum = createEEnum(SESSION_OPENING_STATUS);

		// Create data types
		notMyPhysicalCardExceptionEDataType = createEDataType(NOT_MY_PHYSICAL_CARD_EXCEPTION);
		notInFrontOfATMHardwareExceptionEDataType = createEDataType(NOT_IN_FRONT_OF_ATM_HARDWARE_EXCEPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		naturalPersonEClass.getESuperTypes().add(this.getComponent());
		physicalBankCardEClass.getESuperTypes().add(this.getComponent());
		accountMngrEClass.getESuperTypes().add(this.getComponent());
		customerEClass.getESuperTypes().add(this.getComponent());
		accountEClass.getESuperTypes().add(this.getComponent());
		bankCardEClass.getESuperTypes().add(this.getComponent());
		bankGatewayEClass.getESuperTypes().add(this.getComponent());
		atmControllerEClass.getESuperTypes().add(this.getComponent());
		hwEClass.getESuperTypes().add(this.getComponent());
		bankNoteEClass.getESuperTypes().add(this.getComponent());
		sessionEClass.getESuperTypes().add(this.getComponent());
		swiftNetworkEClass.getESuperTypes().add(this.getComponent());
		sessionOpeningEClass.getESuperTypes().add(this.getComponent());

		// Initialize classes, features, and operations; add parameters
		initEClass(naturalPersonEClass, NaturalPerson.class, "NaturalPerson", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNaturalPerson_AvailableCards(), this.getPhysicalBankCard(), this.getPhysicalBankCard_Owner(), "availableCards", null, 0, -1, NaturalPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNaturalPerson_Uses(), this.getHW(), null, "uses", null, 0, -1, NaturalPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNaturalPerson_Owns(), this.getBankNote(), null, "owns", null, 0, -1, NaturalPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNaturalPerson_Name(), ecorePackage.getEString(), "name", null, 0, 1, NaturalPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNaturalPerson_TakeMoney(), ecorePackage.getEBoolean(), "takeMoney", null, 0, 1, NaturalPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getNaturalPerson__InsertCard__PhysicalBankCard(), this.getInsertedCardStatus(), "insertCard", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPhysicalBankCard(), "card", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getNotInFrontOfATMHardwareException());
		addEException(op, this.getNotMyPhysicalCardException());

		op = initEOperation(getNaturalPerson__EnterPin__int(), this.getInsertedCardStatus(), "enterPin", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pin", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getNaturalPerson__Withdraw__int(), this.getWithdrawMoneyStatus(), "withdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getNaturalPerson__ConfigureTakeMoney__boolean(), null, "configureTakeMoney", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "b", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getNaturalPerson__TakeCard(), null, "takeCard", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getNaturalPerson__TakeMoney(), null, "takeMoney", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getNaturalPerson__Exit(), null, "exit", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(componentEClass, Component.class, "Component", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(physicalBankCardEClass, PhysicalBankCard.class, "PhysicalBankCard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPhysicalBankCard_Number(), ecorePackage.getEInt(), "number", null, 1, 1, PhysicalBankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalBankCard_Pin(), ecorePackage.getEInt(), "pin", null, 0, 1, PhysicalBankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalBankCard_Issuer(), this.getAccountMngr(), null, "issuer", null, 0, 1, PhysicalBankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalBankCard_Owner(), this.getNaturalPerson(), this.getNaturalPerson_AvailableCards(), "owner", null, 0, 1, PhysicalBankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getPhysicalBankCard__InsertCard(), null, "insertCard", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(accountMngrEClass, AccountMngr.class, "AccountMngr", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccountMngr_Customers(), this.getCustomer(), null, "customers", null, 0, -1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccountMngr_Accounts(), this.getAccount(), null, "accounts", null, 0, -1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccountMngr_Gateway(), this.getBankGateway(), this.getBankGateway_AccountMngrs(), "gateway", null, 1, 1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccountMngr_Sessions(), this.getSession(), this.getSession_Owner(), "sessions", null, 0, -1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccountMngr_IdleSessions(), this.getSession(), null, "idleSessions", null, 0, -1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAccountMngr_Name(), ecorePackage.getEString(), "name", null, 0, 1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccountMngr_OpenedSessions(), this.getSessionToATMControllerMap(), null, "openedSessions", null, 0, -1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccountMngr_Withdrawls(), this.getSessionToIntegerMap(), null, "withdrawls", null, 0, -1, AccountMngr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAccountMngr__OpenSession__int_ATMController(), this.getSessionOpening(), "openSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getATMController(), "controller", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccountMngr__Withdraw__int_Session(), this.getWithdrawMoneyStatus(), "withdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "session", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccountMngr__CancelSession__Session(), this.getSessionTransactionState(), "cancelSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "session", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccountMngr__CommitSession__Session(), this.getSessionTransactionState(), "commitSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "session", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAccountMngr__ExitSession(), null, "exitSession", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAccountMngr__ExitSession2(), null, "exitSession2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(customerEClass, Customer.class, "Customer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCustomer_Accounts(), this.getAccount(), this.getAccount_Holder(), "accounts", null, 0, -1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCustomer_Natural(), this.getNaturalPerson(), null, "natural", null, 0, 1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCustomer_Id(), ecorePackage.getEInt(), "id", null, 0, 1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCustomer_Name(), ecorePackage.getEString(), "name", null, 0, 1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(accountEClass, Account.class, "Account", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccount_Cards(), this.getBankCard(), this.getBankCard_Account(), "cards", null, 0, -1, Account.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAccount_Balance(), ecorePackage.getEInt(), "balance", "0", 1, 1, Account.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccount_Holder(), this.getCustomer(), this.getCustomer_Accounts(), "holder", null, 0, -1, Account.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAccount__IsAccountFor__int(), ecorePackage.getEBoolean(), "isAccountFor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "physicalCardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccount__GetBankCard__int(), this.getBankCard(), "getBankCard", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "physicalCardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccount__Withdraw__int(), null, "withdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccount__CreateSession__int(), null, "createSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(bankCardEClass, BankCard.class, "BankCard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBankCard_Owner(), this.getCustomer(), null, "owner", null, 0, 1, BankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBankCard_Number(), ecorePackage.getEInt(), "number", null, 0, 1, BankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBankCard_Physical(), this.getPhysicalBankCard(), null, "physical", null, 0, 1, BankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBankCard_Account(), this.getAccount(), this.getAccount_Cards(), "account", null, 0, 1, BankCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBankCard__CreateSession(), null, "createSession", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(bankGatewayEClass, BankGateway.class, "BankGateway", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBankGateway_ATMs(), this.getATMController(), this.getATMController_Gateway(), "ATMs", null, 0, -1, BankGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBankGateway_Swift(), this.getSwiftNetwork(), this.getSwiftNetwork_Gateways(), "swift", null, 0, 1, BankGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBankGateway_AccountMngrs(), this.getAccountMngr(), this.getAccountMngr_Gateway(), "accountMngrs", null, 0, -1, BankGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBankGateway_Sessions(), this.getSessionToAccountMngrMap(), null, "sessions", null, 0, -1, BankGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBankGateway_SessionsState(), this.getSessionToSessionStateMap(), null, "sessionsState", null, 0, -1, BankGateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getBankGateway__Withdraw__int_Session(), this.getWithdrawMoneyStatus(), "withdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "session", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBankGateway__OpenSession__int_AccountMngr_ATMController(), this.getSessionOpening(), "openSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "card", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccountMngr(), "issuer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getATMController(), "controller", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBankGateway__CommitSession__Session(), this.getSessionTransactionState(), "commitSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "session", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBankGateway__CancelSession__Session(), this.getSessionTransactionState(), "cancelSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "session", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getBankGateway__ExitSession(), null, "exitSession", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(atmControllerEClass, ATMController.class, "ATMController", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getATMController_Hw(), this.getHW(), this.getHW_Controller(), "hw", null, 1, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getATMController_Session(), this.getSession(), null, "session", null, 0, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getATMController_Gateway(), this.getBankGateway(), this.getBankGateway_ATMs(), "gateway", null, 1, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getATMController_CurrentCardNum(), ecorePackage.getEInt(), "currentCardNum", null, 0, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getATMController_CurrentIssuer(), this.getAccountMngr(), null, "currentIssuer", null, 0, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getATMController_NbTentatives(), ecorePackage.getEInt(), "nbTentatives", null, 0, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getATMController_CurrentPin(), ecorePackage.getEInt(), "currentPin", null, 0, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getATMController_Sessions(), this.getIntegerToSessionMap(), null, "sessions", null, 0, -1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getATMController_LastSessionTransState(), this.getSessionTransactionState(), "lastSessionTransState", null, 0, 1, ATMController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getATMController__InsertCard__int_AccountMngr_int(), null, "insertCard", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccountMngr(), "issuer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pin", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__WithdrawMoney__int(), this.getWithdrawMoneyStatus(), "withdrawMoney", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__SetPin__int(), this.getInsertedCardStatus(), "setPin", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "enteredPin", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__CancelSession__Session(), null, "cancelSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSession(), "s", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__CommitSession__int(), this.getSessionTransactionState(), "commitSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__RollbackWithdraw__int(), this.getSessionTransactionState(), "rollbackWithdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cardNumber", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__TakeMoney__EList(), null, "takeMoney", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBankNote(), "money", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getATMController__PresentMoney__Integer(), null, "presentMoney", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getATMController__TakeCard(), null, "takeCard", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getATMController__Exit(), null, "exit", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getATMController__ExitSession(), null, "exitSession", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getATMController__PresentCard(), null, "presentCard", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(hwEClass, dk.ingi.petur.bankingsystem.HW.class, "HW", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHW_Inserted(), this.getPhysicalBankCard(), null, "inserted", null, 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHW_Held(), this.getPhysicalBankCard(), null, "held", null, 0, -1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHW_Notes(), this.getBankNote(), null, "notes", null, 0, -1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHW_Ready(), this.getBankNote(), null, "ready", null, 0, -1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHW_Show(), this.getPhysicalBankCard(), null, "show", null, 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHW_Controller(), this.getATMController(), this.getATMController_Hw(), "controller", null, 1, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_IsPinRequested(), ecorePackage.getEBoolean(), "isPinRequested", null, 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_ShowCardOn1InvalidPin(), ecorePackage.getEBoolean(), "showCardOn1InvalidPin", null, 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_RetractCardOn3InvalidPin(), ecorePackage.getEBoolean(), "retractCardOn3InvalidPin", null, 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_RetractCardOnTimeOut(), ecorePackage.getELong(), "retractCardOnTimeOut", "5000", 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_RetractMoneyOnTimeOut(), ecorePackage.getELong(), "retractMoneyOnTimeOut", "3000", 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_MoneyTaken(), ecorePackage.getEBoolean(), "moneyTaken", null, 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHW_CardTakenBack(), ecorePackage.getEBoolean(), "cardTakenBack", "false", 0, 1, dk.ingi.petur.bankingsystem.HW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getHW__InsertCard(), null, "insertCard", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getHW__AskPin(), null, "askPin", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__Display__String(), null, "display", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "string", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__EnterPin__int(), this.getInsertedCardStatus(), "enterPin", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pin", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__CancelSession__int(), null, "cancelSession", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cardNum", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getHW__PresentCard(), null, "presentCard", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getHW__ConfigureShowCardOn1InvalidPin(), null, "configureShowCardOn1InvalidPin", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getHW__ConfigureRetractCardOn3InvalidPin(), null, "configureRetractCardOn3InvalidPin", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__ConfigureRetractCardOnTimeOut__long(), null, "configureRetractCardOnTimeOut", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getELong(), "timeOutMillisec", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__Operation__Operations(), this.getOperationChoiceStatus(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOperations(), "op", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__Withdraw__int(), this.getWithdrawMoneyStatus(), "withdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "i", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__ConfigureRetractMoneyOnTimeOut__long(), null, "configureRetractMoneyOnTimeOut", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getELong(), "timeOutMillisec", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getHW__TakeMoney(), this.getBankNote(), "takeMoney", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getHW__PresentMoney__Integer(), null, "presentMoney", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getHW__Exit(), null, "exit", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(bankNoteEClass, BankNote.class, "BankNote", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getBankNote__TakeMoney(), null, "takeMoney", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(sessionEClass, Session.class, "Session", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSession_Card(), this.getBankCard(), null, "card", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSession_ATM(), this.getATMController(), null, "ATM", null, 1, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSession_Owner(), this.getAccountMngr(), this.getAccountMngr_Sessions(), "owner", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSession_Account(), this.getAccount(), null, "account", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSession_OpeningStatus(), this.getSessionOpening(), null, "openingStatus", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getSession__Withdraw__int(), null, "withdraw", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "amount", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getSession__ExitSession(), null, "exitSession", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(swiftNetworkEClass, SwiftNetwork.class, "SwiftNetwork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSwiftNetwork_Gateways(), this.getBankGateway(), this.getBankGateway_Swift(), "gateways", null, 0, -1, SwiftNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(settingEClass, Setting.class, "Setting", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSetting_Objects(), ecorePackage.getEObject(), null, "objects", null, 0, -1, Setting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sessionToATMControllerMapEClass, Map.Entry.class, "SessionToATMControllerMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSessionToATMControllerMap_Key(), this.getSession(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSessionToATMControllerMap_Value(), this.getATMController(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sessionToIntegerMapEClass, Map.Entry.class, "SessionToIntegerMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSessionToIntegerMap_Key(), this.getSession(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSessionToIntegerMap_Value(), ecorePackage.getEIntegerObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sessionOpeningEClass, SessionOpening.class, "SessionOpening", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSessionOpening_OpeningStatus(), this.getSessionOpeningStatus(), "openingStatus", null, 0, 1, SessionOpening.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSessionOpening_Session(), this.getSession(), null, "session", null, 0, 1, SessionOpening.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerToSessionMapEClass, Map.Entry.class, "IntegerToSessionMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntegerToSessionMap_Value(), this.getSession(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntegerToSessionMap_Key(), ecorePackage.getEIntegerObject(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sessionToAccountMngrMapEClass, Map.Entry.class, "SessionToAccountMngrMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSessionToAccountMngrMap_Key(), this.getSession(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSessionToAccountMngrMap_Value(), this.getAccountMngr(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sessionToSessionStateMapEClass, Map.Entry.class, "SessionToSessionStateMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSessionToSessionStateMap_Key(), this.getSession(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSessionToSessionStateMap_Value(), this.getSessionState(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.class, "WithdrawMoneyStatus");
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.NO_SESSION);
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.DISPENSE_MONEY);
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.INSUFFICIENT_BALANCE);
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.NETWORK_ERROR);
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.SESSION_ERROR);
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.TAKE_MONEY);
		addEEnumLiteral(withdrawMoneyStatusEEnum, WithdrawMoneyStatus.UNAVAILABLE_OP);

		initEEnum(sessionTransactionStateEEnum, SessionTransactionState.class, "SessionTransactionState");
		addEEnumLiteral(sessionTransactionStateEEnum, SessionTransactionState.COMMITTED);
		addEEnumLiteral(sessionTransactionStateEEnum, SessionTransactionState.CANCELLED);
		addEEnumLiteral(sessionTransactionStateEEnum, SessionTransactionState.NO_SUCH_SESSION);

		initEEnum(insertedCardStatusEEnum, InsertedCardStatus.class, "InsertedCardStatus");
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.OK);
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.UNRECOGNIZED);
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.UNREADABLE);
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.EXPIRED);
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.INVALID_PIN);
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.RETAIN_CARD_3PIN_ERROR);
		addEEnumLiteral(insertedCardStatusEEnum, InsertedCardStatus.NO_CARD);

		initEEnum(operationChoiceStatusEEnum, OperationChoiceStatus.class, "OperationChoiceStatus");
		addEEnumLiteral(operationChoiceStatusEEnum, OperationChoiceStatus.AVAILABLE);
		addEEnumLiteral(operationChoiceStatusEEnum, OperationChoiceStatus.UNAVAILABLE);

		initEEnum(operationsEEnum, Operations.class, "Operations");
		addEEnumLiteral(operationsEEnum, Operations.WITHDRAW);
		addEEnumLiteral(operationsEEnum, Operations.GET_BALANCE);

		initEEnum(sessionStateEEnum, SessionState.class, "SessionState");
		addEEnumLiteral(sessionStateEEnum, SessionState.IDLE);
		addEEnumLiteral(sessionStateEEnum, SessionState.ONGOING_OPERATION);
		addEEnumLiteral(sessionStateEEnum, SessionState.TIMEOUT);
		addEEnumLiteral(sessionStateEEnum, SessionState.CANCELLED);
		addEEnumLiteral(sessionStateEEnum, SessionState.COMMITTED);

		initEEnum(sessionOpeningStatusEEnum, SessionOpeningStatus.class, "SessionOpeningStatus");
		addEEnumLiteral(sessionOpeningStatusEEnum, SessionOpeningStatus.SESSION_OPENED);
		addEEnumLiteral(sessionOpeningStatusEEnum, SessionOpeningStatus.UNKNOWN_CUSTOMER);
		addEEnumLiteral(sessionOpeningStatusEEnum, SessionOpeningStatus.SESSION_ERROR);
		addEEnumLiteral(sessionOpeningStatusEEnum, SessionOpeningStatus.UNAUTHORIZED_ATM);

		// Initialize data types
		initEDataType(notMyPhysicalCardExceptionEDataType, NotMyPhysicalCardException.class, "NotMyPhysicalCardException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(notInFrontOfATMHardwareExceptionEDataType, NotInFrontOfATMHardwareException.class, "NotInFrontOfATMHardwareException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //BankingsystemPackageImpl
