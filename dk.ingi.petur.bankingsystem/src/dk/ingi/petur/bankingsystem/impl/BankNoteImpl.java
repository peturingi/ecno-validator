/**
 */
package dk.ingi.petur.bankingsystem.impl;

import org.eclipse.emf.ecore.EClass;

import dk.ingi.petur.bankingsystem.BankNote;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bank Note</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BankNoteImpl extends ComponentImpl implements BankNote {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BankNoteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.BANK_NOTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void takeMoney() {
		// Intentionally blank.
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.BANK_NOTE___TAKE_MONEY:
				takeMoney();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //BankNoteImpl
