/**
 * 
 */
package dk.ingi.petur.bankingsystem.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.Account;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankGateway;
import dk.ingi.petur.bankingsystem.BankingsystemFactory;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Customer;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.SessionOpening;
import dk.ingi.petur.bankingsystem.SessionOpeningStatus;
import dk.ingi.petur.bankingsystem.SessionTransactionState;
import dk.ingi.petur.bankingsystem.WithdrawMoneyStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Account Mngr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getCustomers <em>Customers</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getAccounts <em>Accounts</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getGateway <em>Gateway</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getSessions <em>Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getIdleSessions <em>Idle Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getOpenedSessions <em>Opened Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountMngrImpl#getWithdrawls <em>Withdrawls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccountMngrImpl extends ComponentImpl implements AccountMngr {
	/**
	 * The cached value of the '{@link #getCustomers() <em>Customers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomers()
	 * @generated
	 * @ordered
	 */
	protected EList<Customer> customers;

	/**
	 * The cached value of the '{@link #getAccounts() <em>Accounts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccounts()
	 * @generated
	 * @ordered
	 */
	protected EList<Account> accounts;

	/**
	 * The cached value of the '{@link #getGateway() <em>Gateway</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGateway()
	 * @generated
	 * @ordered
	 */
	protected BankGateway gateway;

	/**
	 * The cached value of the '{@link #getSessions() <em>Sessions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessions()
	 * @generated
	 * @ordered
	 */
	protected EList<Session> sessions;

	/**
	 * The cached value of the '{@link #getIdleSessions() <em>Idle Sessions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdleSessions()
	 * @generated
	 * @ordered
	 */
	protected EList<Session> idleSessions;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOpenedSessions() <em>Opened Sessions</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpenedSessions()
	 * @generated
	 * @ordered
	 */
	protected EMap<Session, ATMController> openedSessions;

	/**
	 * The cached value of the '{@link #getWithdrawls() <em>Withdrawls</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWithdrawls()
	 * @generated
	 * @ordered
	 */
	protected EMap<Session, Integer> withdrawls;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccountMngrImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.ACCOUNT_MNGR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Customer> getCustomers() {
		if (customers == null) {
			customers = new EObjectContainmentEList<Customer>(Customer.class, this, BankingsystemPackage.ACCOUNT_MNGR__CUSTOMERS);
		}
		return customers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Account> getAccounts() {
		if (accounts == null) {
			accounts = new EObjectContainmentEList<Account>(Account.class, this, BankingsystemPackage.ACCOUNT_MNGR__ACCOUNTS);
		}
		return accounts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankGateway getGateway() {
		if (gateway != null && gateway.eIsProxy()) {
			InternalEObject oldGateway = (InternalEObject)gateway;
			gateway = (BankGateway)eResolveProxy(oldGateway);
			if (gateway != oldGateway) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.ACCOUNT_MNGR__GATEWAY, oldGateway, gateway));
			}
		}
		return gateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankGateway basicGetGateway() {
		return gateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGateway(BankGateway newGateway, NotificationChain msgs) {
		BankGateway oldGateway = gateway;
		gateway = newGateway;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ACCOUNT_MNGR__GATEWAY, oldGateway, newGateway);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGateway(BankGateway newGateway) {
		if (newGateway != gateway) {
			NotificationChain msgs = null;
			if (gateway != null)
				msgs = ((InternalEObject)gateway).eInverseRemove(this, BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS, BankGateway.class, msgs);
			if (newGateway != null)
				msgs = ((InternalEObject)newGateway).eInverseAdd(this, BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS, BankGateway.class, msgs);
			msgs = basicSetGateway(newGateway, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ACCOUNT_MNGR__GATEWAY, newGateway, newGateway));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Session> getSessions() {
		if (sessions == null) {
			sessions = new EObjectContainmentWithInverseEList<Session>(Session.class, this, BankingsystemPackage.ACCOUNT_MNGR__SESSIONS, BankingsystemPackage.SESSION__OWNER);
		}
		return sessions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Session> getIdleSessions() {
		if (idleSessions == null) {
			idleSessions = new EObjectContainmentEList<Session>(Session.class, this, BankingsystemPackage.ACCOUNT_MNGR__IDLE_SESSIONS);
		}
		return idleSessions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ACCOUNT_MNGR__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Session, ATMController> getOpenedSessions() {
		if (openedSessions == null) {
			openedSessions = new EcoreEMap<Session,ATMController>(BankingsystemPackage.Literals.SESSION_TO_ATM_CONTROLLER_MAP, SessionToATMControllerMapImpl.class, this, BankingsystemPackage.ACCOUNT_MNGR__OPENED_SESSIONS);
		}
		return openedSessions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Session, Integer> getWithdrawls() {
		if (withdrawls == null) {
			withdrawls = new EcoreEMap<Session,Integer>(BankingsystemPackage.Literals.SESSION_TO_INTEGER_MAP, SessionToIntegerMapImpl.class, this, BankingsystemPackage.ACCOUNT_MNGR__WITHDRAWLS);
		}
		return withdrawls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionOpening openSession(int cardNumber, ATMController controller) {
		boolean isOpened = false;
		final SessionOpening so = BankingsystemFactory.eINSTANCE.createSessionOpening();
		// Find the account
		
		for (Account acc : getAccounts()) {			
			if (acc.getBankCard(cardNumber) != null) {
				final Session s;
				synchronized(this.idleSessions) {
					if (this.idleSessions.size() > 0) {
						s = this.idleSessions.get(0);
						this.idleSessions.remove(s);
					}
					else {
						s = null;
					}
				}
				s.setAccount(acc);
				s.setCard(acc.getBankCard(cardNumber));
				s.setOwner(this);
				s.setATM(controller);
				s.setOpeningStatus(so); // Hack, as so must be contained by something.
				so.setSession(s);
				so.setOpeningStatus(SessionOpeningStatus.SESSION_OPENED);
				getOpenedSessions().put(s, controller);
				isOpened = true;
				acc.createSession(cardNumber);
				break;
			}
			
			if (isOpened) 
				break;
		}
		if (!isOpened) {
			so.setOpeningStatus(SessionOpeningStatus.SESSION_ERROR);
		}
		return so;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public WithdrawMoneyStatus withdraw(int amount, Session session) {
		WithdrawMoneyStatus ret = null;
		if (getOpenedSessions().containsKey(session)) {
			Account acc = session.getAccount();
			if (acc.getBalance() >= amount) {
				session.withdraw(amount);
				getWithdrawls().put(session, amount);
				ret = WithdrawMoneyStatus.DISPENSE_MONEY;
			} else {
				ret = WithdrawMoneyStatus.INSUFFICIENT_BALANCE;
			}
		} else {
			ret = WithdrawMoneyStatus.NO_SESSION;
		}
		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionTransactionState cancelSession(Session session) {
		if (getOpenedSessions().containsKey(session)) {	
			Integer amount = getWithdrawls().get(session);
			if (amount != null) {
				Account acc = session.getAccount();
				acc.setBalance(acc.getBalance() + amount);
				System.out.println("AccountMgr: Session Cancelled, restored amount from ongoing withdraw operation.");
			}
			getOpenedSessions().remove(session);
			return SessionTransactionState.CANCELLED;
		} else {
			return SessionTransactionState.NO_SUCH_SESSION;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionTransactionState commitSession(Session session) {
		SessionTransactionState sts = null;
		if (getOpenedSessions().containsKey(session)) {
			getOpenedSessions().remove(session);
			sts = SessionTransactionState.COMMITTED;
		} else {
			sts = SessionTransactionState.NO_SUCH_SESSION;
		}
		return sts;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Pétur Ingi Egilsson
	 * @generated NOT
	 */
	public void exitSession() {
		// XXX this assumes only a single session exists.
		getSessions().get(0).exitSession();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Pétur Ingi Egilsson
	 * @generated NOT
	 */
	public void exitSession2() {
		// Intentionally blank.
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT_MNGR__GATEWAY:
				if (gateway != null)
					msgs = ((InternalEObject)gateway).eInverseRemove(this, BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS, BankGateway.class, msgs);
				return basicSetGateway((BankGateway)otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__SESSIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSessions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT_MNGR__CUSTOMERS:
				return ((InternalEList<?>)getCustomers()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__ACCOUNTS:
				return ((InternalEList<?>)getAccounts()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__GATEWAY:
				return basicSetGateway(null, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__SESSIONS:
				return ((InternalEList<?>)getSessions()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__IDLE_SESSIONS:
				return ((InternalEList<?>)getIdleSessions()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__OPENED_SESSIONS:
				return ((InternalEList<?>)getOpenedSessions()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT_MNGR__WITHDRAWLS:
				return ((InternalEList<?>)getWithdrawls()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT_MNGR__CUSTOMERS:
				return getCustomers();
			case BankingsystemPackage.ACCOUNT_MNGR__ACCOUNTS:
				return getAccounts();
			case BankingsystemPackage.ACCOUNT_MNGR__GATEWAY:
				if (resolve) return getGateway();
				return basicGetGateway();
			case BankingsystemPackage.ACCOUNT_MNGR__SESSIONS:
				return getSessions();
			case BankingsystemPackage.ACCOUNT_MNGR__IDLE_SESSIONS:
				return getIdleSessions();
			case BankingsystemPackage.ACCOUNT_MNGR__NAME:
				return getName();
			case BankingsystemPackage.ACCOUNT_MNGR__OPENED_SESSIONS:
				if (coreType) return getOpenedSessions();
				else return getOpenedSessions().map();
			case BankingsystemPackage.ACCOUNT_MNGR__WITHDRAWLS:
				if (coreType) return getWithdrawls();
				else return getWithdrawls().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT_MNGR__CUSTOMERS:
				getCustomers().clear();
				getCustomers().addAll((Collection<? extends Customer>)newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__ACCOUNTS:
				getAccounts().clear();
				getAccounts().addAll((Collection<? extends Account>)newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__GATEWAY:
				setGateway((BankGateway)newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__SESSIONS:
				getSessions().clear();
				getSessions().addAll((Collection<? extends Session>)newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__IDLE_SESSIONS:
				getIdleSessions().clear();
				getIdleSessions().addAll((Collection<? extends Session>)newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__NAME:
				setName((String)newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__OPENED_SESSIONS:
				((EStructuralFeature.Setting)getOpenedSessions()).set(newValue);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__WITHDRAWLS:
				((EStructuralFeature.Setting)getWithdrawls()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT_MNGR__CUSTOMERS:
				getCustomers().clear();
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__ACCOUNTS:
				getAccounts().clear();
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__GATEWAY:
				setGateway((BankGateway)null);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__SESSIONS:
				getSessions().clear();
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__IDLE_SESSIONS:
				getIdleSessions().clear();
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__NAME:
				setName(NAME_EDEFAULT);
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__OPENED_SESSIONS:
				getOpenedSessions().clear();
				return;
			case BankingsystemPackage.ACCOUNT_MNGR__WITHDRAWLS:
				getWithdrawls().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT_MNGR__CUSTOMERS:
				return customers != null && !customers.isEmpty();
			case BankingsystemPackage.ACCOUNT_MNGR__ACCOUNTS:
				return accounts != null && !accounts.isEmpty();
			case BankingsystemPackage.ACCOUNT_MNGR__GATEWAY:
				return gateway != null;
			case BankingsystemPackage.ACCOUNT_MNGR__SESSIONS:
				return sessions != null && !sessions.isEmpty();
			case BankingsystemPackage.ACCOUNT_MNGR__IDLE_SESSIONS:
				return idleSessions != null && !idleSessions.isEmpty();
			case BankingsystemPackage.ACCOUNT_MNGR__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case BankingsystemPackage.ACCOUNT_MNGR__OPENED_SESSIONS:
				return openedSessions != null && !openedSessions.isEmpty();
			case BankingsystemPackage.ACCOUNT_MNGR__WITHDRAWLS:
				return withdrawls != null && !withdrawls.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.ACCOUNT_MNGR___OPEN_SESSION__INT_ATMCONTROLLER:
				return openSession((Integer)arguments.get(0), (ATMController)arguments.get(1));
			case BankingsystemPackage.ACCOUNT_MNGR___WITHDRAW__INT_SESSION:
				return withdraw((Integer)arguments.get(0), (Session)arguments.get(1));
			case BankingsystemPackage.ACCOUNT_MNGR___CANCEL_SESSION__SESSION:
				return cancelSession((Session)arguments.get(0));
			case BankingsystemPackage.ACCOUNT_MNGR___COMMIT_SESSION__SESSION:
				return commitSession((Session)arguments.get(0));
			case BankingsystemPackage.ACCOUNT_MNGR___EXIT_SESSION:
				exitSession();
				return null;
			case BankingsystemPackage.ACCOUNT_MNGR___EXIT_SESSION2:
				exitSession2();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AccountMngrImpl
