package dk.ingi.petur.bankingsystem.impl;

/**
 * 
 * @author Lom Hillah
 *
 */
public class NotInFrontOfATMHardwareException extends Exception {

	private static final long serialVersionUID = -6053251628868806928L;

	public NotInFrontOfATMHardwareException() {
		super();
	}

	public NotInFrontOfATMHardwareException(String message) {
		super(message);
	}

	public NotInFrontOfATMHardwareException(Throwable cause) {
		super(cause);
	}

	public NotInFrontOfATMHardwareException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotInFrontOfATMHardwareException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
