/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankGateway;
import dk.ingi.petur.bankingsystem.BankNote;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.HW;
import dk.ingi.petur.bankingsystem.InsertedCardStatus;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.SessionOpening;
import dk.ingi.petur.bankingsystem.SessionTransactionState;
import dk.ingi.petur.bankingsystem.WithdrawMoneyStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ATM Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getHw <em>Hw</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getSession <em>Session</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getGateway <em>Gateway</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getCurrentCardNum <em>Current Card Num</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getCurrentIssuer <em>Current Issuer</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getNbTentatives <em>Nb Tentatives</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getCurrentPin <em>Current Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getSessions <em>Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.ATMControllerImpl#getLastSessionTransState <em>Last Session Trans State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ATMControllerImpl extends ComponentImpl implements ATMController {
	/**
	 * The cached value of the '{@link #getHw() <em>Hw</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHw()
	 * @generated
	 * @ordered
	 */
	protected HW hw;

	/**
	 * The cached value of the '{@link #getSession() <em>Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSession()
	 * @generated
	 * @ordered
	 */
	protected Session session;

	/**
	 * The cached value of the '{@link #getGateway() <em>Gateway</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGateway()
	 * @generated
	 * @ordered
	 */
	protected BankGateway gateway;

	/**
	 * The default value of the '{@link #getCurrentCardNum() <em>Current Card Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentCardNum()
	 * @generated
	 * @ordered
	 */
	protected static final int CURRENT_CARD_NUM_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCurrentCardNum() <em>Current Card Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentCardNum()
	 * @generated
	 * @ordered
	 */
	protected int currentCardNum = CURRENT_CARD_NUM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCurrentIssuer() <em>Current Issuer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentIssuer()
	 * @generated
	 * @ordered
	 */
	protected AccountMngr currentIssuer;

	/**
	 * The default value of the '{@link #getNbTentatives() <em>Nb Tentatives</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbTentatives()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_TENTATIVES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNbTentatives() <em>Nb Tentatives</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbTentatives()
	 * @generated
	 * @ordered
	 */
	protected int nbTentatives = NB_TENTATIVES_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrentPin() <em>Current Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentPin()
	 * @generated
	 * @ordered
	 */
	protected static final int CURRENT_PIN_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCurrentPin() <em>Current Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentPin()
	 * @generated
	 * @ordered
	 */
	protected int currentPin = CURRENT_PIN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSessions() <em>Sessions</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessions()
	 * @generated
	 * @ordered
	 */
	protected EMap<Integer, Session> sessions;

	/**
	 * The default value of the '{@link #getLastSessionTransState() <em>Last Session Trans State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastSessionTransState()
	 * @generated
	 * @ordered
	 */
	protected static final SessionTransactionState LAST_SESSION_TRANS_STATE_EDEFAULT = SessionTransactionState.COMMITTED;

	/**
	 * The cached value of the '{@link #getLastSessionTransState() <em>Last Session Trans State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastSessionTransState()
	 * @generated
	 * @ordered
	 */
	protected SessionTransactionState lastSessionTransState = LAST_SESSION_TRANS_STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ATMControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.ATM_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HW getHw() {
		if (hw != null && hw.eIsProxy()) {
			InternalEObject oldHw = (InternalEObject)hw;
			hw = (HW)eResolveProxy(oldHw);
			if (hw != oldHw) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.ATM_CONTROLLER__HW, oldHw, hw));
			}
		}
		return hw;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HW basicGetHw() {
		return hw;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHw(HW newHw, NotificationChain msgs) {
		HW oldHw = hw;
		hw = newHw;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__HW, oldHw, newHw);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHw(HW newHw) {
		if (newHw != hw) {
			NotificationChain msgs = null;
			if (hw != null)
				msgs = ((InternalEObject)hw).eInverseRemove(this, BankingsystemPackage.HW__CONTROLLER, HW.class, msgs);
			if (newHw != null)
				msgs = ((InternalEObject)newHw).eInverseAdd(this, BankingsystemPackage.HW__CONTROLLER, HW.class, msgs);
			msgs = basicSetHw(newHw, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__HW, newHw, newHw));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Session getSession() {
		if (session != null && session.eIsProxy()) {
			InternalEObject oldSession = (InternalEObject)session;
			session = (Session)eResolveProxy(oldSession);
			if (session != oldSession) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.ATM_CONTROLLER__SESSION, oldSession, session));
			}
		}
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Session basicGetSession() {
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSession(Session newSession) {
		Session oldSession = session;
		session = newSession;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__SESSION, oldSession, session));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankGateway getGateway() {
		if (gateway != null && gateway.eIsProxy()) {
			InternalEObject oldGateway = (InternalEObject)gateway;
			gateway = (BankGateway)eResolveProxy(oldGateway);
			if (gateway != oldGateway) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.ATM_CONTROLLER__GATEWAY, oldGateway, gateway));
			}
		}
		return gateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankGateway basicGetGateway() {
		return gateway;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGateway(BankGateway newGateway, NotificationChain msgs) {
		BankGateway oldGateway = gateway;
		gateway = newGateway;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__GATEWAY, oldGateway, newGateway);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGateway(BankGateway newGateway) {
		if (newGateway != gateway) {
			NotificationChain msgs = null;
			if (gateway != null)
				msgs = ((InternalEObject)gateway).eInverseRemove(this, BankingsystemPackage.BANK_GATEWAY__AT_MS, BankGateway.class, msgs);
			if (newGateway != null)
				msgs = ((InternalEObject)newGateway).eInverseAdd(this, BankingsystemPackage.BANK_GATEWAY__AT_MS, BankGateway.class, msgs);
			msgs = basicSetGateway(newGateway, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__GATEWAY, newGateway, newGateway));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCurrentCardNum() {
		return currentCardNum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentCardNum(int newCurrentCardNum) {
		int oldCurrentCardNum = currentCardNum;
		currentCardNum = newCurrentCardNum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__CURRENT_CARD_NUM, oldCurrentCardNum, currentCardNum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccountMngr getCurrentIssuer() {
		if (currentIssuer != null && currentIssuer.eIsProxy()) {
			InternalEObject oldCurrentIssuer = (InternalEObject)currentIssuer;
			currentIssuer = (AccountMngr)eResolveProxy(oldCurrentIssuer);
			if (currentIssuer != oldCurrentIssuer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.ATM_CONTROLLER__CURRENT_ISSUER, oldCurrentIssuer, currentIssuer));
			}
		}
		return currentIssuer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccountMngr basicGetCurrentIssuer() {
		return currentIssuer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentIssuer(AccountMngr newCurrentIssuer) {
		AccountMngr oldCurrentIssuer = currentIssuer;
		currentIssuer = newCurrentIssuer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__CURRENT_ISSUER, oldCurrentIssuer, currentIssuer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbTentatives() {
		return nbTentatives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbTentatives(int newNbTentatives) {
		int oldNbTentatives = nbTentatives;
		nbTentatives = newNbTentatives;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__NB_TENTATIVES, oldNbTentatives, nbTentatives));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCurrentPin() {
		return currentPin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentPin(int newCurrentPin) {
		int oldCurrentPin = currentPin;
		currentPin = newCurrentPin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__CURRENT_PIN, oldCurrentPin, currentPin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Integer, Session> getSessions() {
		if (sessions == null) {
			sessions = new EcoreEMap<Integer,Session>(BankingsystemPackage.Literals.INTEGER_TO_SESSION_MAP, IntegerToSessionMapImpl.class, this, BankingsystemPackage.ATM_CONTROLLER__SESSIONS);
		}
		return sessions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionTransactionState getLastSessionTransState() {
		return lastSessionTransState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastSessionTransState(SessionTransactionState newLastSessionTransState) {
		SessionTransactionState oldLastSessionTransState = lastSessionTransState;
		lastSessionTransState = newLastSessionTransState == null ? LAST_SESSION_TRANS_STATE_EDEFAULT : newLastSessionTransState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ATM_CONTROLLER__LAST_SESSION_TRANS_STATE, oldLastSessionTransState, lastSessionTransState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public void insertCard(int cardNumber, AccountMngr issuer, int pin) {
		// First ask for the pin code
		setCurrentCardNum(cardNumber);;
		setCurrentIssuer(issuer);
		setCurrentPin(pin);
		setNbTentatives(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public WithdrawMoneyStatus withdrawMoney(int amount) {
		WithdrawMoneyStatus wms = null;
		// is there an ongoing session?
		final Integer card = getCurrentCardNum();
		final Session s = getSessions().get(card);
		if (s != null) {
			wms =  getGateway().withdraw(amount, s);
		} else {
			wms = WithdrawMoneyStatus.NO_SESSION;
		}
		if (wms == WithdrawMoneyStatus.DISPENSE_MONEY) {
			presentMoney(amount);
		}
		return wms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public InsertedCardStatus setPin(int enteredPin) {
		InsertedCardStatus ret = null;
		if (currentPin == enteredPin) {
			SessionOpening so = getGateway().openSession(getCurrentCardNum(), getCurrentIssuer(), this);
			setNbTentatives(1);
			// store current session
			switch (so.getOpeningStatus()) {
			case SESSION_ERROR:
				ret = InsertedCardStatus.UNRECOGNIZED;
				break;
			case SESSION_OPENED:
				ret = InsertedCardStatus.OK;
				getSessions().put(currentCardNum, so.getSession());
				break;
			default:
				ret = InsertedCardStatus.UNRECOGNIZED;
				break;
			}
		} else {
			setNbTentatives(getNbTentatives()+1);
			if (getNbTentatives() == 3) {
				ret = InsertedCardStatus.RETAIN_CARD_3PIN_ERROR;
			}
			ret = InsertedCardStatus.INVALID_PIN;
		}
		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public void cancelSession(Session s) {
		if (getSessions().containsKey(s)) {
			getHw().cancelSession(getCurrentCardNum());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionTransactionState commitSession(int cardNumber) {
		Session s = getSessions().get(cardNumber).getValue();
		if (s != null) {
			 System.out.println("Controller : committing session");
			 setLastSessionTransState(getGateway().commitSession(s));
			
		} else {
			 setLastSessionTransState(SessionTransactionState.NO_SUCH_SESSION);
		}
		return getLastSessionTransState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionTransactionState rollbackWithdraw(int cardNumber) {
		Session s = getSessions().get(cardNumber).getValue();
		if (s != null) {
			 System.out.println("Controller : rolling back withdrawal.");
			 setLastSessionTransState(getGateway().cancelSession(s));
		} else {
			setLastSessionTransState(SessionTransactionState.NO_SUCH_SESSION);
		}
		return getLastSessionTransState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void takeMoney(EList<BankNote> money) {
		// Intentionally blank.
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void presentMoney(Integer amount) {
		getHw().presentMoney(amount);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void takeCard() {
		// Intentionally blank.
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void exit() {
		getGateway().exitSession();
		getHw().presentCard();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void exitSession() {
		// Intentionally blank.
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void presentCard() {
		// Intentionally blank.
	}
	

	/**
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void takeCard2() {
		// Intentionally blank.
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.ATM_CONTROLLER__HW:
				if (hw != null)
					msgs = ((InternalEObject)hw).eInverseRemove(this, BankingsystemPackage.HW__CONTROLLER, HW.class, msgs);
				return basicSetHw((HW)otherEnd, msgs);
			case BankingsystemPackage.ATM_CONTROLLER__GATEWAY:
				if (gateway != null)
					msgs = ((InternalEObject)gateway).eInverseRemove(this, BankingsystemPackage.BANK_GATEWAY__AT_MS, BankGateway.class, msgs);
				return basicSetGateway((BankGateway)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.ATM_CONTROLLER__HW:
				return basicSetHw(null, msgs);
			case BankingsystemPackage.ATM_CONTROLLER__GATEWAY:
				return basicSetGateway(null, msgs);
			case BankingsystemPackage.ATM_CONTROLLER__SESSIONS:
				return ((InternalEList<?>)getSessions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.ATM_CONTROLLER__HW:
				if (resolve) return getHw();
				return basicGetHw();
			case BankingsystemPackage.ATM_CONTROLLER__SESSION:
				if (resolve) return getSession();
				return basicGetSession();
			case BankingsystemPackage.ATM_CONTROLLER__GATEWAY:
				if (resolve) return getGateway();
				return basicGetGateway();
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_CARD_NUM:
				return getCurrentCardNum();
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_ISSUER:
				if (resolve) return getCurrentIssuer();
				return basicGetCurrentIssuer();
			case BankingsystemPackage.ATM_CONTROLLER__NB_TENTATIVES:
				return getNbTentatives();
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_PIN:
				return getCurrentPin();
			case BankingsystemPackage.ATM_CONTROLLER__SESSIONS:
				if (coreType) return getSessions();
				else return getSessions().map();
			case BankingsystemPackage.ATM_CONTROLLER__LAST_SESSION_TRANS_STATE:
				return getLastSessionTransState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.ATM_CONTROLLER__HW:
				setHw((HW)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__SESSION:
				setSession((Session)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__GATEWAY:
				setGateway((BankGateway)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_CARD_NUM:
				setCurrentCardNum((Integer)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_ISSUER:
				setCurrentIssuer((AccountMngr)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__NB_TENTATIVES:
				setNbTentatives((Integer)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_PIN:
				setCurrentPin((Integer)newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__SESSIONS:
				((EStructuralFeature.Setting)getSessions()).set(newValue);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__LAST_SESSION_TRANS_STATE:
				setLastSessionTransState((SessionTransactionState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.ATM_CONTROLLER__HW:
				setHw((HW)null);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__SESSION:
				setSession((Session)null);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__GATEWAY:
				setGateway((BankGateway)null);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_CARD_NUM:
				setCurrentCardNum(CURRENT_CARD_NUM_EDEFAULT);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_ISSUER:
				setCurrentIssuer((AccountMngr)null);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__NB_TENTATIVES:
				setNbTentatives(NB_TENTATIVES_EDEFAULT);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_PIN:
				setCurrentPin(CURRENT_PIN_EDEFAULT);
				return;
			case BankingsystemPackage.ATM_CONTROLLER__SESSIONS:
				getSessions().clear();
				return;
			case BankingsystemPackage.ATM_CONTROLLER__LAST_SESSION_TRANS_STATE:
				setLastSessionTransState(LAST_SESSION_TRANS_STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.ATM_CONTROLLER__HW:
				return hw != null;
			case BankingsystemPackage.ATM_CONTROLLER__SESSION:
				return session != null;
			case BankingsystemPackage.ATM_CONTROLLER__GATEWAY:
				return gateway != null;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_CARD_NUM:
				return currentCardNum != CURRENT_CARD_NUM_EDEFAULT;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_ISSUER:
				return currentIssuer != null;
			case BankingsystemPackage.ATM_CONTROLLER__NB_TENTATIVES:
				return nbTentatives != NB_TENTATIVES_EDEFAULT;
			case BankingsystemPackage.ATM_CONTROLLER__CURRENT_PIN:
				return currentPin != CURRENT_PIN_EDEFAULT;
			case BankingsystemPackage.ATM_CONTROLLER__SESSIONS:
				return sessions != null && !sessions.isEmpty();
			case BankingsystemPackage.ATM_CONTROLLER__LAST_SESSION_TRANS_STATE:
				return lastSessionTransState != LAST_SESSION_TRANS_STATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.ATM_CONTROLLER___INSERT_CARD__INT_ACCOUNTMNGR_INT:
				insertCard((Integer)arguments.get(0), (AccountMngr)arguments.get(1), (Integer)arguments.get(2));
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___WITHDRAW_MONEY__INT:
				return withdrawMoney((Integer)arguments.get(0));
			case BankingsystemPackage.ATM_CONTROLLER___SET_PIN__INT:
				return setPin((Integer)arguments.get(0));
			case BankingsystemPackage.ATM_CONTROLLER___CANCEL_SESSION__SESSION:
				cancelSession((Session)arguments.get(0));
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___COMMIT_SESSION__INT:
				return commitSession((Integer)arguments.get(0));
			case BankingsystemPackage.ATM_CONTROLLER___ROLLBACK_WITHDRAW__INT:
				return rollbackWithdraw((Integer)arguments.get(0));
			case BankingsystemPackage.ATM_CONTROLLER___TAKE_MONEY__ELIST:
				takeMoney((EList<BankNote>)arguments.get(0));
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___PRESENT_MONEY__INTEGER:
				presentMoney((Integer)arguments.get(0));
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___TAKE_CARD:
				takeCard();
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___EXIT:
				exit();
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___EXIT_SESSION:
				exitSession();
				return null;
			case BankingsystemPackage.ATM_CONTROLLER___PRESENT_CARD:
				presentCard();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (currentCardNum: ");
		result.append(currentCardNum);
		result.append(", nbTentatives: ");
		result.append(nbTentatives);
		result.append(", currentPin: ");
		result.append(currentPin);
		result.append(", lastSessionTransState: ");
		result.append(lastSessionTransState);
		result.append(')');
		return result.toString();
	}

} //ATMControllerImpl
