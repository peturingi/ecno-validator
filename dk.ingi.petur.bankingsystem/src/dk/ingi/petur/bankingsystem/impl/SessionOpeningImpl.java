/**
 */
package dk.ingi.petur.bankingsystem.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.SessionOpening;
import dk.ingi.petur.bankingsystem.SessionOpeningStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Session Opening</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionOpeningImpl#getOpeningStatus <em>Opening Status</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionOpeningImpl#getSession <em>Session</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SessionOpeningImpl extends ComponentImpl implements SessionOpening {
	/**
	 * The default value of the '{@link #getOpeningStatus() <em>Opening Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpeningStatus()
	 * @generated
	 * @ordered
	 */
	protected static final SessionOpeningStatus OPENING_STATUS_EDEFAULT = SessionOpeningStatus.SESSION_OPENED;
	/**
	 * The cached value of the '{@link #getOpeningStatus() <em>Opening Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpeningStatus()
	 * @generated
	 * @ordered
	 */
	protected SessionOpeningStatus openingStatus = OPENING_STATUS_EDEFAULT;
	/**
	 * The cached value of the '{@link #getSession() <em>Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSession()
	 * @generated
	 * @ordered
	 */
	protected Session session;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SessionOpeningImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.SESSION_OPENING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Session getSession() {
		if (session != null && session.eIsProxy()) {
			InternalEObject oldSession = (InternalEObject)session;
			session = (Session)eResolveProxy(oldSession);
			if (session != oldSession) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.SESSION_OPENING__SESSION, oldSession, session));
			}
		}
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Session basicGetSession() {
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSession(Session newSession) {
		Session oldSession = session;
		session = newSession;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION_OPENING__SESSION, oldSession, session));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionOpeningStatus getOpeningStatus() {
		return openingStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpeningStatus(SessionOpeningStatus newOpeningStatus) {
		SessionOpeningStatus oldOpeningStatus = openingStatus;
		openingStatus = newOpeningStatus == null ? OPENING_STATUS_EDEFAULT : newOpeningStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION_OPENING__OPENING_STATUS, oldOpeningStatus, openingStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.SESSION_OPENING__OPENING_STATUS:
				return getOpeningStatus();
			case BankingsystemPackage.SESSION_OPENING__SESSION:
				if (resolve) return getSession();
				return basicGetSession();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.SESSION_OPENING__OPENING_STATUS:
				setOpeningStatus((SessionOpeningStatus)newValue);
				return;
			case BankingsystemPackage.SESSION_OPENING__SESSION:
				setSession((Session)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.SESSION_OPENING__OPENING_STATUS:
				setOpeningStatus(OPENING_STATUS_EDEFAULT);
				return;
			case BankingsystemPackage.SESSION_OPENING__SESSION:
				setSession((Session)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.SESSION_OPENING__OPENING_STATUS:
				return openingStatus != OPENING_STATUS_EDEFAULT;
			case BankingsystemPackage.SESSION_OPENING__SESSION:
				return session != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (openingStatus: ");
		result.append(openingStatus);
		result.append(')');
		return result.toString();
	}

} //SessionOpeningImpl
