/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.ingi.petur.bankingsystem.Account;
import dk.ingi.petur.bankingsystem.BankCard;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Customer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Account</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountImpl#getCards <em>Cards</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountImpl#getBalance <em>Balance</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.AccountImpl#getHolder <em>Holder</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccountImpl extends ComponentImpl implements Account {
	/**
	 * The cached value of the '{@link #getCards() <em>Cards</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCards()
	 * @generated
	 * @ordered
	 */
	protected EList<BankCard> cards;

	/**
	 * The default value of the '{@link #getBalance() <em>Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBalance()
	 * @generated
	 * @ordered
	 */
	protected static final int BALANCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBalance() <em>Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBalance()
	 * @generated
	 * @ordered
	 */
	protected int balance = BALANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHolder() <em>Holder</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHolder()
	 * @generated
	 * @ordered
	 */
	protected EList<Customer> holder;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccountImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.ACCOUNT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BankCard> getCards() {
		if (cards == null) {
			cards = new EObjectContainmentWithInverseEList<BankCard>(BankCard.class, this, BankingsystemPackage.ACCOUNT__CARDS, BankingsystemPackage.BANK_CARD__ACCOUNT);
		}
		return cards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBalance() {
		return balance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBalance(int newBalance) {
		int oldBalance = balance;
		balance = newBalance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.ACCOUNT__BALANCE, oldBalance, balance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Customer> getHolder() {
		if (holder == null) {
			holder = new EObjectWithInverseResolvingEList.ManyInverse<Customer>(Customer.class, this, BankingsystemPackage.ACCOUNT__HOLDER, BankingsystemPackage.CUSTOMER__ACCOUNTS);
		}
		return holder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public boolean isAccountFor(int physicalCardNumber) {
		for (BankCard card : this.getCards()) {
			if (card.getPhysical().getNumber() == physicalCardNumber) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public BankCard getBankCard(int physicalCardNumber) {
		for (BankCard card : this.getCards()) {
			if (card.getNumber() == physicalCardNumber) {
				return card;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void withdraw(int amount) {
		this.balance -= amount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void createSession(int cardNumber) {
		getBankCard(cardNumber).createSession();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT__CARDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCards()).basicAdd(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT__HOLDER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHolder()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT__CARDS:
				return ((InternalEList<?>)getCards()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.ACCOUNT__HOLDER:
				return ((InternalEList<?>)getHolder()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT__CARDS:
				return getCards();
			case BankingsystemPackage.ACCOUNT__BALANCE:
				return getBalance();
			case BankingsystemPackage.ACCOUNT__HOLDER:
				return getHolder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT__CARDS:
				getCards().clear();
				getCards().addAll((Collection<? extends BankCard>)newValue);
				return;
			case BankingsystemPackage.ACCOUNT__BALANCE:
				setBalance((Integer)newValue);
				return;
			case BankingsystemPackage.ACCOUNT__HOLDER:
				getHolder().clear();
				getHolder().addAll((Collection<? extends Customer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT__CARDS:
				getCards().clear();
				return;
			case BankingsystemPackage.ACCOUNT__BALANCE:
				setBalance(BALANCE_EDEFAULT);
				return;
			case BankingsystemPackage.ACCOUNT__HOLDER:
				getHolder().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.ACCOUNT__CARDS:
				return cards != null && !cards.isEmpty();
			case BankingsystemPackage.ACCOUNT__BALANCE:
				return balance != BALANCE_EDEFAULT;
			case BankingsystemPackage.ACCOUNT__HOLDER:
				return holder != null && !holder.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.ACCOUNT___IS_ACCOUNT_FOR__INT:
				return isAccountFor((Integer)arguments.get(0));
			case BankingsystemPackage.ACCOUNT___GET_BANK_CARD__INT:
				return getBankCard((Integer)arguments.get(0));
			case BankingsystemPackage.ACCOUNT___WITHDRAW__INT:
				withdraw((Integer)arguments.get(0));
				return null;
			case BankingsystemPackage.ACCOUNT___CREATE_SESSION__INT:
				createSession((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (balance: ");
		result.append(balance);
		result.append(')');
		return result.toString();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 * @author Lom Hillah
	 * @generated NOT
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + balance;
		result = prime * result + ((cards == null) ? 0 : cards.hashCode());
		result = prime * result + ((holder == null) ? 0 : holder.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @author Lom Hillah
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountImpl other = (AccountImpl) obj;
		if (balance != other.balance)
			return false;
		if (cards == null) {
			if (other.cards != null)
				return false;
		} else if (!cards.equals(other.cards))
			return false;
		if (holder == null) {
			if (other.holder != null)
				return false;
		} else if (!holder.equals(other.holder))
			return false;
		return true;
	}
	
} //AccountImpl
