/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankGateway;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.SessionOpening;
import dk.ingi.petur.bankingsystem.SessionOpeningStatus;
import dk.ingi.petur.bankingsystem.SessionState;
import dk.ingi.petur.bankingsystem.SessionTransactionState;
import dk.ingi.petur.bankingsystem.SwiftNetwork;
import dk.ingi.petur.bankingsystem.WithdrawMoneyStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bank Gateway</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl#getATMs <em>AT Ms</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl#getSwift <em>Swift</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl#getAccountMngrs <em>Account Mngrs</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl#getSessions <em>Sessions</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.BankGatewayImpl#getSessionsState <em>Sessions State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BankGatewayImpl extends ComponentImpl implements BankGateway {
	/**
	 * The cached value of the '{@link #getATMs() <em>AT Ms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATMs()
	 * @generated
	 * @ordered
	 */
	protected EList<ATMController> atMs;

	/**
	 * The cached value of the '{@link #getSwift() <em>Swift</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSwift()
	 * @generated
	 * @ordered
	 */
	protected SwiftNetwork swift;

	/**
	 * The cached value of the '{@link #getAccountMngrs() <em>Account Mngrs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccountMngrs()
	 * @generated
	 * @ordered
	 */
	protected EList<AccountMngr> accountMngrs;

	/**
	 * The cached value of the '{@link #getSessions() <em>Sessions</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessions()
	 * @generated
	 * @ordered
	 */
	protected EMap<Session, AccountMngr> sessions;

	/**
	 * The cached value of the '{@link #getSessionsState() <em>Sessions State</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessionsState()
	 * @generated
	 * @ordered
	 */
	protected EMap<Session, SessionState> sessionsState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BankGatewayImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.BANK_GATEWAY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ATMController> getATMs() {
		if (atMs == null) {
			atMs = new EObjectWithInverseResolvingEList<ATMController>(ATMController.class, this, BankingsystemPackage.BANK_GATEWAY__AT_MS, BankingsystemPackage.ATM_CONTROLLER__GATEWAY);
		}
		return atMs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwiftNetwork getSwift() {
		if (swift != null && swift.eIsProxy()) {
			InternalEObject oldSwift = (InternalEObject)swift;
			swift = (SwiftNetwork)eResolveProxy(oldSwift);
			if (swift != oldSwift) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.BANK_GATEWAY__SWIFT, oldSwift, swift));
			}
		}
		return swift;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwiftNetwork basicGetSwift() {
		return swift;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSwift(SwiftNetwork newSwift, NotificationChain msgs) {
		SwiftNetwork oldSwift = swift;
		swift = newSwift;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.BANK_GATEWAY__SWIFT, oldSwift, newSwift);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSwift(SwiftNetwork newSwift) {
		if (newSwift != swift) {
			NotificationChain msgs = null;
			if (swift != null)
				msgs = ((InternalEObject)swift).eInverseRemove(this, BankingsystemPackage.SWIFT_NETWORK__GATEWAYS, SwiftNetwork.class, msgs);
			if (newSwift != null)
				msgs = ((InternalEObject)newSwift).eInverseAdd(this, BankingsystemPackage.SWIFT_NETWORK__GATEWAYS, SwiftNetwork.class, msgs);
			msgs = basicSetSwift(newSwift, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.BANK_GATEWAY__SWIFT, newSwift, newSwift));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccountMngr> getAccountMngrs() {
		if (accountMngrs == null) {
			accountMngrs = new EObjectWithInverseResolvingEList<AccountMngr>(AccountMngr.class, this, BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS, BankingsystemPackage.ACCOUNT_MNGR__GATEWAY);
		}
		return accountMngrs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Session, AccountMngr> getSessions() {
		if (sessions == null) {
			sessions = new EcoreEMap<Session,AccountMngr>(BankingsystemPackage.Literals.SESSION_TO_ACCOUNT_MNGR_MAP, SessionToAccountMngrMapImpl.class, this, BankingsystemPackage.BANK_GATEWAY__SESSIONS);
		}
		return sessions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Session, SessionState> getSessionsState() {
		if (sessionsState == null) {
			sessionsState = new EcoreEMap<Session,SessionState>(BankingsystemPackage.Literals.SESSION_TO_SESSION_STATE_MAP, SessionToSessionStateMapImpl.class, this, BankingsystemPackage.BANK_GATEWAY__SESSIONS_STATE);
		}
		return sessionsState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public WithdrawMoneyStatus withdraw(int amount, Session session) {
		WithdrawMoneyStatus wms = null;
		// Check session and fetch corresponding account manager
		final AccountMngr accMgr = sessions.get(session);
		if (accMgr != null) {
			synchronized (sessionsState) {
				sessionsState.put(session, SessionState.ONGOING_OPERATION);
			}
			wms  = accMgr.withdraw(amount, session);
		} else {
			wms = WithdrawMoneyStatus.NO_SESSION;
		}
		return wms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionOpening openSession(int card, AccountMngr issuer, ATMController controller) {
		SessionOpening res = null;
		// is this ATM controller registered?
		boolean isRegistered = false;
		boolean found = false;
		for (ATMController ctrl : getATMs()) {
			if (ctrl == controller) {
				isRegistered = true;
				break;
			}
		}
		if (!isRegistered) {
			res = new SessionOpeningImpl();
			res.setOpeningStatus(SessionOpeningStatus.UNAUTHORIZED_ATM);
		} else {
			for (AccountMngr acc : getAccountMngrs()) {
				if (acc.equals(issuer)) {
					res = acc.openSession(card, controller);
					// successfully opened a session
					final Session s = res.getSession();
					//final AccountMngr currentAcc = acc;
					getSessions().put(s, acc);
					getSessionsState().put(s, SessionState.IDLE);
					/* set timeout
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						@Override
						public void run() {
							synchronized (sessionsState) {
								// check if there is an operation going on,
								// otherwise cancel the session
								if (sessionsState.get(s).equals(SessionState.IDLE)) {
									SessionTransactionState sts = currentAcc.cancelSession(s);
									if (sts.equals(SessionTransactionState.CANCELLED)) {
										sessionsState.put(s, SessionState.CANCELLED);
										currentCtrl.cancelSession(s);
									}
								}
							}

						}
					}, (1 / 2) * 60 * 1000);
					*/
					found = true;
					break;
				}
			}
			if (!found) {
				res = new SessionOpeningImpl();
				res.setOpeningStatus(SessionOpeningStatus.SESSION_ERROR);
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionTransactionState commitSession(Session session) {
		SessionTransactionState sts = null;
		AccountMngr accMgr = getSessions().get(session);
		if (accMgr != null){
			sts = accMgr.commitSession(session);
			switch (sts) {
			case COMMITTED:
				synchronized (sessionsState) {
					getSessionsState().put(session, SessionState.COMMITTED);
					getSessions().remove(session);
				}
				System.out.println("GATEWAY : Session Committed");
				break;
			default:
				break;
			}
		} else {
			sts = SessionTransactionState.NO_SUCH_SESSION;
		}
		return sts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public SessionTransactionState cancelSession(Session session) {
		SessionTransactionState sts = null;
		AccountMngr accMgr = getSessions().get(session);
		if (accMgr != null) {
			sts = accMgr.cancelSession(session);
			switch (sts) {
			case CANCELLED:
				synchronized (sessionsState) {
					getSessionsState().put(session, SessionState.CANCELLED);
					getSessionsState().remove(session);
				}
				System.out.println("GATEWAY : Session Cancelled.");
				break;
			default:
				break;
			}
		} else {
			sts = SessionTransactionState.NO_SUCH_SESSION;
		}
		return sts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void exitSession() {
		// XXX ASSUMES A SINGLE ACCOUNT MANAGER!
		assert(getAccountMngrs().size() <= 1);
		
		getAccountMngrs().get(0).exitSession();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.BANK_GATEWAY__AT_MS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getATMs()).basicAdd(otherEnd, msgs);
			case BankingsystemPackage.BANK_GATEWAY__SWIFT:
				if (swift != null)
					msgs = ((InternalEObject)swift).eInverseRemove(this, BankingsystemPackage.SWIFT_NETWORK__GATEWAYS, SwiftNetwork.class, msgs);
				return basicSetSwift((SwiftNetwork)otherEnd, msgs);
			case BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAccountMngrs()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.BANK_GATEWAY__AT_MS:
				return ((InternalEList<?>)getATMs()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.BANK_GATEWAY__SWIFT:
				return basicSetSwift(null, msgs);
			case BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS:
				return ((InternalEList<?>)getAccountMngrs()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS:
				return ((InternalEList<?>)getSessions()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS_STATE:
				return ((InternalEList<?>)getSessionsState()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.BANK_GATEWAY__AT_MS:
				return getATMs();
			case BankingsystemPackage.BANK_GATEWAY__SWIFT:
				if (resolve) return getSwift();
				return basicGetSwift();
			case BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS:
				return getAccountMngrs();
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS:
				if (coreType) return getSessions();
				else return getSessions().map();
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS_STATE:
				if (coreType) return getSessionsState();
				else return getSessionsState().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.BANK_GATEWAY__AT_MS:
				getATMs().clear();
				getATMs().addAll((Collection<? extends ATMController>)newValue);
				return;
			case BankingsystemPackage.BANK_GATEWAY__SWIFT:
				setSwift((SwiftNetwork)newValue);
				return;
			case BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS:
				getAccountMngrs().clear();
				getAccountMngrs().addAll((Collection<? extends AccountMngr>)newValue);
				return;
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS:
				((EStructuralFeature.Setting)getSessions()).set(newValue);
				return;
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS_STATE:
				((EStructuralFeature.Setting)getSessionsState()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.BANK_GATEWAY__AT_MS:
				getATMs().clear();
				return;
			case BankingsystemPackage.BANK_GATEWAY__SWIFT:
				setSwift((SwiftNetwork)null);
				return;
			case BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS:
				getAccountMngrs().clear();
				return;
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS:
				getSessions().clear();
				return;
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS_STATE:
				getSessionsState().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.BANK_GATEWAY__AT_MS:
				return atMs != null && !atMs.isEmpty();
			case BankingsystemPackage.BANK_GATEWAY__SWIFT:
				return swift != null;
			case BankingsystemPackage.BANK_GATEWAY__ACCOUNT_MNGRS:
				return accountMngrs != null && !accountMngrs.isEmpty();
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS:
				return sessions != null && !sessions.isEmpty();
			case BankingsystemPackage.BANK_GATEWAY__SESSIONS_STATE:
				return sessionsState != null && !sessionsState.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.BANK_GATEWAY___WITHDRAW__INT_SESSION:
				return withdraw((Integer)arguments.get(0), (Session)arguments.get(1));
			case BankingsystemPackage.BANK_GATEWAY___OPEN_SESSION__INT_ACCOUNTMNGR_ATMCONTROLLER:
				return openSession((Integer)arguments.get(0), (AccountMngr)arguments.get(1), (ATMController)arguments.get(2));
			case BankingsystemPackage.BANK_GATEWAY___COMMIT_SESSION__SESSION:
				return commitSession((Session)arguments.get(0));
			case BankingsystemPackage.BANK_GATEWAY___CANCEL_SESSION__SESSION:
				return cancelSession((Session)arguments.get(0));
			case BankingsystemPackage.BANK_GATEWAY___EXIT_SESSION:
				exitSession();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //BankGatewayImpl
