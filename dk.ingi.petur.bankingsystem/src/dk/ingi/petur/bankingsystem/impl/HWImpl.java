/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.BankNote;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.HW;
import dk.ingi.petur.bankingsystem.InsertedCardStatus;
import dk.ingi.petur.bankingsystem.OperationChoiceStatus;
import dk.ingi.petur.bankingsystem.Operations;
import dk.ingi.petur.bankingsystem.PhysicalBankCard;
import dk.ingi.petur.bankingsystem.WithdrawMoneyStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HW</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getInserted <em>Inserted</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getHeld <em>Held</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getNotes <em>Notes</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getReady <em>Ready</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getShow <em>Show</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getController <em>Controller</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#isIsPinRequested <em>Is Pin Requested</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#isShowCardOn1InvalidPin <em>Show Card On1 Invalid Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#isRetractCardOn3InvalidPin <em>Retract Card On3 Invalid Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getRetractCardOnTimeOut <em>Retract Card On Time Out</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#getRetractMoneyOnTimeOut <em>Retract Money On Time Out</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#isMoneyTaken <em>Money Taken</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.HWImpl#isCardTakenBack <em>Card Taken Back</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HWImpl extends ComponentImpl implements HW {
	/**
	 * The cached value of the '{@link #getInserted() <em>Inserted</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInserted()
	 * @generated
	 * @ordered
	 */
	protected PhysicalBankCard inserted;

	/**
	 * The cached value of the '{@link #getHeld() <em>Held</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeld()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalBankCard> held;

	/**
	 * The cached value of the '{@link #getNotes() <em>Notes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotes()
	 * @generated
	 * @ordered
	 */
	protected EList<BankNote> notes;

	/**
	 * The cached value of the '{@link #getReady() <em>Ready</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReady()
	 * @generated
	 * @ordered
	 */
	protected EList<BankNote> ready;

	/**
	 * The cached value of the '{@link #getShow() <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShow()
	 * @generated
	 * @ordered
	 */
	protected PhysicalBankCard show;

	/**
	 * The cached value of the '{@link #getController() <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getController()
	 * @generated
	 * @ordered
	 */
	protected ATMController controller;

	/**
	 * The default value of the '{@link #isIsPinRequested() <em>Is Pin Requested</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsPinRequested()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_PIN_REQUESTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsPinRequested() <em>Is Pin Requested</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsPinRequested()
	 * @generated
	 * @ordered
	 */
	protected boolean isPinRequested = IS_PIN_REQUESTED_EDEFAULT;

	/**
	 * The default value of the '{@link #isShowCardOn1InvalidPin() <em>Show Card On1 Invalid Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowCardOn1InvalidPin()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_CARD_ON1_INVALID_PIN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isShowCardOn1InvalidPin() <em>Show Card On1 Invalid Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowCardOn1InvalidPin()
	 * @generated
	 * @ordered
	 */
	protected boolean showCardOn1InvalidPin = SHOW_CARD_ON1_INVALID_PIN_EDEFAULT;

	/**
	 * The default value of the '{@link #isRetractCardOn3InvalidPin() <em>Retract Card On3 Invalid Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRetractCardOn3InvalidPin()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RETRACT_CARD_ON3_INVALID_PIN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRetractCardOn3InvalidPin() <em>Retract Card On3 Invalid Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRetractCardOn3InvalidPin()
	 * @generated
	 * @ordered
	 */
	protected boolean retractCardOn3InvalidPin = RETRACT_CARD_ON3_INVALID_PIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getRetractCardOnTimeOut() <em>Retract Card On Time Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRetractCardOnTimeOut()
	 * @generated
	 * @ordered
	 */
	protected static final long RETRACT_CARD_ON_TIME_OUT_EDEFAULT = 5000L;

	/**
	 * The cached value of the '{@link #getRetractCardOnTimeOut() <em>Retract Card On Time Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRetractCardOnTimeOut()
	 * @generated
	 * @ordered
	 */
	protected long retractCardOnTimeOut = RETRACT_CARD_ON_TIME_OUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRetractMoneyOnTimeOut() <em>Retract Money On Time Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRetractMoneyOnTimeOut()
	 * @generated
	 * @ordered
	 */
	protected static final long RETRACT_MONEY_ON_TIME_OUT_EDEFAULT = 3000L;

	/**
	 * The cached value of the '{@link #getRetractMoneyOnTimeOut() <em>Retract Money On Time Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRetractMoneyOnTimeOut()
	 * @generated
	 * @ordered
	 */
	protected long retractMoneyOnTimeOut = RETRACT_MONEY_ON_TIME_OUT_EDEFAULT;

	/**
	 * The default value of the '{@link #isMoneyTaken() <em>Money Taken</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMoneyTaken()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MONEY_TAKEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMoneyTaken() <em>Money Taken</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMoneyTaken()
	 * @generated
	 * @ordered
	 */
	protected boolean moneyTaken = MONEY_TAKEN_EDEFAULT;

	/**
	 * The default value of the '{@link #isCardTakenBack() <em>Card Taken Back</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCardTakenBack()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CARD_TAKEN_BACK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCardTakenBack() <em>Card Taken Back</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCardTakenBack()
	 * @generated
	 * @ordered
	 */
	protected boolean cardTakenBack = CARD_TAKEN_BACK_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HWImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.HW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalBankCard getInserted() {
		if (inserted != null && inserted.eIsProxy()) {
			InternalEObject oldInserted = (InternalEObject)inserted;
			inserted = (PhysicalBankCard)eResolveProxy(oldInserted);
			if (inserted != oldInserted) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.HW__INSERTED, oldInserted, inserted));
			}
		}
		return inserted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalBankCard basicGetInserted() {
		return inserted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	public InsertedCardStatus setInserted(PhysicalBankCard newInserted) {
		if (newInserted != inserted) {
			NotificationChain msgs = null;
			if (inserted != null)
				msgs = ((InternalEObject)inserted).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BankingsystemPackage.HW__INSERTED, null, msgs);
			if (newInserted != null)
				inserted=newInserted;
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__INSERTED, newInserted, newInserted));
		
		if (newInserted != null) {
			if (getController() != null) getController().insertCard(newInserted.getNumber(), newInserted.getIssuer(), newInserted.getPin());
			return InsertedCardStatus.OK;
		} else {
			return InsertedCardStatus.NO_CARD;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalBankCard> getHeld() {
		if (held == null) {
			held = new EObjectContainmentEList<PhysicalBankCard>(PhysicalBankCard.class, this, BankingsystemPackage.HW__HELD);
		}
		return held;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BankNote> getNotes() {
		if (notes == null) {
			notes = new EObjectContainmentEList<BankNote>(BankNote.class, this, BankingsystemPackage.HW__NOTES);
		}
		return notes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BankNote> getReady() {
		if (ready == null) {
			ready = new EObjectContainmentEList<BankNote>(BankNote.class, this, BankingsystemPackage.HW__READY);
		}
		return ready;
	}
	
	/**
	 * @generated NOT
	 */
	public void takeCard() {
		cardTakenBack = true;
		this.controller.takeCard();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public PhysicalBankCard getShow() {
//		if (cardTakenTimer != null) {
//			cardTakenTimer.cancel();
//		}
		return show;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShow(PhysicalBankCard newShow, NotificationChain msgs) {
		PhysicalBankCard oldShow = show;
		show = newShow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__SHOW, oldShow, newShow);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setShow(PhysicalBankCard newShow) {
		if (newShow != show) {
			NotificationChain msgs = null;
			if (show != null)
				msgs = ((InternalEObject)show).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BankingsystemPackage.HW__SHOW, null, msgs);
			if (newShow != null)
				msgs = ((InternalEObject)newShow).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BankingsystemPackage.HW__SHOW, null, msgs);
			msgs = basicSetShow(newShow, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__SHOW, newShow, newShow));
		
		this.display("Please, take back your card.");
		/* Set timeout */
//		cardTakenTimer = new Timer();
//		cardTakenTimer.schedule(new TimerTask() {
//			@Override
//			public void run() {
//				synchronized (show) {
//					// check if card is retaken by customer
//					// otherwise retain it
//					if (!cardTakenBack) {
//						getHeld().add(show);
//						display("Timeout on shown card. Retracted it. Please contact your bank.");
//						show = null;
//					}
//				}
//
//			}
//		}, retractCardOnTimeOut);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATMController getController() {
		if (controller != null && controller.eIsProxy()) {
			InternalEObject oldController = (InternalEObject)controller;
			controller = (ATMController)eResolveProxy(oldController);
			if (controller != oldController) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.HW__CONTROLLER, oldController, controller));
			}
		}
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATMController basicGetController() {
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetController(ATMController newController, NotificationChain msgs) {
		ATMController oldController = controller;
		controller = newController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__CONTROLLER, oldController, newController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setController(ATMController newController) {
		if (newController != controller) {
			NotificationChain msgs = null;
			if (controller != null)
				msgs = ((InternalEObject)controller).eInverseRemove(this, BankingsystemPackage.ATM_CONTROLLER__HW, ATMController.class, msgs);
			if (newController != null)
				msgs = ((InternalEObject)newController).eInverseAdd(this, BankingsystemPackage.ATM_CONTROLLER__HW, ATMController.class, msgs);
			msgs = basicSetController(newController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__CONTROLLER, newController, newController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsPinRequested() {
		return isPinRequested;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPinRequested(boolean newIsPinRequested) {
		boolean oldIsPinRequested = isPinRequested;
		isPinRequested = newIsPinRequested;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__IS_PIN_REQUESTED, oldIsPinRequested, isPinRequested));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowCardOn1InvalidPin() {
		return showCardOn1InvalidPin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowCardOn1InvalidPin(boolean newShowCardOn1InvalidPin) {
		boolean oldShowCardOn1InvalidPin = showCardOn1InvalidPin;
		showCardOn1InvalidPin = newShowCardOn1InvalidPin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__SHOW_CARD_ON1_INVALID_PIN, oldShowCardOn1InvalidPin, showCardOn1InvalidPin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRetractCardOn3InvalidPin() {
		return retractCardOn3InvalidPin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRetractCardOn3InvalidPin(boolean newRetractCardOn3InvalidPin) {
		boolean oldRetractCardOn3InvalidPin = retractCardOn3InvalidPin;
		retractCardOn3InvalidPin = newRetractCardOn3InvalidPin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__RETRACT_CARD_ON3_INVALID_PIN, oldRetractCardOn3InvalidPin, retractCardOn3InvalidPin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getRetractCardOnTimeOut() {
		return retractCardOnTimeOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRetractCardOnTimeOut(long newRetractCardOnTimeOut) {
		long oldRetractCardOnTimeOut = retractCardOnTimeOut;
		retractCardOnTimeOut = newRetractCardOnTimeOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__RETRACT_CARD_ON_TIME_OUT, oldRetractCardOnTimeOut, retractCardOnTimeOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getRetractMoneyOnTimeOut() {
		return retractMoneyOnTimeOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRetractMoneyOnTimeOut(long newRetractMoneyOnTimeOut) {
		long oldRetractMoneyOnTimeOut = retractMoneyOnTimeOut;
		retractMoneyOnTimeOut = newRetractMoneyOnTimeOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__RETRACT_MONEY_ON_TIME_OUT, oldRetractMoneyOnTimeOut, retractMoneyOnTimeOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMoneyTaken() {
		return moneyTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMoneyTaken(boolean newMoneyTaken) {
		boolean oldMoneyTaken = moneyTaken;
		moneyTaken = newMoneyTaken;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__MONEY_TAKEN, oldMoneyTaken, moneyTaken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCardTakenBack() {
		return cardTakenBack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardTakenBack(boolean newCardTakenBack) {
		boolean oldCardTakenBack = cardTakenBack;
		cardTakenBack = newCardTakenBack;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.HW__CARD_TAKEN_BACK, oldCardTakenBack, cardTakenBack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void insertCard() {
		throw new UnsupportedOperationException("Use setInserted instead.");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void askPin() {
		setIsPinRequested(true);
		display("Enter Pin code");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void display(String string) {
		System.out.println(string);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public InsertedCardStatus enterPin(int pin) {
		InsertedCardStatus status;
		if (getInserted() != null) {
			status = getController().setPin(pin);
			switch (status) {
			case INVALID_PIN:
				if (!isShowCardOn1InvalidPin()) {
					this.display("Invalid PIN, please try again");
					setIsPinRequested(true);
				} else {
					this.display("Invalid PIN.");
					setIsPinRequested(false);
					this.setShow(getInserted());
				}
				break;
			case RETAIN_CARD_3PIN_ERROR:
				this.getHeld().add(getInserted());
				this.display("Holding card following 3 PIN erroneous tentatives");
				setIsPinRequested(false);
				break;
			case OK:
				this.display("PIN OK. Choose operation to perform.");
				setIsPinRequested(false);
				break;
			case UNRECOGNIZED:
				this.display("Unrecognized card, or no associated account found.");
				setIsPinRequested(false);
				this.cancelSession(getInserted().getNumber());
				break;
			default:
				// show the card to the user to take it back
				isPinRequested = false;
				this.cancelSession(getInserted().getNumber());
				break;
			}
		} else {
			status = InsertedCardStatus.NO_CARD;
		}
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelSession(int cardNum) {
		if (getInserted() != null && cardNum == getInserted().getNumber()) {
			display("Session cancelled.");
			presentCard();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void presentCard() {
		setShow(getInserted());
		inserted=null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void configureShowCardOn1InvalidPin() {
		setShowCardOn1InvalidPin(true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void configureRetractCardOn3InvalidPin() {
		setRetractCardOn3InvalidPin(true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void configureRetractCardOnTimeOut(long timeOutMillisec) {
		setRetractCardOnTimeOut(timeOutMillisec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public OperationChoiceStatus operation(Operations op) {
		OperationChoiceStatus ret;
		switch (op) {
		case WITHDRAW:
			if (this.getNotes().size() > 0) {
				this.display("Choose amount");
				ret = OperationChoiceStatus.AVAILABLE;
			} else {
				this.display("Sorry, no bank note to dispense.");
				ret = OperationChoiceStatus.UNAVAILABLE;
			}
			break;
		case GET_BALANCE:
			this.display("Operation unavailable.");
			ret = OperationChoiceStatus.UNAVAILABLE;
			break;
		default:
			this.display("Unknown operation");
			ret = OperationChoiceStatus.UNAVAILABLE;
			break;
		}
		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public WithdrawMoneyStatus withdraw(int i) {
		WithdrawMoneyStatus ret = null;
		// withdraw only if a card is inserted and it has bank notes
		if (getInserted() != null && getNotes().size() >= i && i > 0) {
//			setMoneyTaken(true);
			ret = getController().withdrawMoney(i);
			switch (ret) {
			case DISPENSE_MONEY:
				// Symbolically we decrease the number of bank notes and present
				// the one picked to the user.
				getReady().addAll(getNotes().subList(0, i));
				getNotes().removeAll(getReady());
				ret = WithdrawMoneyStatus.TAKE_MONEY;
				this.display("Please take the bank notes.");
				// We have to set up a timeout here.
//				takeMoneyTimer = new Timer();
//				takeMoneyTimer.schedule(new TimerTask() {
//					@Override
//					public void run() {
//						synchronized (ready) {
//							// check if money is taken by customer
//							// otherwise retract it
//							setMoneyTaken(false);
//							getNotes().add(ready.get(0));
//							display("Hardware: Timeout on shown money. Retracted it. Cancelling withdraw transaction.");
//							SessionTransactionState sts = getController().rollbackWithdraw(getInserted().getNumber());
//							switch (sts) {
//							case CANCELLED:
//								cancelSession(getInserted().getNumber());
//								break;
//
//							default:
//								break;
//							}
//						}
//					}
//				}, retractMoneyOnTimeOut);
				break;
			default:
				this.display("The withdraw operation cannot be completed.");
				break;
			}
		}

		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void configureRetractMoneyOnTimeOut(long timeOutMillisec) {
		setRetractMoneyOnTimeOut(timeOutMillisec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<BankNote> takeMoney() {
		final EList<BankNote> money = new BasicEList<>();
		money.addAll(getReady());
		for (BankNote note : getReady()) {
			note.takeMoney();
		}
		getController().takeMoney(getReady());
		getReady().clear();
		return money;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void presentMoney(Integer amount) {
		System.out.println("Money presented. Take it.");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void exit() {
		this.getController().exit();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.HW__CONTROLLER:
				if (controller != null)
					msgs = ((InternalEObject)controller).eInverseRemove(this, BankingsystemPackage.ATM_CONTROLLER__HW, ATMController.class, msgs);
				return basicSetController((ATMController)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.HW__INSERTED:
				inserted=null;
			case BankingsystemPackage.HW__HELD:
				return ((InternalEList<?>)getHeld()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.HW__NOTES:
				return ((InternalEList<?>)getNotes()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.HW__READY:
				if (moneyTaken) {
					System.out.println("USER REMOVED THE BANK NOTES");
					// Need to commit the transaction
					//SessionTransactionState sts = getController().commitSession(inserted.getNumber());
					// need to cancel timer
//					takeMoneyTimer.cancel();
					presentCard();
				}
				return ((InternalEList<?>)getReady()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.HW__SHOW:
				return basicSetShow(null, msgs);
			case BankingsystemPackage.HW__CONTROLLER:
				return basicSetController(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.HW__INSERTED:
				if (resolve) return getInserted();
				return basicGetInserted();
			case BankingsystemPackage.HW__HELD:
				return getHeld();
			case BankingsystemPackage.HW__NOTES:
				return getNotes();
			case BankingsystemPackage.HW__READY:
				return getReady();
			case BankingsystemPackage.HW__SHOW:
				return getShow();
			case BankingsystemPackage.HW__CONTROLLER:
				if (resolve) return getController();
				return basicGetController();
			case BankingsystemPackage.HW__IS_PIN_REQUESTED:
				return isIsPinRequested();
			case BankingsystemPackage.HW__SHOW_CARD_ON1_INVALID_PIN:
				return isShowCardOn1InvalidPin();
			case BankingsystemPackage.HW__RETRACT_CARD_ON3_INVALID_PIN:
				return isRetractCardOn3InvalidPin();
			case BankingsystemPackage.HW__RETRACT_CARD_ON_TIME_OUT:
				return getRetractCardOnTimeOut();
			case BankingsystemPackage.HW__RETRACT_MONEY_ON_TIME_OUT:
				return getRetractMoneyOnTimeOut();
			case BankingsystemPackage.HW__MONEY_TAKEN:
				return isMoneyTaken();
			case BankingsystemPackage.HW__CARD_TAKEN_BACK:
				return isCardTakenBack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.HW__INSERTED:
				setInserted((PhysicalBankCard)newValue);
				return;
			case BankingsystemPackage.HW__HELD:
				getHeld().clear();
				getHeld().addAll((Collection<? extends PhysicalBankCard>)newValue);
				return;
			case BankingsystemPackage.HW__NOTES:
				getNotes().clear();
				getNotes().addAll((Collection<? extends BankNote>)newValue);
				return;
			case BankingsystemPackage.HW__READY:
				getReady().clear();
				getReady().addAll((Collection<? extends BankNote>)newValue);
				return;
			case BankingsystemPackage.HW__SHOW:
				setShow((PhysicalBankCard)newValue);
				return;
			case BankingsystemPackage.HW__CONTROLLER:
				setController((ATMController)newValue);
				return;
			case BankingsystemPackage.HW__IS_PIN_REQUESTED:
				setIsPinRequested((Boolean)newValue);
				return;
			case BankingsystemPackage.HW__SHOW_CARD_ON1_INVALID_PIN:
				setShowCardOn1InvalidPin((Boolean)newValue);
				return;
			case BankingsystemPackage.HW__RETRACT_CARD_ON3_INVALID_PIN:
				setRetractCardOn3InvalidPin((Boolean)newValue);
				return;
			case BankingsystemPackage.HW__RETRACT_CARD_ON_TIME_OUT:
				setRetractCardOnTimeOut((Long)newValue);
				return;
			case BankingsystemPackage.HW__RETRACT_MONEY_ON_TIME_OUT:
				setRetractMoneyOnTimeOut((Long)newValue);
				return;
			case BankingsystemPackage.HW__MONEY_TAKEN:
				setMoneyTaken((Boolean)newValue);
				return;
			case BankingsystemPackage.HW__CARD_TAKEN_BACK:
				setCardTakenBack((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.HW__INSERTED:
				setInserted((PhysicalBankCard)null);
				return;
			case BankingsystemPackage.HW__HELD:
				getHeld().clear();
				return;
			case BankingsystemPackage.HW__NOTES:
				getNotes().clear();
				return;
			case BankingsystemPackage.HW__READY:
				getReady().clear();
				return;
			case BankingsystemPackage.HW__SHOW:
				setShow((PhysicalBankCard)null);
				return;
			case BankingsystemPackage.HW__CONTROLLER:
				setController((ATMController)null);
				return;
			case BankingsystemPackage.HW__IS_PIN_REQUESTED:
				setIsPinRequested(IS_PIN_REQUESTED_EDEFAULT);
				return;
			case BankingsystemPackage.HW__SHOW_CARD_ON1_INVALID_PIN:
				setShowCardOn1InvalidPin(SHOW_CARD_ON1_INVALID_PIN_EDEFAULT);
				return;
			case BankingsystemPackage.HW__RETRACT_CARD_ON3_INVALID_PIN:
				setRetractCardOn3InvalidPin(RETRACT_CARD_ON3_INVALID_PIN_EDEFAULT);
				return;
			case BankingsystemPackage.HW__RETRACT_CARD_ON_TIME_OUT:
				setRetractCardOnTimeOut(RETRACT_CARD_ON_TIME_OUT_EDEFAULT);
				return;
			case BankingsystemPackage.HW__RETRACT_MONEY_ON_TIME_OUT:
				setRetractMoneyOnTimeOut(RETRACT_MONEY_ON_TIME_OUT_EDEFAULT);
				return;
			case BankingsystemPackage.HW__MONEY_TAKEN:
				setMoneyTaken(MONEY_TAKEN_EDEFAULT);
				return;
			case BankingsystemPackage.HW__CARD_TAKEN_BACK:
				setCardTakenBack(CARD_TAKEN_BACK_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.HW__INSERTED:
				return inserted != null;
			case BankingsystemPackage.HW__HELD:
				return held != null && !held.isEmpty();
			case BankingsystemPackage.HW__NOTES:
				return notes != null && !notes.isEmpty();
			case BankingsystemPackage.HW__READY:
				return ready != null && !ready.isEmpty();
			case BankingsystemPackage.HW__SHOW:
				return show != null;
			case BankingsystemPackage.HW__CONTROLLER:
				return controller != null;
			case BankingsystemPackage.HW__IS_PIN_REQUESTED:
				return isPinRequested != IS_PIN_REQUESTED_EDEFAULT;
			case BankingsystemPackage.HW__SHOW_CARD_ON1_INVALID_PIN:
				return showCardOn1InvalidPin != SHOW_CARD_ON1_INVALID_PIN_EDEFAULT;
			case BankingsystemPackage.HW__RETRACT_CARD_ON3_INVALID_PIN:
				return retractCardOn3InvalidPin != RETRACT_CARD_ON3_INVALID_PIN_EDEFAULT;
			case BankingsystemPackage.HW__RETRACT_CARD_ON_TIME_OUT:
				return retractCardOnTimeOut != RETRACT_CARD_ON_TIME_OUT_EDEFAULT;
			case BankingsystemPackage.HW__RETRACT_MONEY_ON_TIME_OUT:
				return retractMoneyOnTimeOut != RETRACT_MONEY_ON_TIME_OUT_EDEFAULT;
			case BankingsystemPackage.HW__MONEY_TAKEN:
				return moneyTaken != MONEY_TAKEN_EDEFAULT;
			case BankingsystemPackage.HW__CARD_TAKEN_BACK:
				return cardTakenBack != CARD_TAKEN_BACK_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.HW___INSERT_CARD:
				insertCard();
				return null;
			case BankingsystemPackage.HW___ASK_PIN:
				askPin();
				return null;
			case BankingsystemPackage.HW___DISPLAY__STRING:
				display((String)arguments.get(0));
				return null;
			case BankingsystemPackage.HW___ENTER_PIN__INT:
				return enterPin((Integer)arguments.get(0));
			case BankingsystemPackage.HW___CANCEL_SESSION__INT:
				cancelSession((Integer)arguments.get(0));
				return null;
			case BankingsystemPackage.HW___PRESENT_CARD:
				presentCard();
				return null;
			case BankingsystemPackage.HW___CONFIGURE_SHOW_CARD_ON1_INVALID_PIN:
				configureShowCardOn1InvalidPin();
				return null;
			case BankingsystemPackage.HW___CONFIGURE_RETRACT_CARD_ON3_INVALID_PIN:
				configureRetractCardOn3InvalidPin();
				return null;
			case BankingsystemPackage.HW___CONFIGURE_RETRACT_CARD_ON_TIME_OUT__LONG:
				configureRetractCardOnTimeOut((Long)arguments.get(0));
				return null;
			case BankingsystemPackage.HW___OPERATION__OPERATIONS:
				return operation((Operations)arguments.get(0));
			case BankingsystemPackage.HW___WITHDRAW__INT:
				return withdraw((Integer)arguments.get(0));
			case BankingsystemPackage.HW___CONFIGURE_RETRACT_MONEY_ON_TIME_OUT__LONG:
				configureRetractMoneyOnTimeOut((Long)arguments.get(0));
				return null;
			case BankingsystemPackage.HW___TAKE_MONEY:
				return takeMoney();
			case BankingsystemPackage.HW___PRESENT_MONEY__INTEGER:
				presentMoney((Integer)arguments.get(0));
				return null;
			case BankingsystemPackage.HW___EXIT:
				exit();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isPinRequested: ");
		result.append(isPinRequested);
		result.append(", showCardOn1InvalidPin: ");
		result.append(showCardOn1InvalidPin);
		result.append(", retractCardOn3InvalidPin: ");
		result.append(retractCardOn3InvalidPin);
		result.append(", retractCardOnTimeOut: ");
		result.append(retractCardOnTimeOut);
		result.append(", retractMoneyOnTimeOut: ");
		result.append(retractMoneyOnTimeOut);
		result.append(", moneyTaken: ");
		result.append(moneyTaken);
		result.append(", cardTakenBack: ");
		result.append(cardTakenBack);
		result.append(')');
		return result.toString();
	}
	
//	/**
//	 * @generated NOT
//	 */
//	private Timer takeMoneyTimer;
	
//	/**
//	 * @generated NOT
//	 */
//	private Timer cardTakenTimer;

} //HWImpl
