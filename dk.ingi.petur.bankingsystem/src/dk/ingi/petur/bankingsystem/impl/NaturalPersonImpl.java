/**
 */
package dk.ingi.petur.bankingsystem.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.ingi.petur.bankingsystem.BankNote;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.HW;
import dk.ingi.petur.bankingsystem.InsertedCardStatus;
import dk.ingi.petur.bankingsystem.NaturalPerson;
import dk.ingi.petur.bankingsystem.OperationChoiceStatus;
import dk.ingi.petur.bankingsystem.Operations;
import dk.ingi.petur.bankingsystem.PhysicalBankCard;
import dk.ingi.petur.bankingsystem.WithdrawMoneyStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Natural Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl#getAvailableCards <em>Available Cards</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl#getUses <em>Uses</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl#getOwns <em>Owns</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.NaturalPersonImpl#isTakeMoney <em>Take Money</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NaturalPersonImpl extends ComponentImpl implements NaturalPerson {
	/**
	 * The cached value of the '{@link #getAvailableCards() <em>Available Cards</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableCards()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalBankCard> availableCards;

	/**
	 * The cached value of the '{@link #getUses() <em>Uses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUses()
	 * @generated
	 * @ordered
	 */
	protected EList<HW> uses;

	/**
	 * The cached value of the '{@link #getOwns() <em>Owns</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwns()
	 * @generated
	 * @ordered
	 */
	protected EList<BankNote> owns;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isTakeMoney() <em>Take Money</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTakeMoney()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TAKE_MONEY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTakeMoney() <em>Take Money</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTakeMoney()
	 * @generated
	 * @ordered
	 */
	protected boolean takeMoney = TAKE_MONEY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NaturalPersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.NATURAL_PERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalBankCard> getAvailableCards() {
		if (availableCards == null) {
			availableCards = new EObjectContainmentWithInverseEList<PhysicalBankCard>(PhysicalBankCard.class, this, BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS, BankingsystemPackage.PHYSICAL_BANK_CARD__OWNER);
		}
		return availableCards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HW> getUses() {
		if (uses == null) {
			uses = new EObjectResolvingEList<HW>(HW.class, this, BankingsystemPackage.NATURAL_PERSON__USES);
		}
		return uses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BankNote> getOwns() {
		if (owns == null) {
			owns = new EObjectContainmentEList<BankNote>(BankNote.class, this, BankingsystemPackage.NATURAL_PERSON__OWNS);
		}
		return owns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.NATURAL_PERSON__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTakeMoney() {
		return takeMoney;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTakeMoney(boolean newTakeMoney) {
		boolean oldTakeMoney = takeMoney;
		takeMoney = newTakeMoney;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.NATURAL_PERSON__TAKE_MONEY, oldTakeMoney, takeMoney));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public InsertedCardStatus insertCard(PhysicalBankCard card) throws NotInFrontOfATMHardwareException, NotMyPhysicalCardException {
		EList<PhysicalBankCard> availableCards = getAvailableCards();
		assert(availableCards != null);
		
		boolean isMyCard = true;
		if (card != null && availableCards.contains(card)) {
			isMyCard = true;
		}
		if (!isMyCard)
			throw new NotMyPhysicalCardException("Not my physical card: " + card.getNumber());
		if (getUses().isEmpty()) 
			throw new NotInFrontOfATMHardwareException("Not in front of any hardware");
		EList<HW> uses = getUses();
		assert(uses != null && uses.size() > 0);
		HW hw = uses.get(0);
		
		return hw.setInserted(card);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public InsertedCardStatus enterPin(int pin) {
		final EList<HW> uses = getUses();
		assert(uses != null);
		assert(uses.size() > 0);
		if (pin < 0)
			throw new IllegalArgumentException("Pin must not be negative.");
		
		final InsertedCardStatus status = uses.get(0).enterPin(pin);
		assert(status != null);

		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public WithdrawMoneyStatus withdraw(int amount) {
		if (amount <= 0)
			throw new IllegalArgumentException("Amount must be positive and above zero.");
		WithdrawMoneyStatus wms;
		HW hw = getUses().get(0);
		OperationChoiceStatus st = hw.operation(Operations.WITHDRAW);
		assert(st != null);
		switch (st) {
		case AVAILABLE:
			wms = hw.withdraw(amount);
			switch (wms) {
			case TAKE_MONEY:
				if (isTakeMoney()) {
					getOwns().add(hw.getReady().get(0));
				}
				break;
			default:
				break;
			}
			break;
		case UNAVAILABLE:
			wms = WithdrawMoneyStatus.UNAVAILABLE_OP;
			break;
		default:
			wms = WithdrawMoneyStatus.UNAVAILABLE_OP;
			break;
		}
		assert(wms != null);
		return wms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public void configureTakeMoney(boolean b) {
		setTakeMoney(b);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @author Lom Hillah
	 * @generated NOT
	 */
	public void takeCard() {
		this.getAvailableCards().add(getUses().get(0).getShow());
		getUses().get(0).takeCard();
		assert(getUses().get(0).getHeld().size() == 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void takeMoney() {
		assert(getUses().size() <= 1);
		if (getUses().size() == 0) {
			System.err.println("Not in front of an ATM");
			System.exit(-1);
		}
		getOwns().addAll(getUses().get(0).takeMoney());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void exit() {
		EList<HW> uses = getUses();
		assert(uses.size() <= 1);
		
		if (!uses.isEmpty())
			uses.get(0).exit();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAvailableCards()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS:
				return ((InternalEList<?>)getAvailableCards()).basicRemove(otherEnd, msgs);
			case BankingsystemPackage.NATURAL_PERSON__OWNS:
				return ((InternalEList<?>)getOwns()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS:
				return getAvailableCards();
			case BankingsystemPackage.NATURAL_PERSON__USES:
				return getUses();
			case BankingsystemPackage.NATURAL_PERSON__OWNS:
				return getOwns();
			case BankingsystemPackage.NATURAL_PERSON__NAME:
				return getName();
			case BankingsystemPackage.NATURAL_PERSON__TAKE_MONEY:
				return isTakeMoney();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS:
				getAvailableCards().clear();
				getAvailableCards().addAll((Collection<? extends PhysicalBankCard>)newValue);
				return;
			case BankingsystemPackage.NATURAL_PERSON__USES:
				getUses().clear();
				getUses().addAll((Collection<? extends HW>)newValue);
				return;
			case BankingsystemPackage.NATURAL_PERSON__OWNS:
				getOwns().clear();
				getOwns().addAll((Collection<? extends BankNote>)newValue);
				return;
			case BankingsystemPackage.NATURAL_PERSON__NAME:
				setName((String)newValue);
				return;
			case BankingsystemPackage.NATURAL_PERSON__TAKE_MONEY:
				setTakeMoney((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS:
				getAvailableCards().clear();
				return;
			case BankingsystemPackage.NATURAL_PERSON__USES:
				getUses().clear();
				return;
			case BankingsystemPackage.NATURAL_PERSON__OWNS:
				getOwns().clear();
				return;
			case BankingsystemPackage.NATURAL_PERSON__NAME:
				setName(NAME_EDEFAULT);
				return;
			case BankingsystemPackage.NATURAL_PERSON__TAKE_MONEY:
				setTakeMoney(TAKE_MONEY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.NATURAL_PERSON__AVAILABLE_CARDS:
				return availableCards != null && !availableCards.isEmpty();
			case BankingsystemPackage.NATURAL_PERSON__USES:
				return uses != null && !uses.isEmpty();
			case BankingsystemPackage.NATURAL_PERSON__OWNS:
				return owns != null && !owns.isEmpty();
			case BankingsystemPackage.NATURAL_PERSON__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case BankingsystemPackage.NATURAL_PERSON__TAKE_MONEY:
				return takeMoney != TAKE_MONEY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.NATURAL_PERSON___INSERT_CARD__PHYSICALBANKCARD:
				try {
					return insertCard((PhysicalBankCard)arguments.get(0));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case BankingsystemPackage.NATURAL_PERSON___ENTER_PIN__INT:
				return enterPin((Integer)arguments.get(0));
			case BankingsystemPackage.NATURAL_PERSON___WITHDRAW__INT:
				return withdraw((Integer)arguments.get(0));
			case BankingsystemPackage.NATURAL_PERSON___CONFIGURE_TAKE_MONEY__BOOLEAN:
				configureTakeMoney((Boolean)arguments.get(0));
				return null;
			case BankingsystemPackage.NATURAL_PERSON___TAKE_CARD:
				takeCard();
				return null;
			case BankingsystemPackage.NATURAL_PERSON___TAKE_MONEY:
				takeMoney();
				return null;
			case BankingsystemPackage.NATURAL_PERSON___EXIT:
				exit();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", takeMoney: ");
		result.append(takeMoney);
		result.append(')');
		return result.toString();
	}

} //NaturalPersonImpl
