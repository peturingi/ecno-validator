/**
 */
package dk.ingi.petur.bankingsystem.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.Account;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankCard;
import dk.ingi.petur.bankingsystem.BankingsystemPackage;
import dk.ingi.petur.bankingsystem.Session;
import dk.ingi.petur.bankingsystem.SessionOpening;

import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Session</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionImpl#getCard <em>Card</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionImpl#getATM <em>ATM</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionImpl#getAccount <em>Account</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.impl.SessionImpl#getOpeningStatus <em>Opening Status</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SessionImpl extends ComponentImpl implements Session {
	/**
	 * The cached value of the '{@link #getCard() <em>Card</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCard()
	 * @generated
	 * @ordered
	 */
	protected BankCard card;

	/**
	 * The cached value of the '{@link #getATM() <em>ATM</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATM()
	 * @generated
	 * @ordered
	 */
	protected ATMController atm;

	/**
	 * The cached value of the '{@link #getAccount() <em>Account</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccount()
	 * @generated
	 * @ordered
	 */
	protected Account account;

	/**
	 * The cached value of the '{@link #getOpeningStatus() <em>Opening Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpeningStatus()
	 * @generated
	 * @ordered
	 */
	protected SessionOpening openingStatus;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SessionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingsystemPackage.Literals.SESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankCard getCard() {
		if (card != null && card.eIsProxy()) {
			InternalEObject oldCard = (InternalEObject)card;
			card = (BankCard)eResolveProxy(oldCard);
			if (card != oldCard) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.SESSION__CARD, oldCard, card));
			}
		}
		return card;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankCard basicGetCard() {
		return card;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCard(BankCard newCard) {
		BankCard oldCard = card;
		card = newCard;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION__CARD, oldCard, card));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATMController getATM() {
		if (atm != null && atm.eIsProxy()) {
			InternalEObject oldATM = (InternalEObject)atm;
			atm = (ATMController)eResolveProxy(oldATM);
			if (atm != oldATM) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.SESSION__ATM, oldATM, atm));
			}
		}
		return atm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATMController basicGetATM() {
		return atm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATM(ATMController newATM) {
		ATMController oldATM = atm;
		atm = newATM;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION__ATM, oldATM, atm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccountMngr getOwner() {
		if (eContainerFeatureID() != BankingsystemPackage.SESSION__OWNER) return null;
		return (AccountMngr)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwner(AccountMngr newOwner, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOwner, BankingsystemPackage.SESSION__OWNER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwner(AccountMngr newOwner) {
		if (newOwner != eInternalContainer() || (eContainerFeatureID() != BankingsystemPackage.SESSION__OWNER && newOwner != null)) {
			if (EcoreUtil.isAncestor(this, newOwner))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOwner != null)
				msgs = ((InternalEObject)newOwner).eInverseAdd(this, BankingsystemPackage.ACCOUNT_MNGR__SESSIONS, AccountMngr.class, msgs);
			msgs = basicSetOwner(newOwner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION__OWNER, newOwner, newOwner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Account getAccount() {
		if (account != null && account.eIsProxy()) {
			InternalEObject oldAccount = (InternalEObject)account;
			account = (Account)eResolveProxy(oldAccount);
			if (account != oldAccount) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BankingsystemPackage.SESSION__ACCOUNT, oldAccount, account));
			}
		}
		return account;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Account basicGetAccount() {
		return account;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccount(Account newAccount) {
		Account oldAccount = account;
		account = newAccount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION__ACCOUNT, oldAccount, account));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionOpening getOpeningStatus() {
		return openingStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOpeningStatus(SessionOpening newOpeningStatus, NotificationChain msgs) {
		SessionOpening oldOpeningStatus = openingStatus;
		openingStatus = newOpeningStatus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION__OPENING_STATUS, oldOpeningStatus, newOpeningStatus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpeningStatus(SessionOpening newOpeningStatus) {
		if (newOpeningStatus != openingStatus) {
			NotificationChain msgs = null;
			if (openingStatus != null)
				msgs = ((InternalEObject)openingStatus).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BankingsystemPackage.SESSION__OPENING_STATUS, null, msgs);
			if (newOpeningStatus != null)
				msgs = ((InternalEObject)newOpeningStatus).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BankingsystemPackage.SESSION__OPENING_STATUS, null, msgs);
			msgs = basicSetOpeningStatus(newOpeningStatus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankingsystemPackage.SESSION__OPENING_STATUS, newOpeningStatus, newOpeningStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * test
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void withdraw(int amount) {
		this.account.withdraw(amount);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author P�tur Ingi Egilsson
	 * @generated NOT
	 */
	public void exitSession() {
		getOwner().exitSession2();
	}
	
		
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.SESSION__OWNER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOwner((AccountMngr)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankingsystemPackage.SESSION__OWNER:
				return basicSetOwner(null, msgs);
			case BankingsystemPackage.SESSION__OPENING_STATUS:
				return basicSetOpeningStatus(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case BankingsystemPackage.SESSION__OWNER:
				return eInternalContainer().eInverseRemove(this, BankingsystemPackage.ACCOUNT_MNGR__SESSIONS, AccountMngr.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankingsystemPackage.SESSION__CARD:
				if (resolve) return getCard();
				return basicGetCard();
			case BankingsystemPackage.SESSION__ATM:
				if (resolve) return getATM();
				return basicGetATM();
			case BankingsystemPackage.SESSION__OWNER:
				return getOwner();
			case BankingsystemPackage.SESSION__ACCOUNT:
				if (resolve) return getAccount();
				return basicGetAccount();
			case BankingsystemPackage.SESSION__OPENING_STATUS:
				return getOpeningStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankingsystemPackage.SESSION__CARD:
				setCard((BankCard)newValue);
				return;
			case BankingsystemPackage.SESSION__ATM:
				setATM((ATMController)newValue);
				return;
			case BankingsystemPackage.SESSION__OWNER:
				setOwner((AccountMngr)newValue);
				return;
			case BankingsystemPackage.SESSION__ACCOUNT:
				setAccount((Account)newValue);
				return;
			case BankingsystemPackage.SESSION__OPENING_STATUS:
				setOpeningStatus((SessionOpening)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.SESSION__CARD:
				setCard((BankCard)null);
				return;
			case BankingsystemPackage.SESSION__ATM:
				setATM((ATMController)null);
				return;
			case BankingsystemPackage.SESSION__OWNER:
				setOwner((AccountMngr)null);
				return;
			case BankingsystemPackage.SESSION__ACCOUNT:
				setAccount((Account)null);
				return;
			case BankingsystemPackage.SESSION__OPENING_STATUS:
				setOpeningStatus((SessionOpening)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankingsystemPackage.SESSION__CARD:
				return card != null;
			case BankingsystemPackage.SESSION__ATM:
				return atm != null;
			case BankingsystemPackage.SESSION__OWNER:
				return getOwner() != null;
			case BankingsystemPackage.SESSION__ACCOUNT:
				return account != null;
			case BankingsystemPackage.SESSION__OPENING_STATUS:
				return openingStatus != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingsystemPackage.SESSION___WITHDRAW__INT:
				withdraw((Integer)arguments.get(0));
				return null;
			case BankingsystemPackage.SESSION___EXIT_SESSION:
				exitSession();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //SessionImpl
