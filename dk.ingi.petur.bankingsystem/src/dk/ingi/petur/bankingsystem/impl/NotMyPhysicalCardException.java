package dk.ingi.petur.bankingsystem.impl;

/**
 * @author Lom Hillah
 *
 */
public class NotMyPhysicalCardException extends Exception {

	private static final long serialVersionUID = -6435831702867451672L;

	public NotMyPhysicalCardException() {
		super();
	}

	public NotMyPhysicalCardException(String message) {
		super(message);
	}

	public NotMyPhysicalCardException(Throwable cause) {
		super(cause);
	}

	public NotMyPhysicalCardException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotMyPhysicalCardException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
