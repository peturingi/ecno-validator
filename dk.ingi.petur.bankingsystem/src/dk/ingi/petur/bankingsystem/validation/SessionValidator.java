/**
 *
 * $Id$
 */
package dk.ingi.petur.bankingsystem.validation;

import dk.ingi.petur.bankingsystem.ATMController;
import dk.ingi.petur.bankingsystem.Account;
import dk.ingi.petur.bankingsystem.AccountMngr;
import dk.ingi.petur.bankingsystem.BankCard;
import dk.ingi.petur.bankingsystem.SessionOpening;

/**
 * A sample validator interface for {@link dk.ingi.petur.bankingsystem.Session}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface SessionValidator {
	boolean validate();

	boolean validateCard(BankCard value);
	boolean validateATM(ATMController value);
	boolean validateOwner(AccountMngr value);
	boolean validateAccount(Account value);

	boolean validateOpeningStatus(SessionOpening value);
}
