/**
 */
package dk.ingi.petur.bankingsystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HW</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getInserted <em>Inserted</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getHeld <em>Held</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getNotes <em>Notes</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getReady <em>Ready</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getShow <em>Show</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getController <em>Controller</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#isIsPinRequested <em>Is Pin Requested</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#isShowCardOn1InvalidPin <em>Show Card On1 Invalid Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#isRetractCardOn3InvalidPin <em>Retract Card On3 Invalid Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getRetractCardOnTimeOut <em>Retract Card On Time Out</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#getRetractMoneyOnTimeOut <em>Retract Money On Time Out</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#isMoneyTaken <em>Money Taken</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.HW#isCardTakenBack <em>Card Taken Back</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW()
 * @model
 * @generated
 */
public interface HW extends Component {
	/**
	 * Returns the value of the '<em><b>Inserted</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inserted</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inserted</em>' reference.
	 * @see #setInserted(PhysicalBankCard)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_Inserted()
	 * @model
	 * @generated
	 */
	PhysicalBankCard getInserted();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#getInserted <em>Inserted</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return
	 * @param value the new value of the '<em>Inserted</em>' containment reference.
	 * @see #getInserted()
	 * @generated NOT
	 */
	InsertedCardStatus setInserted(PhysicalBankCard value);

	/**
	 * Returns the value of the '<em><b>Held</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.PhysicalBankCard}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Held</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Held</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_Held()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalBankCard> getHeld();

	/**
	 * Returns the value of the '<em><b>Notes</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.BankNote}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notes</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_Notes()
	 * @model containment="true"
	 * @generated
	 */
	EList<BankNote> getNotes();

	/**
	 * Returns the value of the '<em><b>Ready</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.bankingsystem.BankNote}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ready</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ready</em>' containment reference list.
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_Ready()
	 * @model containment="true"
	 * @generated
	 */
	EList<BankNote> getReady();
	
	/**
	 * @generated NOT
	 */
	void takeCard();

	/**
	 * Returns the value of the '<em><b>Show</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show</em>' containment reference.
	 * @see #setShow(PhysicalBankCard)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_Show()
	 * @model containment="true"
	 * @generated
	 */
	PhysicalBankCard getShow();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#getShow <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show</em>' containment reference.
	 * @see #getShow()
	 * @generated
	 */
	void setShow(PhysicalBankCard value);

	/**
	 * Returns the value of the '<em><b>Controller</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.ATMController#getHw <em>Hw</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' reference.
	 * @see #setController(ATMController)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_Controller()
	 * @see dk.ingi.petur.bankingsystem.ATMController#getHw
	 * @model opposite="hw" required="true"
	 * @generated
	 */
	ATMController getController();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#getController <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(ATMController value);

	/**
	 * Returns the value of the '<em><b>Is Pin Requested</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Pin Requested</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Pin Requested</em>' attribute.
	 * @see #setIsPinRequested(boolean)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_IsPinRequested()
	 * @model
	 * @generated
	 */
	boolean isIsPinRequested();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#isIsPinRequested <em>Is Pin Requested</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Pin Requested</em>' attribute.
	 * @see #isIsPinRequested()
	 * @generated
	 */
	void setIsPinRequested(boolean value);

	/**
	 * Returns the value of the '<em><b>Show Card On1 Invalid Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Card On1 Invalid Pin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Card On1 Invalid Pin</em>' attribute.
	 * @see #setShowCardOn1InvalidPin(boolean)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_ShowCardOn1InvalidPin()
	 * @model
	 * @generated
	 */
	boolean isShowCardOn1InvalidPin();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#isShowCardOn1InvalidPin <em>Show Card On1 Invalid Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Card On1 Invalid Pin</em>' attribute.
	 * @see #isShowCardOn1InvalidPin()
	 * @generated
	 */
	void setShowCardOn1InvalidPin(boolean value);

	/**
	 * Returns the value of the '<em><b>Retract Card On3 Invalid Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Retract Card On3 Invalid Pin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Retract Card On3 Invalid Pin</em>' attribute.
	 * @see #setRetractCardOn3InvalidPin(boolean)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_RetractCardOn3InvalidPin()
	 * @model
	 * @generated
	 */
	boolean isRetractCardOn3InvalidPin();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#isRetractCardOn3InvalidPin <em>Retract Card On3 Invalid Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Retract Card On3 Invalid Pin</em>' attribute.
	 * @see #isRetractCardOn3InvalidPin()
	 * @generated
	 */
	void setRetractCardOn3InvalidPin(boolean value);

	/**
	 * Returns the value of the '<em><b>Retract Card On Time Out</b></em>' attribute.
	 * The default value is <code>"5000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Retract Card On Time Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Retract Card On Time Out</em>' attribute.
	 * @see #setRetractCardOnTimeOut(long)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_RetractCardOnTimeOut()
	 * @model default="5000"
	 * @generated
	 */
	long getRetractCardOnTimeOut();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#getRetractCardOnTimeOut <em>Retract Card On Time Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Retract Card On Time Out</em>' attribute.
	 * @see #getRetractCardOnTimeOut()
	 * @generated
	 */
	void setRetractCardOnTimeOut(long value);

	/**
	 * Returns the value of the '<em><b>Retract Money On Time Out</b></em>' attribute.
	 * The default value is <code>"3000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Retract Money On Time Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Retract Money On Time Out</em>' attribute.
	 * @see #setRetractMoneyOnTimeOut(long)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_RetractMoneyOnTimeOut()
	 * @model default="3000"
	 * @generated
	 */
	long getRetractMoneyOnTimeOut();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#getRetractMoneyOnTimeOut <em>Retract Money On Time Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Retract Money On Time Out</em>' attribute.
	 * @see #getRetractMoneyOnTimeOut()
	 * @generated
	 */
	void setRetractMoneyOnTimeOut(long value);

	/**
	 * Returns the value of the '<em><b>Money Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Money Taken</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Money Taken</em>' attribute.
	 * @see #setMoneyTaken(boolean)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_MoneyTaken()
	 * @model
	 * @generated
	 */
	boolean isMoneyTaken();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#isMoneyTaken <em>Money Taken</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Money Taken</em>' attribute.
	 * @see #isMoneyTaken()
	 * @generated
	 */
	void setMoneyTaken(boolean value);

	/**
	 * Returns the value of the '<em><b>Card Taken Back</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Card Taken Back</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Card Taken Back</em>' attribute.
	 * @see #setCardTakenBack(boolean)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getHW_CardTakenBack()
	 * @model default="false"
	 * @generated
	 */
	boolean isCardTakenBack();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.HW#isCardTakenBack <em>Card Taken Back</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Card Taken Back</em>' attribute.
	 * @see #isCardTakenBack()
	 * @generated
	 */
	void setCardTakenBack(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void insertCard();
	
	/**
	 * @generated NOT
	 */
	void askPin();

	/**
	 * @generated NOT
	 */
	InsertedCardStatus enterPin(int pin);

	/**
	 * @generated NOT
	 */
	void cancelSession(int cardNum);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void presentCard();

	/**
	 * @generated NOT
	 */
	void configureShowCardOn1InvalidPin();

	/**
	 * @generated NOT
	 */
	void configureRetractCardOn3InvalidPin();

	/**
	 * @generated NOT
	 */
	void configureRetractCardOnTimeOut(long timeOutMillisec);

	/**
	 * @generated NOT
	 */
	void configureRetractMoneyOnTimeOut(long timeOutMillisec);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<BankNote> takeMoney();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void presentMoney(Integer amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void exit();

	/**
	 * @generated NOT
	 */
	void display(String msg);

	/**
	 * @generated NOT
	 */
	OperationChoiceStatus operation(Operations op);

	/**
	 * @generated NOT
	 */
	WithdrawMoneyStatus withdraw(int i);
	
} // HW
