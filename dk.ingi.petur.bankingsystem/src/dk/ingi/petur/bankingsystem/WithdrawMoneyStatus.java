/**
 */
package dk.ingi.petur.bankingsystem;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Withdraw Money Status</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getWithdrawMoneyStatus()
 * @model
 * @generated
 */
public enum WithdrawMoneyStatus implements Enumerator {
	/**
	 * The '<em><b>NO SESSION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_SESSION_VALUE
	 * @generated
	 * @ordered
	 */
	NO_SESSION(0, "NO_SESSION", "NO_SESSION"),

	/**
	 * The '<em><b>DISPENSE MONEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISPENSE_MONEY_VALUE
	 * @generated
	 * @ordered
	 */
	DISPENSE_MONEY(1, "DISPENSE_MONEY", "DISPENSE_MONEY"),

	/**
	 * The '<em><b>INSUFFICIENT BALANCE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INSUFFICIENT_BALANCE_VALUE
	 * @generated
	 * @ordered
	 */
	INSUFFICIENT_BALANCE(2, "INSUFFICIENT_BALANCE", "INSUFFICIENT_BALANCE"),

	/**
	 * The '<em><b>NETWORK ERROR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NETWORK_ERROR_VALUE
	 * @generated
	 * @ordered
	 */
	NETWORK_ERROR(3, "NETWORK_ERROR", ""),

	/**
	 * The '<em><b>SESSION ERROR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SESSION_ERROR_VALUE
	 * @generated
	 * @ordered
	 */
	SESSION_ERROR(4, "SESSION_ERROR", "SESSION_ERROR"),

	/**
	 * The '<em><b>TAKE MONEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TAKE_MONEY_VALUE
	 * @generated
	 * @ordered
	 */
	TAKE_MONEY(5, "TAKE_MONEY", "TAKE_MONEY"),

	/**
	 * The '<em><b>UNAVAILABLE OP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNAVAILABLE_OP_VALUE
	 * @generated
	 * @ordered
	 */
	UNAVAILABLE_OP(6, "UNAVAILABLE_OP", "UNAVAILABLE_OP");

	/**
	 * The '<em><b>NO SESSION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NO SESSION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_SESSION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NO_SESSION_VALUE = 0;

	/**
	 * The '<em><b>DISPENSE MONEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISPENSE MONEY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISPENSE_MONEY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DISPENSE_MONEY_VALUE = 1;

	/**
	 * The '<em><b>INSUFFICIENT BALANCE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INSUFFICIENT BALANCE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INSUFFICIENT_BALANCE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INSUFFICIENT_BALANCE_VALUE = 2;

	/**
	 * The '<em><b>NETWORK ERROR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NETWORK ERROR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NETWORK_ERROR
	 * @model literal=""
	 * @generated
	 * @ordered
	 */
	public static final int NETWORK_ERROR_VALUE = 3;

	/**
	 * The '<em><b>SESSION ERROR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SESSION ERROR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SESSION_ERROR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SESSION_ERROR_VALUE = 4;

	/**
	 * The '<em><b>TAKE MONEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TAKE MONEY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TAKE_MONEY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TAKE_MONEY_VALUE = 5;

	/**
	 * The '<em><b>UNAVAILABLE OP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNAVAILABLE OP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNAVAILABLE_OP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNAVAILABLE_OP_VALUE = 6;

	/**
	 * An array of all the '<em><b>Withdraw Money Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final WithdrawMoneyStatus[] VALUES_ARRAY =
		new WithdrawMoneyStatus[] {
			NO_SESSION,
			DISPENSE_MONEY,
			INSUFFICIENT_BALANCE,
			NETWORK_ERROR,
			SESSION_ERROR,
			TAKE_MONEY,
			UNAVAILABLE_OP,
		};

	/**
	 * A public read-only list of all the '<em><b>Withdraw Money Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<WithdrawMoneyStatus> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Withdraw Money Status</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static WithdrawMoneyStatus get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			WithdrawMoneyStatus result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Withdraw Money Status</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static WithdrawMoneyStatus getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			WithdrawMoneyStatus result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Withdraw Money Status</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static WithdrawMoneyStatus get(int value) {
		switch (value) {
			case NO_SESSION_VALUE: return NO_SESSION;
			case DISPENSE_MONEY_VALUE: return DISPENSE_MONEY;
			case INSUFFICIENT_BALANCE_VALUE: return INSUFFICIENT_BALANCE;
			case NETWORK_ERROR_VALUE: return NETWORK_ERROR;
			case SESSION_ERROR_VALUE: return SESSION_ERROR;
			case TAKE_MONEY_VALUE: return TAKE_MONEY;
			case UNAVAILABLE_OP_VALUE: return UNAVAILABLE_OP;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private WithdrawMoneyStatus(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //WithdrawMoneyStatus
