/**
 */
package dk.ingi.petur.bankingsystem;

import dk.ingi.petur.bankingsystem.impl.NotInFrontOfATMHardwareException;
import dk.ingi.petur.bankingsystem.impl.NotMyPhysicalCardException;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Bank Card</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getNumber <em>Number</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getPin <em>Pin</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getIssuer <em>Issuer</em>}</li>
 *   <li>{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getPhysicalBankCard()
 * @model
 * @generated
 */
public interface PhysicalBankCard extends Component {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getPhysicalBankCard_Number()
	 * @model required="true"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * Returns the value of the '<em><b>Pin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pin</em>' attribute.
	 * @see #setPin(int)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getPhysicalBankCard_Pin()
	 * @model
	 * @generated
	 */
	int getPin();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getPin <em>Pin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pin</em>' attribute.
	 * @see #getPin()
	 * @generated
	 */
	void setPin(int value);

	/**
	 * Returns the value of the '<em><b>Issuer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Issuer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Issuer</em>' reference.
	 * @see #setIssuer(AccountMngr)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getPhysicalBankCard_Issuer()
	 * @model
	 * @generated
	 */
	AccountMngr getIssuer();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getIssuer <em>Issuer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Issuer</em>' reference.
	 * @see #getIssuer()
	 * @generated
	 */
	void setIssuer(AccountMngr value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.bankingsystem.NaturalPerson#getAvailableCards <em>Available Cards</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(NaturalPerson)
	 * @see dk.ingi.petur.bankingsystem.BankingsystemPackage#getPhysicalBankCard_Owner()
	 * @see dk.ingi.petur.bankingsystem.NaturalPerson#getAvailableCards
	 * @model opposite="availableCards" transient="false"
	 * @generated
	 */
	NaturalPerson getOwner();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.bankingsystem.PhysicalBankCard#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(NaturalPerson value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void insertCard() throws NotInFrontOfATMHardwareException, NotMyPhysicalCardException;

} // PhysicalBankCard
