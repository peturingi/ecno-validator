/**
 */
package dk.ingi.petur.trace;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lifeline</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.Lifeline#getMessageOccurrences <em>Message Occurrences</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Lifeline#getObject <em>Object</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Lifeline#getObjectClass <em>Object Class</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.trace.TracePackage#getLifeline()
 * @model
 * @generated
 */
public interface Lifeline extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Message Occurrences</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.trace.MessageOccurrence}.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.trace.MessageOccurrence#getLifeline <em>Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Occurrences</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Occurrences</em>' containment reference list.
	 * @see dk.ingi.petur.trace.TracePackage#getLifeline_MessageOccurrences()
	 * @see dk.ingi.petur.trace.MessageOccurrence#getLifeline
	 * @model opposite="lifeline" containment="true"
	 * @generated
	 */
	EList<MessageOccurrence> getMessageOccurrences();

	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(EObject)
	 * @see dk.ingi.petur.trace.TracePackage#getLifeline_Object()
	 * @model required="true"
	 * @generated
	 */
	EObject getObject();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.Lifeline#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Object Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Class</em>' attribute.
	 * @see #setObjectClass(String)
	 * @see dk.ingi.petur.trace.TracePackage#getLifeline_ObjectClass()
	 * @model
	 * @generated
	 */
	String getObjectClass();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.Lifeline#getObjectClass <em>Object Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Class</em>' attribute.
	 * @see #getObjectClass()
	 * @generated
	 */
	void setObjectClass(String value);

} // Lifeline
