/**
 */
package dk.ingi.petur.trace;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.Trace#getLifelines <em>Lifelines</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Trace#getSetting <em>Setting</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Trace#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.trace.TracePackage#getTrace()
 * @model
 * @generated
 */
public interface Trace extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Lifelines</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.trace.Lifeline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifelines</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifelines</em>' containment reference list.
	 * @see dk.ingi.petur.trace.TracePackage#getTrace_Lifelines()
	 * @model containment="true"
	 * @generated
	 */
	EList<Lifeline> getLifelines();

	/**
	 * Returns the value of the '<em><b>Setting</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Setting</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Setting</em>' containment reference.
	 * @see #setSetting(EObject)
	 * @see dk.ingi.petur.trace.TracePackage#getTrace_Setting()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EObject getSetting();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.Trace#getSetting <em>Setting</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Setting</em>' containment reference.
	 * @see #getSetting()
	 * @generated
	 */
	void setSetting(EObject value);

	/**
	 * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.trace.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Messages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Messages</em>' containment reference list.
	 * @see dk.ingi.petur.trace.TracePackage#getTrace_Messages()
	 * @model containment="true"
	 * @generated
	 */
	EList<Message> getMessages();

} // Trace
