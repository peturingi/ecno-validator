/**
 */
package dk.ingi.petur.trace;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.Message#getSource <em>Source</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Message#getTarget <em>Target</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Message#getPayload <em>Payload</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Message#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.trace.TracePackage#getMessage()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='PayloadNotEmpty HasSourceOrTarget'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot PayloadNotEmpty='payload &lt;&gt; \'\'' HasSourceOrTarget='source &lt;&gt; null or target &lt;&gt; null'"
 * @generated
 */
public interface Message extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(MessageOccurrence)
	 * @see dk.ingi.petur.trace.TracePackage#getMessage_Source()
	 * @model
	 * @generated
	 */
	MessageOccurrence getSource();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.Message#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(MessageOccurrence value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(MessageOccurrence)
	 * @see dk.ingi.petur.trace.TracePackage#getMessage_Target()
	 * @model
	 * @generated
	 */
	MessageOccurrence getTarget();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.Message#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(MessageOccurrence value);

	/**
	 * Returns the value of the '<em><b>Payload</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Payload</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Payload</em>' attribute.
	 * @see #setPayload(String)
	 * @see dk.ingi.petur.trace.TracePackage#getMessage_Payload()
	 * @model required="true"
	 * @generated
	 */
	String getPayload();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.Message#getPayload <em>Payload</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Payload</em>' attribute.
	 * @see #getPayload()
	 * @generated
	 */
	void setPayload(String value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link dk.ingi.petur.trace.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see dk.ingi.petur.trace.TracePackage#getMessage_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getParameters();

} // Message
