/**
 */
package dk.ingi.petur.trace;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.Value#getObjectValues <em>Object Values</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Value#getIntValues <em>Int Values</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.Value#getStringValues <em>String Values</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.trace.TracePackage#getValue()
 * @model
 * @generated
 */
public interface Value extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Object Values</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Values</em>' reference list.
	 * @see dk.ingi.petur.trace.TracePackage#getValue_ObjectValues()
	 * @model
	 * @generated
	 */
	EList<EObject> getObjectValues();

	/**
	 * Returns the value of the '<em><b>Int Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Int Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Int Values</em>' attribute list.
	 * @see dk.ingi.petur.trace.TracePackage#getValue_IntValues()
	 * @model
	 * @generated
	 */
	EList<Integer> getIntValues();

	/**
	 * Returns the value of the '<em><b>String Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Values</em>' attribute list.
	 * @see dk.ingi.petur.trace.TracePackage#getValue_StringValues()
	 * @model
	 * @generated
	 */
	EList<String> getStringValues();

} // Value
