/**
 */
package dk.ingi.petur.trace.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.ingi.petur.trace.Lifeline;
import dk.ingi.petur.trace.MessageOccurrence;
import dk.ingi.petur.trace.MessageOccurrenceType;
import dk.ingi.petur.trace.TracePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Occurrence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.impl.MessageOccurrenceImpl#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.impl.MessageOccurrenceImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.impl.MessageOccurrenceImpl#getLifeline <em>Lifeline</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageOccurrenceImpl extends NamedElementImpl implements MessageOccurrence {
	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final int TIMESTAMP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected int timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final MessageOccurrenceType TYPE_EDEFAULT = MessageOccurrenceType.SEND;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected MessageOccurrenceType type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageOccurrenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.MESSAGE_OCCURRENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(int newTimestamp) {
		int oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.MESSAGE_OCCURRENCE__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageOccurrenceType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MessageOccurrenceType newType) {
		MessageOccurrenceType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.MESSAGE_OCCURRENCE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lifeline getLifeline() {
		if (eContainerFeatureID() != TracePackage.MESSAGE_OCCURRENCE__LIFELINE) return null;
		return (Lifeline)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLifeline(Lifeline newLifeline, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLifeline, TracePackage.MESSAGE_OCCURRENCE__LIFELINE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLifeline(Lifeline newLifeline) {
		if (newLifeline != eInternalContainer() || (eContainerFeatureID() != TracePackage.MESSAGE_OCCURRENCE__LIFELINE && newLifeline != null)) {
			if (EcoreUtil.isAncestor(this, newLifeline))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLifeline != null)
				msgs = ((InternalEObject)newLifeline).eInverseAdd(this, TracePackage.LIFELINE__MESSAGE_OCCURRENCES, Lifeline.class, msgs);
			msgs = basicSetLifeline(newLifeline, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.MESSAGE_OCCURRENCE__LIFELINE, newLifeline, newLifeline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLifeline((Lifeline)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				return basicSetLifeline(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				return eInternalContainer().eInverseRemove(this, TracePackage.LIFELINE__MESSAGE_OCCURRENCES, Lifeline.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.MESSAGE_OCCURRENCE__TIMESTAMP:
				return getTimestamp();
			case TracePackage.MESSAGE_OCCURRENCE__TYPE:
				return getType();
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				return getLifeline();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.MESSAGE_OCCURRENCE__TIMESTAMP:
				setTimestamp((Integer)newValue);
				return;
			case TracePackage.MESSAGE_OCCURRENCE__TYPE:
				setType((MessageOccurrenceType)newValue);
				return;
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				setLifeline((Lifeline)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.MESSAGE_OCCURRENCE__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
			case TracePackage.MESSAGE_OCCURRENCE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				setLifeline((Lifeline)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.MESSAGE_OCCURRENCE__TIMESTAMP:
				return timestamp != TIMESTAMP_EDEFAULT;
			case TracePackage.MESSAGE_OCCURRENCE__TYPE:
				return type != TYPE_EDEFAULT;
			case TracePackage.MESSAGE_OCCURRENCE__LIFELINE:
				return getLifeline() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timestamp: ");
		result.append(timestamp);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //MessageOccurrenceImpl
