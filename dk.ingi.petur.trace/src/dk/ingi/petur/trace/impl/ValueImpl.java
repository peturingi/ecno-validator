/**
 */
package dk.ingi.petur.trace.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import dk.ingi.petur.trace.TracePackage;
import dk.ingi.petur.trace.Value;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.impl.ValueImpl#getObjectValues <em>Object Values</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.impl.ValueImpl#getIntValues <em>Int Values</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.impl.ValueImpl#getStringValues <em>String Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueImpl extends NamedElementImpl implements Value {
	/**
	 * The cached value of the '{@link #getObjectValues() <em>Object Values</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectValues()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> objectValues;

	/**
	 * The cached value of the '{@link #getIntValues() <em>Int Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> intValues;

	/**
	 * The cached value of the '{@link #getStringValues() <em>String Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringValues()
	 * @generated
	 * @ordered
	 */
	protected EList<String> stringValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getObjectValues() {
		if (objectValues == null) {
			objectValues = new EObjectResolvingEList<EObject>(EObject.class, this, TracePackage.VALUE__OBJECT_VALUES);
		}
		return objectValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getIntValues() {
		if (intValues == null) {
			intValues = new EDataTypeUniqueEList<Integer>(Integer.class, this, TracePackage.VALUE__INT_VALUES);
		}
		return intValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getStringValues() {
		if (stringValues == null) {
			stringValues = new EDataTypeUniqueEList<String>(String.class, this, TracePackage.VALUE__STRING_VALUES);
		}
		return stringValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.VALUE__OBJECT_VALUES:
				return getObjectValues();
			case TracePackage.VALUE__INT_VALUES:
				return getIntValues();
			case TracePackage.VALUE__STRING_VALUES:
				return getStringValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.VALUE__OBJECT_VALUES:
				getObjectValues().clear();
				getObjectValues().addAll((Collection<? extends EObject>)newValue);
				return;
			case TracePackage.VALUE__INT_VALUES:
				getIntValues().clear();
				getIntValues().addAll((Collection<? extends Integer>)newValue);
				return;
			case TracePackage.VALUE__STRING_VALUES:
				getStringValues().clear();
				getStringValues().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.VALUE__OBJECT_VALUES:
				getObjectValues().clear();
				return;
			case TracePackage.VALUE__INT_VALUES:
				getIntValues().clear();
				return;
			case TracePackage.VALUE__STRING_VALUES:
				getStringValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.VALUE__OBJECT_VALUES:
				return objectValues != null && !objectValues.isEmpty();
			case TracePackage.VALUE__INT_VALUES:
				return intValues != null && !intValues.isEmpty();
			case TracePackage.VALUE__STRING_VALUES:
				return stringValues != null && !stringValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (intValues: ");
		result.append(intValues);
		result.append(", stringValues: ");
		result.append(stringValues);
		result.append(')');
		return result.toString();
	}

} //ValueImpl
