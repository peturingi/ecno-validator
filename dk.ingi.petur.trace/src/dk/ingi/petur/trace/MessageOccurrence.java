/**
 */
package dk.ingi.petur.trace;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Occurrence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.ingi.petur.trace.MessageOccurrence#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.MessageOccurrence#getType <em>Type</em>}</li>
 *   <li>{@link dk.ingi.petur.trace.MessageOccurrence#getLifeline <em>Lifeline</em>}</li>
 * </ul>
 *
 * @see dk.ingi.petur.trace.TracePackage#getMessageOccurrence()
 * @model
 * @generated
 */
public interface MessageOccurrence extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(int)
	 * @see dk.ingi.petur.trace.TracePackage#getMessageOccurrence_Timestamp()
	 * @model required="true"
	 * @generated
	 */
	int getTimestamp();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.MessageOccurrence#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(int value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.ingi.petur.trace.MessageOccurrenceType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.ingi.petur.trace.MessageOccurrenceType
	 * @see #setType(MessageOccurrenceType)
	 * @see dk.ingi.petur.trace.TracePackage#getMessageOccurrence_Type()
	 * @model required="true"
	 * @generated
	 */
	MessageOccurrenceType getType();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.MessageOccurrence#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.ingi.petur.trace.MessageOccurrenceType
	 * @see #getType()
	 * @generated
	 */
	void setType(MessageOccurrenceType value);

	/**
	 * Returns the value of the '<em><b>Lifeline</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.ingi.petur.trace.Lifeline#getMessageOccurrences <em>Message Occurrences</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifeline</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifeline</em>' container reference.
	 * @see #setLifeline(Lifeline)
	 * @see dk.ingi.petur.trace.TracePackage#getMessageOccurrence_Lifeline()
	 * @see dk.ingi.petur.trace.Lifeline#getMessageOccurrences
	 * @model opposite="messageOccurrences" required="true" transient="false"
	 * @generated
	 */
	Lifeline getLifeline();

	/**
	 * Sets the value of the '{@link dk.ingi.petur.trace.MessageOccurrence#getLifeline <em>Lifeline</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lifeline</em>' container reference.
	 * @see #getLifeline()
	 * @generated
	 */
	void setLifeline(Lifeline value);

} // MessageOccurrence
