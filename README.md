# README #

ECNO Validator (version 1.0.0)

## Prerequisites ##
The following is a list of prerequisites for the ECNO Validator:

* **Eclipse Modeling Tools** from https://www.eclipse.org/downloads/packages/eclipse-modeling-tools/neonr. _Note: Eclipse Mars is also known to work._
* **OCL Tools** from _Help->Install Modeling Components_
* **ePNK and ECNO Tool** by installing everything from _update site_ http://www2.compute.dtu.dk/~ekki/projects/ePNK/1.1/update/
* **AspectJ Development Tools** by installing everything from _update site_ http://download.eclipse.org/tools/ajdt/46/dev/update
* **Guava: Google Core Libraries for Java Source** from _update site_ http://download.eclipse.org/tools/orbit/downloads/drops/R20160520211859/repository/

## Installing the ECNO Validator ##
Import git@bitbucket.org:peturingi/ecno-validator.git

## Contents ##

The imported workspace contains the following projects:

| Project | Description |
|--|--|
| dk.ingi.petur.bankingsystem | An implementation of the _Banking System example_. |
| dk.ingi.petur.example.workers | The workers example -- an extension of _dk.dtu.imm.se.ecno.example.workers_. The Ecore model has been changed and a Setting.java has been added. This was done to support state space generation. See README-extension in the project for further information on the modifications which were made. |
| dk.ingi.petur.trace | The metamodel of _traces_. |
| dk.ingi.petur.trace.edit | Generated from _dk.ingi.petur.trace_ (no manual code). |
| dk.ingi.petur.trace.editor | Generated from _dk.ingi.petur.trace_ (no manual code). |
| dk.ingi.petur.tracer | The Tracer. |
| dk.ingi.petur.validator | The Validator. |
| dk.ingi.petur.validator.eclipse | Enables Validation directly from Eclipse |
| dk.ingi.petur.validator.mapping | The metamodel of _mappings_. |
| dk.ingi.petur.validator.mapping.edit | Generated from _dk.ingi.petur.validator.mapping_ (no manual code).|
| dk.ingi.petur.validator.mapping.editor | Generated from _dk.ingi.petur.validator.mapping_ (no manual code). |
| dk.ingi.petur.workers | An implementation of the _Workers Example (dk.ingi.petur.example.workers)_. |
| dk.ingi.petur.validator.demo | Contains a a state space, an operation mapping and two traces (one which validates and one which does not). |

## Validation ##

To validate a system against an ECNO specification:

* generate a setting specific ECNO state space of the ECNO specification,
* implement a Trace driver,
* configure the Tracer so it can trace the system,
* trace the system by running the Trace driver as a _AspectJ Load-Time Weaving Application_,
* map operations, and their parameters, from the system to event types, and
* select the operation map, the ECNO state space and the trace, right click and select **ECNO Validate**.

We present an example:

### Generating a setting specific ECNO state space ###
A detailed description of how this is done is out scope for this README.
An ECNO state space is available for the _Workers example_ in **dk.ingi.petur.example.workers/models/config1.statespace**. That state space is for the standard setting of the _Workers example_ which is available at **dk.ingi.petur.example.workers/models/Setting.xmi**.

### Implementing a Trace driver for the _Workers example_ ###
An example of a Trace driver for the _Workers example_ is available at **dk.ingi.petur.tracer/src/dk/ingi/petur/tracer/examples/TraceWorkers.java**.

In **main(...)**, a setting is created for the _Workers example_.
It is important that this setting reflects the one which was used to generate the ECNO state space against which the resulting trace is to be validated.
Once the setting has been created, **main(...)** calls **testDanArriveDepartARriveDoJobD** which drives the execution of the _Workers example's_ implementation.

### Configuring the Tracer to trace the _Workers example_ ###

To configure the Tracer to trace the _Workers example_, do the following:

* Right click the project **dk.ingi.petur.tracer** and select _Properties_.
* Select _AspectJ Build_.
* Click _Add Project..._
* Add the project **dk.ingi.petur.workers** _(NOT dk.ingi.petur.example.workers)_. _Note: You will be reminded to add -xmlConfigured as a compiler option. This is not needed, click Yes._
* Open the file **dk.ingi.petur.tracer/src/META-INF/aop.xml**, ensure that the second line reads **<\!ENTITY package "dk.ingi.petur.workers">**

### Running the Tracer ###
The Tracer is implemented in AspectJ.
Given that it has been properly configured, it will run when ever the _Workers example's_ driver is executed.
To this extend, create a _run configuration_ of the type _AspectJ Load-Time Weaving Application_ for the Trace driver, like this:

* Right click the project **dk.ingi.petur.tracer**, then select _Run As_ and click on _Run Configurations..._.
* Double click on  _AspectJ Load-Time Weaving Application_.
* Click on the _Search..._ button in the _Main_ tab in the _Main class:_ box.
* Select **TraceWorkers - dk.ingi.petur.tracer.examples**.
* Click the _LTW Aspectpath_ tab.
* Click on _User Entries_, then on _Add Projects..._.
* Choose **dk.ingi.petur.tracer** in _Project Selection_.
* Click on *Run* to execute the Tracer.

Once execution has finished, two traces should be saved to *dk.ingi.petur.tracer/traces/*.
_Note: You might need to refresh the folder to see the two traces. Also, if the file *dk.ingi.petur.tracer/src/META-INF/aop-ajc.xml* is created then it must be deleted before the Trace can be executed again._

### Mapping Operations to Event Types ###
The validation algorithm needs to know which operations in the real system correspond to event types in the ECNO specification.
Such a mapping for this example is available at **dk.ingi.petur.validator.demo/workers.operationmapping**

### Validation ###
The project **dk.ingi.petur.validator.demo** contains the aforementioned state space, traces and operation mapping.
To validate, select a state space, a trace and an operation mapping (by holding down the ctrl key while selecting them with the mouse) then right click and select _ECNO Validate_.

If validation is successful then a mapping will be written to the hard-drive. The location to which it is written is given as output on Eclipse's Console _(note: in the console on Eclipse which was used to start the runtime workspace)_.

## A note on the OCL Validation of Mappings ##
Due to a [bug in EMF](https://bugs.eclipse.org/bugs/show_bug.cgi?id=494221), OCL Validation of _mappings_ is not performed automatically. However, _mappings_ can be validated if imported into the runtime workspace. To this extent, double click on the mapping file to open it in the Mapping Editor, then right click the root entry and select _OCL Validate_.